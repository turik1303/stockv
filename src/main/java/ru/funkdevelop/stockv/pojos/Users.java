package ru.funkdevelop.stockv.pojos;
// Generated Jun 7, 2017 2:28:49 PM by Hibernate Tools 4.3.1


import java.util.HashSet;
import java.util.Set;

/**
 * Users generated by hbm2java
 */
public class Users  implements java.io.Serializable {


     private Integer id;
     private Roles roles;
     private String login;
     private String password;
     private String firstName;
     private String middleName;
     private String lastName;
     private Set priceLogs = new HashSet(0);
     private Set documentses = new HashSet(0);

    public Users() {
    }

    public Users(Roles roles, String login, String password, String firstName, String middleName, String lastName, Set priceLogs, Set documentses) {
       this.roles = roles;
       this.login = login;
       this.password = password;
       this.firstName = firstName;
       this.middleName = middleName;
       this.lastName = lastName;
       this.priceLogs = priceLogs;
       this.documentses = documentses;
    }
   
    public Integer getId() {
        return this.id;
    }
    
    public void setId(Integer id) {
        this.id = id;
    }
    public Roles getRoles() {
        return this.roles;
    }
    
    public void setRoles(Roles roles) {
        this.roles = roles;
    }
    public String getLogin() {
        return this.login;
    }
    
    public void setLogin(String login) {
        this.login = login;
    }
    public String getPassword() {
        return this.password;
    }
    
    public void setPassword(String password) {
        this.password = password;
    }
    public String getFirstName() {
        return this.firstName;
    }
    
    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }
    public String getMiddleName() {
        return this.middleName;
    }
    
    public void setMiddleName(String middleName) {
        this.middleName = middleName;
    }
    public String getLastName() {
        return this.lastName;
    }
    
    public void setLastName(String lastName) {
        this.lastName = lastName;
    }
    public Set getPriceLogs() {
        return this.priceLogs;
    }
    
    public void setPriceLogs(Set priceLogs) {
        this.priceLogs = priceLogs;
    }
    public Set getDocumentses() {
        return this.documentses;
    }
    
    public void setDocumentses(Set documentses) {
        this.documentses = documentses;
    }




}


