package ru.funkdevelop.stockv.pojos;
// Generated Jun 7, 2017 2:28:49 PM by Hibernate Tools 4.3.1


import java.util.HashSet;
import java.util.Set;

/**
 * Packages generated by hbm2java
 */
public class Packages  implements java.io.Serializable, Cloneable {


     private Integer id;
     private String packageName;
     private Set goodsesForBasePackageId = new HashSet(0);
     private Set goodsesForMainPackageId = new HashSet(0);

    public Packages() {
    }

    public Packages(String packageName, Set goodsesForBasePackageId, Set goodsesForMainPackageId) {
       this.packageName = packageName;
       this.goodsesForBasePackageId = goodsesForBasePackageId;
       this.goodsesForMainPackageId = goodsesForMainPackageId;
    }
   
    public Integer getId() {
        return this.id;
    }
    
    public void setId(Integer id) {
        this.id = id;
    }
    public String getPackageName() {
        return this.packageName;
    }
    
    public void setPackageName(String packageName) {
        this.packageName = packageName;
    }
    public Set getGoodsesForBasePackageId() {
        return this.goodsesForBasePackageId;
    }
    
    public void setGoodsesForBasePackageId(Set goodsesForBasePackageId) {
        this.goodsesForBasePackageId = goodsesForBasePackageId;
    }
    public Set getGoodsesForMainPackageId() {
        return this.goodsesForMainPackageId;
    }
    
    public void setGoodsesForMainPackageId(Set goodsesForMainPackageId) {
        this.goodsesForMainPackageId = goodsesForMainPackageId;
    }

    @Override
    protected Object clone() throws CloneNotSupportedException {
        return super.clone(); //To change body of generated methods, choose Tools | Templates.
    }




}


