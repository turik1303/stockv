package ru.funkdevelop.stockv;

import ru.funkdevelop.stockv.views.documents.DocumentsView;
import javax.servlet.annotation.WebServlet;

import com.vaadin.annotations.Theme;
import com.vaadin.annotations.VaadinServletConfiguration;
import com.vaadin.navigator.Navigator;
import com.vaadin.server.VaadinRequest;
import com.vaadin.server.VaadinService;
import com.vaadin.server.VaadinServlet;
import com.vaadin.server.VaadinSession;
import com.vaadin.ui.UI;
import ru.funkdevelop.stockv.pojos.Users;
import ru.funkdevelop.stockv.views.*;
import ru.funkdevelop.stockv.views.documents.CashReceiptView;
import ru.funkdevelop.stockv.views.documents.IncomeView;
import ru.funkdevelop.stockv.views.documents.OutcomeView;
import ru.funkdevelop.stockv.views.documents.RemainderEnteringView;
import ru.funkdevelop.stockv.views.documents.WritingOffView;
import ru.funkdevelop.stockv.views.reports.ContragentCalculationsReportView;
import ru.funkdevelop.stockv.views.reports.DocumentRegistryReportView;
import ru.funkdevelop.stockv.views.reports.RemainderReportView;

/**
 * This UI is the application entry point. A UI may either represent a browser
 * window (or tab) or some part of a html page where a Vaadin application is
 * embedded.
 * <p>
 * The UI is initialized using {@link #init(VaadinRequest)}. This method is
 * intended to be overridden to add component to the user interface and
 * initialize non-component functionality.
 */
@Theme("mytheme")
public class MyUI extends UI {

    Navigator navigator;

    @Override
    protected void init(VaadinRequest vaadinRequest) {

        VaadinSession.getCurrent().getSession().setMaxInactiveInterval(86400);

        navigator = new Navigator(this, this);
        navigator.addView("MainView", new MainView());
        navigator.addView("FirstView", new FirstView());
        navigator.addView("LoginView", new LoginView());
        navigator.addView("UserControl", new UserControlView());
        navigator.addView("ContragentControl", new ContragentControlView());
        navigator.addView("BankControl", new BankControlView());
        navigator.addView("GoodTypeControl", new GoodTypeControlView());
        navigator.addView("PackageControl", new PackageControlView());
        navigator.addView("ContragentTypeControl", new ContragentTypeControlView());
        navigator.addView("PriceTypeControl", new PriceTypeControlView());
        navigator.addView("SertificateControl", new SertificateControlView());
        navigator.addView("GoodControl", new GoodControlView());
        navigator.addView("DocumentsView", new DocumentsView());
        navigator.addView("Income", new IncomeView());
        navigator.addView("Outcome", new OutcomeView());
        navigator.addView("RemainderEnter", new RemainderEnteringView());
        navigator.addView("WritingOff", new WritingOffView());
        navigator.addView("CashReceipt", new CashReceiptView());
        navigator.addView("RemainderReport", new RemainderReportView());
        navigator.addView("ContragentClaculationsReport", new ContragentCalculationsReportView());
        navigator.addView("DocumentRegistryReport", new DocumentRegistryReportView());
        Users currentUser = (Users) VaadinService.getCurrentRequest().getWrappedSession().getAttribute("user");
        if (currentUser == null) {
            navigator.navigateTo("LoginView");
        }
    }

    @WebServlet(urlPatterns = "/*", name = "MyUIServlet", asyncSupported = true)
    @VaadinServletConfiguration(ui = MyUI.class, productionMode = false, closeIdleSessions = true, heartbeatInterval = 300)
    public static class MyUIServlet extends VaadinServlet {
    }
}
