/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ru.funkdevelop.stockv;

import java.util.Date;
import java.util.List;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;
import ru.funkdevelop.stockv.pojos.*;

/**
 *
 * @author artur
 */
public class StockHelper {

    private Session getSession() {
        Session session = null;
        session = HibernateUtil.getSessionFactory().getCurrentSession();
        return session;
    }

    public List<Users> getUsers() {
        List<Users> users = null;
        Session session = getSession();
        Transaction tr = session.beginTransaction();
        try {
            Query q = session.createQuery("from Users order by id");
            users = (List<Users>) q.list();
            tr.commit();
        } catch (Exception e) {
            tr.rollback();
            e.printStackTrace();
        }
        return users;
    }

    public List<Roles> getRoles() {
        List<Roles> roles = null;
        Session session = getSession();
        Transaction tr = session.beginTransaction();
        try {
            Query q = session.createQuery("from Roles order by id");
            roles = (List<Roles>) q.list();
            tr.commit();
        } catch (Exception e) {
            tr.rollback();
            e.printStackTrace();
        }
        return roles;
    }

    public List<Banks> getBanks() {
        List<Banks> banks = null;
        Session session = getSession();
        Transaction tr = session.beginTransaction();
        try {
            Query q = session.createQuery("from Banks order by id");
            banks = (List<Banks>) q.list();
            tr.commit();

        } catch (Exception e) {
            tr.rollback();
            e.printStackTrace();
        }
        return banks;
    }

    public List<GoodsType> getGoodsTypes() {
        List<GoodsType> types = null;
        Session session = getSession();
        Transaction tr = session.beginTransaction();
        try {
            Query q = session.createQuery("from GoodsType order by id");
            types = (List<GoodsType>) q.list();
            tr.commit();

        } catch (Exception e) {
            tr.rollback();
            e.printStackTrace();
        }
        return types;
    }

    public List<Packages> getPackages() {
        List<Packages> packages = null;
        Session session = getSession();
        Transaction tr = session.beginTransaction();
        try {
            Query q = session.createQuery("from Packages order by id");
            packages = (List<Packages>) q.list();
            tr.commit();

        } catch (Exception e) {
            tr.rollback();
            e.printStackTrace();
        }
        return packages;
    }

    public List<ContragentType> getContragentTypes() {
        List<ContragentType> types = null;
        Session session = getSession();
        Transaction tr = session.beginTransaction();
        try {
            Query q = session.createQuery("from ContragentType order by id");
            types = (List<ContragentType>) q.list();
            tr.commit();
        } catch (Exception e) {
            tr.rollback();
            e.printStackTrace();
        }
        return types;
    }

    public List<Contragents> getContragents() {
        List<Contragents> contragents = null;
        Session session = getSession();
        Transaction tr = session.beginTransaction();
        try {
            Query q = session.createQuery("from Contragents order by id");
            contragents = (List<Contragents>) q.list();
            tr.commit();
        } catch (Exception e) {
            e.printStackTrace();
            tr.rollback();
        }
        return contragents;
    }

    public List<PriceType> getPriceTypes() {
        List<PriceType> types = null;
        Session session = getSession();
        Transaction tr = session.beginTransaction();
        try {
            Query q = session.createQuery("from PriceType order by id");
            types = (List<PriceType>) q.list();
            tr.commit();
        } catch (Exception e) {
            tr.rollback();
            e.printStackTrace();
        }
        return types;
    }

    public List<Goods> getAllGoods() {
        List<Goods> goods = null;
        Session session = getSession();
        Transaction tr = session.beginTransaction();
        try {
            Query q = session.createQuery("from Goods order by id");
            goods = (List<Goods>) q.list();
            tr.commit();
        } catch (Exception e) {
            tr.rollback();
            e.printStackTrace();
        }
        return goods;
    }

    public Goods getGoodById(int goodId) {
        Goods result = null;
        Session session = getSession();
        Transaction tr = session.beginTransaction();
        try {
            Query q = session.createQuery("from Goods where id = :id");
            q.setParameter("id", goodId);
            result = (Goods) q.list().get(0);
            tr.commit();
        } catch (Exception e) {
            tr.rollback();
            e.printStackTrace();
        }
        return result;
    }

    public List<Prices> getPricesByGoodId(int goodId) {
        List<Prices> prices = null;
        Session session = getSession();
        Transaction tr = session.beginTransaction();
        try {
            Query q = session.createQuery("from Prices where good_id = :id");
            q.setParameter("id", goodId);
            prices = (List<Prices>) q.list();
            tr.commit();
        } catch (Exception e) {
            tr.rollback();
            e.printStackTrace();
        }
        return prices;
    }

    public Double getRetailPriceByGoodId(int goodId) {
        Double result = 0.0;
        List<Prices> tmp = getPricesByGoodId(goodId);
        for (Prices prices : tmp) {
            if (prices.getPriceType().getId() == 2) {
                result = prices.getResult();
            }
        }
        return result;
    }

    public List<Prices> getPricesByType(int type) {
        List<Prices> prices = null;
        Session session = getSession();
        Transaction tr = session.beginTransaction();
        try {
            Query q = session.createQuery("from Prices where price_type_id = :type");
            q.setParameter("type", type);
            prices = (List<Prices>) q.list();
            tr.commit();
        } catch (Exception e) {
            tr.rollback();
            e.printStackTrace();
        }
        return prices;
    }

    public int getMaxGoodId() {
        int max = 0;
        List<Integer> res = null;
        Session session = getSession();
        Transaction tr = session.beginTransaction();
        try {
            Query q = session.createQuery("Select max(g.id) from Goods g");
            res = (List<Integer>) q.list();
            max = res.get(0);
            tr.commit();
        } catch (Exception e) {
            tr.rollback();
            e.printStackTrace();
        }
        return max;
    }

    public List<PriceLog> getGoodPriceLog(int goodId) {
        List<PriceLog> log = null;
        Session session = getSession();
        Transaction tr = session.beginTransaction();
        try {
            Query q = session.createQuery("from PriceLog where good_id = :goodId");
            q.setParameter("goodId", goodId);
            log = (List<PriceLog>) q.list();
            tr.commit();
        } catch (Exception e) {
            tr.rollback();
            e.printStackTrace();
        }
        return log;
    }

    public List<Sertificates> getAllSertificates() {
        List<Sertificates> serts = null;
        Session session = getSession();
        Transaction tr = session.beginTransaction();
        try {
            Query q = session.createQuery("from Sertificates order by id");
            serts = q.list();
            tr.commit();
        } catch (Exception e) {
            tr.rollback();
            e.printStackTrace();
        }
        return serts;
    }

    public int getMaxSertificateId() {
        int max = 0;
        List<Integer> res = null;
        Session session = getSession();
        Transaction tr = session.beginTransaction();
        try {
            Query q = session.createQuery("Select max(s.id) from Sertificates s");
            res = (List<Integer>) q.list();
            max = res.get(0);
            tr.commit();
        } catch (Exception e) {
            tr.rollback();
            e.printStackTrace();
        }
        return max;
    }

    public int getMaxBillId() {
        int max = 0;
        List<Integer> res = null;
        Session session = getSession();
        Transaction tr = session.beginTransaction();
        try {
            Query q = session.createQuery("Select max(b.id) from Documents b");
            res = (List<Integer>) q.list();
            if (res != null & res.get(0) != null) {
                max = res.get(0);
            } else {
                max = 0;
            }
            tr.commit();
        } catch (Exception e) {
            tr.rollback();
            e.printStackTrace();
        }
        return max;
    }

    public List<Documents> getAllDocuments() {
        List<Documents> documents = null;
        Session session = getSession();
        Transaction tr = session.beginTransaction();
        try {
            Query q = session.createQuery("from Documents Order by number");
            documents = (List<Documents>) q.list();
            tr.commit();
        } catch (Exception e) {
            tr.rollback();
            e.printStackTrace();
        }
        return documents;
    }
    
    public Documents getDocumentById(int documentId) {
        List<Documents> documents = null;
        Session session = getSession();
        Transaction tr = session.beginTransaction();
        try {
            Query q = session.createQuery("from Documents where id = :documentId");
            q.setParameter("documentId", documentId);
            documents = (List<Documents>) q.list();
            tr.commit();
        } catch (Exception e) {
            tr.rollback();
            e.printStackTrace();
        }
        return documents.get(0);
    }
    
    public List<DocumentGoods> getBillGoods(int billId) {
        List<DocumentGoods> billGoods = null;
        Session session = getSession();
        Transaction tr = session.beginTransaction();
        try {
            Query q = session.createQuery("from DocumentGoods where document_id = :billId order by id");
            q.setParameter("billId", billId);
            billGoods = (List<DocumentGoods>) q.list();
            tr.commit();
        } catch (Exception e) {
            tr.rollback();
            e.printStackTrace();
        }
        return billGoods;
    }
    
    public List<Documents> getDocumentsByDates(Date begin, Date end) {
        List<Documents> documents = null;
        Session session = getSession();
        Transaction tr = session.beginTransaction();
        java.sql.Date tmpBegin = new java.sql.Date(begin.getTime());
        java.sql.Date tmpEnd = new java.sql.Date(end.getTime());
        try {
            Query q = session.createQuery("from Documents where (date >= :begin and date <= :end) order by date");
            q.setParameter("begin", tmpBegin);
            q.setParameter("end", tmpEnd);
            documents = (List<Documents>) q.list();
            tr.commit();
        } catch (Exception e) {
            tr.rollback();
            e.printStackTrace();
        }
        return documents;
    }
    
    public List<Documents> getDocumentsByDate(Date date) {
        List<Documents> documents = null;
        Session session = getSession();
        Transaction tr = session.beginTransaction();
        try {
            Query q = session.createQuery("from Documents where date = :date");
            q.setParameter("date", date);            
            documents = (List<Documents>) q.list();
            tr.commit();
        } catch (Exception e) {
            tr.rollback();
            e.printStackTrace();
        }
        return documents;
    }
    
    public void addDBItem(Object item) {
        Session session = getSession();
        Transaction tr = session.beginTransaction();
        try {
            session.save(item);
            session.flush();
            tr.commit();
        } catch (Exception e) {
            tr.rollback();
            e.printStackTrace();
        }
    }

    public void updateDBItem(Object item) {
        Session session = getSession();
        Transaction tr = session.beginTransaction();
        try {
            session.update(item);
            session.flush();
            tr.commit();
        } catch (Exception e) {
            tr.rollback();
            e.printStackTrace();
        }
    }

    public void saveOrUpdateDBItem(Object item) {
        Session session = getSession();
        Transaction tr = session.beginTransaction();
        try {
            session.saveOrUpdate(item);
            session.flush();
            tr.commit();
        } catch (Exception e) {
            tr.rollback();
            e.printStackTrace();
        }
    }

    public void deleteDBItem(Object item) {
        Session session = getSession();
        Transaction tr = session.beginTransaction();
        try {
            session.delete(item);
            session.flush();
            tr.commit();
        } catch (Exception e) {
            tr.rollback();
            e.printStackTrace();
        }
    }

}
