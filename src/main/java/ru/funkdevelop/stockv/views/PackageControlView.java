/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ru.funkdevelop.stockv.views;

import com.vaadin.event.selection.SelectionEvent;
import com.vaadin.event.selection.SelectionListener;
import com.vaadin.navigator.Navigator;
import com.vaadin.navigator.View;
import com.vaadin.navigator.ViewChangeListener;
import com.vaadin.server.Page;
import com.vaadin.ui.Alignment;
import com.vaadin.ui.Button;
import com.vaadin.ui.Grid;
import com.vaadin.ui.GridLayout;
import com.vaadin.ui.Panel;
import com.vaadin.ui.TextField;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.Window;
import com.vaadin.ui.renderers.NumberRenderer;
import com.vaadin.ui.renderers.TextRenderer;
import java.util.HashSet;
import java.util.List;
import ru.funkdevelop.stockv.MyUI;
import ru.funkdevelop.stockv.StockHelper;
import ru.funkdevelop.stockv.pojos.Packages;
import ru.funkdevelop.stockv.views.service.MainMenu;
import ru.funkdevelop.stockv.views.service.UserData;

/**
 *
 * @author artur
 */
public class PackageControlView extends GridLayout implements View {

    Navigator navigator;
    StockHelper stockHelper;
    List<Packages> packages;
    Packages newPackage, editPackage, deletePackage;

    @Override
    public void enter(ViewChangeListener.ViewChangeEvent event) {
        VerticalLayout fullLayout = new VerticalLayout();
        navigator = getUI().getNavigator();
        stockHelper = new StockHelper();
        packages = stockHelper.getPackages();
        newPackage = new Packages();
        editPackage = packages.get(0);
        GridLayout layout = new GridLayout(3, 3);
        UserData userData = new UserData();
        MainMenu mainMenu = new MainMenu(navigator);

        Panel allPackages = new Panel("Список всех типов фасовки");
        VerticalLayout allPackagesLayout = new VerticalLayout();
        Grid<Packages> allPackagesGrid = new Grid<>(Packages.class);
        allPackagesGrid.setItems(packages);
        allPackagesGrid.removeAllColumns();
        allPackagesGrid.addColumn(Packages::getId, new NumberRenderer("%02d")).setCaption("ID");
        allPackagesGrid.addColumn(Packages::getPackageName, new TextRenderer()).setCaption("Наименование типа фасовки");
        allPackagesGrid.setSelectionMode(Grid.SelectionMode.SINGLE);
        allPackagesGrid.addSelectionListener(new SelectionListener<Packages>() {
            @Override
            public void selectionChange(SelectionEvent<Packages> event) {
                HashSet<Packages> hashSet = (HashSet<Packages>) allPackagesGrid.getSelectedItems();
                for (Packages pack : hashSet) {
                    editPackage = pack;
                    deletePackage = pack;
                }
            }
        });

        allPackagesLayout.addComponent(allPackagesGrid);
        allPackagesGrid.setSizeFull();
        allPackagesLayout.setSizeFull();
        allPackages.setContent(allPackagesLayout);
        allPackages.setWidth(25.0f, Unit.CM);
        allPackagesLayout.setMargin(false);
        allPackagesLayout.setSpacing(false);

        Panel packagesControl = new Panel("Управление");
        VerticalLayout packagesControLayout = new VerticalLayout();
        packagesControLayout.addComponent(new Button("Добавить", new Button.ClickListener() {
            @Override
            public void buttonClick(Button.ClickEvent event) {
                goToAddPackage();
            }
        }));
        packagesControLayout.addComponent(new Button("Редактировать", new Button.ClickListener() {
            @Override
            public void buttonClick(Button.ClickEvent event) {
                goToEditPackage();
            }
        }));
        packagesControLayout.addComponent(new Button("Удалить", new Button.ClickListener() {
            @Override
            public void buttonClick(Button.ClickEvent event) {
                doDeletePackage();
            }
        }));

        packagesControLayout.getComponent(0).setWidth("200px");
        packagesControLayout.setComponentAlignment(packagesControLayout.getComponent(0), Alignment.TOP_CENTER);
        packagesControLayout.getComponent(1).setWidth("200px");
        packagesControLayout.setComponentAlignment(packagesControLayout.getComponent(1), Alignment.TOP_CENTER);
        packagesControLayout.getComponent(2).setWidth("200px");
        packagesControLayout.setComponentAlignment(packagesControLayout.getComponent(2), Alignment.TOP_CENTER);

        packagesControl.setContent(packagesControLayout);
        packagesControl.setWidth("250px");

        userData.setHeight(19.0f, Unit.CM);
        allPackages.setHeight(19.0f, Unit.CM);
        packagesControl.setHeight(19.0f, Unit.CM);

        layout.addComponent(mainMenu, 0, 0, 2, 0);
        layout.addComponent(userData, 0, 1, 0, 1);
        layout.addComponent(allPackages, 1, 1, 1, 1);
        layout.addComponent(packagesControl, 2, 1, 2, 1);

        layout.setColumnExpandRatio(0, 0.0f);
        layout.setColumnExpandRatio(1, 0.0f);
        layout.setColumnExpandRatio(2, 0.0f);
        layout.setRowExpandRatio(0, 0.0f);
        layout.setRowExpandRatio(1, 0.0f);
        layout.setRowExpandRatio(2, 0.0f);

        layout.setComponentAlignment(allPackages, Alignment.TOP_CENTER);

        fullLayout.addComponent(layout);
        fullLayout.setComponentAlignment(layout, Alignment.MIDDLE_CENTER);
        fullLayout.setMargin(false);
        fullLayout.setSpacing(false);

        getUI().setContent(fullLayout);

    }

    private void goToAddPackage() {
        Window addPackageWindow = new Window("Добавить тип фасовки");
        VerticalLayout verticalLayout = new VerticalLayout();
        TextField packageName = new TextField("Наименование типа фасовки");
        packageName.setWidth(10.0f, Unit.CM);
        Button doAddPackageButton = new Button("Добавить", new Button.ClickListener() {
            @Override
            public void buttonClick(Button.ClickEvent event) {
                newPackage.setPackageName(packageName.getValue());
                doAddPackage();
            }
        });
        verticalLayout.addComponents(packageName, doAddPackageButton);
        verticalLayout.setComponentAlignment(packageName, Alignment.TOP_CENTER);
        verticalLayout.setComponentAlignment(doAddPackageButton, Alignment.TOP_CENTER);
        addPackageWindow.setContent(verticalLayout);
        addPackageWindow.setWidth(350.0f, Unit.PIXELS);
        addPackageWindow.center();
        addPackageWindow.setModal(true);
        MyUI.getCurrent().addWindow(addPackageWindow);
    }

    private void doAddPackage() {
        stockHelper.addDBItem(newPackage);
        this.newPackage = new Packages();
        packages = stockHelper.getPackages();
        Page.getCurrent().reload();
    }

    private void goToEditPackage() {
        Window editPackageWindow = new Window("Редактировать тип фасовки");
        VerticalLayout verticalLayout = new VerticalLayout();
        TextField packageName = new TextField("Наименование типа фасовки", this.editPackage.getPackageName());
        packageName.setWidth(10.0f, Unit.CM);
        Button doEditPackageButton = new Button("Сохранить", new Button.ClickListener() {
            @Override
            public void buttonClick(Button.ClickEvent event) {
                editPackage.setPackageName(packageName.getValue());
                doEditPackage();
            }
        });
        verticalLayout.addComponents(packageName, doEditPackageButton);
        verticalLayout.setComponentAlignment(packageName, Alignment.TOP_CENTER);
        verticalLayout.setComponentAlignment(doEditPackageButton, Alignment.TOP_CENTER);
        editPackageWindow.setContent(verticalLayout);
        editPackageWindow.setWidth(350.0f, Unit.PIXELS);
        editPackageWindow.center();
        editPackageWindow.setModal(true);
        MyUI.getCurrent().addWindow(editPackageWindow);
    }

    private void doEditPackage() {
        stockHelper.updateDBItem(this.editPackage);
        this.editPackage = new Packages();
        this.deletePackage = null;
        Page.getCurrent().reload();
    }

    private void doDeletePackage() {
        stockHelper.deleteDBItem(deletePackage);
        this.deletePackage = null;
        this.editPackage = new Packages();
        Page.getCurrent().reload();
    }

}
