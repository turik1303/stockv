/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ru.funkdevelop.stockv.views.service;

import com.vaadin.data.provider.ListDataProvider;
import com.vaadin.event.selection.SelectionEvent;
import com.vaadin.event.selection.SelectionListener;
import com.vaadin.ui.Alignment;
import com.vaadin.ui.Button;
import com.vaadin.ui.ComboBox;
import com.vaadin.ui.Grid;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.TextField;
import com.vaadin.ui.components.grid.HeaderRow;
import com.vaadin.ui.renderers.NumberRenderer;
import com.vaadin.ui.renderers.TextRenderer;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Set;
import ru.funkdevelop.stockv.StockHelper;
import ru.funkdevelop.stockv.pojos.Contragents;
import ru.funkdevelop.stockv.pojos.Goods;
import ru.funkdevelop.stockv.pojos.GoodsType;

/**
 *
 * @author artur
 */
public class AllGoodsGrid extends Grid<Goods> {

    private TextField goodArticul;
    private TextField goodName;
    private TextField goodMinPrice;
    private TextField goodMaxPrice;
    private TextField goodRemainder;
    private ComboBox goodType;
    private ComboBox goodProvider;
    private StockHelper stockHelper;
    private List<Goods> selectedGoods;

    public AllGoodsGrid(List<Goods> allGoods, List<Contragents> contragents, List<GoodsType> types) {
        this.selectedGoods = new ArrayList<>();
        this.stockHelper = new StockHelper();
        this.addColumn(Goods::getCode, new TextRenderer()).setCaption("Артикул");
        this.addColumn(Goods::getName, new TextRenderer()).setCaption("Наименование");
        this.addColumn((goods) -> goods.getGoodsType().getTypeName(), new TextRenderer()).setCaption("Тип");
        this.addColumn((goods) -> goods.getContragents().getDisplayName(), new TextRenderer()).setCaption("Поставщик");
        this.addColumn((goods) -> {
            return stockHelper.getRetailPriceByGoodId(goods.getId());
        }, new NumberRenderer("%3.2f")).setCaption("Розничная цена");
        
        this.addColumn((goods) -> {
            String result;
            Double factor = goods.getMainFactor();
            Boolean isInBasePackage = goods.getUseBasePackage();
            if (isInBasePackage) {
                String packageName = goods.getPackagesByBasePackageId().getPackageName();
                result = String.valueOf(goods.getRemainder()) + " " + packageName;
                
            } else {
                String packageName = goods.getPackagesByMainPackageId().getPackageName();
                result = String.valueOf(goods.getRemainder() / factor) + " " + packageName;
            }
            return result;
        }, new TextRenderer()).setCaption("Остаток");        
        this.setSelectionMode(SelectionMode.MULTI);
        this.addSelectionListener(new SelectionListener<Goods>() {
            @Override
            public void selectionChange(SelectionEvent<Goods> event) {
                Set<Goods> tmp = event.getAllSelectedItems();
                selectedGoods.clear();
                for (Goods goods : tmp) {
                    selectedGoods.add(goods);
                }
            }
        });
        this.setItems(allGoods);
        this.setSizeFull();

        goodArticul = new TextField();
        goodArticul.setPlaceholder("Артикул...");
        goodArticul.setWidth(2.0f, Unit.CM);
        goodArticul.setHeight(0.8f, Unit.CM);

        goodName = new TextField();
        goodName.setPlaceholder("Наименование...");
        goodName.setSizeFull();
        goodName.setHeight(0.8f, Unit.CM);

        goodMinPrice = new TextField();
        goodMinPrice.setPlaceholder("От...");
        goodMinPrice.setWidth(2.0f, Unit.CM);
        goodMinPrice.setHeight(0.8f, Unit.CM);

        goodMaxPrice = new TextField();
        goodMaxPrice.setPlaceholder("До...");
        goodMaxPrice.setWidth(2.0f, Unit.CM);
        goodMaxPrice.setHeight(0.8f, Unit.CM);

        goodRemainder = new TextField();
        goodRemainder.setPlaceholder("От...");
        goodRemainder.setWidth(2.5f, Unit.CM);
        goodRemainder.setHeight(0.8f, Unit.CM);

        HashMap<String, Contragents> providers = new HashMap<>();
        for (Contragents contragent : contragents) {
            if (contragent.getContragentType().getId() == 1) {
                providers.put(contragent.getDisplayName(), contragent);
            }
        }
        HashMap<String, GoodsType> goodsTypes = new HashMap<>();
        for (GoodsType type : types) {
            goodsTypes.put(type.getTypeName(), type);
        }

        goodType = new ComboBox();
        goodType.setPlaceholder("Тип товара...");
        goodType.setItems(goodsTypes.keySet());
        goodType.setEmptySelectionAllowed(false);
        goodType.setSizeFull();
        goodType.setHeight(0.8f, Unit.CM);

        goodProvider = new ComboBox();
        goodProvider.setPlaceholder("Поставщик...");
        goodProvider.setItems(providers.keySet());
        goodProvider.setEmptySelectionAllowed(false);
        goodProvider.setSizeFull();
        goodProvider.setHeight(0.8f, Unit.CM);

        Button doFilterButton = new Button("Применить", new Button.ClickListener() {
            @Override
            public void buttonClick(Button.ClickEvent event) {
                doFilter();
            }
        });
        doFilterButton.setSizeFull();
        doFilterButton.setHeight(0.8f, Unit.CM);
        Button doResetFilterButton = new Button("Сбросить", new Button.ClickListener() {
            @Override
            public void buttonClick(Button.ClickEvent event) {
                doResetFilter();
            }
        });
        doResetFilterButton.setSizeFull();
        doResetFilterButton.setHeight(0.8f, Unit.CM);
        HeaderRow filterRow = this.appendHeaderRow();
        filterRow.getCell(this.getColumns().get(0)).setComponent(goodArticul);
        filterRow.getCell(this.getColumns().get(1)).setComponent(goodName);
        filterRow.getCell(this.getColumns().get(2)).setComponent(goodType);
        filterRow.getCell(this.getColumns().get(3)).setComponent(goodProvider);
        HorizontalLayout prices = new HorizontalLayout();
        prices.addComponent(goodMinPrice);
        prices.setComponentAlignment(goodMinPrice, Alignment.TOP_CENTER);
        prices.addComponent(goodMaxPrice);
        prices.setComponentAlignment(goodMaxPrice, Alignment.TOP_CENTER);
        filterRow.getCell(this.getColumns().get(4)).setComponent(prices);
        filterRow.getCell(this.getColumns().get(5)).setComponent(goodRemainder);
        HeaderRow buttonsRow = this.appendHeaderRow();
        buttonsRow.getCell(this.getColumns().get(4)).setComponent(doFilterButton);
        buttonsRow.getCell(this.getColumns().get(5)).setComponent(doResetFilterButton);

    }

    public List<Goods> getSelectedGoods() {
        return selectedGoods;
    }

    public void setSelectedGoods(List<Goods> selectedGoods) {
        this.selectedGoods = selectedGoods;
    }

    
    
    private void doFilter() {

        ListDataProvider<Goods> dataProvider = (ListDataProvider<Goods>) this.getDataProvider();
        if (!goodArticul.isEmpty()) {
            dataProvider.addFilter(Goods::getCode, s -> caseInsensitiveContains(s, goodArticul.getValue()));
        }
        if (!goodName.isEmpty()) {
            dataProvider.addFilter(Goods::getName, s -> caseInsensitiveContains(s, goodName.getValue()));
        }
        if (!goodType.isEmpty()) {
            dataProvider.addFilter((goods) -> goods.getGoodsType().getTypeName(),
                    s -> caseInsensitiveContains(s, (String) goodType.getValue()));
        }
        if (!goodProvider.isEmpty()) {
            dataProvider.addFilter((goods) -> goods.getContragents().getDisplayName(),
                    s -> caseInsensitiveContains(s, (String) goodProvider.getValue()));
        }
        if (!goodMinPrice.isEmpty() & !goodMaxPrice.isEmpty()) {
            Double minValue = Double.parseDouble(goodMinPrice.getValue());
            Double maxValue = Double.parseDouble(goodMaxPrice.getValue());
            dataProvider.addFilter((goods) -> {
                return stockHelper.getRetailPriceByGoodId(goods.getId());
            },
                    d -> inDoublerange(d, minValue, maxValue));
        }
        if (!goodRemainder.isEmpty()) {
            Double minRemainder = Double.parseDouble(goodRemainder.getValue());
            dataProvider.addFilter(Goods::getFactRemainder, d -> moreThan(d, minRemainder));
        }
    }

    private void doResetFilter() {
        ListDataProvider<Goods> dataProvider = (ListDataProvider<Goods>) this.getDataProvider();
        dataProvider.clearFilters();
        goodArticul.clear();
        goodMaxPrice.clear();
        goodMinPrice.clear();
        goodName.clear();
        goodProvider.clear();
        goodRemainder.clear();
        goodType.clear();
    }

    private Boolean caseInsensitiveContains(String where, String what) {
        return where.toLowerCase().contains(what.toLowerCase());
    }

    private Boolean inDoublerange(Double what, Double min, Double max) {
        return what >= min & what <= max;
    }

    private Boolean moreThan(Double what, Double where) {
        return what >= where;
    }

}
