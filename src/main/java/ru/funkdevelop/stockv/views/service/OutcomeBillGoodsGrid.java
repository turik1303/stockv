/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ru.funkdevelop.stockv.views.service;

import com.vaadin.ui.renderers.TextRenderer;
import java.util.List;
import ru.funkdevelop.stockv.pojos.DocumentGoods;
import ru.funkdevelop.stockv.pojos.Goods;

/**
 *
 * @author artur
 */
public class OutcomeBillGoodsGrid extends BillGoodsGrid {

    public OutcomeBillGoodsGrid(List<DocumentGoods> items) {
        super(items);
        this.addColumn((documentGoods) -> {
            String result;
            Goods good = documentGoods.getGoods();
            result = String.valueOf(good.getRemainder()) + " " + good.getPackagesByBasePackageId().getPackageName();
            return result;
        }, new TextRenderer()).setCaption("Остаток");
    }
    
    
}
