/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ru.funkdevelop.stockv.views.service;

import com.vaadin.data.HasValue;
import com.vaadin.data.provider.ListDataProvider;
import com.vaadin.server.Sizeable;
import com.vaadin.server.VaadinService;
import com.vaadin.ui.Button;
import com.vaadin.ui.Grid;
import com.vaadin.ui.Label;
import com.vaadin.ui.TextField;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.Window;
import com.vaadin.ui.components.grid.ItemClickListener;
import com.vaadin.ui.renderers.TextRenderer;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import ru.funkdevelop.stockv.MyUI;
import ru.funkdevelop.stockv.StockHelper;
import ru.funkdevelop.stockv.pojos.Goods;
import ru.funkdevelop.stockv.pojos.PriceLog;
import ru.funkdevelop.stockv.pojos.PriceType;
import ru.funkdevelop.stockv.pojos.Prices;
import ru.funkdevelop.stockv.pojos.Users;

/**
 *
 * @author artur
 */
public class GoodPricesController {

    private StockHelper stockHelper;
    private List<Prices> priceList;
    private int goodId;
    public GoodPricesController(int goodId) {
        this.goodId = goodId;
        stockHelper = new StockHelper();
        priceList = stockHelper.getPricesByGoodId(goodId);
    }

    public List<Prices> getPriceList() {
        return priceList;
    }

    public void setPriceList(List<Prices> priceList) {
        this.priceList = priceList;
    }

    public Double getPurchasePrice() {
        Prices price = null;
        for (Prices prices : priceList) {
            if (prices.getPriceType().getId() == 1) {
                price = prices;
            }
        }
        if (price != null) {
            return price.getResult();
        } else {
            return 0.0;
        }
    }

    public Double getRetailPrice() {
        Prices price = null;
        for (Prices prices : priceList) {
            if (prices.getPriceType().getId() == 2) {
                price = prices;
            }
        }
        if (price != null) {
            return price.getResult();
        } else {
            return 0.0;
        }
    }

    public Double getSmallWholesalePrice() {
        Prices price = null;
        for (Prices prices : priceList) {
            if (prices.getPriceType().getId() == 3) {
                price = prices;
            }
        }
        if (price != null) {
            return price.getResult();
        } else {
            return 0.0;
        }
    }

    public Double getWholesalePrice() {
        Prices price = null;
        for (Prices prices : priceList) {
            if (prices.getPriceType().getId() == 4) {
                price = prices;
            }
        }
        if (price != null) {
            return price.getResult();
        } else {
            return 0.0;
        }
    }

    public Window getPriceListWindow() {
        TextField startPriceEditor = new TextField();
        TextField chargeEditor = new TextField();
        TextField resultEditor = new TextField();
        
        List<Prices> oldPrices = new ArrayList<>();
        for (Prices prices : priceList) {
            Prices tmp = new Prices(prices.getGoods(), prices.getPriceType(), prices.getCharge(), prices.getResult(), prices.getStartPrice());
            oldPrices.add(tmp);
        }
        
        Window priceListWindow = new Window("Список цен");
        VerticalLayout priceListLayout = new VerticalLayout();
        Grid<Prices> pricesGrid = new Grid(Prices.class);
        pricesGrid.removeAllColumns();
        pricesGrid.addColumn((prices) -> {
            return prices.getPriceType().getName();
        }, new TextRenderer()).setCaption("Наименование цены");
        pricesGrid.addColumn(Prices::getStartPrice, new TextRenderer()).setCaption("Начальная цена");
        pricesGrid.addColumn(Prices::getCharge, new TextRenderer()).setCaption("Наценка");
        pricesGrid.addColumn(Prices::getResult, new TextRenderer()).setCaption("Итоговая цена");        
        pricesGrid.setItems(priceList);
        pricesGrid.addItemClickListener(new ItemClickListener<Object>() {
            @Override
            public void itemClick(Grid.ItemClick<Object> event) {
                Prices item = (Prices) event.getItem();
                Window priceEditWindow = new Window("Редактирование цены");
                VerticalLayout priceEditLayout = new VerticalLayout();
                Label priceName = new Label(item.getPriceType().getName());
                TextField startPrice = new TextField("Начальная цена", String.valueOf(item.getStartPrice()));
                TextField charge = new TextField("Наценка", String.valueOf(item.getCharge()));
                TextField resultPrice = new TextField("Итоговая цена", String.valueOf(item.getResult()));
                startPrice.addValueChangeListener(new HasValue.ValueChangeListener<String>() {
                    @Override
                    public void valueChange(HasValue.ValueChangeEvent<String> event) {
                        Double startPriceValue = Double.parseDouble(startPrice.getValue());
                        Double chargeValue = Double.parseDouble(charge.getValue());
                        Double resultValue = startPriceValue + startPriceValue * chargeValue / 100.0;
                        resultValue = round(resultValue, 2);
                        resultPrice.setValue(String.valueOf(resultValue));
                    }
                });
                charge.addValueChangeListener(new HasValue.ValueChangeListener<String>() {
                    @Override
                    public void valueChange(HasValue.ValueChangeEvent<String> event) {
                        Double startPriceValue = Double.parseDouble(startPrice.getValue());
                        Double chargeValue = Double.parseDouble(charge.getValue());
                        Double resultValue = startPriceValue + startPriceValue * chargeValue / 100.0;
                        resultValue = round(resultValue, 2);
                        resultPrice.setValue(String.valueOf(resultValue));
                    }
                });
                Button saveButton = new Button("Сохранить", new Button.ClickListener() {
                    @Override
                    public void buttonClick(Button.ClickEvent event) {
                        item.setStartPrice(Double.parseDouble(startPrice.getValue()));
                        item.setCharge(Double.parseDouble(charge.getValue()));
                        item.setResult(Double.parseDouble(resultPrice.getValue()));
                        if (item.getPriceType().getId() == 1)
                            for (Prices prices : priceList) {                                
                                prices.setStartPrice(item.getResult());
                                Double startPriceValue = prices.getStartPrice();
                                Double chargeValue = prices.getCharge();                                
                                Double resultValue = startPriceValue + startPriceValue * chargeValue / 100.0;
                                resultValue = round(resultValue, 2);
                                prices.setResult(resultValue);                                
                            }
                        for (int i = 0; i < priceList.size(); i++) {
                            if (oldPrices.get(i).getResult() != priceList.get(i).getResult())
                                logPriceChange(priceList.get(i), oldPrices.get(i).getResult(), priceList.get(i).getResult());
                                
                            stockHelper.updateDBItem(priceList.get(i));
                        }
                        ListDataProvider<Prices> prov = (ListDataProvider<Prices>) pricesGrid.getDataProvider();
                        prov.refreshAll();
                        priceEditWindow.close();
                    }
                });
                priceEditLayout.addComponents(priceName, startPrice, charge, resultPrice, saveButton);
                priceEditWindow.setContent(priceEditLayout);
                priceEditWindow.center();
                priceEditWindow.setModal(true);
                MyUI.getCurrent().addWindow(priceEditWindow);
            }
        });
        pricesGrid.setSizeFull();
        pricesGrid.setHeightByRows(4.0);
        priceListLayout.setWidth(20.0f, Sizeable.Unit.CM);
        
        priceListLayout.addComponent(pricesGrid);
        priceListLayout.addComponent(new Button("Сохранить", new Button.ClickListener() {
            @Override
            public void buttonClick(Button.ClickEvent event) {
//                for (Prices prices : priceList) {
//                    stockHelper.updateDBItem(prices);
//                }           
                priceListWindow.close();
            }
        }));
        priceListWindow.setContent(priceListLayout);
        return priceListWindow;
    }
    
    private void logPriceChange(Prices price, Double oldPrice, Double newPrice) {
        Goods good = stockHelper.getGoodById(goodId);
        PriceType type = price.getPriceType();
        Users currentUser = (Users) VaadinService.getCurrentRequest().getWrappedSession().getAttribute("user");
        Date date = new Date();
        PriceLog log = new PriceLog(good, type, currentUser, date, oldPrice, newPrice);
        stockHelper.addDBItem(log);
    }
    
    private double round(double number, int scale) {
        int pow = 10;
        for (int i = 1; i < scale; i++) {
            pow *= 10;
        }
        double tmp = number * pow;
        return (double) (int) ((tmp - (int) tmp) >= 0.5 ? tmp + 1 : tmp) / pow;
    }

}
