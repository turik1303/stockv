/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ru.funkdevelop.stockv.views.service;

import com.vaadin.navigator.Navigator;
import com.vaadin.ui.MenuBar;

/**
 *
 * @author artur
 */
public class MainMenu extends MenuBar{
    Navigator navigator;
    public MainMenu(Navigator navigator) {
        this.navigator = navigator;
        MenuItem homeItem = this.addItem("Главная страница", ((MenuItem selectedItem) -> {
            gotoHome();
        }));
        MenuItem serviceItem = this.addItem("Сервис", null);
        MenuItem userControlItem = serviceItem.addItem("Управление пользователями", (MenuItem selectedItem) -> {
            goToUserControl();
        });
        MenuItem contragentControlItem = serviceItem.addItem("Управление контрагентами", (MenuItem selectedItem) -> {
            goToContragentControl();
        });
        MenuItem fieldsControlItem = serviceItem.addItem("Редактирование полей", null);
        MenuItem bankControlItem = fieldsControlItem.addItem("Управление банками", (MenuItem selectedItem) -> {
            goToBankControl();
        });
        MenuItem goodTypeControlItem = fieldsControlItem.addItem("Управление типами товаров", (MenuItem selectedItem) -> {
            goToGoodTypeControl();
        });
        MenuItem packageTypeControlItem = fieldsControlItem.addItem("Управление типами фасовки", (MenuItem selectedItem) -> {
            goToPackageControl();
        });
        MenuItem contragentTypeControlItem = fieldsControlItem.addItem("Управление типами контрагентов", (MenuItem selectedItem) -> {
            goToContragentTypeControl();
        });
        MenuItem priceTypeControlItem = fieldsControlItem.addItem("Управление типами цен", (MenuItem selectedItem) -> {
            goToPriceTypeControl();
        });
        MenuItem sertificateControlItem = serviceItem.addItem("Управление сертификатами", (MenuItem selectedItem) -> {
            goToSertificateControl();
        });
        MenuItem goods = this.addItem("Товары", (selectedItem) -> {
            goToGoodControl();
        });
        MenuItem documents = this.addItem("Документы", null);
        MenuItem documentsLog = documents.addItem("Журнал документов", (selectedItem) -> {
            goToDocuments();
        });
        MenuItem income = documents.addItem("Поступление ТМЦ", (selectedItem) -> {
            goToIncome();
        });
        MenuItem outcome = documents.addItem("Реализация ТМЦ", (selectedItem) -> {
            goToOutCome();
        });
        MenuItem remainder = documents.addItem("Ввод остатков ТМЦ", (selectedItem) -> {
            goToRemainder();
        });
        MenuItem writingOff = documents.addItem("Списание ТМЦ", (selectedItem) -> {
            goToWritingOff();
        });
        MenuItem cashReceipt = documents.addItem("Приходный кассовый ордер", (selectedItem) -> {
            goTocahsReceipt();
        });
        MenuItem reports = this.addItem("Отчеты", null);
        MenuItem remaiderReport = reports.addItem("Ведомость по остаткам ТМЦ", (selectedItem) -> {
            goToRemainderReport();
        });
        MenuItem contragentCalculationsReport = reports.addItem("Взаиморасчеты по контрагентам", (selectedItem) -> {
            goToContragentCalculationsReport();
        });
        MenuItem documentRegistryReport = reports.addItem("Реестр документов", (selectedItem) -> {
            goToDocumentRegistryReport();
        });
    }
    
    
    
    
    
    private void goToUserControl() {
        if (navigator != null)
            navigator.navigateTo("UserControl");
    }
    
    private void goToContragentControl() {
        if (navigator != null)
            navigator.navigateTo("ContragentControl");
    }
    
    private void goToBankControl() {
        if (navigator != null)
            navigator.navigateTo("BankControl");
    }
    
    private void goToGoodTypeControl() {
        if (navigator != null)
            navigator.navigateTo("GoodTypeControl");
    }
    
    private void goToPackageControl() {
        if (navigator != null)
            navigator.navigateTo("PackageControl");
    }
    
    private void goToContragentTypeControl() {
        if (navigator != null)
            navigator.navigateTo("ContragentTypeControl");
    }
    
    private void goToPriceTypeControl() {
        if (navigator != null)
            navigator.navigateTo("PriceTypeControl");
    }
    
    private void goToSertificateControl() {
        if (navigator != null)
            navigator.navigateTo("SertificateControl");
    }
    
    private void goToGoodControl() {
        if (navigator != null)
            navigator.navigateTo("GoodControl");
    }
            
    private void gotoHome() {
        if (navigator != null)
            navigator.navigateTo("MainView");
    }
    
    private void goToDocuments() {
        if (navigator != null)
            navigator.navigateTo("DocumentsView");
    }
    
    private void goToIncome() {
        if (navigator != null)
            navigator.navigateTo("Income");
    }
    
    private void goToOutCome() {
        if (navigator != null)
            navigator.navigateTo("Outcome");
    }
    
    private void goToRemainder() {
        if (navigator != null)
            navigator.navigateTo("RemainderEnter");
    }

    private void goToWritingOff() {
        if (navigator != null)
            navigator.navigateTo("WritingOff");
    }
    
    private void goToRemainderReport() {
        if (navigator != null)
            navigator.navigateTo("RemainderReport");
    }

    private void goTocahsReceipt() {
        if (navigator != null)
            navigator.navigateTo("CashReceipt");
    }

    private void goToContragentCalculationsReport() {
        if (navigator != null)
            navigator.navigateTo("ContragentClaculationsReport");
    }

    private void goToDocumentRegistryReport() {
        if (navigator != null)
            navigator.navigateTo("DocumentRegistryReport");
    }
}
