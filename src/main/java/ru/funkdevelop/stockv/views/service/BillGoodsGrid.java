/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ru.funkdevelop.stockv.views.service;

import com.vaadin.ui.Grid;
import com.vaadin.ui.Label;
import com.vaadin.ui.components.grid.HeaderRow;
import com.vaadin.ui.renderers.NumberRenderer;
import com.vaadin.ui.renderers.TextRenderer;
import java.util.List;
import ru.funkdevelop.stockv.pojos.DocumentGoods;
import ru.funkdevelop.stockv.pojos.Goods;

/**
 *
 * @author artur
 */
public class BillGoodsGrid extends Grid<DocumentGoods>{
    private Double totalValue;
    private List<DocumentGoods> items;
    public BillGoodsGrid(List<DocumentGoods> items) {
        this.items = items;
        this.addColumn((documentGoods) -> {
            Goods tmp = documentGoods.getGoods();
            return tmp.getName();
        }, new TextRenderer()).setCaption("Наименование");
        this.addColumn(DocumentGoods::getPrice, new NumberRenderer("%.2f")).setCaption("Цена");
        this.addColumn(DocumentGoods::getQuantity, new NumberRenderer("%.2f")).setCaption("Количество");
        this.addColumn((documentGoods) -> { 
            String result;
            Goods tmp = documentGoods.getGoods();
            if (tmp.getUseBasePackage())
                result = tmp.getPackagesByBasePackageId().getPackageName();
            else
                result = tmp.getPackagesByMainPackageId().getPackageName();            
            return result;
        }, new TextRenderer()).setCaption("Тип фасовки");
        this.addColumn(DocumentGoods::getTotalValue, new NumberRenderer("%.2f")).setCaption("Стоимость");
        this.setItems(items);                
        this.setSizeFull();
        this.setHeight(13.1f, Unit.CM);
        
        
        HeaderRow statsRow = this.appendHeaderRow();
        int numberOfGoods = 0;
        totalValue = 0.0;
        
        for (DocumentGoods item : items) {
            numberOfGoods += item.getQuantity();
            totalValue += item.getTotalValue();
        }
        
        totalValue = round(totalValue, 2);
        Label total = new Label("Итого: " + String.valueOf(totalValue) + " руб.");
        
        statsRow.getCell(this.getColumns().get(4)).setComponent(total);
    }
    
    public Double getValue() {
        this.totalValue = 0.0;
        for (DocumentGoods item : items) {            
            totalValue += item.getTotalValue();
        }
        return this.totalValue;
    }
    
    private double round(double number, int scale) {
        int pow = 10;
        for (int i = 1; i < scale; i++) {
            pow *= 10;
        }
        double tmp = number * pow;
        return (double) (int) ((tmp - (int) tmp) >= 0.5 ? tmp + 1 : tmp) / pow;
    }
}
