/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ru.funkdevelop.stockv.views.service;

import com.vaadin.server.VaadinService;
import com.vaadin.ui.Label;
import com.vaadin.ui.Panel;
import com.vaadin.ui.VerticalLayout;
import ru.funkdevelop.stockv.pojos.Users;

/**
 *
 * @author artur
 */
public class UserData extends Panel{
    Users currentUser;
    public UserData() {
        currentUser = (Users) VaadinService.getCurrentRequest().getWrappedSession().getAttribute("user");
        
        this.setCaption("Текущий пользователь:");
        VerticalLayout layout = new VerticalLayout();
        Label login = new Label("Логин: " + currentUser.getLogin());
        Label role = new Label("Роль: " + currentUser.getRoles().getName());
        Label firstName = new Label("Имя: " + currentUser.getFirstName());
        Label middleName = new Label("Отчество: " + currentUser.getMiddleName());
        Label lastName = new Label("Фамилия: " + currentUser.getLastName());
        layout.addComponents(login, role, lastName, firstName, middleName);

        this.setContent(layout);
        this.setWidth("250px");
    }
    
}
