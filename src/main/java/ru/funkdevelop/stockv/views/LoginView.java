/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ru.funkdevelop.stockv.views;

import com.vaadin.navigator.Navigator;
import com.vaadin.navigator.View;
import com.vaadin.navigator.ViewChangeListener;
import com.vaadin.server.VaadinService;
import com.vaadin.ui.Alignment;
import com.vaadin.ui.Button;
import com.vaadin.ui.GridLayout;
import com.vaadin.ui.Notification;
import com.vaadin.ui.Panel;
import com.vaadin.ui.PasswordField;
import com.vaadin.ui.TextField;
import com.vaadin.ui.VerticalLayout;
import java.util.List;
import ru.funkdevelop.stockv.StockHelper;
import ru.funkdevelop.stockv.pojos.Users;

/**
 *
 * @author artur
 */
public class LoginView extends GridLayout implements View {

    Navigator navigator;
    private StockHelper stockHelper;

    @Override
    public void enter(ViewChangeListener.ViewChangeEvent event) {
        navigator = getUI().getNavigator();
        this.stockHelper = new StockHelper();
        GridLayout layout = new GridLayout();
        getUI().setContent(layout);
        Panel loginPanel = new Panel("<center>Авторизация</center>");

        VerticalLayout loginPanelLayout = new VerticalLayout();
        loginPanel.setContent(loginPanelLayout);
        TextField login = new TextField("Логин");
        PasswordField password = new PasswordField("Пароль");
        Button doLoginButton = new Button("Вход", new Button.ClickListener() {
            @Override
            public void buttonClick(Button.ClickEvent event) {
                doLogin(login.getValue(), password.getValue());
            }
        });
        loginPanelLayout.addComponents(login, password, doLoginButton);
        loginPanelLayout.setComponentAlignment(login, Alignment.TOP_CENTER);
        loginPanelLayout.setComponentAlignment(password, Alignment.MIDDLE_CENTER);
        loginPanelLayout.setComponentAlignment(doLoginButton, Alignment.BOTTOM_CENTER);
        loginPanel.setWidth("15%");
        layout.setSizeFull();
        layout.addComponent(loginPanel);
        layout.setComponentAlignment(loginPanel, Alignment.MIDDLE_CENTER);

    }

    private void doLogin(String login, String password) {
        List<Users> users = stockHelper.getUsers();
        Users currentUser = null;
        for (Users user : users) {
            if (user.getLogin().equals(login) && user.getPassword().equals(password))
                currentUser = user;
        }
        if (currentUser != null) {
            VaadinService.getCurrentRequest().getWrappedSession()
                .setAttribute("user", currentUser);  
            
            if (navigator != null) 
                navigator.navigateTo("MainView");
        } else {
            Notification.show("Неверный логин или пароль!");
        }
    }

}
