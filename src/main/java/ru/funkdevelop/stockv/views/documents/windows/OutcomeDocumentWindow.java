/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ru.funkdevelop.stockv.views.documents.windows;

import com.vaadin.data.HasValue;
import com.vaadin.data.ValueContext;
import com.vaadin.data.converter.LocalDateToDateConverter;
import com.vaadin.data.provider.ListDataProvider;
import com.vaadin.event.selection.SelectionEvent;
import com.vaadin.event.selection.SelectionListener;
import com.vaadin.server.BrowserWindowOpener;
import com.vaadin.server.Page;
import com.vaadin.server.VaadinRequest;
import com.vaadin.server.VaadinService;
import com.vaadin.shared.ui.ContentMode;
import com.vaadin.ui.Alignment;
import com.vaadin.ui.Button;
import com.vaadin.ui.CheckBox;
import com.vaadin.ui.ComboBox;
import com.vaadin.ui.DateField;
import com.vaadin.ui.Grid;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Label;
import com.vaadin.ui.Notification;
import com.vaadin.ui.Panel;
import com.vaadin.ui.TextField;
import com.vaadin.ui.UI;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.Window;
import com.vaadin.ui.components.grid.ItemClickListener;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import ru.funkdevelop.stockv.MyUI;
import ru.funkdevelop.stockv.StockHelper;
import ru.funkdevelop.stockv.pojos.Contragents;
import ru.funkdevelop.stockv.pojos.DocumentGoods;
import ru.funkdevelop.stockv.pojos.Documents;
import ru.funkdevelop.stockv.pojos.Goods;
import ru.funkdevelop.stockv.pojos.Prices;
import ru.funkdevelop.stockv.pojos.Users;
import ru.funkdevelop.stockv.views.service.AllGoodsGrid;
import ru.funkdevelop.stockv.views.service.BillGoodsGrid;
import ru.funkdevelop.stockv.views.service.GoodPricesController;
import ru.funkdevelop.stockv.views.service.RussianMoney;

/**
 *
 * @author artur
 */
public class OutcomeDocumentWindow extends Window {

    Documents document;
    private List<Contragents> allContragents;
    private StockHelper stockHelper;
    private List<DocumentGoods> billGoods;
    HashMap<String, Contragents> buyers = new HashMap<>();
    private OutcomeDocumentWindow.BillGoodsWindow billGoodsWindow;
    private Boolean isEditable = false;
    public static int documentId;

    public OutcomeDocumentWindow(Documents document) {
        documentId = document.getId();
        this.document = document;
        this.stockHelper = new StockHelper();
        this.allContragents = stockHelper.getContragents();
        this.billGoods = stockHelper.getBillGoods(document.getId());
        for (Contragents contragent : allContragents) {
            if (contragent.getContragentType().getId() != 1) {
                buyers.put(contragent.getDisplayName(), contragent);
            }
        }

        this.setCaption("Документ № " + String.valueOf(this.document.getNumber()));
        HorizontalLayout fullLayout = new HorizontalLayout();
        VerticalLayout layout = new VerticalLayout();
        Panel mainData = new Panel("Основная информация");
        VerticalLayout mainDataLayout = new VerticalLayout();

        Panel documentData = new Panel("Документ");
        HorizontalLayout documentDataLayout = new HorizontalLayout();

        TextField docType = new TextField("Тип документа", "Реализация (купля-продажа)");
        docType.setEnabled(false);
        docType.setWidth(9.0f, Unit.CM);

        TextField docNum = new TextField("Номер", String.valueOf(this.document.getNumber()));
        docNum.setEnabled(false);
        docNum.setWidth(3.0f, Unit.CM);

        LocalDateToDateConverter dateConverter = new LocalDateToDateConverter();

        DateField docDate = new DateField("Дата", dateConverter.convertToPresentation(this.document.getDate(), new ValueContext()));
        docDate.setEnabled(false);
        docDate.setDateFormat("dd.MM.yyyy");
        docDate.setWidth(6.0f, Unit.CM);

        documentDataLayout.addComponents(docType, docNum, docDate);
        documentDataLayout.setSizeFull();
        documentDataLayout.setComponentAlignment(docType, Alignment.MIDDLE_LEFT);
        documentDataLayout.setComponentAlignment(docNum, Alignment.TOP_RIGHT);
        documentDataLayout.setComponentAlignment(docDate, Alignment.TOP_RIGHT);
        documentData.setContent(documentDataLayout);
        documentDataLayout.setSpacing(true);
        documentDataLayout.setMargin(true);

        HorizontalLayout line2 = new HorizontalLayout();
        Panel providerData = new Panel("Поставщик");
        VerticalLayout providerDataLayout = new VerticalLayout();

        TextField docProviderName = new TextField("Поставщик", this.document.getFirm());
        docProviderName.setEnabled(false);
        docProviderName.setWidth(8.0f, Unit.CM);

        TextField docWarehouse = new TextField("Склад", this.document.getWarehouse());
        docWarehouse.setEnabled(false);
        docWarehouse.setWidth(8.0f, Unit.CM);

        providerDataLayout.addComponents(docProviderName, docWarehouse);
        providerDataLayout.setComponentAlignment(docProviderName, Alignment.TOP_CENTER);
        providerDataLayout.setComponentAlignment(docWarehouse, Alignment.TOP_CENTER);
        providerData.setContent(providerDataLayout);

        Panel buyerData = new Panel("Покупатель");
        VerticalLayout buyerDataLayout = new VerticalLayout();

        ComboBox docBuyer = new ComboBox("Покупатель", buyers.keySet());
        docBuyer.setEnabled(false);
        docBuyer.setValue(document.getContragents().getDisplayName());
        docBuyer.setEmptySelectionAllowed(false);
        docBuyer.setWidth(8.0f, Unit.CM);

        TextField buyerCredit = new TextField("Долг контрагента", String.valueOf(this.document.getContragents().getCredit()));
        buyerCredit.setEnabled(false);
        buyerCredit.setWidth(8.0f, Unit.CM);

        TextField docContract = new TextField("Договор", this.document.getContragents().getMainContract());
        docContract.setEnabled(false);
        docContract.setWidth(8.0f, Unit.CM);

        DateField docPayUntil = new DateField("Оплата до",
                dateConverter.convertToPresentation(this.document.getPayUntil(), new ValueContext()));
        docPayUntil.setEnabled(false);
        docPayUntil.setDateFormat("dd.MM.yyyy");
        docPayUntil.setWidth(8.0f, Unit.CM);

        buyerDataLayout.addComponents(docBuyer, docContract, buyerCredit, docPayUntil);
        buyerDataLayout.setComponentAlignment(docBuyer, Alignment.TOP_CENTER);
        buyerDataLayout.setComponentAlignment(docContract, Alignment.TOP_CENTER);
        buyerDataLayout.setComponentAlignment(buyerCredit, Alignment.TOP_CENTER);
        buyerDataLayout.setComponentAlignment(docPayUntil, Alignment.TOP_CENTER);
        buyerData.setContent(buyerDataLayout);

        line2.addComponents(providerData, buyerData);
        line2.setComponentAlignment(buyerData, Alignment.TOP_LEFT);
        line2.setComponentAlignment(providerData, Alignment.TOP_RIGHT);
        line2.setSizeFull();

        Panel paymentsData = new Panel("Оплата");
        HorizontalLayout paymentDataLayout = new HorizontalLayout();
        VerticalLayout summs = new VerticalLayout();

        TextField docSumm = new TextField("Сумма", String.valueOf(this.document.getSumm()));
        docSumm.setEnabled(false);

        TextField docOldSumm = new TextField("Сумма до изменений", String.valueOf(this.document.getSumm()));
        docOldSumm.setEnabled(false);

        summs.addComponents(docSumm, docOldSumm);
        summs.setSpacing(false);
        summs.setMargin(false);

        VerticalLayout payments = new VerticalLayout();
        TextField docNewPayment = new TextField("Новый платеж", "0.0");
        docNewPayment.setEnabled(false);

        TextField docPayment = new TextField("Сумма платежей", String.valueOf(this.document.getPaidAmount()));
        docPayment.setEnabled(false);

        payments.addComponents(docNewPayment, docPayment);
        payments.setSpacing(false);
        payments.setMargin(false);

        VerticalLayout changes = new VerticalLayout();
        TextField docNewChange = new TextField("Cдача", "0.0");
        docNewChange.setEnabled(false);

        TextField docChange = new TextField("Всего сдачи", String.valueOf(this.document.getChangeAmount()));
        docChange.setEnabled(false);

        changes.addComponents(docNewChange, docChange);
        changes.setSpacing(false);
        changes.setMargin(false);

        Button calculateChange = new Button("Рассчитать", new Button.ClickListener() {
            @Override
            public void buttonClick(Button.ClickEvent event) {
                try {
                    Double newPayment = Double.parseDouble(docNewPayment.getValue().replace(",", "."));
                    Double summ = Double.parseDouble(docSumm.getValue().replace(",", "."));
                    Double paidAmount = document.getPaidAmount() - document.getChangeAmount();
                    Double amountToPay = summ - paidAmount;
                    Double newChange = newPayment - amountToPay;
                    if (newChange >= 0) {
                        docChange.setValue(String.valueOf(document.getChangeAmount() + newChange));
                        docNewChange.setValue(String.valueOf(newChange));
                    }
                } catch (NumberFormatException e) {

                }

            }
        });

        paymentDataLayout.addComponents(summs, payments, changes, calculateChange);
        paymentDataLayout.setSpacing(true);
        paymentDataLayout.setMargin(true);
        paymentDataLayout.setComponentAlignment(summs, Alignment.TOP_CENTER);
        paymentDataLayout.setComponentAlignment(payments, Alignment.TOP_CENTER);
        paymentDataLayout.setComponentAlignment(changes, Alignment.TOP_CENTER);
        paymentDataLayout.setComponentAlignment(calculateChange, Alignment.MIDDLE_CENTER);
        paymentDataLayout.setSizeFull();
        paymentsData.setContent(paymentDataLayout);

        documentData.setWidth(20.0f, Unit.CM);
        buyerData.setWidth(9.85f, Unit.CM);
        providerData.setWidth(9.85f, Unit.CM);
        buyerData.setHeight(9.5f, Unit.CM);
        providerData.setHeight(9.5f, Unit.CM);

        Panel control = new Panel("Управление");
        VerticalLayout controlLayout = new VerticalLayout();

        Button setFullPaid = new Button("Оплатить", new Button.ClickListener() {
            @Override
            public void buttonClick(Button.ClickEvent event) {
                try {
                    Double oldSumm = document.getSumm();
                    Double paidAmount = document.getPaidAmount();
                    Double newSumm = Double.parseDouble(docSumm.getValue().replace(",", "."));
                    Double changeSize = document.getChangeAmount();
                    Double result = newSumm - (paidAmount - changeSize);
                    docNewPayment.setValue(String.valueOf(result));
                    calculateChange.click();
                } catch (NumberFormatException e) {

                }
            }
        });
        setFullPaid.setEnabled(!this.document.getIsFullyPaid());

        Button goToPrintBillButton = new Button("Печать накладной");
        BrowserWindowOpener opener
                = new BrowserWindowOpener(MyPopupUI.class);
        opener.setFeatures("resizable");

        opener.extend(goToPrintBillButton);

        Button loadDocumentGoods = new Button("Список товаров", new Button.ClickListener() {
            @Override
            public void buttonClick(Button.ClickEvent event) {

                billGoodsWindow = new OutcomeDocumentWindow.BillGoodsWindow(billGoods);
                billGoodsWindow.addCloseListener(new CloseListener() {
                    @Override
                    public void windowClose(CloseEvent e) {
                        Double discount = document.getContragents().getDiscount();
                        docSumm.setValue(String.valueOf(billGoodsWindow.getSumm() - billGoodsWindow.getSumm() * discount / 100.0));
                        if (document.getSumm() != billGoodsWindow.getSumm() - billGoodsWindow.getSumm() * discount / 100.0) {
                            document.setIsFullyPaid(false);
                            setFullPaid.setEnabled(true);
                        }

                    }
                });
            }
        });

        Button setIsCarried;
        setIsCarried = new Button("Провести", new Button.ClickListener() {
            @Override
            public void buttonClick(Button.ClickEvent event) {
                List<DocumentGoods> carriedBillGoods = stockHelper.getBillGoods(document.getId());
                double oldSumm = document.getSumm();
                double oldPaidAmount = document.getPaidAmount() - document.getChangeAmount();
                Date date = new Date();
                Date time = new Date();
                Calendar tmp = new GregorianCalendar();
                tmp.set(docDate.getValue().getYear(), docDate.getValue().getMonthValue() - 1, docDate.getValue().getDayOfMonth());
                date.setTime(tmp.getTimeInMillis());
                document.setDate(date);
                document.setTime(time);
                document.setContragents(buyers.get(docBuyer.getValue()));
                Date payDay = new Date();
                Calendar c = new GregorianCalendar();
                c.set(docPayUntil.getValue().getYear(), docPayUntil.getValue().getMonthValue() - 1, docPayUntil.getValue().getDayOfMonth());
                payDay.setTime(c.getTimeInMillis());
                document.setPayUntil(payDay);
                document.setSumm(Double.parseDouble(docSumm.getValue()));
                document.setPaidAmount(Double.parseDouble(docPayment.getValue()) + Double.parseDouble(docNewPayment.getValue()));
                document.setChangeAmount(Double.parseDouble(docChange.getValue()));
//                double newSumm = document.getSumm();
                Boolean isFP;
                if (document.getPaidAmount() >= document.getSumm()) {
                    isFP = true;
                } else {
                    isFP = false;
                }
                document.setIsFullyPaid(isFP);
                HashSet<DocumentGoods> tmpSet = new HashSet<>();
                document.setUsers((Users) VaadinService.getCurrentRequest().getWrappedSession().getAttribute("user"));
                for (DocumentGoods documentGoods : billGoods) {
                    documentGoods.setDocuments(document);
                    tmpSet.add(documentGoods);
                }
                document.setDocumentGoodses(tmpSet);

                List<DocumentGoods> oldGoods = stockHelper.getBillGoods(document.getId());
                for (DocumentGoods billGood : billGoods) {
                    stockHelper.saveOrUpdateDBItem(billGood);
                }
                for (DocumentGoods oldGood : oldGoods) {
                    Boolean isFound = false;
                    for (DocumentGoods billGood : billGoods) {
                        if (billGood.getGoods().getId() == oldGood.getGoods().getId()) {
                            isFound = true;
                        }
                    }
                    if (!isFound) {
                        stockHelper.deleteDBItem(oldGood);
                        Goods tmpGood = oldGood.getGoods();
                        Double factor = tmpGood.getMainFactor();
                        Boolean isInBasePackage = tmpGood.getUseBasePackage();
                        if (isInBasePackage) {
                            tmpGood.setFactRemainder(tmpGood.getFactRemainder() + oldGood.getQuantity());
                            tmpGood.setRemainder(tmpGood.getRemainder() + oldGood.getQuantity());
                        } else {
                            tmpGood.setFactRemainder(tmpGood.getFactRemainder() + oldGood.getQuantity() * factor);
                            tmpGood.setRemainder(tmpGood.getRemainder() + oldGood.getQuantity() * factor);
                        }
                        stockHelper.updateDBItem(tmpGood);
                    }
                }
                List<DocumentGoods> unCarriedBillGoods = stockHelper.getBillGoods(document.getId());
                for (DocumentGoods unCarriedBillGood : unCarriedBillGoods) {
                    Goods good = unCarriedBillGood.getGoods();
                    DocumentGoods tmpGoods = new DocumentGoods(-1);
                    for (DocumentGoods carriedBillGood : carriedBillGoods) {
                        if (carriedBillGood.getGoods().getId() == unCarriedBillGood.getGoods().getId()) {
                            tmpGoods = carriedBillGood;
                        }
                    }
                    if (tmpGoods.getId() != -1) {
                        Double oldQuantity = tmpGoods.getQuantity();
                        Double newQuantity = unCarriedBillGood.getQuantity();
                        Double change = 0.0;
                        if (document.getIsCarried()) {
                            change = newQuantity - oldQuantity;
                        } else {
                            change = newQuantity;
                        }
//                        Double change = newQuantity - oldQuantity;
                        Double factor = unCarriedBillGood.getGoods().getMainFactor();
                        Boolean isInBasePackage = unCarriedBillGood.getGoods().getUseBasePackage();
                        if (isInBasePackage) {
                            if (change == 0.0) {
                                good.setFactRemainder(good.getFactRemainder());
                            } else {
                                good.setFactRemainder(good.getFactRemainder() - change);
                            }
                            if (document.getIsCarried()) {
                                good.setRemainder(good.getRemainder() - change);
                            }
                        } else {
                            if (change == 0.0) {
                                good.setFactRemainder(good.getFactRemainder());
                            } else {
                                good.setFactRemainder(good.getFactRemainder() - change * factor);
                            }
                            if (document.getIsCarried()) {
                                good.setRemainder(good.getRemainder() - change * factor);
                            }
                        }
                    } else {
                        Double newQuantity = unCarriedBillGood.getQuantity();
                        Double factor = unCarriedBillGood.getGoods().getMainFactor();
                        Boolean isInBasePackage = unCarriedBillGood.getGoods().getUseBasePackage();
                        if (isInBasePackage) {
                            good.setFactRemainder(good.getFactRemainder() - newQuantity);
                            good.setRemainder(good.getRemainder() - newQuantity);
                        } else {
                            good.setFactRemainder(good.getFactRemainder() - newQuantity * factor);
                            good.setRemainder(good.getRemainder() - newQuantity * factor);
                        }

                    }
                    stockHelper.updateDBItem(good);
                }

                double newSumm = document.getSumm();
                double newPaidAmount = document.getPaidAmount() - document.getChangeAmount();
                double oldCredit = document.getContragents().getCredit();
                double amountToPay;
                
                    amountToPay = newSumm - (newPaidAmount - oldPaidAmount) - oldSumm;
                
                double newCredit = oldCredit + amountToPay;
                document.getContragents().setCredit(newCredit);
//                double newPaidAmount = Double.parseDouble(docNewPayment.getValue());
//                
//                double oldCredit = document.getContragents().getCredit();
//                double amountToPay = newSumm - oldSumm;
////                double newChangeAmount = Double.parseDouble(docNewChange.getValue());
////                double change = (newPaidAmount - newChangeAmount) - amountToPay;
//                double totalPaidAmount = document.getPaidAmount();
//                
//                double newPaidAmount = Double.parseDouble(docNewPayment.getValue()) - Double.parseDouble(docNewChange.getValue());
////                double oldAmountToPay = oldSumm - (oldPaidAmount + newPaidAmount);
////                double newAmountToPay = newSumm - (oldPaidAmount + newPaidAmount);
////                double amountToPay = 0.0; 
////                if (totalPaidAmount != 0.0)
////                    amountToPay = newAmountToPay - oldAmountToPay;
////                else
////                    amountToPay = newAmountToPay;
//                double oldCredit = document.getContragents().getCredit();
//                double newCredit = 0.0;
//                if (document.getIsCarried())
//                    newCredit = oldCredit + (newSumm - oldSumm - totalPaidAmount);
//                else
//                    newCredit = oldCredit + newSumm;
//                
//                if (document.getIsFullyPaid())
//                    newCredit = oldCredit - newSumm;
//                else if (totalPaidAmount != 0.0)
//                    newCredit = oldCredit - newPaidAmount;

//                document.getContragents().setCredit(newCredit);
                document.setIsCarried(true);
                stockHelper.updateDBItem(document);
                stockHelper.updateDBItem(document.getContragents());

                Page.getCurrent().reload();
            }
        });
        setIsCarried.setEnabled(!this.document.getIsCarried());

        Button save = new Button("Сохранить", new Button.ClickListener() {
            @Override
            public void buttonClick(Button.ClickEvent event) {
                Date date = new Date();
                Date time = new Date();
                Calendar tmp = new GregorianCalendar();
                tmp.set(docDate.getValue().getYear(), docDate.getValue().getMonthValue() - 1, docDate.getValue().getDayOfMonth());
                date.setTime(tmp.getTimeInMillis());
                document.setDate(date);
                document.setTime(time);
                document.setContragents(buyers.get(docBuyer.getValue()));
                Date payDay = new Date();
                Calendar c = new GregorianCalendar();
                c.set(docPayUntil.getValue().getYear(), docPayUntil.getValue().getMonthValue() - 1, docPayUntil.getValue().getDayOfMonth());
                payDay.setTime(c.getTimeInMillis());
                document.setPayUntil(payDay);
                document.setSumm(Double.parseDouble(docSumm.getValue()));
                document.setPaidAmount(Double.parseDouble(docPayment.getValue()));
                document.setChangeAmount(Double.parseDouble(docChange.getValue()));
                Boolean isFP;
                if (document.getPaidAmount() >= document.getSumm()) {
                    isFP = true;
                } else {
                    isFP = false;
                }
                document.setIsFullyPaid(isFP);
                HashSet<DocumentGoods> tmpSet = new HashSet<>();
                document.setUsers((Users) VaadinService.getCurrentRequest().getWrappedSession().getAttribute("user"));
                for (DocumentGoods documentGoods : billGoods) {
                    documentGoods.setDocuments(document);
                    tmpSet.add(documentGoods);
                }
                document.setDocumentGoodses(tmpSet);
                stockHelper.updateDBItem(document);
                List<DocumentGoods> oldGoods = stockHelper.getBillGoods(document.getId());
                for (DocumentGoods billGood : billGoods) {
                    stockHelper.saveOrUpdateDBItem(billGood);
                    Double quantity = billGood.getQuantity();
                    Double initialQuantity = billGood.getGoods().getRemainder();
                    Double factor = billGood.getGoods().getMainFactor();
                    Boolean isInBasePackage = billGood.getGoods().getUseBasePackage();
                    if (isInBasePackage) {
                        billGood.getGoods().setRemainder(initialQuantity - (quantity - initialQuantity));
                    } else {
                        billGood.getGoods().setRemainder(initialQuantity - (quantity - initialQuantity) * factor);
                    }
                    stockHelper.updateDBItem(billGood.getGoods());
                }
                for (DocumentGoods oldGood : oldGoods) {
                    Boolean isFound = false;
                    for (DocumentGoods billGood : billGoods) {
                        if (billGood.getGoods().getId() == oldGood.getGoods().getId()) {
                            isFound = true;
                        }
                    }
                    if (!isFound) {
                        Goods good = oldGood.getGoods();
                        Double factor = good.getMainFactor();
                        Boolean isInBasePackage = good.getUseBasePackage();
                        if (isInBasePackage) {
                            good.setRemainder(good.getRemainder() + oldGood.getQuantity());
                        } else {
                            good.setRemainder(good.getRemainder() + oldGood.getQuantity() * factor);
                        }
                        good.setRemainder(good.getRemainder() + oldGood.getQuantity());
                        stockHelper.deleteDBItem(oldGood);
                    }
                }

                Page.getCurrent().reload();
            }
        });
        save.setEnabled(false);

        Button delete = new Button("Удалить", new Button.ClickListener() {
            @Override
            public void buttonClick(Button.ClickEvent event) {
                List<DocumentGoods> goodsList = stockHelper.getBillGoods(document.getId());
                for (DocumentGoods documentGoods : goodsList) {
                    stockHelper.deleteDBItem(documentGoods);
                }
                stockHelper.deleteDBItem(document);

                Page.getCurrent().reload();
            }
        });
        delete.setEnabled(!this.document.getIsCarried());

        Button edit = new Button("Редактировать", new Button.ClickListener() {
            @Override
            public void buttonClick(Button.ClickEvent event) {
                docDate.setEnabled(true);
                docBuyer.setEnabled(true);
                docPayUntil.setEnabled(true);
                docNewPayment.setEnabled(true);
                docSumm.setEnabled(true);
                save.setEnabled(true);
                setIsCarried.setEnabled(true);
                isEditable = true;
            }
        });

        goToPrintBillButton.setWidth(5.0f, Unit.CM);
        loadDocumentGoods.setWidth(5.0f, Unit.CM);
        setIsCarried.setWidth(5.0f, Unit.CM);
        setFullPaid.setWidth(5.0f, Unit.CM);
        save.setWidth(5.0f, Unit.CM);
        delete.setWidth(5.0f, Unit.CM);
        edit.setWidth(5.0f, Unit.CM);
        controlLayout.addComponents(goToPrintBillButton, loadDocumentGoods, edit, setIsCarried, setFullPaid, save, delete);
        control.setContent(controlLayout);

        mainDataLayout.addComponents(documentData, line2, paymentsData);
        mainData.setContent(mainDataLayout);
        layout.addComponent(mainData);
        layout.addComponent(control);
        fullLayout.addComponents(mainData, control);
        fullLayout.setMargin(true);
        fullLayout.setSpacing(true);

        this.setWidth(27.3f, Unit.CM);
        this.setContent(fullLayout);
        this.center();
        this.setModal(true);
        MyUI.getCurrent().addWindow(this);
    }

    public static class MyPopupUI extends UI {

        private int docId = documentId;
        private StockHelper stockHelper = new StockHelper();
        private Documents bill = stockHelper.getDocumentById(docId);
        private List<DocumentGoods> billGoods = stockHelper.getBillGoods(docId);

        @Override
        public void init(VaadinRequest request) {
            getPage().setTitle("Печать накладной");
            SimpleDateFormat format = new SimpleDateFormat("dd.MM.yyyy");
            // Have some content for it
            VerticalLayout content = new VerticalLayout();
            String data = "<!DOCTYPE html>\n"
                    + "<html>\n"
                    + "    <head></head>\n"
                    + "<style type=\"text/css\">\n"
                    + "@page {\n"
                    + "margin: 0cm;\n"
                    + "overflow: visible\n"
                    + "}\n"
                    + "tr{page-break-inside: avoid;}"
                    + "</style>\n"
                    + "    <body>\n"
                    + "<font face=\"Arial\">"
                    + "        <h2>Накладная № " + String.valueOf(bill.getNumber()) + " от " + format.format(bill.getDate()) + "</h2>\n"
                    + "        <hr>\n"
                    + "        <table border=\"0\" cellpadding=\"1\" cellspacing=\"1\">\n"
                    + "	        <tbody>\n"
                    + "		        <tr>\n"
                    + "			        <td>Поставщик: </td>\n"
                    + "			        <td><b>И.П. Валиахметова Л.Н. 452607, Башкортостан Респ, г.Октябрьский, Северная ул, д.21, кор.13</b></td>                    \n"
                    + "		        </tr>\n"
                    + "                <tr>\n"
                    + "                    <td>&nbsp;</td>\n"
                    + "                </tr>\n"
                    + "                <tr>\n"
                    + "                    <td>Покупатель: </td>\n"
                    + "			        <td><b>" + bill.getContragents().getOfficialName() + " " + bill.getContragents().getPhones() + "</b></td>\n"
                    + "                </tr>\n"
                    + "                <tr>\n"
                    + "                    <td>&nbsp;</td>\n"
                    + "                </tr>\n"
                    + "                <tr>\n"
                    + "                    <td>Склад: </td>\n"
                    + "			        <td><b>" + bill.getWarehouse() + "</b></td>\n"
                    + "                </tr>                \n"
                    + "	        </tbody>\n"
                    + "        </table>\n"
                    + "<font size = \"2\">\n"
                    + "<table border=\"1\" cellpadding=\"1\" cellspacing=\"0\" style=\"width: 19cm;\">\n"
                    + "	<tbody>\n"
                    + "		<tr>\n"
                    + "			<td rowspan=\"2\">№</td>\n"
                    + "			<td rowspan=\"2\" width=\"30%\">Товар</td>\n"
                    + "			<td rowspan=\"2\">Кол-во</td>\n"
                    + "			<td rowspan=\"2\">Ед.</td>\n"
                    + "			<td rowspan=\"2\">Цена</td>\n"
                    + "			<td rowspan=\"2\">Цена со скидкой</td>\n"
                    + "			<td rowspan=\"2\">Фасовка</td>\n"
                    + "			<td rowspan=\"2\">Сумма без скидки</td>\n"
                    + "			<td colspan=\"2\">Скидка</td>\n"
                    + "			<td rowspan=\"2\">Сумма со скидкой</td>\n"
                    + "			<td rowspan=\"2\">Остаток</td>\n"
                    + "		</tr>\n"
                    + "		<tr>\n"
                    + "			<td>Сумма</td>\n"
                    + "			<td>&nbsp;%</td>\n"
                    + "		</tr>\n";

            for (int i = 0; i < billGoods.size(); i++) {
                DocumentGoods tmpGood = billGoods.get(i);
                String number = String.valueOf(i + 1);
                String name = tmpGood.getGoods().getName();
                String quantity = String.valueOf(tmpGood.getQuantity());
                String packageName;
                if (tmpGood.getGoods().getUseBasePackage()) {
                    packageName = tmpGood.getGoods().getPackagesByBasePackageId().getPackageName();
                } else {
                    packageName = tmpGood.getGoods().getPackagesByMainPackageId().getPackageName();
                }
                String price = String.valueOf(tmpGood.getPrice());
                String priceWDiscount = String.valueOf(tmpGood.getPrice()
                        - tmpGood.getPrice() * bill.getContragents().getDiscount() / 100.0);
                String packageSize = String.valueOf(tmpGood.getGoods().getMainFactor())
                        + tmpGood.getGoods().getPackagesByBasePackageId().getPackageName();
                String summWODiscount = String.valueOf(tmpGood.getTotalValue());
                String discountSize = String.valueOf(tmpGood.getTotalValue() * bill.getContragents().getDiscount() / 100.0);
                String discountPercentage = String.valueOf(bill.getContragents().getDiscount()) + " %";
                String summWDiscount = String.valueOf(tmpGood.getTotalValue()
                        - tmpGood.getTotalValue() * bill.getContragents().getDiscount() / 100.0);
                String factRemainder = String.valueOf(tmpGood.getGoods().getFactRemainder());

                data = data + "<tr style=\"page-break-before: always;\">"
                        + "			<td>" + number + "</td>\n"
                        + "			<td>" + name + "</td>\n"
                        + "			<td>" + quantity + "</td>\n"
                        + "			<td>" + packageName + "</td>\n"
                        + "			<td>" + price + "</td>\n"
                        + "			<td>" + priceWDiscount + "</td>\n"
                        + "			<td>" + packageSize + "</td>\n"
                        + "			<td>" + summWODiscount + "</td>\n"
                        + "			<td>" + discountSize + "</td>\n"
                        + "			<td>" + discountPercentage + "</td>\n"
                        + "			<td>" + summWDiscount + "</td>\n"
                        + "			<td>" + factRemainder + "</td>\n"
                        + "		</tr>\n";

                if (i % 16 == 0 & i > 0 & i < 17) {
                    data = data + "<tr><td>&nbsp;</td></tr>";
                } else if ((i - 16) % 24 == 0 & i > 0 & i > 25) {
                    data = data + "<tr><td>&nbsp;</td></tr>";
                }
            }
            Double summ = bill.getSumm();
            data = data
                    + "	</tbody>\n"
                    + "</table>\n"
                    + "</font>\n"
                    + "        <br/>\n"
                    + "<div align = \"right\"> Итого: " + String.valueOf(summ) + "</div>"
                    + " <br/>\n";

            Double totalPlaces = 0.0;
            Double totalWeight = 0.0;

            for (DocumentGoods billGood : billGoods) {
                totalPlaces += billGood.getQuantity();
                if (billGood.getGoods().getUseBasePackage()) {
                    totalWeight += billGood.getGoods().getBasePackageMass() * billGood.getQuantity();
                } else {
                    totalWeight += billGood.getGoods().getMainPackageMass() * billGood.getQuantity();
                }
            }

            int positionNumber = billGoods.size();

            data = data + "<b>Количество мест = " + String.valueOf(totalPlaces)
                    + ", " + "общий вес = " + String.valueOf(totalWeight)
                    + " кг. <br/><br/>";
            data = data + "Всего наименований " + String.valueOf(positionNumber)
                    + ", на сумму " + String.valueOf(summ) + " руб. <br/><br/>";
            String summString = RussianMoney.digits2text(summ);
            summString = summString.substring(0, 1).toUpperCase() + summString.substring(1);

            data = data + summString + "<br/><hr>";
            data = data + "Отпустил <hr>";
            data = data
                    + "</font>"
                    + "    </body>\n"
                    + "</html>";
            Label label
                    = new Label(data);

            label.setContentMode(ContentMode.HTML);
            label.setSizeFull();

            Button print = new Button("Печать");
            print.addClickListener(new Button.ClickListener() {
                @Override
                public void buttonClick(Button.ClickEvent event) {
                    print.setVisible(false);
                    Page.getCurrent().getJavaScript().execute("document.body.style.overflow = \"auto\";"
                            + "document.body.style.height  = \"auto\"");
                    UI.getCurrent().setSizeUndefined();

                    Page.getCurrent().getJavaScript().execute("window.print();");
                }
            });
            content.addComponent(print);
            content.addComponent(label);
            content.setComponentAlignment(print, Alignment.TOP_RIGHT);
            content.setExpandRatio(print, 0.0f);
            content.setExpandRatio(label, 0.0f);
            this.setSizeUndefined();
            setContent(content);
        }
    }

    final class BillGoodsWindow extends Window {

        private List<DocumentGoods> billGoods;
        private BillGoodsGrid billGoodsGrid;
        private VerticalLayout layout;
        private Set<DocumentGoods> selectedGoods;
        private List<Goods> selectedToAddGoods;
        private Boolean isListEdit;

        public BillGoodsWindow(List<DocumentGoods> billGoods) {
            this.setCaption("Список товаров");
            this.billGoods = billGoods;
            this.layout = new VerticalLayout();
            this.billGoodsGrid = new BillGoodsGrid(billGoods);
            Panel controlPanel = new Panel("Управление");
            HorizontalLayout controlPanelLayout = new HorizontalLayout();

            Button addGoodsButton = new Button("Добавить товары", new Button.ClickListener() {
                @Override
                public void buttonClick(Button.ClickEvent event) {
                    goToAddGoods();
                }
            });
            addGoodsButton.setEnabled(isEditable);

            Button deleteSelectedGoodsButton = new Button("Удалить выбранные товары", new Button.ClickListener() {
                @Override
                public void buttonClick(Button.ClickEvent event) {
                    deleteSelectedGoods();
                }
            });
            deleteSelectedGoodsButton.setEnabled(isEditable);

            Button deleteAllGoodsButton = new Button("Удалить все товары", new Button.ClickListener() {
                @Override
                public void buttonClick(Button.ClickEvent event) {
                    deleteAllGoods();
                }
            });
            deleteAllGoodsButton.setEnabled(isEditable);

            CheckBox isEditList = new CheckBox("Редактировать список товаров");
            isListEdit = isEditList.getValue();
            isEditList.addValueChangeListener(new HasValue.ValueChangeListener<Boolean>() {
                @Override
                public void valueChange(HasValue.ValueChangeEvent<Boolean> event) {
                    isListEdit = event.getValue();
                }
            });
            controlPanelLayout.addComponents(addGoodsButton, deleteSelectedGoodsButton, deleteAllGoodsButton, isEditList);
            controlPanel.setContent(controlPanelLayout);
            controlPanelLayout.setComponentAlignment(isEditList, Alignment.MIDDLE_LEFT);
            this.billGoodsGrid.setSelectionMode(Grid.SelectionMode.MULTI);
            this.billGoodsGrid.addSelectionListener(new SelectionListener<DocumentGoods>() {
                @Override
                public void selectionChange(SelectionEvent<DocumentGoods> event) {
                    selectedGoods = event.getAllSelectedItems();
                }
            });
            this.billGoodsGrid.addItemClickListener(new ItemClickListener<DocumentGoods>() {
                @Override
                public void itemClick(Grid.ItemClick<DocumentGoods> event) {
                    if (!isListEdit & isEditable) {
                        goToEditGood(event.getItem());
                    }
                }
            });

            layout.addComponent(controlPanel);
            layout.addComponent(billGoodsGrid);

            this.setContent(layout);
            this.center();
            this.setModal(true);
            MyUI.getCurrent().addWindow(this);

        }

        private void goToAddGoods() {
            Window addGoodsWindow = new Window("Выбор товаров");
            VerticalLayout addGoodsWindowLayout = new VerticalLayout();
            AllGoodsGrid allGoodsGrid = new AllGoodsGrid(stockHelper.getAllGoods(), allContragents, stockHelper.getGoodsTypes());
            addGoodsWindowLayout.addComponent(allGoodsGrid);
            allGoodsGrid.setHeight(16.0f, Unit.CM);
            HorizontalLayout buttons = new HorizontalLayout();
            Button doAddGoodsButton = new Button("Добавить выбранные", new Button.ClickListener() {
                @Override
                public void buttonClick(Button.ClickEvent event) {
                    selectedToAddGoods = allGoodsGrid.getSelectedGoods();
                    doAddGoods();
                    addGoodsWindow.close();
                }
            });

            Button doCancellButton = new Button("Отмена", new Button.ClickListener() {
                @Override
                public void buttonClick(Button.ClickEvent event) {
                    selectedToAddGoods.clear();
                    addGoodsWindow.close();

                }
            });

            buttons.addComponents(doAddGoodsButton, doCancellButton);
            buttons.setComponentAlignment(doAddGoodsButton, Alignment.TOP_CENTER);
            buttons.setComponentAlignment(doCancellButton, Alignment.TOP_CENTER);
            buttons.setSizeFull();
            addGoodsWindowLayout.addComponent(buttons);
            addGoodsWindow.setContent(addGoodsWindowLayout);
            addGoodsWindow.center();
            addGoodsWindow.setModal(true);
            addGoodsWindow.setHeight(19.0f, Unit.CM);
            addGoodsWindow.setWidth(35.0f, Unit.CM);
            MyUI.getCurrent().addWindow(addGoodsWindow);
        }

        private void doAddGoods() {

            for (Goods selectedGood : selectedToAddGoods) {
                DocumentGoods docGood = new DocumentGoods();
                List<Prices> goodPrices = stockHelper.getPricesByGoodId(selectedGood.getId());
                for (Prices goodPrice : goodPrices) {
                    if (goodPrice.getPriceType().getId() == 1) {
                        docGood.setPrice(goodPrice.getResult());
                    }
                }
                docGood.setGoods(selectedGood);
                docGood.setQuantity(0.0);
                docGood.setTotalValue(0.0);
                billGoods.add(docGood);
            }
            layout.removeComponent(billGoodsGrid);
            billGoodsGrid = new BillGoodsGrid(billGoods);
            this.billGoodsGrid.setSelectionMode(Grid.SelectionMode.MULTI);
            this.billGoodsGrid.addSelectionListener(new SelectionListener<DocumentGoods>() {
                @Override
                public void selectionChange(SelectionEvent<DocumentGoods> event) {
                    selectedGoods = event.getAllSelectedItems();
                }
            });
            billGoodsGrid.addItemClickListener(new ItemClickListener<DocumentGoods>() {
                @Override
                public void itemClick(Grid.ItemClick<DocumentGoods> event) {
                    if (!isListEdit) {
                        goToEditGood(event.getItem());
                    }
                }
            });
            layout.addComponent(billGoodsGrid);
            selectedToAddGoods.clear();
        }

        private void goToEditGood(DocumentGoods item) {
            Window editDocumentGoodWindow = new Window();
            VerticalLayout editDocumentGoodLayout = new VerticalLayout();

            Label itemName = new Label(item.getGoods().getName());

            Button editItemPrices = new Button("Редактировать цены", new Button.ClickListener() {
                @Override
                public void buttonClick(Button.ClickEvent event) {
                    GoodPricesController pricesController = new GoodPricesController(item.getGoods().getId());
                    Window pr = pricesController.getPriceListWindow();
                    pr.center();
                    pr.setModal(true);
                    MyUI.getCurrent().addWindow(pr);
                }
            });
            HashMap<String, Prices> pricesMap = new HashMap<>();
            List<Prices> pricesList = stockHelper.getPricesByGoodId(item.getGoods().getId());
            for (Prices prices : pricesList) {
                pricesMap.put(prices.getPriceType().getName(), prices);
            }

            TextField itemQuantity = new TextField("Количество", String.valueOf(item.getQuantity()));
            itemQuantity.setEnabled(false);
            TextField itemTotalValue = new TextField("Стоимость", String.valueOf(item.getTotalValue()));
            itemTotalValue.addValueChangeListener(new HasValue.ValueChangeListener<String>() {
                @Override
                public void valueChange(HasValue.ValueChangeEvent<String> event) {
                    itemTotalValue.setValue(itemTotalValue.getValue().replace(",", "."));
                }
            });
            itemTotalValue.setEnabled(false);

            ComboBox itemSelectPrice = new ComboBox("Тип цены", pricesMap.keySet());
            itemSelectPrice.setEmptySelectionAllowed(false);
            itemSelectPrice.addValueChangeListener(new HasValue.ValueChangeListener() {
                @Override
                public void valueChange(HasValue.ValueChangeEvent event) {
                    String value = (String) event.getValue();
                    Prices p = pricesMap.get(value);
                    item.setPrice(p.getResult());
                    itemQuantity.setEnabled(true);
                    itemTotalValue.setEnabled(true);
                }
            });

            itemQuantity.addValueChangeListener(new HasValue.ValueChangeListener<String>() {
                @Override
                public void valueChange(HasValue.ValueChangeEvent<String> event) {
                    if (!event.getValue().isEmpty()) {
                        Boolean isUseBasePackage = item.getGoods().getUseBasePackage();
                        Double remainder = item.getGoods().getRemainder();
                        Double requestedQuantity = 0.0;
                        String value = event.getValue();
//                    value = value.replace("/[^0-9]/", "");
                        value = value.replace(",", ".");
                        itemQuantity.setValue(value);
                        try {
                            requestedQuantity = Double.parseDouble(value);
                        } catch (NumberFormatException e) {

                        }
                        if (!isUseBasePackage) {
                            requestedQuantity *= item.getGoods().getMainFactor();
                        }
                        if (requestedQuantity > remainder) {

                            Notification tooMuch = new Notification("<center>Недостаточно товара или он был зарезервирован <br/>"
                                    + "Установлено максимально допустимое количество товара</center>",
                                    Notification.Type.WARNING_MESSAGE);
                            tooMuch.setHtmlContentAllowed(true);
                            tooMuch.setDelayMsec(10000);
                            tooMuch.show(Page.getCurrent());
                            item.setQuantity(remainder);
                            if (!isUseBasePackage) {
                                itemQuantity.setValue(String.valueOf(remainder / item.getGoods().getMainFactor()));
                            } else {
                                itemQuantity.setValue(String.valueOf(remainder));
                            }
                        } else {
                            item.setQuantity(Double.parseDouble(value));
                        }
                        Double tmpValue = item.getQuantity() * item.getPrice();
                        if (!isUseBasePackage) {
                            tmpValue *= item.getGoods().getMainFactor();
                        }
                        itemTotalValue.setValue(String.valueOf(round(tmpValue, 2)));
                    }
                }
            });

            Button save = new Button("Сохранить", new Button.ClickListener() {
                @Override
                public void buttonClick(Button.ClickEvent event) {
                    item.setQuantity(Double.parseDouble(itemQuantity.getValue().replace(",", ".")));
                    item.setTotalValue(Double.parseDouble(itemTotalValue.getValue().replace(",", ".")));
                    layout.removeComponent(billGoodsGrid);

                    billGoodsGrid = new BillGoodsGrid(billGoods);
                    billGoodsGrid.setSelectionMode(Grid.SelectionMode.MULTI);
                    billGoodsGrid.addSelectionListener(new SelectionListener<DocumentGoods>() {
                        @Override
                        public void selectionChange(SelectionEvent<DocumentGoods> event) {
                            selectedGoods = event.getAllSelectedItems();
                        }
                    });
                    billGoodsGrid.addItemClickListener(new ItemClickListener<DocumentGoods>() {
                        @Override
                        public void itemClick(Grid.ItemClick<DocumentGoods> event) {
                            if (!isListEdit) {
                                goToEditGood(event.getItem());
                            }
                        }
                    });
                    GoodPricesController pc = new GoodPricesController(item.getGoods().getId());
                    for (DocumentGoods documentGoodsItem : billGoods) {
                        if (documentGoodsItem.equals(item)) {
                            documentGoodsItem.setPrice(pc.getPurchasePrice());
                        }
                    }
                    ListDataProvider<DocumentGoods> prov = (ListDataProvider<DocumentGoods>) billGoodsGrid.getDataProvider();
                    prov.refreshAll();
                    layout.addComponent(billGoodsGrid);

                    editDocumentGoodWindow.close();
                }
            });

            editDocumentGoodLayout.addComponents(itemName, editItemPrices, itemSelectPrice, itemQuantity, itemTotalValue, save);
            editDocumentGoodWindow.setContent(editDocumentGoodLayout);
            editDocumentGoodWindow.center();
            editDocumentGoodWindow.setModal(true);
            MyUI.getCurrent().addWindow(editDocumentGoodWindow);
        }

        public Double getSumm() {
            return billGoodsGrid.getValue();
        }

        public List<DocumentGoods> getBillGoods() {
            return billGoods;
        }

        private void deleteSelectedGoods() {
            for (DocumentGoods selectedGood : selectedGoods) {
                billGoods.remove(selectedGood);
            }
            layout.removeComponent(billGoodsGrid);
            billGoodsGrid = new BillGoodsGrid(billGoods);
            billGoodsGrid.setSelectionMode(Grid.SelectionMode.MULTI);
            billGoodsGrid.addSelectionListener(new SelectionListener<DocumentGoods>() {
                @Override
                public void selectionChange(SelectionEvent<DocumentGoods> event) {
                    selectedGoods = event.getAllSelectedItems();
                }
            });
            billGoodsGrid.addItemClickListener(new ItemClickListener<DocumentGoods>() {
                @Override
                public void itemClick(Grid.ItemClick<DocumentGoods> event) {
                    if (!isListEdit) {
                        goToEditGood(event.getItem());
                    }
                }
            });
            layout.addComponent(billGoodsGrid);

        }

        private void deleteAllGoods() {
            billGoods.clear();
            layout.removeComponent(billGoodsGrid);
            billGoodsGrid = new BillGoodsGrid(billGoods);
            billGoodsGrid.setSelectionMode(Grid.SelectionMode.MULTI);
            billGoodsGrid.addSelectionListener(new SelectionListener<DocumentGoods>() {
                @Override
                public void selectionChange(SelectionEvent<DocumentGoods> event) {
                    selectedGoods = event.getAllSelectedItems();
                }
            });
            billGoodsGrid.addItemClickListener(new ItemClickListener<DocumentGoods>() {
                @Override
                public void itemClick(Grid.ItemClick<DocumentGoods> event) {
                    if (!isListEdit) {
                        goToEditGood(event.getItem());
                    }
                }
            });
            layout.addComponent(billGoodsGrid);
        }

    }

    private double round(double number, int scale) {
        int pow = 10;
        for (int i = 1; i < scale; i++) {
            pow *= 10;
        }
        double tmp = number * pow;
        return (double) (int) ((tmp - (int) tmp) >= 0.5 ? tmp + 1 : tmp) / pow;
    }

    private double round(Double number, int scale) {
        int pow = 10;
        for (int i = 1; i < scale; i++) {
            pow *= 10;
        }
        double tmp = number * pow;
        return (double) (int) ((tmp - (int) tmp) >= 0.5 ? tmp + 1 : tmp) / pow;
    }
}
