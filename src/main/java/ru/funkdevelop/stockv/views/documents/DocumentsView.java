/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ru.funkdevelop.stockv.views.documents;

import com.vaadin.data.provider.ListDataProvider;
import com.vaadin.navigator.Navigator;
import com.vaadin.navigator.View;
import com.vaadin.navigator.ViewChangeListener;
import com.vaadin.ui.Alignment;
import com.vaadin.ui.Button;
import com.vaadin.ui.ComboBox;
import com.vaadin.ui.DateField;
import com.vaadin.ui.Grid;
import com.vaadin.ui.GridLayout;
import com.vaadin.ui.Panel;
import com.vaadin.ui.UI;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.components.grid.HeaderRow;
import com.vaadin.ui.components.grid.ItemClickListener;
import com.vaadin.ui.renderers.DateRenderer;
import com.vaadin.ui.renderers.NumberRenderer;
import com.vaadin.ui.renderers.TextRenderer;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.List;
import ru.funkdevelop.stockv.StockHelper;
import ru.funkdevelop.stockv.pojos.Contragents;
import ru.funkdevelop.stockv.pojos.Documents;
import ru.funkdevelop.stockv.pojos.Users;
import ru.funkdevelop.stockv.views.documents.windows.CashReceiptWindow;
import ru.funkdevelop.stockv.views.documents.windows.IncomeDocumentWindow;
import ru.funkdevelop.stockv.views.documents.windows.OutcomeDocumentWindow;
import ru.funkdevelop.stockv.views.documents.windows.RemainderDocumentWindow;
import ru.funkdevelop.stockv.views.documents.windows.WritingOffDocumentWindow;
import ru.funkdevelop.stockv.views.service.MainMenu;
import ru.funkdevelop.stockv.views.service.UserData;

/**
 *
 * @author artur
 */
public class DocumentsView extends GridLayout implements View {

    StockHelper stockHelper;
    MainMenu mainMenu;
    UserData userData;
    Navigator navigator;

    List<Documents> allDocuments;

    @Override
    public void enter(ViewChangeListener.ViewChangeEvent event) {
        stockHelper = new StockHelper();
        navigator = UI.getCurrent().getNavigator();
        userData = new UserData();
        mainMenu = new MainMenu(navigator);

        VerticalLayout fullLayout = new VerticalLayout();

        GridLayout layout = new GridLayout(3, 3);

        Panel allDocumentsPanel = new Panel("Cписок документов");
        DocumentsGrid allDocumentsgrid = new DocumentsGrid();
        allDocumentsgrid.addItemClickListener(new ItemClickListener<Documents>() {
            @Override
            public void itemClick(Grid.ItemClick<Documents> event) {
                int documentType = event.getItem().getType();
                IncomeDocumentWindow incomeDocumentWindow;
                OutcomeDocumentWindow outcomeDocumentWindow;
                RemainderDocumentWindow remainderDocumentWindow;
                WritingOffDocumentWindow writingOffDocumentWindow;
                CashReceiptWindow cashReceiptWindow;
                if (documentType == 1) {
                    incomeDocumentWindow = new IncomeDocumentWindow(event.getItem());
                } else if (documentType == 2) {
                    outcomeDocumentWindow = new OutcomeDocumentWindow(event.getItem());
                } else if (documentType == 3) {
                    remainderDocumentWindow = new RemainderDocumentWindow(event.getItem());
                } else if (documentType == 4) {
                    writingOffDocumentWindow = new WritingOffDocumentWindow(event.getItem());
                } else if (documentType == 5) {
                    cashReceiptWindow = new CashReceiptWindow(event.getItem());
                }
            }
        });
        allDocumentsgrid.setSizeFull();
        allDocumentsPanel.setContent(allDocumentsgrid);

        allDocumentsPanel.setSizeFull();
        allDocumentsPanel.setWidth(32.0f, Unit.CM);
        allDocumentsPanel.setHeight(19.0f, Unit.CM);

        layout.addComponent(mainMenu, 0, 0, 2, 0);
        layout.addComponent(userData, 0, 1, 0, 1);
        layout.addComponent(allDocumentsPanel, 1, 1, 2, 1);
        layout.setColumnExpandRatio(0, 0.0f);
        layout.setColumnExpandRatio(1, 0.0f);
        layout.setColumnExpandRatio(2, 0.0f);
        layout.setRowExpandRatio(0, 0.0f);
        layout.setRowExpandRatio(1, 0.0f);
        layout.setRowExpandRatio(2, 0.0f);
        layout.setSpacing(false);
        layout.setMargin(false);
        fullLayout.addComponent(layout);
        fullLayout.setComponentAlignment(layout, Alignment.MIDDLE_CENTER);

        fullLayout.setMargin(false);
        fullLayout.setSpacing(false);

        getUI().setContent(fullLayout);

    }

    final class DocumentsGrid extends Grid<Documents> {

        DateField docDate;
        ComboBox contragent;
        ComboBox author;

        public DocumentsGrid() {
            allDocuments = stockHelper.getAllDocuments();
            this.addColumn(Documents::getNumber, new NumberRenderer("%06d")).setCaption("#");

            this.addColumn((documents) -> {
                String result;
                Boolean isCarried = documents.getIsCarried();
                if (isCarried) {
                    result = "Д";
                } else {
                    result = "Н";
                }
                return result;
            }, new TextRenderer()).setCaption("Пров.");

            this.addColumn((documents) -> {
                String result;
                Integer type = documents.getType();
                switch (type) { //добавить тип 5 для ПКО
                    case 1:
                        result = "П";   //Приход
                        break;
                    case 2:
                        result = "Р";   //Расход
                        break;
                    case 3:
                        result = "О";   //Ввод остатков
                        break;
                    case 4:
                        result = "C";   //Списание
                        break;
                    case 5:
                        result = "ПКО";
                        break;
                    default:
                        result = "-";   //Неизвестный тип документа - никогда не выполнится
                        break;
                }
                return result;
            }, new TextRenderer()).setCaption("Тип");

            this.addColumn(Documents::getDate, new DateRenderer(new SimpleDateFormat("dd.MM.yyyy"))).setCaption("Дата");

            this.addColumn(Documents::getTime, new TextRenderer()).setCaption("Время");

            this.addColumn((documents) -> {
                String result;
                Double summ = documents.getSumm();
                result = String.valueOf(summ);
                return result;
            }, new TextRenderer()).setCaption("Cумма");

            this.addColumn((documents) -> documents.getContragents().getDisplayName(), new TextRenderer()).setCaption("Контрагент");

            this.addColumn((documents) -> documents.getUsers().getLogin(), new TextRenderer()).setCaption("Автор");

            this.addColumn(Documents::getFirm, new TextRenderer()).setCaption("Фирма");

            this.setItems(allDocuments);

            this.docDate = new DateField();
            this.docDate.setPlaceholder("Дата...");
            this.docDate.setSizeFull();
            this.docDate.setHeight(0.8f, Unit.CM);
            this.docDate.setWidth(2.5f, Unit.CM);
            this.docDate.setDateFormat("dd.MM.yyyy");
            

            List<Contragents> allContragents = stockHelper.getContragents();
            HashMap<String, Contragents> contragentsMap = new HashMap<>();
            for (Contragents allContragent : allContragents) {
                contragentsMap.put(allContragent.getDisplayName(), allContragent);
            }
            this.contragent = new ComboBox();
            this.contragent.setPlaceholder("Контрагент...");
            this.contragent.setItems(contragentsMap.keySet());
            this.contragent.setSizeFull();
            this.contragent.setHeight(0.8f, Unit.CM);
            this.contragent.setWidth(2.5f, Unit.CM);
            this.contragent.setPopupWidth(null);

            List<Users> allUsers = stockHelper.getUsers();
            HashMap<String, Users> usersMap = new HashMap<>();
            for (Users allUser : allUsers) {
                usersMap.put(allUser.getLogin(), allUser);
            }
            this.author = new ComboBox();
            this.author.setPlaceholder("Автор...");
            this.author.setItems(usersMap.keySet());
            this.author.setSizeFull();
            this.author.setHeight(0.8f, Unit.CM);
            this.author.setWidth(2.5f, Unit.CM);
            this.author.setPopupWidth(null);

            Button doFilterButton = new Button("ОК", new Button.ClickListener() {
                @Override
                public void buttonClick(Button.ClickEvent event) {
                    doFilter();
                }
            });
            doFilterButton.setSizeFull();
            doFilterButton.setHeight(0.8f, Unit.CM);
            doFilterButton.setWidth(2.0f, Unit.CM);
            Button doResetFilterButton = new Button("Сброс", new Button.ClickListener() {
                @Override
                public void buttonClick(Button.ClickEvent event) {
                    doResetFilter();
                }
            });
            doResetFilterButton.setSizeFull();
        doResetFilterButton.setHeight(0.8f, Unit.CM);
        doResetFilterButton.setWidth(2.0f, Unit.CM);
        HeaderRow filterRow = this.appendHeaderRow();
        filterRow.getCell(this.getColumns().get(3)).setComponent(docDate);
        filterRow.getCell(this.getColumns().get(6)).setComponent(contragent);
        filterRow.getCell(this.getColumns().get(7)).setComponent(author);
        filterRow.getCell(this.getColumns().get(0)).setComponent(doFilterButton);       
        HeaderRow buttonsRow = this.appendHeaderRow();
        buttonsRow.getCell(this.getColumns().get(0)).setComponent(doResetFilterButton);
            
        }

        private void doFilter() {

            ListDataProvider<Documents> dataProvider = (ListDataProvider<Documents>) this.getDataProvider();
            if (!docDate.isEmpty()) {
                Calendar tmp = new GregorianCalendar();
                tmp.set(docDate.getValue().getYear(), docDate.getValue().getMonthValue() - 1, docDate.getValue().getDayOfMonth());
                Date d = new Date();
                d.setTime(tmp.getTimeInMillis());
                String date = new SimpleDateFormat("dd.MM.yyyy").format(d);
                
                dataProvider.addFilter((documents) -> new SimpleDateFormat("dd.MM.yyyy").format(documents.getDate()),
                        s -> caseInsensitiveContains(s, date));
            }
            if (!contragent.isEmpty()) {
                dataProvider.addFilter((documents) -> documents.getContragents().getDisplayName(),
                        s -> caseInsensitiveContains(s, (String) contragent.getValue()));
            }
            if (!author.isEmpty()) {
                dataProvider.addFilter((documents) -> documents.getUsers().getLogin(),
                        s -> caseInsensitiveContains(s, (String) author.getValue()));
            }

        }

        private void doResetFilter() {
            ListDataProvider<Documents> dataProvider = (ListDataProvider<Documents>) this.getDataProvider();
            dataProvider.clearFilters();
            docDate.clear();
            contragent.clear();
            author.clear();
        }

        private Boolean caseInsensitiveContains(String where, String what) {
            return where.toLowerCase().contains(what.toLowerCase());
        }
    }
}
