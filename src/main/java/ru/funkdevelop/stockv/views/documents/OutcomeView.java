/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ru.funkdevelop.stockv.views.documents;

import com.vaadin.data.HasValue;
import com.vaadin.data.provider.ListDataProvider;
import com.vaadin.navigator.Navigator;
import com.vaadin.navigator.View;
import com.vaadin.navigator.ViewChangeListener;
import com.vaadin.server.BrowserWindowOpener;
import com.vaadin.server.Page;
import com.vaadin.server.VaadinRequest;
import com.vaadin.server.VaadinService;
import com.vaadin.shared.ui.ContentMode;
import com.vaadin.ui.Alignment;
import com.vaadin.ui.Button;
import com.vaadin.ui.CheckBox;
import com.vaadin.ui.ComboBox;
import com.vaadin.ui.DateField;
import com.vaadin.ui.Grid;
import com.vaadin.ui.GridLayout;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Label;
import com.vaadin.ui.Notification;
import com.vaadin.ui.Panel;
import com.vaadin.ui.TabSheet;
import com.vaadin.ui.TextField;
import com.vaadin.ui.UI;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.Window;
import com.vaadin.ui.components.grid.ItemClickListener;
import com.vaadin.ui.themes.ValoTheme;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import ru.funkdevelop.stockv.MyUI;
import ru.funkdevelop.stockv.StockHelper;
import ru.funkdevelop.stockv.pojos.Contragents;
import ru.funkdevelop.stockv.pojos.DocumentGoods;
import ru.funkdevelop.stockv.pojos.Documents;
import ru.funkdevelop.stockv.pojos.Goods;
import ru.funkdevelop.stockv.pojos.GoodsType;
import ru.funkdevelop.stockv.pojos.Prices;
import ru.funkdevelop.stockv.pojos.Users;
import ru.funkdevelop.stockv.views.service.AllGoodsGrid;
import ru.funkdevelop.stockv.views.service.BillGoodsGrid;
import ru.funkdevelop.stockv.views.service.GoodPricesController;
import ru.funkdevelop.stockv.views.service.MainMenu;
import ru.funkdevelop.stockv.views.service.RussianMoney;
import ru.funkdevelop.stockv.views.service.UserData;

/**
 *
 * @author artur
 */
public class OutcomeView extends GridLayout implements View {

    MainMenu mainMenu;
    UserData userData;
    StockHelper stockHelper;
    Navigator navigator;
    List<Contragents> allContragents;
    List<Goods> allGoods;
    List<Goods> selectedGoods;
    List<GoodsType> allGoodsTypes;
    List<DocumentGoods> documentGoodsItems;

    VerticalLayout billGrid;
    BillGoodsGrid billGoods;

    DocumentGoods selectedItem;

    CheckBox isEditList;

    TextField docTotal;

    Button goToPrintBillButton;

    private int billNumber;
    private Date billDate;
    private Date billTime;
    private String billFirm;
    private String billWarehouse;
    private Contragents billProvider;
    private String billContract;
    private Date billPayUntil;
    private Double billSumm;
    private Double billPaymentSize = 0.0;
    private Double billChangeSize = 0.0;
    private Users billAuthor;
    private Boolean billIsCarried;
    private Boolean billIsFullyPaid = false;

    @Override
    public void enter(ViewChangeListener.ViewChangeEvent event) {
        navigator = UI.getCurrent().getNavigator();
        mainMenu = new MainMenu(navigator);
        userData = new UserData();
        stockHelper = new StockHelper();
        allContragents = stockHelper.getContragents();
        documentGoodsItems = new ArrayList<>();
        allGoods = stockHelper.getAllGoods();
        selectedGoods = new ArrayList<>();
        allGoodsTypes = stockHelper.getGoodsTypes();

        billGoods = new BillGoodsGrid(documentGoodsItems);

        GridLayout layout = new GridLayout(3, 3);

        TabSheet tabSheet = new TabSheet();
        tabSheet.addStyleName(ValoTheme.TABSHEET_FRAMED);
        tabSheet.addStyleName(ValoTheme.TABSHEET_PADDED_TABBAR);

        VerticalLayout header = new VerticalLayout();

        Panel docHeader = new Panel("Документ");
        HorizontalLayout docHeaderLayout = new HorizontalLayout();
        TextField docType = new TextField("Тип документа", "Реализация (купля - продажа)");
        docType.setWidth(10.0f, Unit.CM);
        docType.setEnabled(false);
        this.billNumber = stockHelper.getMaxBillId();

        this.billNumber += 1;

        TextField docNumber = new TextField("Номер", String.valueOf(this.billNumber));
        docNumber.setEnabled(false);
        DateField docDate = new DateField("Дата", LocalDate.now());
        docDate.setDateFormat("dd.MM.yyyy");
        docHeaderLayout.addComponents(docType, docNumber, docDate);
        docHeaderLayout.setComponentAlignment(docType, Alignment.TOP_CENTER);
        docHeaderLayout.setComponentAlignment(docNumber, Alignment.TOP_CENTER);
        docHeaderLayout.setComponentAlignment(docDate, Alignment.TOP_CENTER);
        docHeaderLayout.setSizeFull();
        docHeaderLayout.setMargin(true);
        docHeaderLayout.setSpacing(true);
        docHeader.setContent(docHeaderLayout);

        HorizontalLayout line2 = new HorizontalLayout();

        Panel provider = new Panel("Поставщик");
        VerticalLayout providerLayout = new VerticalLayout();

        TextField providerName = new TextField("Поставщик", "И.П. Валиахметова Л.Н.");
        providerName.setWidth(14.0f, Unit.CM);

        TextField warehouseName = new TextField("Склад", "Основной склад");
        warehouseName.setWidth(14.0f, Unit.CM);

        HorizontalLayout buttons = new HorizontalLayout();

        Button save = new Button("Сохранить");

        Button checkout = new Button("Провести");

        goToPrintBillButton = new Button("Печать накладной");
        goToPrintBillButton.setEnabled(false);
        BrowserWindowOpener opener
                = new BrowserWindowOpener(OutcomeView.MyPopupUI.class);
        opener.setFeatures("resizable");

        opener.extend(goToPrintBillButton);

        buttons.addComponents(save, checkout, goToPrintBillButton);
        buttons.setComponentAlignment(save, Alignment.MIDDLE_CENTER);
        buttons.setComponentAlignment(checkout, Alignment.MIDDLE_CENTER);
        buttons.setComponentAlignment(goToPrintBillButton, Alignment.MIDDLE_CENTER);
        buttons.setSizeFull();
        providerLayout.addComponents(providerName, warehouseName, buttons);
        providerLayout.setComponentAlignment(providerName, Alignment.TOP_CENTER);
        providerLayout.setComponentAlignment(warehouseName, Alignment.TOP_CENTER);
        providerLayout.setSizeFull();
        providerLayout.setMargin(true);
        providerLayout.setSpacing(true);
        provider.setContent(providerLayout);

        Panel buyer = new Panel("Покупатель");
        VerticalLayout buyerLayout = new VerticalLayout();

        TextField contract = new TextField("Договор");
        contract.setEnabled(false);
        contract.setWidth(14.0f, Unit.CM);

        TextField buyerCredit = new TextField("Долг контрагента");
        buyerCredit.setEnabled(false);
        buyerCredit.setWidth(14.0f, Unit.CM);

        HashMap<String, Contragents> buyers = new HashMap<>();
        for (Contragents allContragent : allContragents) {
            if (allContragent.getContragentType().getId() != 1) {
                buyers.put(allContragent.getDisplayName(), allContragent);
            }
        }
        ComboBox selectBuyer = new ComboBox("Покупатель", buyers.keySet());
        selectBuyer.setEmptySelectionAllowed(false);
        selectBuyer.setWidth(14.0f, Unit.CM);
        selectBuyer.addValueChangeListener(new HasValue.ValueChangeListener() {
            @Override
            public void valueChange(HasValue.ValueChangeEvent event) {
                Contragents c = buyers.get(event.getValue());
                contract.setValue(c.getMainContract());
                buyerCredit.setValue(String.valueOf(c.getCredit()) + " руб.");
            }
        });

        DateField payUntil = new DateField("Оплата до", LocalDate.now());
        payUntil.setDateFormat("dd.MM.yyyy");
        payUntil.setWidth(14.0f, Unit.CM);
        buyerLayout.addComponents(selectBuyer, contract, buyerCredit, payUntil);
        buyerLayout.setComponentAlignment(selectBuyer, Alignment.TOP_CENTER);
        buyerLayout.setComponentAlignment(contract, Alignment.TOP_CENTER);
        buyerLayout.setComponentAlignment(payUntil, Alignment.TOP_CENTER);
        buyerLayout.setComponentAlignment(buyerCredit, Alignment.TOP_CENTER);
        buyerLayout.setSizeFull();
        buyerLayout.setMargin(true);
        buyerLayout.setSpacing(true);
        buyer.setContent(buyerLayout);

        save.addClickListener(new Button.ClickListener() {
            @Override
            public void buttonClick(Button.ClickEvent event) {
                Date date = new Date();
                Date time = new Date();
                Calendar tmp = new GregorianCalendar();
                tmp.set(docDate.getValue().getYear(), docDate.getValue().getMonthValue() - 1, docDate.getValue().getDayOfMonth());
                date.setTime(tmp.getTimeInMillis());
                billDate = date;
                billTime = time;
                billFirm = providerName.getValue();
                billWarehouse = warehouseName.getValue();
                billProvider = buyers.get(selectBuyer.getValue());
                billContract = contract.getValue();
                Date payDay = new Date();
                Calendar c = new GregorianCalendar();
                c.set(payUntil.getValue().getYear(), payUntil.getValue().getMonthValue() - 1, payUntil.getValue().getDayOfMonth());
                payDay.setTime(c.getTimeInMillis());
                billPayUntil = payDay;
                billNumber = Integer.parseInt(docNumber.getValue());
                doSaveDocument(documentGoodsItems, false);
            }
        });

        checkout.addClickListener(new Button.ClickListener() {
            @Override
            public void buttonClick(Button.ClickEvent event) {
                Date date = new Date();
                Date time = new Date();
                Calendar tmp = new GregorianCalendar();
                tmp.set(docDate.getValue().getYear(), docDate.getValue().getMonthValue() - 1, docDate.getValue().getDayOfMonth());
                date.setTime(tmp.getTimeInMillis());
                billDate = date;
                billTime = time;
                billFirm = providerName.getValue();
                billWarehouse = warehouseName.getValue();
                billProvider = buyers.get(selectBuyer.getValue());
                billContract = contract.getValue();
                Date payDay = new Date();
                Calendar c = new GregorianCalendar();
                c.set(payUntil.getValue().getYear(), payUntil.getValue().getMonthValue() - 1, payUntil.getValue().getDayOfMonth());
                payDay.setTime(c.getTimeInMillis());
                billPayUntil = payDay;
                billNumber = Integer.parseInt(docNumber.getValue());
                doCheckoutDocument(documentGoodsItems, true);
            }
        });

        buyer.setHeight(9.0f, Unit.CM);
        provider.setHeight(9.0f, Unit.CM);

        line2.addComponent(provider);
        line2.addComponent(buyer);
        line2.setSizeFull();
        Panel paymentData = new Panel("Информация о платеже");
        HorizontalLayout line3 = new HorizontalLayout();

        docTotal = new TextField("Общая стоимость", String.valueOf(billGoods.getValue()));

        TextField docPaidAmount = new TextField("Всего оплачено", String.valueOf(billPaymentSize));
        TextField docChangeAmount = new TextField("Сдача", String.valueOf(billChangeSize));

        docChangeAmount.setEnabled(false);
        Button calculateChange = new Button("Рассчитать сдачу", new Button.ClickListener() {
            @Override
            public void buttonClick(Button.ClickEvent event) {
                try {
                    billPaymentSize = Double.parseDouble(docPaidAmount.getValue());
                    if (billPaymentSize >= billSumm) {
                        billChangeSize = round(billPaymentSize - billSumm, 2);
                        billIsFullyPaid = true;
                    } else {
                        billIsFullyPaid = false;
                    }
//                docTotal.setValue(String.valueOf(billSumm));
                    docChangeAmount.setValue(String.valueOf(billChangeSize));
                } catch (NumberFormatException e) {

                }

            }
        });
        calculateChange.setEnabled(false);
        Button calculateDiscount = new Button("Рассчитать скидку", new Button.ClickListener() {
            @Override
            public void buttonClick(Button.ClickEvent event) {
                Double discount = buyers.get(selectBuyer.getValue()).getDiscount();
                billSumm = round(billSumm - billSumm * discount / 100.0, 2);
                docTotal.setValue(String.valueOf(billSumm));
                calculateChange.setEnabled(true);
                event.getButton().setEnabled(false);
            }
        });
        docTotal.addValueChangeListener(new HasValue.ValueChangeListener<String>() {
            @Override
            public void valueChange(HasValue.ValueChangeEvent<String> event) {
                try {
                    billSumm = Double.parseDouble(event.getValue());
                } catch (NumberFormatException e) {

                }
            }
        });
        line3.addComponents(docTotal, docPaidAmount, docChangeAmount, calculateDiscount, calculateChange);
        line3.setComponentAlignment(docTotal, Alignment.MIDDLE_CENTER);

        line3.setComponentAlignment(docPaidAmount, Alignment.MIDDLE_CENTER);
        line3.setComponentAlignment(docChangeAmount, Alignment.MIDDLE_CENTER);
        line3.setComponentAlignment(calculateDiscount, Alignment.MIDDLE_CENTER);
        line3.setComponentAlignment(calculateChange, Alignment.MIDDLE_CENTER);
        line3.setSizeFull();
        line3.setSpacing(true);
        line3.setMargin(true);
        paymentData.setContent(line3);

        header.addComponent(docHeader);
        header.addComponent(line2);
        header.addComponent(paymentData);
        VerticalLayout tabView = new VerticalLayout();

        Panel goodsList = new Panel("Список товаров");
        VerticalLayout goodsListLayout = new VerticalLayout();

        HorizontalLayout goodsListHeader = new HorizontalLayout();
        Button doAddGoodsButton = new Button("Добавить товары", new Button.ClickListener() {
            @Override
            public void buttonClick(Button.ClickEvent event) {
                goToAddGoods();
            }
        });

        Button doDeleteGoodsButton = new Button("Удалить товар", new Button.ClickListener() {
            @Override
            public void buttonClick(Button.ClickEvent event) {

                Iterator it = documentGoodsItems.iterator();
                DocumentGoods doc = null;
                while (it.hasNext()) {
                    doc = (DocumentGoods) it.next();
                    if (doc.equals(selectedItem)) {
                        it.remove();

                    }
                }
                billGrid.removeAllComponents();
                BillGoodsGrid billGoods = new BillGoodsGrid(documentGoodsItems);
                billGoods.addItemClickListener(new ItemClickListener<DocumentGoods>() {
                    @Override
                    public void itemClick(Grid.ItemClick<DocumentGoods> event) {
                        DocumentGoods item = event.getItem();
                        goToEditDocumentGood(item);
                    }
                });
                billGrid.addComponent(billGoods);
                billSumm = billGoods.getValue();
            }
        });

        isEditList = new CheckBox("Редактировать список товаров");

        goodsListHeader.addComponents(doAddGoodsButton, doDeleteGoodsButton, isEditList);
        goodsListHeader.setComponentAlignment(doAddGoodsButton, Alignment.MIDDLE_CENTER);
        goodsListHeader.setComponentAlignment(doDeleteGoodsButton, Alignment.MIDDLE_CENTER);

        goodsListHeader.setComponentAlignment(isEditList, Alignment.MIDDLE_CENTER);
        goodsListLayout.addComponent(goodsListHeader);

        goodsList.setContent(goodsListLayout);
        tabView.addComponent(goodsList);

        billGoods.addItemClickListener(new ItemClickListener<DocumentGoods>() {
            @Override
            public void itemClick(Grid.ItemClick<DocumentGoods> event) {

                DocumentGoods item = event.getItem();
                if (isEditList.getValue() == false) {
                    goToEditDocumentGood(item);
                } else {
                    selectedItem = event.getItem();
                }

            }
        });

        billGrid = new VerticalLayout();
        billGrid.addComponent(billGoods);
        billGrid.setSizeFull();
        billGrid.setMargin(false);
        billGrid.setSpacing(false);
        billGoods.setSizeFull();
        billGoods.setHeight(13.1f, Unit.CM);
        goodsListLayout.addComponent(billGrid);

        tabSheet.addTab(header, "Шапка");
        tabSheet.addTab(tabView, "Табличная часть");
        tabSheet.setHeight(99.0f, Unit.PERCENTAGE);

        tabSheet.addSelectedTabChangeListener(new TabSheet.SelectedTabChangeListener() {
            @Override
            public void selectedTabChange(TabSheet.SelectedTabChangeEvent event) {
                if (event.getTabSheet().getSelectedTab().equals(header)) {
                    billSumm = billGoods.getValue();
                    docTotal.setValue(String.valueOf(billSumm));

                }
            }
        });

        Panel tabPanel = new Panel("Данные документа", tabSheet);
        tabPanel.setHeight(19.0f, Unit.CM);
        tabPanel.setWidth(32.0f, Unit.CM);
        layout.addComponent(mainMenu, 0, 0, 2, 0);
        layout.addComponent(userData, 0, 1, 0, 1);
        layout.addComponent(tabPanel, 1, 1, 1, 2);

        layout.setColumnExpandRatio(0, 0.0f);
        layout.setColumnExpandRatio(1, 0.0f);
        layout.setColumnExpandRatio(2, 0.0f);
        layout.setRowExpandRatio(0, 0.0f);
        layout.setRowExpandRatio(1, 0.0f);
        layout.setRowExpandRatio(2, 0.0f);
        layout.setSpacing(false);
        layout.setMargin(false);

        VerticalLayout fullLayout = new VerticalLayout();
        fullLayout.addComponent(layout);
        fullLayout.setComponentAlignment(layout, Alignment.MIDDLE_CENTER);
        fullLayout.setMargin(false);
        fullLayout.setSpacing(false);

        getUI().setContent(fullLayout);

    }

    private void goToAddGoods() {
        Window addGoodsWindow = new Window("Выбор товаров");
        VerticalLayout addGoodsWindowLayout = new VerticalLayout();
        AllGoodsGrid allGoodsGrid = new AllGoodsGrid(allGoods, allContragents, allGoodsTypes);
        addGoodsWindowLayout.addComponent(allGoodsGrid);
        allGoodsGrid.setHeight(16.0f, Unit.CM);
        HorizontalLayout buttons = new HorizontalLayout();
        Button doAddGoodsButton = new Button("Добавить выбранные", new Button.ClickListener() {
            @Override
            public void buttonClick(Button.ClickEvent event) {
                selectedGoods = allGoodsGrid.getSelectedGoods();
                doAddGoods();
                addGoodsWindow.close();
            }
        });

        Button doCancellButton = new Button("Отмена", new Button.ClickListener() {
            @Override
            public void buttonClick(Button.ClickEvent event) {
                selectedGoods.clear();
                addGoodsWindow.close();

            }
        });

        buttons.addComponents(doAddGoodsButton, doCancellButton);
        buttons.setComponentAlignment(doAddGoodsButton, Alignment.TOP_CENTER);
        buttons.setComponentAlignment(doCancellButton, Alignment.TOP_CENTER);
        buttons.setSizeFull();
        addGoodsWindowLayout.addComponent(buttons);
        addGoodsWindow.setContent(addGoodsWindowLayout);
        addGoodsWindow.center();
        addGoodsWindow.setModal(true);
        addGoodsWindow.setHeight(19.0f, Unit.CM);
        addGoodsWindow.setWidth(35.0f, Unit.CM);
        MyUI.getCurrent().addWindow(addGoodsWindow);
    }

    private void doAddGoods() {

        for (Goods selectedGood : selectedGoods) {
            DocumentGoods docGood = new DocumentGoods();
            List<Prices> goodPrices = stockHelper.getPricesByGoodId(selectedGood.getId());
            for (Prices goodPrice : goodPrices) {
                if (goodPrice.getPriceType().getId() == 1) {
                    docGood.setPrice(goodPrice.getResult());
                }
            }
            docGood.setGoods(selectedGood);
            docGood.setQuantity(0.0);
            docGood.setTotalValue(0.0);
            documentGoodsItems.add(docGood);
        }
        billGrid.removeAllComponents();
        BillGoodsGrid billGoods = new BillGoodsGrid(documentGoodsItems);
        billGoods.addItemClickListener(new ItemClickListener<DocumentGoods>() {
            @Override
            public void itemClick(Grid.ItemClick<DocumentGoods> event) {
                DocumentGoods item = event.getItem();
                if (isEditList.getValue() == false) {
                    goToEditDocumentGood(item);
                } else {
                    selectedItem = event.getItem();
                }
            }
        });
        billGrid.addComponent(billGoods);
        selectedGoods.clear();
        this.billSumm = billGoods.getValue();
    }

    private void goToEditDocumentGood(DocumentGoods item) {
        Window editDocumentGoodWindow = new Window();
        VerticalLayout editDocumentGoodLayout = new VerticalLayout();

        selectedItem = item;

        Label itemName = new Label(item.getGoods().getName());
        GoodPricesController pricesController = new GoodPricesController(item.getGoods().getId());
        Button editItemPrices = new Button("Редактировать цены", new Button.ClickListener() {
            @Override
            public void buttonClick(Button.ClickEvent event) {

                Window pr = pricesController.getPriceListWindow();
                pr.center();
                pr.setModal(true);
                MyUI.getCurrent().addWindow(pr);

            }
        });
        HashMap<String, Prices> pricesMap = new HashMap<>();
        List<Prices> pricesList = stockHelper.getPricesByGoodId(item.getGoods().getId());
        for (Prices prices : pricesList) {
            pricesMap.put(prices.getPriceType().getName(), prices);
        }

        TextField itemQuantity = new TextField("Количество", Double.toString(item.getQuantity()));
        itemQuantity.setEnabled(false);
        TextField itemTotalValue = new TextField("Стоимость", Double.toString(item.getTotalValue()));
        itemTotalValue.setEnabled(false);

        ComboBox itemSelectPrice = new ComboBox("Тип цены", pricesMap.keySet());
        itemSelectPrice.setEmptySelectionAllowed(false);
        itemSelectPrice.addValueChangeListener(new HasValue.ValueChangeListener() {
            @Override
            public void valueChange(HasValue.ValueChangeEvent event) {
                String value = (String) event.getValue();
                Prices p = pricesMap.get(value);
                item.setPrice(p.getResult());
                itemQuantity.setEnabled(true);
                itemTotalValue.setEnabled(true);
            }
        });

        itemQuantity.addValueChangeListener(new HasValue.ValueChangeListener<String>() {
            @Override
            public void valueChange(HasValue.ValueChangeEvent<String> event) {
                itemQuantity.setValue(itemQuantity.getValue().replace(",", "."));
                if (!event.getValue().isEmpty()) {
                    Boolean isUseBasePackage = item.getGoods().getUseBasePackage();
                    Double remainder = item.getGoods().getRemainder();
                    Double requestedQuantity = 0.0;
                    String value = event.getValue();
//                    value = value.replace("/[^0-9]/", "");
                    value = value.replace(",", ".");
                    itemQuantity.setValue(value);
                    try {
                        requestedQuantity = Double.parseDouble(value);
                    } catch (NumberFormatException e) {

                    }
                    if (!isUseBasePackage) {
                        requestedQuantity *= item.getGoods().getMainFactor();
                    }
                    if (requestedQuantity > remainder) {

                        Notification tooMuch = new Notification("<center>Недостаточно товара или он был зарезервирован <br/>"
                                + "Установлено максимально допустимое количество товара</center>",
                                Notification.Type.WARNING_MESSAGE);
                        tooMuch.setHtmlContentAllowed(true);
                        tooMuch.setDelayMsec(10000);
                        tooMuch.show(Page.getCurrent());
                        item.setQuantity(remainder);
                        if (!isUseBasePackage) {
                            itemQuantity.setValue(String.valueOf(remainder / item.getGoods().getMainFactor()));
                        } else {
                            itemQuantity.setValue(String.valueOf(remainder));
                        }
                    } else {
                        item.setQuantity(Double.parseDouble(value));
                    }
                    Double tmpValue = item.getQuantity() * item.getPrice();
                    if (!isUseBasePackage) {
                        tmpValue *= item.getGoods().getMainFactor();
                    }
                    itemTotalValue.setValue(String.valueOf(round(tmpValue, 2)));
                }
            }
        });

        Button save = new Button("Сохранить", new Button.ClickListener() {
            @Override
            public void buttonClick(Button.ClickEvent event) {
                item.setQuantity(Double.parseDouble(itemQuantity.getValue().replace(",", ".")));
                item.setTotalValue(Double.parseDouble(itemTotalValue.getValue().replace(",", ".")));

                billGrid.removeAllComponents();
                BillGoodsGrid billGoods = new BillGoodsGrid(documentGoodsItems);
                billGoods.addItemClickListener(new ItemClickListener<DocumentGoods>() {
                    @Override
                    public void itemClick(Grid.ItemClick<DocumentGoods> event) {
                        DocumentGoods item = event.getItem();
                        if (isEditList.getValue() == false) {
                            goToEditDocumentGood(item);
                        } else {
                            selectedItem = event.getItem();
                        }
                        billSumm = billGoods.getValue();
                    }
                });
                GoodPricesController pc = new GoodPricesController(item.getGoods().getId());
                for (DocumentGoods documentGoodsItem : documentGoodsItems) {
                    if (documentGoodsItem.equals(item)) {
                        documentGoodsItem.setPrice(pc.getPurchasePrice());
                    }
                }
                ListDataProvider<DocumentGoods> prov = (ListDataProvider<DocumentGoods>) billGoods.getDataProvider();
                prov.refreshAll();
                billGrid.addComponent(billGoods);
                selectedGoods.clear();

                editDocumentGoodWindow.close();
            }
        });

        editDocumentGoodLayout.addComponents(itemName, editItemPrices, itemSelectPrice, itemQuantity, itemTotalValue, save);
        editDocumentGoodWindow.setContent(editDocumentGoodLayout);
        editDocumentGoodWindow.center();
        editDocumentGoodWindow.setModal(true);
        MyUI.getCurrent().addWindow(editDocumentGoodWindow);

    }

    private void deleteDocumentGoods(List<DocumentGoods> allDocumentGoods, List<DocumentGoods> selectedDocumentGoods) {
        allDocumentGoods.removeAll(selectedDocumentGoods);
    }

    private void doSaveDocument(List<DocumentGoods> goodsList, Boolean isCarried) {

        int documentId = stockHelper.getMaxBillId();
        if (documentId == 0) {
            documentId += 1;
        }
        Documents document = new Documents();
        billAuthor = (Users) VaadinService.getCurrentRequest().getWrappedSession().getAttribute("user");
        billIsCarried = isCarried;
        document.setChangeAmount(billChangeSize);
        document.setContragents(billProvider);
        document.setCurrency("руб.");
        document.setDate(billDate);
        document.setFirm(billFirm);
        document.setIsCarried(billIsCarried);
        document.setIsFullyPaid(billIsFullyPaid);
        document.setNumber(billNumber);
        document.setPaidAmount(billPaymentSize);
        document.setSumm(billSumm);
        document.setTime(billTime);
        document.setType(2);        //тип расходного документа - 2
        document.setUsers(billAuthor);
        document.setWarehouse(billWarehouse);
        document.setPayUntil(billPayUntil);
        HashSet<DocumentGoods> tmp = new HashSet<>();
        for (DocumentGoods documentGoods : goodsList) {
            documentGoods.setDocuments(document);
            tmp.add(documentGoods);
        }
        document.setDocumentGoodses(tmp);
        stockHelper.addDBItem(document);

        for (DocumentGoods documentGoods : goodsList) {
            stockHelper.addDBItem(documentGoods);
            Double quantity = documentGoods.getQuantity();
            Double initialQuantity = documentGoods.getGoods().getRemainder();
            Double factor = documentGoods.getGoods().getMainFactor();
            Boolean isInBasePackage = documentGoods.getGoods().getUseBasePackage();
            if (isInBasePackage) {
                documentGoods.getGoods().setRemainder(initialQuantity - quantity);
            } else {
                documentGoods.getGoods().setRemainder(initialQuantity - quantity * factor);
            }
            stockHelper.updateDBItem(documentGoods.getGoods());
        }
        billNumber = 0;

        Page.getCurrent().reload();

    }

    private void doCheckoutDocument(List<DocumentGoods> goodsList, Boolean isCarried) {
        int documentId = stockHelper.getMaxBillId();
        if (documentId == 0) {
            documentId += 1;
        }
        Documents document = new Documents();
        billAuthor = (Users) VaadinService.getCurrentRequest().getWrappedSession().getAttribute("user");
        billIsCarried = isCarried;
        document.setChangeAmount(billChangeSize);
        document.setContragents(billProvider);
        document.setCurrency("руб.");
        document.setDate(billDate);
        document.setFirm(billFirm);
        document.setIsCarried(billIsCarried);
        document.setIsFullyPaid(billIsFullyPaid);
        document.setNumber(billNumber);
        document.setPaidAmount(billPaymentSize);
        document.setSumm(billSumm);
        document.setTime(billTime);
        document.setType(2);        //тип расходного документа - 2
        document.setUsers(billAuthor);
        document.setWarehouse(billWarehouse);
        document.setPayUntil(billPayUntil);
        HashSet<DocumentGoods> tmp = new HashSet<>();
        for (DocumentGoods documentGoods : goodsList) {
            documentGoods.setDocuments(document);
            tmp.add(documentGoods);
        }
        document.setDocumentGoodses(tmp);
        stockHelper.addDBItem(document);

        for (DocumentGoods documentGoods : goodsList) {
            stockHelper.addDBItem(documentGoods);
            Double quantity = documentGoods.getQuantity();
            Double initialQuantity = documentGoods.getGoods().getFactRemainder();
            Double initialRemainder = documentGoods.getGoods().getRemainder();
            Double factor = documentGoods.getGoods().getMainFactor();
            Boolean isInBasePackage = documentGoods.getGoods().getUseBasePackage();
            if (isInBasePackage) {
                documentGoods.getGoods().setFactRemainder(initialQuantity - quantity);
                documentGoods.getGoods().setRemainder(initialRemainder - quantity);
            } else {
                documentGoods.getGoods().setFactRemainder(initialQuantity - quantity * factor);
                documentGoods.getGoods().setRemainder(initialRemainder - quantity * factor);
            }
            stockHelper.updateDBItem(documentGoods.getGoods());
        }
        billNumber = 0;

        Contragents buyer = document.getContragents();
        if (!billIsFullyPaid) {
            Double credit = billSumm - billPaymentSize;
            buyer.setCredit(buyer.getCredit() + credit);
            stockHelper.updateDBItem(buyer);
        }
        goToPrintBillButton.setEnabled(true);
//        Page.getCurrent().reload();
    }

    private double round(double number, int scale) {
        int pow = 10;
        for (int i = 1; i < scale; i++) {
            pow *= 10;
        }
        double tmp = number * pow;
        return (double) (int) ((tmp - (int) tmp) >= 0.5 ? tmp + 1 : tmp) / pow;
    }

    public static class MyPopupUI extends UI {

        private StockHelper stockHelper = new StockHelper();
        private int docId = stockHelper.getMaxBillId();

        private Documents bill = stockHelper.getDocumentById(docId);
        private List<DocumentGoods> billGoods = stockHelper.getBillGoods(docId);

        @Override
        public void init(VaadinRequest request) {
            getPage().setTitle("Печать накладной");
            SimpleDateFormat format = new SimpleDateFormat("dd.MM.yyyy");
            // Have some content for it
            VerticalLayout content = new VerticalLayout();
            String data = "<!DOCTYPE html>\n"
                    + "<html>\n"
                    + "    <head></head>\n"
                    + "<style type=\"text/css\">\n"
                    + "@page {\n"
                    + "margin: 0cm;\n"
                    + "overflow: visible\n"
                    + "}\n"
                    + "tr{page-break-inside: avoid;}"
                    + "</style>\n"
                    + "    <body>\n"
                    + "<font face=\"Arial\">"
                    + "        <h2>Накладная № " + String.valueOf(bill.getNumber()) + " от " + format.format(bill.getDate()) + "</h2>\n"
                    + "        <hr>\n"
                    + "        <table border=\"0\" cellpadding=\"1\" cellspacing=\"1\">\n"
                    + "	        <tbody>\n"
                    + "		        <tr>\n"
                    + "			        <td>Поставщик: </td>\n"
                    + "			        <td><b>И.П. Валиахметова Л.Н. 452607, Башкортостан Респ, г.Октябрьский, Северная ул, д.21, кор.13</b></td>                    \n"
                    + "		        </tr>\n"
                    + "                <tr>\n"
                    + "                    <td>&nbsp;</td>\n"
                    + "                </tr>\n"
                    + "                <tr>\n"
                    + "                    <td>Покупатель: </td>\n"
                    + "			        <td><b>" + bill.getContragents().getOfficialName() + " " + bill.getContragents().getPhones() + "</b></td>\n"
                    + "                </tr>\n"
                    + "                <tr>\n"
                    + "                    <td>&nbsp;</td>\n"
                    + "                </tr>\n"
                    + "                <tr>\n"
                    + "                    <td>Склад: </td>\n"
                    + "			        <td><b>" + bill.getWarehouse() + "</b></td>\n"
                    + "                </tr>                \n"
                    + "	        </tbody>\n"
                    + "        </table>\n"
                    + "<font size = \"2\">\n"
                    + "<table border=\"1\" cellpadding=\"1\" cellspacing=\"0\" style=\"width: 19cm;\">\n"
                    + "	<tbody>\n"
                    + "		<tr>\n"
                    + "			<td rowspan=\"2\">№</td>\n"
                    + "			<td rowspan=\"2\" width=\"30%\">Товар</td>\n"
                    + "			<td rowspan=\"2\">Кол-во</td>\n"
                    + "			<td rowspan=\"2\">Ед.</td>\n"
                    + "			<td rowspan=\"2\">Цена</td>\n"
                    + "			<td rowspan=\"2\">Цена со скидкой</td>\n"
                    + "			<td rowspan=\"2\">Фасовка</td>\n"
                    + "			<td rowspan=\"2\">Сумма без скидки</td>\n"
                    + "			<td colspan=\"2\">Скидка</td>\n"
                    + "			<td rowspan=\"2\">Сумма со скидкой</td>\n"
                    + "			<td rowspan=\"2\">Остаток</td>\n"
                    + "		</tr>\n"
                    + "		<tr>\n"
                    + "			<td>Сумма</td>\n"
                    + "			<td>&nbsp;%</td>\n"
                    + "		</tr>\n";

            for (int i = 0; i < billGoods.size(); i++) {
                DocumentGoods tmpGood = billGoods.get(i);
                String number = String.valueOf(i + 1);
                String name = tmpGood.getGoods().getName();
                String quantity = String.valueOf(tmpGood.getQuantity());
                String packageName;
                if (tmpGood.getGoods().getUseBasePackage()) {
                    packageName = tmpGood.getGoods().getPackagesByBasePackageId().getPackageName();
                } else {
                    packageName = tmpGood.getGoods().getPackagesByMainPackageId().getPackageName();
                }
                String price = String.valueOf(tmpGood.getPrice());
                String priceWDiscount = String.valueOf(tmpGood.getPrice()
                        - tmpGood.getPrice() * bill.getContragents().getDiscount() / 100.0);
                String packageSize = String.valueOf(tmpGood.getGoods().getMainFactor())
                        + tmpGood.getGoods().getPackagesByBasePackageId().getPackageName();
                String summWODiscount = String.valueOf(tmpGood.getTotalValue());
                String discountSize = String.valueOf(tmpGood.getTotalValue() * bill.getContragents().getDiscount() / 100.0);
                String discountPercentage = String.valueOf(bill.getContragents().getDiscount()) + " %";
                String summWDiscount = String.valueOf(tmpGood.getTotalValue()
                        - tmpGood.getTotalValue() * bill.getContragents().getDiscount() / 100.0);
                String factRemainder = String.valueOf(tmpGood.getGoods().getFactRemainder());

                data = data + "<tr style=\"page-break-before: always;\">"
                        + "			<td>" + number + "</td>\n"
                        + "			<td>" + name + "</td>\n"
                        + "			<td>" + quantity + "</td>\n"
                        + "			<td>" + packageName + "</td>\n"
                        + "			<td>" + price + "</td>\n"
                        + "			<td>" + priceWDiscount + "</td>\n"
                        + "			<td>" + packageSize + "</td>\n"
                        + "			<td>" + summWODiscount + "</td>\n"
                        + "			<td>" + discountSize + "</td>\n"
                        + "			<td>" + discountPercentage + "</td>\n"
                        + "			<td>" + summWDiscount + "</td>\n"
                        + "			<td>" + factRemainder + "</td>\n"
                        + "		</tr>\n";

                if (i % 16 == 0 & i > 0 & i < 17) {
                    data = data + "<tr><td>&nbsp;</td></tr>";
                } else if ((i - 16) % 24 == 0 & i > 0 & i > 25) {
                    data = data + "<tr><td>&nbsp;</td></tr>";
                }
            }
            Double summ = bill.getSumm();
            data = data
                    + "	</tbody>\n"
                    + "</table>\n"
                    + "</font>\n"
                    + "        <br/>\n"
                    + "<div align = \"right\"> Итого: " + String.valueOf(summ) + "</div>"
                    + " <br/>\n";

            Double totalPlaces = 0.0;
            Double totalWeight = 0.0;

            for (DocumentGoods billGood : billGoods) {
                totalPlaces += billGood.getQuantity();
                if (billGood.getGoods().getUseBasePackage()) {
                    totalWeight += billGood.getGoods().getBasePackageMass() * billGood.getQuantity();
                } else {
                    totalWeight += billGood.getGoods().getMainPackageMass() * billGood.getQuantity();
                }
            }

            int positionNumber = billGoods.size();

            data = data + "<b>Количество мест = " + String.valueOf(totalPlaces)
                    + ", " + "общий вес = " + String.valueOf(totalWeight)
                    + " кг. <br/><br/>";
            data = data + "Всего наименований " + String.valueOf(positionNumber)
                    + ", на сумму " + String.valueOf(summ) + " руб. <br/><br/>";
            String summString = RussianMoney.digits2text(summ);
            summString = summString.substring(0, 1).toUpperCase() + summString.substring(1);

            data = data + summString + "<br/><hr>";
            data = data + "Отпустил <hr>";
            data = data
                    + "</font>"
                    + "    </body>\n"
                    + "</html>";
            Label label
                    = new Label(data);

            label.setContentMode(ContentMode.HTML);
            label.setSizeFull();

            Button print = new Button("Печать");
            print.addClickListener(new Button.ClickListener() {
                @Override
                public void buttonClick(Button.ClickEvent event) {
                    print.setVisible(false);
                    Page.getCurrent().getJavaScript().execute("document.body.style.overflow = \"auto\";"
                            + "document.body.style.height  = \"auto\"");
                    UI.getCurrent().setSizeUndefined();

                    Page.getCurrent().getJavaScript().execute("window.print();");
                }
            });
            content.addComponent(print);
            content.addComponent(label);
            content.setComponentAlignment(print, Alignment.TOP_RIGHT);
            content.setExpandRatio(print, 0.0f);
            content.setExpandRatio(label, 0.0f);
            this.setSizeUndefined();
            setContent(content);
        }
    }

}
