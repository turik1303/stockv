/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ru.funkdevelop.stockv.views.documents;

import com.vaadin.data.HasValue;
import com.vaadin.data.provider.ListDataProvider;
import com.vaadin.event.selection.SelectionEvent;
import com.vaadin.event.selection.SelectionListener;
import com.vaadin.navigator.Navigator;
import com.vaadin.navigator.View;
import com.vaadin.navigator.ViewChangeListener;
import com.vaadin.server.Page;
import com.vaadin.server.VaadinService;
import com.vaadin.ui.Alignment;
import com.vaadin.ui.Button;
import com.vaadin.ui.CheckBox;
import com.vaadin.ui.DateField;
import com.vaadin.ui.Grid;
import com.vaadin.ui.GridLayout;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Label;
import com.vaadin.ui.Notification;
import com.vaadin.ui.Panel;
import com.vaadin.ui.TextField;
import com.vaadin.ui.UI;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.Window;
import com.vaadin.ui.components.grid.ItemClickListener;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import ru.funkdevelop.stockv.MyUI;
import ru.funkdevelop.stockv.StockHelper;
import ru.funkdevelop.stockv.pojos.Contragents;
import ru.funkdevelop.stockv.pojos.DocumentGoods;
import ru.funkdevelop.stockv.pojos.Documents;
import ru.funkdevelop.stockv.pojos.Goods;
import ru.funkdevelop.stockv.pojos.GoodsType;
import ru.funkdevelop.stockv.pojos.Prices;
import ru.funkdevelop.stockv.pojos.Users;
import ru.funkdevelop.stockv.views.service.AllGoodsGrid;
import ru.funkdevelop.stockv.views.service.BillGoodsGrid;
import ru.funkdevelop.stockv.views.service.GoodPricesController;
import ru.funkdevelop.stockv.views.service.MainMenu;
import ru.funkdevelop.stockv.views.service.UserData;

/**
 *
 * @author artur
 */
public class WritingOffView extends GridLayout implements View {
StockHelper stockHelper;
    MainMenu mainMenu;
    UserData userData;
    Navigator navigator;
    
    Integer docNum;
            
    BillGoodsGrid billGoods;
    List<Goods> selectedGoods;
    Set<DocumentGoods> selected = new HashSet<>();
    List<DocumentGoods> documentGoodsItems;
    
    VerticalLayout billGrid;
    
    CheckBox isEditList;
    
    List<Contragents> allContragents;
    List<Goods> allGoods;
    List<GoodsType> allGoodsTypes;
    
    DocumentGoods selectedItem;
    
    private Double billSumm;
    
    private Documents document;
    
    @Override
    public void enter(ViewChangeListener.ViewChangeEvent event) {
        stockHelper = new StockHelper();
        navigator = UI.getCurrent().getNavigator();
        userData = new UserData();
        mainMenu = new MainMenu(navigator);
        docNum = stockHelper.getMaxBillId() + 1;
        documentGoodsItems = new ArrayList<>();
        
        allContragents = stockHelper.getContragents();
        allGoods = stockHelper.getAllGoods();
        allGoodsTypes = stockHelper.getGoodsTypes();
        VerticalLayout fullLayout = new VerticalLayout();

        GridLayout layout = new GridLayout(3, 3);
        
        VerticalLayout body = new VerticalLayout();
        Panel docHeader = new Panel("Шапка");
        VerticalLayout docHeaderLayout = new VerticalLayout();
        HorizontalLayout line1 = new HorizontalLayout();
        
        TextField docType = new TextField("Тип документа", "Списание ТМЦ");
        docType.setEnabled(false);
        docType.setWidth(10.0f, Unit.CM);
        
        TextField docNumber = new TextField("Номер", String.valueOf(docNum));
        docNumber.setEnabled(false);
        docNumber.setWidth(10.0f, Unit.CM);
        
        DateField docDate = new DateField("Дата", LocalDate.now());
        docDate.setWidth(10.0f, Unit.CM);
        docDate.setDateFormat("dd.MM.yyyy");
        line1.addComponents(docType, docNumber, docDate);
        docHeaderLayout.addComponent(line1);
        docHeaderLayout.addComponent(new Label("Организация: И.П. Валиахметова Л.Н."));
        docHeaderLayout.addComponent(new Label("Склад: Основной склад"));
        docHeaderLayout.setSizeFull();
        docHeader.setContent(docHeaderLayout);
        
        Panel goodsControl = new Panel("Управление");
        HorizontalLayout goodsControlLayout = new HorizontalLayout();
        
        Button addGoodsButton = new Button("Добавить товары", new Button.ClickListener() {
            @Override
            public void buttonClick(Button.ClickEvent event) {
                goToAddGoods();
            }
        });
        addGoodsButton.setWidth(6.0f, Unit.CM);
        
        Button deleteGoodsButton = new Button("Удалить товары", new Button.ClickListener() {
            @Override
            public void buttonClick(Button.ClickEvent event) {
                deleteSelectedGoods();
            }
        });
        deleteGoodsButton.setWidth(6.0f, Unit.CM);
        
        Button saveBillButton = new Button("Сохранить документ", new Button.ClickListener() {
            @Override
            public void buttonClick(Button.ClickEvent event) {
                document = new Documents();
                document.setType(4);
                document.setChangeAmount(0.0);
                Contragents contr = new Contragents();
                for (Contragents allContragent : allContragents) {
                    if (allContragent.getId() == 6)
                        contr = allContragent;
                }
                document.setContragents(contr);
                document.setCurrency("руб.");
                Date date = new Date();
                Calendar c = new GregorianCalendar();
                int year = docDate.getValue().getYear();
                int month = docDate.getValue().getMonthValue();
                int day = docDate.getValue().getDayOfMonth();
                c.set(year, month - 1, day);
                date.setTime(c.getTimeInMillis());
                document.setDate(date);
                Date time = new Date();
                document.setTime(time);
                Set<DocumentGoods> tmp = new HashSet<>();
                tmp.addAll(documentGoodsItems);
                document.setDocumentGoodses(tmp);
                document.setFirm("И.П. Валиахметова Л.Н.");
                document.setIsCarried(false);
                document.setIsFullyPaid(true);
                document.setNumber(Integer.parseInt(docNumber.getValue()));
                document.setPaidAmount(0.0);
                document.setPayUntil(new Date());
                document.setSumm(0.0);
                document.setWarehouse("Основной склад");                
                document.setUsers((Users) VaadinService.getCurrentRequest().getWrappedSession().getAttribute("user"));
                doSaveBill();
            }
        });
        saveBillButton.setWidth(6.0f, Unit.CM);
        
        Button carryBillButton = new Button("Провести документ", new Button.ClickListener() {
            @Override
            public void buttonClick(Button.ClickEvent event) {
                document = new Documents();
                document.setType(4);
                document.setChangeAmount(0.0);
                Contragents contr = new Contragents();
                for (Contragents allContragent : allContragents) {
                    if (allContragent.getId() == 6)
                        contr = allContragent;
                }
                document.setContragents(contr);
                document.setCurrency("руб.");
                Date date = new Date();
                Calendar c = new GregorianCalendar();
                int year = docDate.getValue().getYear();
                int month = docDate.getValue().getMonthValue();
                int day = docDate.getValue().getDayOfMonth();
                c.set(year, month - 1, day);
                date.setTime(c.getTimeInMillis());
                document.setDate(date);
                Date time = new Date();
                document.setTime(time);
                Set<DocumentGoods> tmp = new HashSet<>();
                tmp.addAll(documentGoodsItems);
                document.setDocumentGoodses(tmp);
                document.setFirm("И.П. Валиахметова Л.Н.");
                document.setIsCarried(true);
                document.setIsFullyPaid(true);
                document.setNumber(Integer.parseInt(docNumber.getValue()));
                document.setPaidAmount(0.0);
                document.setPayUntil(new Date());
                document.setSumm(0.0);
                document.setWarehouse("Основной склад");                
                document.setUsers((Users) VaadinService.getCurrentRequest().getWrappedSession().getAttribute("user"));
                doCarryBill();
            }
        });
        carryBillButton.setWidth(6.0f, Unit.CM);
        
        isEditList = new CheckBox("Редактировать список");
        goodsControlLayout.addComponents(addGoodsButton, deleteGoodsButton, saveBillButton, carryBillButton, isEditList);
        goodsControlLayout.setComponentAlignment(isEditList, Alignment.MIDDLE_CENTER);
        goodsControlLayout.setSizeFull();
        goodsControlLayout.setMargin(true);
        goodsControl.setContent(goodsControlLayout);
        
        Panel goodsList = new Panel("Список товаров");
        billGrid = new VerticalLayout();
        billGoods = new BillGoodsGrid(documentGoodsItems);
        billGoods.setSelectionMode(Grid.SelectionMode.MULTI);
        billGoods.addItemClickListener(new ItemClickListener<DocumentGoods>() {
            @Override
            public void itemClick(Grid.ItemClick<DocumentGoods> event) {
                DocumentGoods item = event.getItem();
                if (isEditList.getValue() == false) {
                    goToEditDocumentGood(item);
                } else {
                    selectedItem = event.getItem();
                }
            }
        });
        billGoods.addSelectionListener(new SelectionListener<DocumentGoods>() {
            @Override
            public void selectionChange(SelectionEvent<DocumentGoods> event) {
                selected = event.getAllSelectedItems();
            }
        });
        billGoods.setSizeFull();
        billGrid.addComponent(billGoods);
        goodsList.setContent(billGrid);
        
        body.addComponent(docHeader);
        body.addComponent(goodsControl);
        body.addComponent(goodsList);
        
        
//        body.setSizeFull();
        body.setWidth(32.0f, Unit.CM);
//        body.setHeight(19.0f, Unit.CM);
        body.setSpacing(false);
        body.setMargin(false);
        layout.addComponent(mainMenu, 0, 0, 2, 0);
        layout.addComponent(userData, 0, 1, 0, 1);
        layout.addComponent(body, 1, 1, 2, 1);
        layout.setColumnExpandRatio(0, 0.0f);
        layout.setColumnExpandRatio(1, 0.0f);
        layout.setColumnExpandRatio(2, 0.0f);
        layout.setRowExpandRatio(0, 0.0f);
        layout.setRowExpandRatio(1, 0.0f);
        layout.setRowExpandRatio(2, 0.0f);
        layout.setSpacing(false);
        layout.setMargin(false);
        fullLayout.addComponent(layout);
        fullLayout.setComponentAlignment(layout, Alignment.MIDDLE_CENTER);

        fullLayout.setMargin(false);
        fullLayout.setSpacing(false);

        getUI().setContent(fullLayout);
    }
    
    private void goToAddGoods() {
        Window addGoodsWindow = new Window("Выбор товаров");
        VerticalLayout addGoodsWindowLayout = new VerticalLayout();
        AllGoodsGrid allGoodsGrid = new AllGoodsGrid(allGoods, allContragents, allGoodsTypes);
        addGoodsWindowLayout.addComponent(allGoodsGrid);
        allGoodsGrid.setHeight(16.0f, Unit.CM);
        HorizontalLayout buttons = new HorizontalLayout();
        Button doAddGoodsButton = new Button("Добавить выбранные", new Button.ClickListener() {
            @Override
            public void buttonClick(Button.ClickEvent event) {
                selectedGoods = allGoodsGrid.getSelectedGoods();
                doAddGoods();
                addGoodsWindow.close();
            }
        });

        Button doCancellButton = new Button("Отмена", new Button.ClickListener() {
            @Override
            public void buttonClick(Button.ClickEvent event) {
                selectedGoods.clear();
                addGoodsWindow.close();

            }
        });

        buttons.addComponents(doAddGoodsButton, doCancellButton);
        buttons.setComponentAlignment(doAddGoodsButton, Alignment.TOP_CENTER);
        buttons.setComponentAlignment(doCancellButton, Alignment.TOP_CENTER);
        buttons.setSizeFull();
        addGoodsWindowLayout.addComponent(buttons);
        addGoodsWindow.setContent(addGoodsWindowLayout);
        addGoodsWindow.center();
        addGoodsWindow.setModal(true);
        addGoodsWindow.setHeight(19.0f, Unit.CM);
        addGoodsWindow.setWidth(35.0f, Unit.CM);
        MyUI.getCurrent().addWindow(addGoodsWindow);
    }

    private void doAddGoods() {

        for (Goods selectedGood : selectedGoods) {
            DocumentGoods docGood = new DocumentGoods();
            List<Prices> goodPrices = stockHelper.getPricesByGoodId(selectedGood.getId());
            for (Prices goodPrice : goodPrices) {
                if (goodPrice.getPriceType().getId() == 1) {
                    docGood.setPrice(goodPrice.getResult());
                }
            }
            docGood.setGoods(selectedGood);
            docGood.setQuantity(0.0);
            docGood.setTotalValue(0.0);
            documentGoodsItems.add(docGood);
        }
        refreshBillGrid();
        selectedGoods.clear();
        
    }
    
    private void goToEditDocumentGood(DocumentGoods item) {
        Window editDocumentGoodWindow = new Window();
        VerticalLayout editDocumentGoodLayout = new VerticalLayout();

        selectedItem = item;

        Label itemName = new Label(item.getGoods().getName());
        
        
        TextField itemQuantity = new TextField("Количество", Double.toString(item.getQuantity()));
        itemQuantity.addValueChangeListener(new HasValue.ValueChangeListener<String>() {
            @Override
            public void valueChange(HasValue.ValueChangeEvent<String> event) {
                Boolean isUseBasePackage = item.getGoods().getUseBasePackage();
                Double remainder = item.getGoods().getRemainder();
                Double requestedQuantity = Double.parseDouble(event.getValue());
                if (!isUseBasePackage) {
                    requestedQuantity *= item.getGoods().getMainFactor();
                }
                if (requestedQuantity > remainder) {

                    Notification tooMuch = new Notification("<center>Недостаточно товара или он был зарезервирован <br/>"
                            + "Установлено максимально допустимое количество товара</center>",
                            Notification.Type.WARNING_MESSAGE);
                    tooMuch.setHtmlContentAllowed(true);
                    tooMuch.setDelayMsec(10000);
                    tooMuch.show(Page.getCurrent());
                    item.setQuantity(remainder);
                    if (!isUseBasePackage) {
                        itemQuantity.setValue(String.valueOf(remainder / item.getGoods().getMainFactor()));
                    } else {
                        itemQuantity.setValue(String.valueOf(remainder));
                    }
                } else {
                    item.setQuantity(Double.parseDouble(event.getValue()));
                }
            }
        });

        Button save = new Button("Сохранить", new Button.ClickListener() {
            @Override
            public void buttonClick(Button.ClickEvent event) {
                item.setQuantity(Double.parseDouble(itemQuantity.getValue()));
                item.setTotalValue(0.0);
                
                
                GoodPricesController pc = new GoodPricesController(item.getGoods().getId());
                for (DocumentGoods documentGoodsItem : documentGoodsItems) {
                    if (documentGoodsItem.equals(item)) {
                        documentGoodsItem.setPrice(pc.getPurchasePrice());
                    }
                }
                refreshBillGrid();
                selectedGoods.clear();

                editDocumentGoodWindow.close();
            }
        });

        editDocumentGoodLayout.addComponents(itemName, itemQuantity, save);
        editDocumentGoodWindow.setContent(editDocumentGoodLayout);
        editDocumentGoodWindow.center();
        editDocumentGoodWindow.setModal(true);
        MyUI.getCurrent().addWindow(editDocumentGoodWindow);

    }
    
    private void deleteSelectedGoods() {
        //selected = this.billGoods.getSelectedItems();
        documentGoodsItems.removeAll(selected);
        refreshBillGrid();
        
    }

    private void refreshBillGrid() throws UnsupportedOperationException {
        billGrid.removeAllComponents();
        BillGoodsGrid billGoods = new BillGoodsGrid(documentGoodsItems);
        billGoods.setSelectionMode(Grid.SelectionMode.MULTI);
        billGoods.setSizeFull();
        billGoods.addItemClickListener(new ItemClickListener<DocumentGoods>() {
            @Override
            public void itemClick(Grid.ItemClick<DocumentGoods> event) {
                DocumentGoods item = event.getItem();
                if (isEditList.getValue() == false) {
                    goToEditDocumentGood(item);
                } else {
                    selectedItem = event.getItem();
                }
            }
        });
        billGoods.addSelectionListener(new SelectionListener<DocumentGoods>() {
            @Override
            public void selectionChange(SelectionEvent<DocumentGoods> event) {
                selected = event.getAllSelectedItems();
            }
        });
        ListDataProvider<DocumentGoods> prov = (ListDataProvider<DocumentGoods>) billGoods.getDataProvider();
        prov.refreshAll();
        billGrid.addComponent(billGoods);
    }
    
    private void doSaveBill() {
        stockHelper.addDBItem(document);
        for (DocumentGoods documentGoodsItem : documentGoodsItems) {
            documentGoodsItem.setDocuments(document);
            stockHelper.addDBItem(documentGoodsItem);
        }
        Page.getCurrent().reload();
    }
    
    private void doCarryBill() {
        stockHelper.addDBItem(document);
        for (DocumentGoods documentGoods : documentGoodsItems) {
            documentGoods.setDocuments(document);
            stockHelper.addDBItem(documentGoods);
            Double quantity = documentGoods.getQuantity();
            Double initialQuantity = documentGoods.getGoods().getFactRemainder();
            Double initialRemainder = documentGoods.getGoods().getRemainder();
            Double factor = documentGoods.getGoods().getMainFactor();
            Boolean isInBasePackage = documentGoods.getGoods().getUseBasePackage();
            if (isInBasePackage) {
                documentGoods.getGoods().setFactRemainder(initialQuantity - quantity);
                documentGoods.getGoods().setRemainder(initialRemainder - quantity);
            } else {
                documentGoods.getGoods().setFactRemainder(initialQuantity - quantity * factor);
                documentGoods.getGoods().setRemainder(initialRemainder - quantity * factor);
            }
            stockHelper.updateDBItem(documentGoods.getGoods());            
        }
        Page.getCurrent().reload();
    }
}
