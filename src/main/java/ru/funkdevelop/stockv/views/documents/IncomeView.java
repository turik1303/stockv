/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ru.funkdevelop.stockv.views.documents;

import com.vaadin.data.HasValue;
import com.vaadin.data.provider.ListDataProvider;
import com.vaadin.navigator.Navigator;
import com.vaadin.navigator.View;
import com.vaadin.navigator.ViewChangeListener;
import com.vaadin.server.Page;
import com.vaadin.server.VaadinService;
import com.vaadin.ui.Alignment;
import com.vaadin.ui.Button;
import com.vaadin.ui.CheckBox;
import com.vaadin.ui.ComboBox;
import com.vaadin.ui.DateField;
import com.vaadin.ui.Grid;
import com.vaadin.ui.GridLayout;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Label;
import com.vaadin.ui.Panel;
import com.vaadin.ui.TabSheet;
import com.vaadin.ui.TextField;
import com.vaadin.ui.UI;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.Window;
import com.vaadin.ui.components.grid.ItemClickListener;
import com.vaadin.ui.themes.ValoTheme;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import ru.funkdevelop.stockv.MyUI;
import ru.funkdevelop.stockv.StockHelper;
import ru.funkdevelop.stockv.pojos.Contragents;
import ru.funkdevelop.stockv.pojos.DocumentGoods;
import ru.funkdevelop.stockv.pojos.Documents;
import ru.funkdevelop.stockv.pojos.Goods;
import ru.funkdevelop.stockv.pojos.GoodsType;
import ru.funkdevelop.stockv.pojos.Prices;
import ru.funkdevelop.stockv.pojos.Users;
import ru.funkdevelop.stockv.views.service.AllGoodsGrid;
import ru.funkdevelop.stockv.views.service.BillGoodsGrid;
import ru.funkdevelop.stockv.views.service.GoodPricesController;
import ru.funkdevelop.stockv.views.service.MainMenu;
import ru.funkdevelop.stockv.views.service.UserData;

/**
 *
 * @author artur
 */
public class IncomeView extends GridLayout implements View {

    MainMenu mainMenu;
    UserData userData;
    StockHelper stockHelper;
    Navigator navigator;
    List<Contragents> allContragents;
    List<Goods> allGoods;
    List<Goods> selectedGoods;
    List<GoodsType> allGoodsTypes;
    List<DocumentGoods> documentGoodsItems;

    VerticalLayout billGrid;
    BillGoodsGrid billGoods;

    DocumentGoods selectedItem;

    CheckBox isEditList;

    TextField docTotal;

    private int billNumber;
    private Date billDate;
    private Date billTime;
    private String billFirm;
    private String billWarehouse;
    private Contragents billProvider;
    private String billContract;
    private Date billPayUntil;
    private Double billSumm;
    private Double billPaymentSize = 0.0;
    private Double billChangeSize = 0.0;
    private Users billAuthor;
    private Boolean billIsCarried;
    private Boolean billIsFullyPaid = false;

    @Override
    public void enter(ViewChangeListener.ViewChangeEvent event) {
        navigator = UI.getCurrent().getNavigator();
        mainMenu = new MainMenu(navigator);
        userData = new UserData();
        stockHelper = new StockHelper();
        allContragents = stockHelper.getContragents();
        documentGoodsItems = new ArrayList<>();
        allGoods = stockHelper.getAllGoods();
        selectedGoods = new ArrayList<>();
        allGoodsTypes = stockHelper.getGoodsTypes();

        billGoods = new BillGoodsGrid(documentGoodsItems);

        GridLayout layout = new GridLayout(3, 3);

        TabSheet tabSheet = new TabSheet();
        tabSheet.addStyleName(ValoTheme.TABSHEET_FRAMED);
        tabSheet.addStyleName(ValoTheme.TABSHEET_PADDED_TABBAR);

        VerticalLayout header = new VerticalLayout();

        Panel docHeader = new Panel("Документ");
        HorizontalLayout docHeaderLayout = new HorizontalLayout();
        TextField docType = new TextField("Тип документа", "Поступление ТМЦ (купля - продажа)");
        docType.setWidth(10.0f, Unit.CM);
        docType.setEnabled(false);
        this.billNumber = stockHelper.getMaxBillId();

        this.billNumber += 1;

        TextField docNumber = new TextField("Номер", String.valueOf(this.billNumber));
        docNumber.setEnabled(false);
        DateField docDate = new DateField("Дата", LocalDate.now());
        docDate.setDateFormat("dd.MM.yyyy");
        docHeaderLayout.addComponents(docType, docNumber, docDate);
        docHeaderLayout.setComponentAlignment(docType, Alignment.TOP_CENTER);
        docHeaderLayout.setComponentAlignment(docNumber, Alignment.TOP_CENTER);
        docHeaderLayout.setComponentAlignment(docDate, Alignment.TOP_CENTER);
        docHeaderLayout.setSizeFull();
        docHeaderLayout.setMargin(true);
        docHeaderLayout.setSpacing(true);
        docHeader.setContent(docHeaderLayout);

        HorizontalLayout line2 = new HorizontalLayout();

        Panel buyer = new Panel("Покупатель");
        VerticalLayout buyerLayout = new VerticalLayout();

        TextField buyerName = new TextField("Покупатель", "И.П. Валиахметова Л.Н.");
        buyerName.setWidth(14.0f, Unit.CM);

        TextField warehouseName = new TextField("Склад", "Основной склад");
        warehouseName.setWidth(14.0f, Unit.CM);

        HorizontalLayout buttons = new HorizontalLayout();

        Button save = new Button("Сохранить");

        Button checkout = new Button("Провести");

        buttons.addComponents(save, checkout);
        buttons.setComponentAlignment(save, Alignment.MIDDLE_CENTER);
        buttons.setComponentAlignment(checkout, Alignment.MIDDLE_CENTER);
        buttons.setSizeFull();
        buyerLayout.addComponents(buyerName, warehouseName, buttons);
        buyerLayout.setComponentAlignment(buyerName, Alignment.TOP_CENTER);
        buyerLayout.setComponentAlignment(warehouseName, Alignment.TOP_CENTER);
        buyerLayout.setSizeFull();
        buyerLayout.setMargin(true);
        buyerLayout.setSpacing(true);
        buyer.setContent(buyerLayout);

        Panel provider = new Panel("Поставщик");
        VerticalLayout providerLayout = new VerticalLayout();

        TextField contract = new TextField("Договор");
        contract.setEnabled(false);
        contract.setWidth(14.0f, Unit.CM);

        HashMap<String, Contragents> providers = new HashMap<>();
        for (Contragents allContragent : allContragents) {
            if (allContragent.getContragentType().getId() == 1) {
                providers.put(allContragent.getDisplayName(), allContragent);
            }
        }
        ComboBox selectProvider = new ComboBox("Поставщик", providers.keySet());
        selectProvider.setEmptySelectionAllowed(false);
        selectProvider.setWidth(14.0f, Unit.CM);
        selectProvider.addValueChangeListener(new HasValue.ValueChangeListener() {
            @Override
            public void valueChange(HasValue.ValueChangeEvent event) {
                Contragents c = providers.get(event.getValue());
                contract.setValue(c.getMainContract());
            }
        });

        DateField payUntil = new DateField("Оплата до", LocalDate.now());
        payUntil.setDateFormat("dd.MM.yyyy");
        payUntil.setWidth(14.0f, Unit.CM);
        providerLayout.addComponents(selectProvider, contract, payUntil);
        providerLayout.setComponentAlignment(selectProvider, Alignment.TOP_CENTER);
        providerLayout.setComponentAlignment(contract, Alignment.TOP_CENTER);
        providerLayout.setComponentAlignment(payUntil, Alignment.TOP_CENTER);
        providerLayout.setSizeFull();
        providerLayout.setMargin(true);
        providerLayout.setSpacing(true);
        provider.setContent(providerLayout);

        save.addClickListener(new Button.ClickListener() {
            @Override
            public void buttonClick(Button.ClickEvent event) {
                Date date = new Date();
                Date time = new Date();
                Calendar tmp = new GregorianCalendar();
                tmp.set(docDate.getValue().getYear(), docDate.getValue().getMonthValue() - 1, docDate.getValue().getDayOfMonth());
                date.setTime(tmp.getTimeInMillis());
                billDate = date;
                billTime = time;
                billFirm = buyerName.getValue();
                billWarehouse = warehouseName.getValue();
                billProvider = providers.get(selectProvider.getValue());
                billContract = contract.getValue();
                Date payDay = new Date();
                Calendar c = new GregorianCalendar();
                c.set(payUntil.getValue().getYear(), payUntil.getValue().getMonthValue() - 1, payUntil.getValue().getDayOfMonth());
                payDay.setTime(c.getTimeInMillis());
                billPayUntil = payDay;
                billNumber = Integer.parseInt(docNumber.getValue());
                doSaveDocument(documentGoodsItems, false);
            }
        });

        checkout.addClickListener(new Button.ClickListener() {
            @Override
            public void buttonClick(Button.ClickEvent event) {
                Date date = new Date();
                Date time = new Date();
                Calendar tmp = new GregorianCalendar();
                tmp.set(docDate.getValue().getYear(), docDate.getValue().getMonthValue() - 1, docDate.getValue().getDayOfMonth());
                date.setTime(tmp.getTimeInMillis());
                billDate = date;
                billTime = time;
                billFirm = buyerName.getValue();
                billWarehouse = warehouseName.getValue();
                billProvider = providers.get(selectProvider.getValue());
                billContract = contract.getValue();
                Date payDay = new Date();
                Calendar c = new GregorianCalendar();
                c.set(payUntil.getValue().getYear(), payUntil.getValue().getMonthValue() - 1, payUntil.getValue().getDayOfMonth());
                payDay.setTime(c.getTimeInMillis());
                billPayUntil = payDay;
                billNumber = Integer.parseInt(docNumber.getValue());
                doCheckoutDocument(documentGoodsItems, true);
            }
        });

        buyer.setHeight(8.0f, Unit.CM);
        provider.setHeight(8.0f, Unit.CM);

        line2.addComponent(buyer);
        line2.addComponent(provider);
        line2.setSizeFull();
        Panel paymentData = new Panel("Информация о платеже");
        HorizontalLayout line3 = new HorizontalLayout();

        docTotal = new TextField("Общая стоимость", String.valueOf(billGoods.getValue()));
        TextField docPaidAmount = new TextField("Всего оплачено", String.valueOf(billPaymentSize));
        TextField docChangeAmount = new TextField("Сдача", String.valueOf(billChangeSize));
        docChangeAmount.setEnabled(false);
        Button calculateChange = new Button("Рассчитать сдачу", new Button.ClickListener() {
            @Override
            public void buttonClick(Button.ClickEvent event) {

                billPaymentSize = Double.parseDouble(docPaidAmount.getValue());
                if (billPaymentSize >= billSumm) {
                    billChangeSize = billPaymentSize - billSumm;
                    billIsFullyPaid = true;
                } else {
                    billIsFullyPaid = false;
                }
                docChangeAmount.setValue(String.valueOf(billChangeSize));
            }
        });
        line3.addComponents(docTotal, docPaidAmount, docChangeAmount, calculateChange);
        line3.setComponentAlignment(docTotal, Alignment.MIDDLE_CENTER);
        line3.setComponentAlignment(docPaidAmount, Alignment.MIDDLE_CENTER);
        line3.setComponentAlignment(docChangeAmount, Alignment.MIDDLE_CENTER);
        line3.setComponentAlignment(calculateChange, Alignment.MIDDLE_CENTER);
        line3.setSizeFull();
        line3.setSpacing(true);
        line3.setMargin(true);
        paymentData.setContent(line3);

        header.addComponent(docHeader);
        header.addComponent(line2);
        header.addComponent(paymentData);
        VerticalLayout tabView = new VerticalLayout();

        Panel goodsList = new Panel("Список товаров");
        VerticalLayout goodsListLayout = new VerticalLayout();

        HorizontalLayout goodsListHeader = new HorizontalLayout();
        Button doAddGoodsButton = new Button("Добавить товары", new Button.ClickListener() {
            @Override
            public void buttonClick(Button.ClickEvent event) {
                goToAddGoods();
            }
        });

        Button doDeleteGoodsButton = new Button("Удалить товары", new Button.ClickListener() {
            @Override
            public void buttonClick(Button.ClickEvent event) {

                Iterator it = documentGoodsItems.iterator();
                DocumentGoods doc = null;
                while (it.hasNext()) {
                    doc = (DocumentGoods) it.next();
                    if (doc.equals(selectedItem)) {
                        it.remove();

                    }
                }
                billGrid.removeAllComponents();
                BillGoodsGrid billGoods = new BillGoodsGrid(documentGoodsItems);
                billGoods.addItemClickListener(new ItemClickListener<DocumentGoods>() {
                    @Override
                    public void itemClick(Grid.ItemClick<DocumentGoods> event) {
                        DocumentGoods item = event.getItem();
                        goToEditDocumentGood(item);
                    }
                });
                billGrid.addComponent(billGoods);
                billSumm = billGoods.getValue();
            }
        });

        isEditList = new CheckBox("Редактировать список товаров");

        goodsListHeader.addComponents(doAddGoodsButton, doDeleteGoodsButton, isEditList);
        goodsListHeader.setComponentAlignment(doAddGoodsButton, Alignment.MIDDLE_CENTER);
        goodsListHeader.setComponentAlignment(doDeleteGoodsButton, Alignment.MIDDLE_CENTER);
        //goodsListHeader.setComponentAlignment(doClearListButton, Alignment.MIDDLE_CENTER);
        goodsListHeader.setComponentAlignment(isEditList, Alignment.MIDDLE_CENTER);
        goodsListLayout.addComponent(goodsListHeader);

        goodsList.setContent(goodsListLayout);
        tabView.addComponent(goodsList);

        billGoods.addItemClickListener(new ItemClickListener<DocumentGoods>() {
            @Override
            public void itemClick(Grid.ItemClick<DocumentGoods> event) {

                DocumentGoods item = event.getItem();
                if (isEditList.getValue() == false) {
                    goToEditDocumentGood(item);
                } else {
                    selectedItem = event.getItem();
                }

            }
        });

        billGrid = new VerticalLayout();
        billGrid.addComponent(billGoods);
        billGrid.setSizeFull();
        billGrid.setMargin(false);
        billGrid.setSpacing(false);
        billGoods.setSizeFull();
        billGoods.setHeight(13.1f, Unit.CM);
        goodsListLayout.addComponent(billGrid);

        tabSheet.addTab(header, "Шапка");
        tabSheet.addTab(tabView, "Табличная часть");
        tabSheet.setHeight(99.0f, Unit.PERCENTAGE);

        tabSheet.addSelectedTabChangeListener(new TabSheet.SelectedTabChangeListener() {
            @Override
            public void selectedTabChange(TabSheet.SelectedTabChangeEvent event) {
                if (event.getTabSheet().getSelectedTab().equals(header)) {
                    billSumm = billGoods.getValue();
                    docTotal.setValue(String.valueOf(billSumm));

                }
            }
        });

        Panel tabPanel = new Panel("Данные документа", tabSheet);
        tabPanel.setHeight(19.0f, Unit.CM);
        tabPanel.setWidth(32.0f, Unit.CM);
        layout.addComponent(mainMenu, 0, 0, 2, 0);
        layout.addComponent(userData, 0, 1, 0, 1);
        layout.addComponent(tabPanel, 1, 1, 1, 2);

        layout.setColumnExpandRatio(0, 0.0f);
        layout.setColumnExpandRatio(1, 0.0f);
        layout.setColumnExpandRatio(2, 0.0f);
        layout.setRowExpandRatio(0, 0.0f);
        layout.setRowExpandRatio(1, 0.0f);
        layout.setRowExpandRatio(2, 0.0f);
        layout.setSpacing(false);
        layout.setMargin(false);

        VerticalLayout fullLayout = new VerticalLayout();
        fullLayout.addComponent(layout);
        fullLayout.setComponentAlignment(layout, Alignment.MIDDLE_CENTER);
        fullLayout.setMargin(false);
        fullLayout.setSpacing(false);

        getUI().setContent(fullLayout);

    }

    private void goToAddGoods() {
        Window addGoodsWindow = new Window("Выбор товаров");
        VerticalLayout addGoodsWindowLayout = new VerticalLayout();
        AllGoodsGrid allGoodsGrid = new AllGoodsGrid(allGoods, allContragents, allGoodsTypes);
        addGoodsWindowLayout.addComponent(allGoodsGrid);
        allGoodsGrid.setHeight(16.0f, Unit.CM);
        HorizontalLayout buttons = new HorizontalLayout();
        Button doAddGoodsButton = new Button("Добавить выбранные", new Button.ClickListener() {
            @Override
            public void buttonClick(Button.ClickEvent event) {
                selectedGoods = allGoodsGrid.getSelectedGoods();
                doAddGoods();
                addGoodsWindow.close();
            }
        });

        Button doCancellButton = new Button("Отмена", new Button.ClickListener() {
            @Override
            public void buttonClick(Button.ClickEvent event) {
                selectedGoods.clear();
                addGoodsWindow.close();

            }
        });

        buttons.addComponents(doAddGoodsButton, doCancellButton);
        buttons.setComponentAlignment(doAddGoodsButton, Alignment.TOP_CENTER);
        buttons.setComponentAlignment(doCancellButton, Alignment.TOP_CENTER);
        buttons.setSizeFull();
        addGoodsWindowLayout.addComponent(buttons);
        addGoodsWindow.setContent(addGoodsWindowLayout);
        addGoodsWindow.center();
        addGoodsWindow.setModal(true);
        addGoodsWindow.setHeight(19.0f, Unit.CM);
        addGoodsWindow.setWidth(35.0f, Unit.CM);
        MyUI.getCurrent().addWindow(addGoodsWindow);
    }

    private void doAddGoods() {

        for (Goods selectedGood : selectedGoods) {
            DocumentGoods docGood = new DocumentGoods();
            List<Prices> goodPrices = stockHelper.getPricesByGoodId(selectedGood.getId());
            for (Prices goodPrice : goodPrices) {
                if (goodPrice.getPriceType().getId() == 1) {
                    docGood.setPrice(goodPrice.getResult());
                }
            }
            docGood.setGoods(selectedGood);
            docGood.setQuantity(0.0);
            docGood.setTotalValue(0.0);
            documentGoodsItems.add(docGood);
        }
        billGrid.removeAllComponents();
        BillGoodsGrid billGoods = new BillGoodsGrid(documentGoodsItems);
        billGoods.addItemClickListener(new ItemClickListener<DocumentGoods>() {
            @Override
            public void itemClick(Grid.ItemClick<DocumentGoods> event) {
                DocumentGoods item = event.getItem();
                if (isEditList.getValue() == false) {
                    goToEditDocumentGood(item);
                } else {
                    selectedItem = event.getItem();
                }
            }
        });
        billGrid.addComponent(billGoods);
        selectedGoods.clear();
        this.billSumm = billGoods.getValue();
    }

    private void goToEditDocumentGood(DocumentGoods item) {
        Window editDocumentGoodWindow = new Window();
        VerticalLayout editDocumentGoodLayout = new VerticalLayout();

        selectedItem = item;

        Label itemName = new Label(item.getGoods().getName());

        Button editItemPrices = new Button("Редактировать цены", new Button.ClickListener() {
            @Override
            public void buttonClick(Button.ClickEvent event) {
                GoodPricesController pricesController = new GoodPricesController(item.getGoods().getId());
                Window pr = pricesController.getPriceListWindow();
                pr.center();
                pr.setModal(true);
                MyUI.getCurrent().addWindow(pr);
            }
        });

        TextField itemQuantity = new TextField("Количество", Double.toString(item.getQuantity()));
        TextField itemTotalValue = new TextField("Стоимость", Double.toString(item.getTotalValue()));

        itemQuantity.addValueChangeListener(new HasValue.ValueChangeListener<String>() {
            @Override
            public void valueChange(HasValue.ValueChangeEvent<String> event) {

                try {
                    Boolean isUseBasePackage = item.getGoods().getUseBasePackage();

                    item.setQuantity(Double.parseDouble(event.getValue()));
                    Double tmpValue = item.getQuantity() * item.getPrice();
                    if (!isUseBasePackage) {
                        tmpValue *= item.getGoods().getMainFactor();
                    }
                    itemTotalValue.setValue(String.valueOf(round(tmpValue, 2)));
                } catch (NumberFormatException e) {
                    itemQuantity.setValue(itemQuantity.getValue().replace(",", "."));
                }

            }
        });

        Button save = new Button("Сохранить", new Button.ClickListener() {
            @Override
            public void buttonClick(Button.ClickEvent event) {
                item.setQuantity(Double.parseDouble(itemQuantity.getValue()));
                item.setTotalValue(Double.parseDouble(itemTotalValue.getValue()));

                billGrid.removeAllComponents();
                BillGoodsGrid billGoods = new BillGoodsGrid(documentGoodsItems);
                billGoods.addItemClickListener(new ItemClickListener<DocumentGoods>() {
                    @Override
                    public void itemClick(Grid.ItemClick<DocumentGoods> event) {
                        DocumentGoods item = event.getItem();
                        if (isEditList.getValue() == false) {
                            goToEditDocumentGood(item);
                        } else {
                            selectedItem = event.getItem();
                        }
                        billSumm = billGoods.getValue();
                    }
                });
                GoodPricesController pc = new GoodPricesController(item.getGoods().getId());
                for (DocumentGoods documentGoodsItem : documentGoodsItems) {
                    if (documentGoodsItem.equals(item)) {
                        documentGoodsItem.setPrice(pc.getPurchasePrice());
                    }
                }
                ListDataProvider<DocumentGoods> prov = (ListDataProvider<DocumentGoods>) billGoods.getDataProvider();
                prov.refreshAll();
                billGrid.addComponent(billGoods);
                selectedGoods.clear();

                editDocumentGoodWindow.close();
            }
        });

        editDocumentGoodLayout.addComponents(itemName, editItemPrices, itemQuantity, itemTotalValue, save);
        editDocumentGoodWindow.setContent(editDocumentGoodLayout);
        editDocumentGoodWindow.center();
        editDocumentGoodWindow.setModal(true);
        MyUI.getCurrent().addWindow(editDocumentGoodWindow);

    }

    private void deleteDocumentGoods(List<DocumentGoods> allDocumentGoods, List<DocumentGoods> selectedDocumentGoods) {
        allDocumentGoods.removeAll(selectedDocumentGoods);
    }

    private void doSaveDocument(List<DocumentGoods> goodsList, Boolean isCarried) {
        int documentId = stockHelper.getMaxBillId();
        if (documentId == 0) {
            documentId += 1;
        }
        Documents document = new Documents();
        billAuthor = (Users) VaadinService.getCurrentRequest().getWrappedSession().getAttribute("user");
        billIsCarried = isCarried;
        document.setChangeAmount(billChangeSize);
        document.setContragents(billProvider);
        document.setCurrency("руб.");
        document.setDate(billDate);
        document.setFirm(billFirm);
        document.setIsCarried(billIsCarried);
        document.setIsFullyPaid(billIsFullyPaid);
        document.setNumber(billNumber);
        document.setPaidAmount(billPaymentSize);
        document.setSumm(billSumm);
        document.setTime(billTime);
        document.setType(1);        //тип приходного документа - 1
        document.setUsers(billAuthor);
        document.setWarehouse(billWarehouse);
        document.setPayUntil(billPayUntil);
        HashSet<DocumentGoods> tmp = new HashSet<>();
        for (DocumentGoods documentGoods : goodsList) {
            documentGoods.setDocuments(document);
            tmp.add(documentGoods);
        }
        document.setDocumentGoodses(tmp);
        stockHelper.addDBItem(document);
        for (DocumentGoods documentGoods : goodsList) {
            stockHelper.addDBItem(documentGoods);
        }
        billNumber = 0;

        Page.getCurrent().reload();
    }

    private void doCheckoutDocument(List<DocumentGoods> goodsList, Boolean isCarried) {
        int documentId = stockHelper.getMaxBillId();
        if (documentId == 0) {
            documentId += 1;
        }
        Documents document = new Documents();
        billAuthor = (Users) VaadinService.getCurrentRequest().getWrappedSession().getAttribute("user");
        billIsCarried = isCarried;
        document.setChangeAmount(billChangeSize);
        document.setContragents(billProvider);
        document.setCurrency("руб.");
        document.setDate(billDate);
        document.setFirm(billFirm);
        document.setIsCarried(billIsCarried);
        document.setIsFullyPaid(billIsFullyPaid);
        document.setNumber(billNumber);
        document.setPaidAmount(billPaymentSize);
        document.setSumm(billSumm);
        document.setTime(billTime);
        document.setType(1);        //тип приходного документа - 1
        document.setUsers(billAuthor);
        document.setWarehouse(billWarehouse);
        document.setPayUntil(billPayUntil);
        HashSet<DocumentGoods> tmp = new HashSet<>();
        for (DocumentGoods documentGoods : goodsList) {
            documentGoods.setDocuments(document);
            tmp.add(documentGoods);
        }
        document.setDocumentGoodses(tmp);
        stockHelper.addDBItem(document);

        for (DocumentGoods documentGoods : goodsList) {
            stockHelper.addDBItem(documentGoods);
            Double quantity = documentGoods.getQuantity();
            Double initialQuantity = documentGoods.getGoods().getFactRemainder();
            Double initRemainder = documentGoods.getGoods().getRemainder();
            Double factor = documentGoods.getGoods().getMainFactor();
            Boolean isInBasePackage = documentGoods.getGoods().getUseBasePackage();
            if (isInBasePackage) {
                documentGoods.getGoods().setFactRemainder(initialQuantity + quantity);
                documentGoods.getGoods().setRemainder(initRemainder + quantity);
            } else {
                documentGoods.getGoods().setFactRemainder(initialQuantity + quantity * factor);
                documentGoods.getGoods().setRemainder(initRemainder + quantity * factor);
            }
            stockHelper.updateDBItem(documentGoods.getGoods());
        }
        billNumber = 0;

        Page.getCurrent().reload();
    }

    private double round(double number, int scale) {
        int pow = 10;
        for (int i = 1; i < scale; i++) {
            pow *= 10;
        }
        double tmp = number * pow;
        return (double) (int) ((tmp - (int) tmp) >= 0.5 ? tmp + 1 : tmp) / pow;
    }
}
