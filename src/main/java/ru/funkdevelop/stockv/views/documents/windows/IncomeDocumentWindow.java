/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ru.funkdevelop.stockv.views.documents.windows;

import com.vaadin.data.HasValue;
import com.vaadin.data.ValueContext;
import com.vaadin.data.converter.LocalDateToDateConverter;
import com.vaadin.data.provider.ListDataProvider;
import com.vaadin.event.selection.SelectionEvent;
import com.vaadin.event.selection.SelectionListener;
import com.vaadin.server.Page;
import com.vaadin.server.VaadinService;
import com.vaadin.ui.Alignment;
import com.vaadin.ui.Button;
import com.vaadin.ui.CheckBox;
import com.vaadin.ui.ComboBox;
import com.vaadin.ui.DateField;
import com.vaadin.ui.Grid;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Label;
import com.vaadin.ui.Panel;
import com.vaadin.ui.TextField;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.Window;
import com.vaadin.ui.components.grid.ItemClickListener;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import ru.funkdevelop.stockv.MyUI;
import ru.funkdevelop.stockv.StockHelper;
import ru.funkdevelop.stockv.pojos.Contragents;
import ru.funkdevelop.stockv.pojos.DocumentGoods;
import ru.funkdevelop.stockv.pojos.Documents;
import ru.funkdevelop.stockv.pojos.Goods;
import ru.funkdevelop.stockv.pojos.Prices;
import ru.funkdevelop.stockv.pojos.Users;
import ru.funkdevelop.stockv.views.service.AllGoodsGrid;
import ru.funkdevelop.stockv.views.service.BillGoodsGrid;
import ru.funkdevelop.stockv.views.service.GoodPricesController;

/**
 *
 * @author artur
 */
public class IncomeDocumentWindow extends Window {

    private Documents document;
    private List<Contragents> allContragents;
    private StockHelper stockHelper;
    private List<DocumentGoods> billGoods;
    HashMap<String, Contragents> providers = new HashMap<>();
    private BillGoodsWindow billGoodsWindow;
    private Boolean isEditable = false;

    public IncomeDocumentWindow(Documents document) {
        this.document = document;
        this.stockHelper = new StockHelper();
        this.allContragents = stockHelper.getContragents();
        this.billGoods = stockHelper.getBillGoods(document.getId());
        for (Contragents contragent : allContragents) {
            if (contragent.getContragentType().getId() == 1) {
                providers.put(contragent.getDisplayName(), contragent);
            }
        }

        this.setCaption("Документ № " + String.valueOf(this.document.getNumber()));
        HorizontalLayout fullLayout = new HorizontalLayout();
        VerticalLayout layout = new VerticalLayout();
        Panel mainData = new Panel("Основная информация");
        VerticalLayout mainDataLayout = new VerticalLayout();

        Panel documentData = new Panel("Документ");
        HorizontalLayout documentDataLayout = new HorizontalLayout();

        TextField docType = new TextField("Тип документа", "Поступление ТМЦ (купля-продажа)");
        docType.setEnabled(false);
        docType.setWidth(9.0f, Unit.CM);

        TextField docNum = new TextField("Номер", String.valueOf(this.document.getNumber()));
        docNum.setEnabled(false);
        docNum.setWidth(3.0f, Unit.CM);

        LocalDateToDateConverter dateConverter = new LocalDateToDateConverter();

        DateField docDate = new DateField("Дата", dateConverter.convertToPresentation(this.document.getDate(), new ValueContext()));
        docDate.setEnabled(false);
        docDate.setDateFormat("dd.MM.yyyy");
        docDate.setWidth(6.0f, Unit.CM);

        documentDataLayout.addComponents(docType, docNum, docDate);
        documentDataLayout.setSizeFull();
        documentDataLayout.setComponentAlignment(docType, Alignment.MIDDLE_LEFT);
        documentDataLayout.setComponentAlignment(docNum, Alignment.TOP_RIGHT);
        documentDataLayout.setComponentAlignment(docDate, Alignment.TOP_RIGHT);
        documentData.setContent(documentDataLayout);
        documentDataLayout.setSpacing(true);
        documentDataLayout.setMargin(true);

        HorizontalLayout line2 = new HorizontalLayout();
        Panel byerData = new Panel("Покупатель");
        VerticalLayout byerDataLayout = new VerticalLayout();

        TextField docByerName = new TextField("Покупатель", this.document.getFirm());
        docByerName.setEnabled(false);
        docByerName.setWidth(8.0f, Unit.CM);

        TextField docWarehouse = new TextField("Склад", this.document.getWarehouse());
        docWarehouse.setEnabled(false);
        docWarehouse.setWidth(8.0f, Unit.CM);

        byerDataLayout.addComponents(docByerName, docWarehouse);
        byerDataLayout.setComponentAlignment(docByerName, Alignment.TOP_CENTER);
        byerDataLayout.setComponentAlignment(docWarehouse, Alignment.TOP_CENTER);
        byerData.setContent(byerDataLayout);

        Panel providerData = new Panel("Поставщик");
        VerticalLayout providerDataLayout = new VerticalLayout();

        ComboBox docProvider = new ComboBox("Поставщик", providers.keySet());
        docProvider.setEnabled(false);
        docProvider.setValue(document.getContragents().getDisplayName());
        docProvider.setEmptySelectionAllowed(false);
        docProvider.setWidth(8.0f, Unit.CM);

        TextField docContract = new TextField("Договор", this.document.getContragents().getMainContract());
        docContract.setEnabled(false);
        docContract.setWidth(8.0f, Unit.CM);

        DateField docPayUntil = new DateField("Оплата до",
                dateConverter.convertToPresentation(this.document.getPayUntil(), new ValueContext()));
        docPayUntil.setEnabled(false);
        docPayUntil.setDateFormat("dd.MM.yyyy");
        docPayUntil.setWidth(8.0f, Unit.CM);

        providerDataLayout.addComponents(docProvider, docContract, docPayUntil);
        providerDataLayout.setComponentAlignment(docProvider, Alignment.TOP_CENTER);
        providerDataLayout.setComponentAlignment(docContract, Alignment.TOP_CENTER);
        providerDataLayout.setComponentAlignment(docPayUntil, Alignment.TOP_CENTER);
        providerData.setContent(providerDataLayout);

        line2.addComponents(byerData, providerData);
        line2.setComponentAlignment(byerData, Alignment.TOP_LEFT);
        line2.setComponentAlignment(providerData, Alignment.TOP_RIGHT);
        line2.setSizeFull();

        Panel paymentsData = new Panel("Оплата");
        HorizontalLayout paymentDataLayout = new HorizontalLayout();

        TextField docSumm = new TextField("Сумма", String.valueOf(this.document.getSumm()));
        docSumm.setEnabled(false);

        VerticalLayout payments = new VerticalLayout();
        TextField docNewPayment = new TextField("Новый платеж");
        docNewPayment.setEnabled(false);

        TextField docPayment = new TextField("Сумма платежей", String.valueOf(this.document.getPaidAmount()));
        docPayment.setEnabled(false);

        payments.addComponents(docNewPayment, docPayment);
        payments.setSpacing(false);
        payments.setMargin(false);

        VerticalLayout changes = new VerticalLayout();
        TextField docNewChange = new TextField("Cдача");
        docNewChange.setEnabled(false);

        TextField docChange = new TextField("Всего сдачи", String.valueOf(this.document.getChangeAmount()));
        docChange.setEnabled(false);

        changes.addComponents(docNewChange, docChange);
        changes.setSpacing(false);
        changes.setMargin(false);

        Button calculateChange = new Button("Рассчитать", new Button.ClickListener() {
            @Override
            public void buttonClick(Button.ClickEvent event) {
                try {
                Double newPayment = Double.parseDouble(docNewPayment.getValue());
                Double summ = Double.parseDouble(docSumm.getValue());
                Double paidAmount = document.getSumm();
                Double amountToPay = summ - paidAmount;
                Double newChange = newPayment - amountToPay;
                if (newChange >= 0) {
                    docChange.setValue(String.valueOf(document.getChangeAmount() + newChange));
                    docNewChange.setValue(String.valueOf(newChange));
                }
                docPayment.setValue(String.valueOf(paidAmount + newPayment));
                } catch (NumberFormatException e) {
                    docPayment.setValue(docPayment.getValue().replace(",", "."));
                    docSumm.setValue(docSumm.getValue().replace(",", "."));
                }

            }
        });

        paymentDataLayout.addComponents(docSumm, payments, changes, calculateChange);
        paymentDataLayout.setSpacing(true);
        paymentDataLayout.setMargin(true);
        paymentDataLayout.setComponentAlignment(docSumm, Alignment.TOP_CENTER);
        paymentDataLayout.setComponentAlignment(payments, Alignment.TOP_CENTER);
        paymentDataLayout.setComponentAlignment(changes, Alignment.TOP_CENTER);
        paymentDataLayout.setComponentAlignment(calculateChange, Alignment.MIDDLE_CENTER);
        paymentDataLayout.setSizeFull();
        paymentsData.setContent(paymentDataLayout);

        documentData.setWidth(20.0f, Unit.CM);
        byerData.setWidth(9.85f, Unit.CM);
        providerData.setWidth(9.85f, Unit.CM);
        byerData.setHeight(8.0f, Unit.CM);
        providerData.setHeight(8.0f, Unit.CM);

        Panel control = new Panel("Управление");
        VerticalLayout controlLayout = new VerticalLayout();

        Button setFullPaid = new Button("Оплатить", new Button.ClickListener() {
            @Override
            public void buttonClick(Button.ClickEvent event) {
                try {
                Double oldSumm = document.getSumm();
                Double newSumm = Double.parseDouble(docSumm.getValue());
                Double result = newSumm - oldSumm;
                docNewPayment.setValue(String.valueOf(result));
                calculateChange.click();
                } catch (NumberFormatException e) {
                    docSumm.setValue(docSumm.getValue().replace(",", "."));
                }
            }
        });
        setFullPaid.setEnabled(!this.document.getIsFullyPaid());

        Button loadDocumentGoods = new Button("Список товаров", new Button.ClickListener() {
            @Override
            public void buttonClick(Button.ClickEvent event) {

                billGoodsWindow = new BillGoodsWindow(billGoods);
                billGoodsWindow.addCloseListener(new CloseListener() {
                    @Override
                    public void windowClose(CloseEvent e) {
                        docSumm.setValue(String.valueOf(billGoodsWindow.getSumm()));
                        if (document.getSumm() != billGoodsWindow.getSumm()) {
                            document.setIsFullyPaid(false);
                            setFullPaid.setEnabled(true);
                        }

                    }
                });
            }
        });

        Button setIsCarried = new Button("Провести", new Button.ClickListener() {
            @Override
            public void buttonClick(Button.ClickEvent event) {
                List<DocumentGoods> carriedBillGoods = stockHelper.getBillGoods(document.getId());
                
                Date date = new Date();
                Date time = new Date();
                Calendar tmp = new GregorianCalendar();
                tmp.set(docDate.getValue().getYear(), docDate.getValue().getMonthValue() - 1, docDate.getValue().getDayOfMonth());
                date.setTime(tmp.getTimeInMillis());
                document.setDate(date);
                document.setTime(time);
                document.setContragents(providers.get(docProvider.getValue()));
                Date payDay = new Date();
                Calendar c = new GregorianCalendar();
                c.set(docPayUntil.getValue().getYear(), docPayUntil.getValue().getMonthValue() - 1, docPayUntil.getValue().getDayOfMonth());
                payDay.setTime(c.getTimeInMillis());
                document.setPayUntil(payDay);
                document.setSumm(Double.parseDouble(docSumm.getValue()));
                document.setPaidAmount(Double.parseDouble(docPayment.getValue()));
                document.setChangeAmount(Double.parseDouble(docChange.getValue()));
                Boolean isFP;
                if (document.getPaidAmount() >= document.getSumm()) {
                    isFP = true;
                } else {
                    isFP = false;
                }
                document.setIsFullyPaid(isFP);
                HashSet<DocumentGoods> tmpSet = new HashSet<>();
                document.setUsers((Users) VaadinService.getCurrentRequest().getWrappedSession().getAttribute("user"));
                for (DocumentGoods documentGoods : billGoods) {
                    documentGoods.setDocuments(document);
                    tmpSet.add(documentGoods);
                }
                document.setDocumentGoodses(tmpSet);
                
                List<DocumentGoods> oldGoods = stockHelper.getBillGoods(document.getId());
                for (DocumentGoods billGood : billGoods) {
                    stockHelper.saveOrUpdateDBItem(billGood);
                }
                for (DocumentGoods oldGood : oldGoods) {
                    Boolean isFound = false;
                    for (DocumentGoods billGood : billGoods) {
                        if (billGood.getGoods().getId() == oldGood.getGoods().getId()) {
                            isFound = true;
                        }
                    }
                    if (!isFound) {
                        stockHelper.deleteDBItem(oldGood);
                        Goods tmpGood = oldGood.getGoods();
                        Double factor = tmpGood.getMainFactor();
                        Boolean isInBasePackage = tmpGood.getUseBasePackage();
                        if (isInBasePackage) {
                            tmpGood.setFactRemainder(tmpGood.getFactRemainder() - oldGood.getQuantity());
                            tmpGood.setRemainder(tmpGood.getRemainder() - oldGood.getQuantity());
                        } else {
                            tmpGood.setFactRemainder(tmpGood.getFactRemainder() - oldGood.getQuantity() * factor);
                            tmpGood.setRemainder(tmpGood.getRemainder() - oldGood.getQuantity() * factor);
                        }
                        stockHelper.updateDBItem(tmpGood);
                    }
                }
                List<DocumentGoods> unCarriedBillGoods = stockHelper.getBillGoods(document.getId());
                for (DocumentGoods unCarriedBillGood : unCarriedBillGoods) {
                    Goods good = unCarriedBillGood.getGoods();
                    DocumentGoods tmpGoods = new DocumentGoods(-1);
                    for (DocumentGoods carriedBillGood : carriedBillGoods) {
                        if (carriedBillGood.getGoods().getId() == unCarriedBillGood.getGoods().getId()) {
                            tmpGoods = carriedBillGood;
                        }
                    }
                    if (tmpGoods.getId() != -1) { //Сделать проверку была ли проведена накладная ранее
                        Double oldQuantity = tmpGoods.getQuantity();                        
                        Double newQuantity = unCarriedBillGood.getQuantity();
                        
                        Double change = 0.0; // - oldQuantity
                        if (document.getIsCarried()) {
                            change = newQuantity - oldQuantity;
                        } else {
                            change = newQuantity;
                        }
                        Double factor = unCarriedBillGood.getGoods().getMainFactor();
                        Boolean isInBasePackage = unCarriedBillGood.getGoods().getUseBasePackage();
                        if (isInBasePackage) {
                            good.setFactRemainder(good.getFactRemainder() + change);
                            good.setRemainder(good.getRemainder() + change);
                        } else {
                            good.setFactRemainder(good.getFactRemainder() + change * factor);
                            good.setRemainder(good.getRemainder() + change * factor);
                        }
                    } else {
                        Double newQuantity = unCarriedBillGood.getQuantity();
                        Double factor = unCarriedBillGood.getGoods().getMainFactor();
                        Boolean isInBasePackage = unCarriedBillGood.getGoods().getUseBasePackage();
                        if (isInBasePackage) {
                            good.setFactRemainder(good.getFactRemainder() + newQuantity);
                            good.setRemainder(good.getRemainder() + newQuantity);
                        } else {
                            good.setFactRemainder(good.getFactRemainder() + newQuantity * factor);
                            good.setRemainder(good.getRemainder() + newQuantity * factor);
                        }

                    }
                    stockHelper.updateDBItem(good);
                }
                
                document.setIsCarried(true);
                stockHelper.updateDBItem(document);
                Page.getCurrent().reload();
            }
        });
        setIsCarried.setEnabled(!this.document.getIsCarried());

        Button save = new Button("Сохранить", new Button.ClickListener() {
            @Override
            public void buttonClick(Button.ClickEvent event) {
                Date date = new Date();
                Date time = new Date();
                Calendar tmp = new GregorianCalendar();
                tmp.set(docDate.getValue().getYear(), docDate.getValue().getMonthValue() - 1, docDate.getValue().getDayOfMonth());
                date.setTime(tmp.getTimeInMillis());
                document.setDate(date);
                document.setTime(time);
                document.setContragents(providers.get(docProvider.getValue()));
                Date payDay = new Date();
                Calendar c = new GregorianCalendar();
                c.set(docPayUntil.getValue().getYear(), docPayUntil.getValue().getMonthValue() - 1, docPayUntil.getValue().getDayOfMonth());
                payDay.setTime(c.getTimeInMillis());
                document.setPayUntil(payDay);
                document.setSumm(Double.parseDouble(docSumm.getValue()));
                document.setPaidAmount(Double.parseDouble(docPayment.getValue()));
                document.setChangeAmount(Double.parseDouble(docChange.getValue()));
                Boolean isFP;
                if (document.getPaidAmount() >= document.getSumm()) {
                    isFP = true;
                } else {
                    isFP = false;
                }
                document.setIsFullyPaid(isFP);
                HashSet<DocumentGoods> tmpSet = new HashSet<>();
                document.setUsers((Users) VaadinService.getCurrentRequest().getWrappedSession().getAttribute("user"));
                for (DocumentGoods documentGoods : billGoods) {
                    documentGoods.setDocuments(document);
                    tmpSet.add(documentGoods);
                }
                document.setDocumentGoodses(tmpSet);
                stockHelper.updateDBItem(document);
                List<DocumentGoods> oldGoods = stockHelper.getBillGoods(document.getId());
                for (DocumentGoods billGood : billGoods) {
                    stockHelper.saveOrUpdateDBItem(billGood);
                }
                for (DocumentGoods oldGood : oldGoods) {
                    Boolean isFound = false;
                    for (DocumentGoods billGood : billGoods) {
                        if (billGood.getGoods().getId() == oldGood.getGoods().getId()) {
                            isFound = true;
                        }
                    }
                    if (!isFound) {
                        stockHelper.deleteDBItem(oldGood);
                    }
                }

                Page.getCurrent().reload();
            }
        });
        save.setEnabled(false);

        Button delete = new Button("Удалить", new Button.ClickListener() {
            @Override
            public void buttonClick(Button.ClickEvent event) {
                List<DocumentGoods> goodsList = stockHelper.getBillGoods(document.getId());
                for (DocumentGoods documentGoods : goodsList) {
                    stockHelper.deleteDBItem(documentGoods);
                }
                stockHelper.deleteDBItem(document);
                
                Page.getCurrent().reload();
            }
        });
        delete.setEnabled(!this.document.getIsCarried());

        Button edit = new Button("Редактировать", new Button.ClickListener() {
            @Override
            public void buttonClick(Button.ClickEvent event) {
                docDate.setEnabled(true);
                docProvider.setEnabled(true);
                docPayUntil.setEnabled(true);
                docNewPayment.setEnabled(true);
                docSumm.setEnabled(true);
                save.setEnabled(true);
                setIsCarried.setEnabled(true);
                isEditable = true;
            }
        });

        loadDocumentGoods.setWidth(5.0f, Unit.CM);
        setIsCarried.setWidth(5.0f, Unit.CM);
        setFullPaid.setWidth(5.0f, Unit.CM);
        save.setWidth(5.0f, Unit.CM);
        delete.setWidth(5.0f, Unit.CM);
        edit.setWidth(5.0f, Unit.CM);
        controlLayout.addComponents(loadDocumentGoods, edit, setIsCarried, setFullPaid, save, delete);
        control.setContent(controlLayout);

        mainDataLayout.addComponents(documentData, line2, paymentsData);
        mainData.setContent(mainDataLayout);
        layout.addComponent(mainData);
        layout.addComponent(control);
        fullLayout.addComponents(mainData, control);
        fullLayout.setMargin(true);
        fullLayout.setSpacing(true);

        this.setContent(fullLayout);
        this.center();
        this.setModal(true);
        MyUI.getCurrent().addWindow(this);
    }

    final class BillGoodsWindow extends Window {

        private List<DocumentGoods> billGoods;
        private BillGoodsGrid billGoodsGrid;
        private VerticalLayout layout;
        private Set<DocumentGoods> selectedGoods;
        private List<Goods> selectedToAddGoods;
        private Boolean isListEdit;

        public BillGoodsWindow(List<DocumentGoods> billGoods) {
            this.setCaption("Список товаров");
            this.billGoods = billGoods;
            this.layout = new VerticalLayout();
            this.billGoodsGrid = new BillGoodsGrid(billGoods);
            Panel controlPanel = new Panel("Управление");
            HorizontalLayout controlPanelLayout = new HorizontalLayout();

            Button addGoodsButton = new Button("Добавить товары", new Button.ClickListener() {
                @Override
                public void buttonClick(Button.ClickEvent event) {
                    goToAddGoods();
                }
            });
            addGoodsButton.setEnabled(isEditable);

            Button deleteSelectedGoodsButton = new Button("Удалить выбранные товары", new Button.ClickListener() {
                @Override
                public void buttonClick(Button.ClickEvent event) {
                    deleteSelectedGoods();
                }
            });
            deleteSelectedGoodsButton.setEnabled(isEditable);

            Button deleteAllGoodsButton = new Button("Удалить все товары", new Button.ClickListener() {
                @Override
                public void buttonClick(Button.ClickEvent event) {
                    deleteAllGoods();
                }
            });
            deleteAllGoodsButton.setEnabled(isEditable);

            CheckBox isEditList = new CheckBox("Редактировать список товаров");
            isListEdit = isEditList.getValue();
            isEditList.addValueChangeListener(new HasValue.ValueChangeListener<Boolean>() {
                @Override
                public void valueChange(HasValue.ValueChangeEvent<Boolean> event) {
                    isListEdit = event.getValue();
                }
            });
            controlPanelLayout.addComponents(addGoodsButton, deleteSelectedGoodsButton, deleteAllGoodsButton, isEditList);
            controlPanel.setContent(controlPanelLayout);
            controlPanelLayout.setComponentAlignment(isEditList, Alignment.MIDDLE_LEFT);
            this.billGoodsGrid.setSelectionMode(Grid.SelectionMode.MULTI);
            this.billGoodsGrid.addSelectionListener(new SelectionListener<DocumentGoods>() {
                @Override
                public void selectionChange(SelectionEvent<DocumentGoods> event) {
                    selectedGoods = event.getAllSelectedItems();
                }
            });
            this.billGoodsGrid.addItemClickListener(new ItemClickListener<DocumentGoods>() {
                @Override
                public void itemClick(Grid.ItemClick<DocumentGoods> event) {
                    if (!isListEdit & isEditable) {
                        goToEditGood(event.getItem());
                    }
                }
            });

            layout.addComponent(controlPanel);
            layout.addComponent(billGoodsGrid);

            this.setContent(layout);
            this.center();
            this.setModal(true);
            MyUI.getCurrent().addWindow(this);

        }

        private void goToAddGoods() {
            Window addGoodsWindow = new Window("Выбор товаров");
            VerticalLayout addGoodsWindowLayout = new VerticalLayout();
            AllGoodsGrid allGoodsGrid = new AllGoodsGrid(stockHelper.getAllGoods(), allContragents, stockHelper.getGoodsTypes());
            addGoodsWindowLayout.addComponent(allGoodsGrid);
            allGoodsGrid.setHeight(16.0f, Unit.CM);
            HorizontalLayout buttons = new HorizontalLayout();
            Button doAddGoodsButton = new Button("Добавить выбранные", new Button.ClickListener() {
                @Override
                public void buttonClick(Button.ClickEvent event) {
                    selectedToAddGoods = allGoodsGrid.getSelectedGoods();
                    doAddGoods();
                    addGoodsWindow.close();
                }
            });

            Button doCancellButton = new Button("Отмена", new Button.ClickListener() {
                @Override
                public void buttonClick(Button.ClickEvent event) {
                    selectedToAddGoods.clear();
                    addGoodsWindow.close();

                }
            });

            buttons.addComponents(doAddGoodsButton, doCancellButton);
            buttons.setComponentAlignment(doAddGoodsButton, Alignment.TOP_CENTER);
            buttons.setComponentAlignment(doCancellButton, Alignment.TOP_CENTER);
            buttons.setSizeFull();
            addGoodsWindowLayout.addComponent(buttons);
            addGoodsWindow.setContent(addGoodsWindowLayout);
            addGoodsWindow.center();
            addGoodsWindow.setModal(true);
            addGoodsWindow.setHeight(19.0f, Unit.CM);
            addGoodsWindow.setWidth(35.0f, Unit.CM);
            MyUI.getCurrent().addWindow(addGoodsWindow);
        }

        private void doAddGoods() {

            for (Goods selectedGood : selectedToAddGoods) {
                DocumentGoods docGood = new DocumentGoods();
                List<Prices> goodPrices = stockHelper.getPricesByGoodId(selectedGood.getId());
                for (Prices goodPrice : goodPrices) {
                    if (goodPrice.getPriceType().getId() == 1) {
                        docGood.setPrice(goodPrice.getResult());
                    }
                }
                docGood.setGoods(selectedGood);
                docGood.setQuantity(0.0);
                docGood.setTotalValue(0.0);
                billGoods.add(docGood);
            }
            layout.removeComponent(billGoodsGrid);
            billGoodsGrid = new BillGoodsGrid(billGoods);
            this.billGoodsGrid.setSelectionMode(Grid.SelectionMode.MULTI);
            this.billGoodsGrid.addSelectionListener(new SelectionListener<DocumentGoods>() {
                @Override
                public void selectionChange(SelectionEvent<DocumentGoods> event) {
                    selectedGoods = event.getAllSelectedItems();
                }
            });
            billGoodsGrid.addItemClickListener(new ItemClickListener<DocumentGoods>() {
                @Override
                public void itemClick(Grid.ItemClick<DocumentGoods> event) {
                    if (!isListEdit) {
                        goToEditGood(event.getItem());
                    }
                }
            });
            layout.addComponent(billGoodsGrid);
            selectedToAddGoods.clear();
        }

        private void goToEditGood(DocumentGoods item) {
            Window editDocumentGoodWindow = new Window();
            VerticalLayout editDocumentGoodLayout = new VerticalLayout();

            Label itemName = new Label(item.getGoods().getName());

            Button editItemPrices = new Button("Редактировать цены", new Button.ClickListener() {
                @Override
                public void buttonClick(Button.ClickEvent event) {
                    GoodPricesController pricesController = new GoodPricesController(item.getGoods().getId());
                    Window pr = pricesController.getPriceListWindow();
                    pr.center();
                    pr.setModal(true);
                    MyUI.getCurrent().addWindow(pr);
                }
            });

            TextField itemQuantity = new TextField("Количество", Double.toString(item.getQuantity()));
            TextField itemTotalValue = new TextField("Стоимость", Double.toString(item.getTotalValue()));

            itemQuantity.addValueChangeListener(new HasValue.ValueChangeListener<String>() {
                @Override
                public void valueChange(HasValue.ValueChangeEvent<String> event) {
                    
                    Boolean isUseBasePackage = item.getGoods().getUseBasePackage();
                    try {
                    item.setQuantity(Double.parseDouble(event.getValue().replace(",", ".")));
                    } catch (NumberFormatException e) {
                        
                    }
                    Double tmpValue = item.getQuantity() * item.getPrice();
                    if (!isUseBasePackage) {
                        tmpValue *= item.getGoods().getMainFactor();
                    }
                    itemTotalValue.setValue(String.valueOf(round(tmpValue, 2)));

                }
            });

            Button save = new Button("Сохранить", new Button.ClickListener() {
                @Override
                public void buttonClick(Button.ClickEvent event) {
                    item.setQuantity(Double.parseDouble(itemQuantity.getValue().replace(",", ".")));
                    item.setTotalValue(Double.parseDouble(itemTotalValue.getValue().replace(",", ".")));
                    layout.removeComponent(billGoodsGrid);

                    billGoodsGrid = new BillGoodsGrid(billGoods);
                    billGoodsGrid.setSelectionMode(Grid.SelectionMode.MULTI);
                    billGoodsGrid.addSelectionListener(new SelectionListener<DocumentGoods>() {
                        @Override
                        public void selectionChange(SelectionEvent<DocumentGoods> event) {
                            selectedGoods = event.getAllSelectedItems();
                        }
                    });
                    billGoodsGrid.addItemClickListener(new ItemClickListener<DocumentGoods>() {
                        @Override
                        public void itemClick(Grid.ItemClick<DocumentGoods> event) {
                            if (!isListEdit) {
                                goToEditGood(event.getItem());
                            }
                        }
                    });
                    GoodPricesController pc = new GoodPricesController(item.getGoods().getId());
                    for (DocumentGoods documentGoodsItem : billGoods) {
                        if (documentGoodsItem.equals(item)) {
                            documentGoodsItem.setPrice(pc.getPurchasePrice());
                        }
                    }
                    ListDataProvider<DocumentGoods> prov = (ListDataProvider<DocumentGoods>) billGoodsGrid.getDataProvider();
                    prov.refreshAll();
                    layout.addComponent(billGoodsGrid);

                    editDocumentGoodWindow.close();
                }
            });

            editDocumentGoodLayout.addComponents(itemName, editItemPrices, itemQuantity, itemTotalValue, save);
            editDocumentGoodWindow.setContent(editDocumentGoodLayout);
            editDocumentGoodWindow.center();
            editDocumentGoodWindow.setModal(true);
            MyUI.getCurrent().addWindow(editDocumentGoodWindow);
        }

        public Double getSumm() {
            return billGoodsGrid.getValue();
        }

        public List<DocumentGoods> getBillGoods() {
            return billGoods;
        }

        private void deleteSelectedGoods() {
            for (DocumentGoods selectedGood : selectedGoods) {
                billGoods.remove(selectedGood);
            }
            layout.removeComponent(billGoodsGrid);
            billGoodsGrid = new BillGoodsGrid(billGoods);
            billGoodsGrid.setSelectionMode(Grid.SelectionMode.MULTI);
            billGoodsGrid.addSelectionListener(new SelectionListener<DocumentGoods>() {
                @Override
                public void selectionChange(SelectionEvent<DocumentGoods> event) {
                    selectedGoods = event.getAllSelectedItems();
                }
            });
            billGoodsGrid.addItemClickListener(new ItemClickListener<DocumentGoods>() {
                @Override
                public void itemClick(Grid.ItemClick<DocumentGoods> event) {
                    if (!isListEdit) {
                        goToEditGood(event.getItem());
                    }
                }
            });
            layout.addComponent(billGoodsGrid);

        }

        private void deleteAllGoods() {
            billGoods.clear();
            layout.removeComponent(billGoodsGrid);
            billGoodsGrid = new BillGoodsGrid(billGoods);
            billGoodsGrid.setSelectionMode(Grid.SelectionMode.MULTI);
            billGoodsGrid.addSelectionListener(new SelectionListener<DocumentGoods>() {
                @Override
                public void selectionChange(SelectionEvent<DocumentGoods> event) {
                    selectedGoods = event.getAllSelectedItems();
                }
            });
            billGoodsGrid.addItemClickListener(new ItemClickListener<DocumentGoods>() {
                @Override
                public void itemClick(Grid.ItemClick<DocumentGoods> event) {
                    if (!isListEdit) {
                        goToEditGood(event.getItem());
                    }
                }
            });
            layout.addComponent(billGoodsGrid);
        }

    }
    
    private double round(double number, int scale) {
        int pow = 10;
        for (int i = 1; i < scale; i++) {
            pow *= 10;
        }
        double tmp = number * pow;
        return (double) (int) ((tmp - (int) tmp) >= 0.5 ? tmp + 1 : tmp) / pow;
    }
    
    private double round(Double number, int scale) {
        int pow = 10;
        for (int i = 1; i < scale; i++) {
            pow *= 10;
        }
        double tmp = number * pow;
        return (double) (int) ((tmp - (int) tmp) >= 0.5 ? tmp + 1 : tmp) / pow;
    }
}
