/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ru.funkdevelop.stockv.views.documents.windows;

import com.vaadin.data.ValueContext;
import com.vaadin.data.converter.LocalDateToDateConverter;
import com.vaadin.data.provider.ListDataProvider;
import com.vaadin.event.selection.SelectionEvent;
import com.vaadin.event.selection.SelectionListener;
import com.vaadin.server.Page;
import com.vaadin.server.VaadinService;
import com.vaadin.ui.Alignment;
import com.vaadin.ui.Button;
import com.vaadin.ui.CheckBox;
import com.vaadin.ui.DateField;
import com.vaadin.ui.Grid;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Label;
import com.vaadin.ui.Panel;
import com.vaadin.ui.TextField;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.Window;
import com.vaadin.ui.components.grid.ItemClickListener;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import ru.funkdevelop.stockv.MyUI;
import ru.funkdevelop.stockv.StockHelper;
import ru.funkdevelop.stockv.pojos.Contragents;
import ru.funkdevelop.stockv.pojos.DocumentGoods;
import ru.funkdevelop.stockv.pojos.Documents;
import ru.funkdevelop.stockv.pojos.Goods;
import ru.funkdevelop.stockv.pojos.GoodsType;
import ru.funkdevelop.stockv.pojos.Prices;
import ru.funkdevelop.stockv.pojos.Users;
import ru.funkdevelop.stockv.views.service.AllGoodsGrid;
import ru.funkdevelop.stockv.views.service.BillGoodsGrid;
import ru.funkdevelop.stockv.views.service.GoodPricesController;

/**
 *
 * @author artur
 */
public class RemainderDocumentWindow extends Window {

    private Documents document;
    private StockHelper stockHelper;
    private List<DocumentGoods> documentGoodsItems;

    CheckBox isEditList;
    VerticalLayout billGrid;
    List<Contragents> allContragents;

    BillGoodsGrid billGoods;
    List<Goods> selectedGoods;
    Set<DocumentGoods> selected = new HashSet<>();

    List<Goods> allGoods;
    List<GoodsType> allGoodsTypes;

    DocumentGoods selectedItem;

    List<DocumentGoods> oldGoods;

    public RemainderDocumentWindow(Documents document) {
        this.document = document;
        this.stockHelper = new StockHelper();
        this.documentGoodsItems = stockHelper.getBillGoods(document.getId());
        this.allContragents = stockHelper.getContragents();
        this.allGoods = stockHelper.getAllGoods();
        this.allGoodsTypes = stockHelper.getGoodsTypes();

        this.oldGoods = stockHelper.getBillGoods(document.getId());

        this.setCaption("Документ № " + String.valueOf(this.document.getNumber()));
        HorizontalLayout fullLayout = new HorizontalLayout();
        VerticalLayout layout = new VerticalLayout();

        Panel docHeader = new Panel("Шапка");
        VerticalLayout docHeaderLayout = new VerticalLayout();
        HorizontalLayout line1 = new HorizontalLayout();

        TextField docType = new TextField("Тип документа", "Ввод остатков ТМЦ на складе");
        docType.setEnabled(false);
        docType.setWidth(10.0f, Unit.CM);

        TextField docNumber = new TextField("Номер", String.valueOf(document.getNumber()));
        docNumber.setEnabled(false);
        docNumber.setWidth(10.0f, Unit.CM);

        LocalDateToDateConverter dateConverter = new LocalDateToDateConverter();

        DateField docDate = new DateField("Дата", dateConverter.convertToPresentation(this.document.getDate(), new ValueContext()));
        docDate.setWidth(10.0f, Unit.CM);
        docDate.setDateFormat("dd.MM.yyyy");

        line1.addComponents(docType, docNumber, docDate);
        docHeaderLayout.addComponent(line1);
        docHeaderLayout.addComponent(new Label("Организация: И.П. Валиахметова Л.Н."));
        docHeaderLayout.addComponent(new Label("Склад: Основной склад"));
        docHeaderLayout.setSizeFull();
        docHeader.setContent(docHeaderLayout);

        Panel goodsControl = new Panel("Управление");
        HorizontalLayout goodsControlLayout = new HorizontalLayout();

        Button addGoodsButton = new Button("Добавить товары", new Button.ClickListener() {
            @Override
            public void buttonClick(Button.ClickEvent event) {
                goToAddGoods();
            }
        });
        addGoodsButton.setWidth(6.0f, Unit.CM);

        Button deleteGoodsButton = new Button("Удалить товары", new Button.ClickListener() {
            @Override
            public void buttonClick(Button.ClickEvent event) {
                deleteSelectedGoods();
            }
        });
        deleteGoodsButton.setWidth(6.0f, Unit.CM);

        Button saveBillButton = new Button("Сохранить документ", new Button.ClickListener() {
            @Override
            public void buttonClick(Button.ClickEvent event) {

                document.setType(3);
                document.setChangeAmount(0.0);
                Contragents contr = new Contragents();
                for (Contragents allContragent : allContragents) {
                    if (allContragent.getId() == 6) {
                        contr = allContragent;
                    }
                }
                document.setContragents(contr);
                document.setCurrency("руб.");
                
                
                Set<DocumentGoods> tmp = new HashSet<>();
                tmp.addAll(documentGoodsItems);
                document.setDocumentGoodses(tmp);
                document.setFirm("И.П. Валиахметова Л.Н.");
                document.setIsCarried(false);
                document.setIsFullyPaid(true);
                document.setNumber(Integer.parseInt(docNumber.getValue()));
                document.setPaidAmount(0.0);
                document.setPayUntil(new Date());
                document.setSumm(0.0);
                document.setWarehouse("Основной склад");
                document.setUsers((Users) VaadinService.getCurrentRequest().getWrappedSession().getAttribute("user"));
                doSaveBill();
            }
        });
        saveBillButton.setWidth(6.0f, Unit.CM);

        Button carryBillButton = new Button("Провести документ", new Button.ClickListener() {
            @Override
            public void buttonClick(Button.ClickEvent event) {

                document.setType(3);
                document.setChangeAmount(0.0);
                Contragents contr = new Contragents();
                for (Contragents allContragent : allContragents) {
                    if (allContragent.getId() == 6) {
                        contr = allContragent;
                    }
                }
                document.setContragents(contr);
                document.setCurrency("руб.");
                
                Set<DocumentGoods> tmp = new HashSet<>();
                tmp.addAll(documentGoodsItems);
                document.setDocumentGoodses(tmp);
                document.setFirm("И.П. Валиахметова Л.Н.");
                document.setIsCarried(true);
                document.setIsFullyPaid(true);
                document.setNumber(Integer.parseInt(docNumber.getValue()));
                document.setPaidAmount(0.0);
                document.setPayUntil(new Date());
                document.setSumm(0.0);
                document.setWarehouse("Основной склад");
                document.setUsers((Users) VaadinService.getCurrentRequest().getWrappedSession().getAttribute("user"));
                doCarryBill();
            }
        });
        carryBillButton.setWidth(6.0f, Unit.CM);

        isEditList = new CheckBox("Редактировать список");
        goodsControlLayout.addComponents(addGoodsButton, deleteGoodsButton, saveBillButton, carryBillButton, isEditList);
        goodsControlLayout.setSizeFull();
        goodsControlLayout.setMargin(true);
        goodsControlLayout.setComponentAlignment(isEditList, Alignment.MIDDLE_CENTER);
        goodsControl.setContent(goodsControlLayout);

        Panel goodsList = new Panel("Список товаров");
        billGrid = new VerticalLayout();
        billGoods = new BillGoodsGrid(documentGoodsItems);
        billGoods.setSelectionMode(Grid.SelectionMode.MULTI);
        billGoods.addItemClickListener(new ItemClickListener<DocumentGoods>() {
            @Override
            public void itemClick(Grid.ItemClick<DocumentGoods> event) {
                DocumentGoods item = event.getItem();
                if (isEditList.getValue() == false) {
                    goToEditDocumentGood(item);
                } else {
                    selectedItem = event.getItem();
                }
            }
        });
        billGoods.addSelectionListener(new SelectionListener<DocumentGoods>() {
            @Override
            public void selectionChange(SelectionEvent<DocumentGoods> event) {
                selected = event.getAllSelectedItems();
            }
        });
        billGoods.setSizeFull();
        billGrid.addComponent(billGoods);
        goodsList.setContent(billGrid);

        layout.addComponent(docHeader);
        docHeader.setWidth(32.0f, Unit.CM);
        layout.addComponent(goodsControl);
        goodsControl.setWidth(32.0f, Unit.CM);
        layout.addComponent(goodsList);
        goodsList.setWidth(32.0f, Unit.CM);
        layout.setSpacing(false);
        layout.setMargin(false);
        fullLayout.addComponents(layout);
        fullLayout.setMargin(true);
        fullLayout.setSpacing(true);

        this.setContent(fullLayout);
        this.center();
        this.setModal(true);
        this.setWidth(32.9f, Unit.CM);
        MyUI.getCurrent().addWindow(this);
    }

    private void goToAddGoods() {
        Window addGoodsWindow = new Window("Выбор товаров");
        VerticalLayout addGoodsWindowLayout = new VerticalLayout();
        AllGoodsGrid allGoodsGrid = new AllGoodsGrid(allGoods, allContragents, allGoodsTypes);
        addGoodsWindowLayout.addComponent(allGoodsGrid);
        allGoodsGrid.setHeight(16.0f, Unit.CM);
        HorizontalLayout buttons = new HorizontalLayout();
        Button doAddGoodsButton = new Button("Добавить выбранные", new Button.ClickListener() {
            @Override
            public void buttonClick(Button.ClickEvent event) {
                selectedGoods = allGoodsGrid.getSelectedGoods();
                doAddGoods();
                addGoodsWindow.close();
            }
        });

        Button doCancellButton = new Button("Отмена", new Button.ClickListener() {
            @Override
            public void buttonClick(Button.ClickEvent event) {
                if (selectedGoods != null) {
                    selectedGoods.clear();
                }
                addGoodsWindow.close();

            }
        });

        buttons.addComponents(doAddGoodsButton, doCancellButton);
        buttons.setComponentAlignment(doAddGoodsButton, Alignment.TOP_CENTER);
        buttons.setComponentAlignment(doCancellButton, Alignment.TOP_CENTER);
        buttons.setSizeFull();
        addGoodsWindowLayout.addComponent(buttons);
        addGoodsWindow.setContent(addGoodsWindowLayout);
        addGoodsWindow.center();
        addGoodsWindow.setModal(true);
        addGoodsWindow.setHeight(19.0f, Unit.CM);
        addGoodsWindow.setWidth(35.0f, Unit.CM);
        MyUI.getCurrent().addWindow(addGoodsWindow);
    }

    private void doAddGoods() {

        for (Goods selectedGood : selectedGoods) {
            DocumentGoods docGood = new DocumentGoods();
            List<Prices> goodPrices = stockHelper.getPricesByGoodId(selectedGood.getId());
            for (Prices goodPrice : goodPrices) {
                if (goodPrice.getPriceType().getId() == 1) {
                    docGood.setPrice(goodPrice.getResult());
                }
            }
            docGood.setGoods(selectedGood);
            docGood.setQuantity(0.0);
            docGood.setTotalValue(0.0);
            documentGoodsItems.add(docGood);
        }
        refreshBillGrid();
        selectedGoods.clear();

    }

    private void goToEditDocumentGood(DocumentGoods item) {
        Window editDocumentGoodWindow = new Window();
        VerticalLayout editDocumentGoodLayout = new VerticalLayout();

        selectedItem = item;

        Label itemName = new Label(item.getGoods().getName());

        TextField itemQuantity = new TextField("Количество", Double.toString(item.getQuantity()));

        Button save = new Button("Сохранить", new Button.ClickListener() {
            @Override
            public void buttonClick(Button.ClickEvent event) {
                item.setQuantity(Double.parseDouble(itemQuantity.getValue()));
                item.setTotalValue(0.0);

                GoodPricesController pc = new GoodPricesController(item.getGoods().getId());
                for (DocumentGoods documentGoodsItem : documentGoodsItems) {
                    if (documentGoodsItem.equals(item)) {
                        documentGoodsItem.setPrice(pc.getPurchasePrice());
                    }
                }
                refreshBillGrid();
                if (selectedGoods != null) {
                    selectedGoods.clear();
                }

                editDocumentGoodWindow.close();
            }
        });

        editDocumentGoodLayout.addComponents(itemName, itemQuantity, save);
        editDocumentGoodWindow.setContent(editDocumentGoodLayout);
        editDocumentGoodWindow.center();
        editDocumentGoodWindow.setModal(true);
        MyUI.getCurrent().addWindow(editDocumentGoodWindow);

    }

    private void deleteSelectedGoods() {
        //selected = this.billGoods.getSelectedItems();
        documentGoodsItems.removeAll(selected);
        refreshBillGrid();

    }

    private void refreshBillGrid() throws UnsupportedOperationException {
        billGrid.removeAllComponents();
        BillGoodsGrid billGoods = new BillGoodsGrid(documentGoodsItems);
        billGoods.setSelectionMode(Grid.SelectionMode.MULTI);
        billGoods.setSizeFull();
        billGoods.addItemClickListener(new ItemClickListener<DocumentGoods>() {
            @Override
            public void itemClick(Grid.ItemClick<DocumentGoods> event) {
                DocumentGoods item = event.getItem();
                if (isEditList.getValue() == false) {
                    goToEditDocumentGood(item);
                } else {
                    selectedItem = event.getItem();
                }
            }
        });
        billGoods.addSelectionListener(new SelectionListener<DocumentGoods>() {
            @Override
            public void selectionChange(SelectionEvent<DocumentGoods> event) {
                selected = event.getAllSelectedItems();
            }
        });
        ListDataProvider<DocumentGoods> prov = (ListDataProvider<DocumentGoods>) billGoods.getDataProvider();
        prov.refreshAll();
        billGrid.addComponent(billGoods);
    }

    private void doSaveBill() {
        stockHelper.updateDBItem(document);
        for (DocumentGoods documentGoodsItem : documentGoodsItems) {
            documentGoodsItem.setDocuments(document);
            stockHelper.saveOrUpdateDBItem(documentGoodsItem);
        }
        Page.getCurrent().reload();
    }

    private void doCarryBill() {

        stockHelper.updateDBItem(document);

        for (DocumentGoods oldGood : oldGoods) {
            DocumentGoods tmp = new DocumentGoods(-1);
            for (DocumentGoods documentGoodsItem : documentGoodsItems) {

                if (oldGood.getId() == documentGoodsItem.getId()) {
                    tmp = oldGood;

                }
            }
            if (tmp.getId() == -1) {
                stockHelper.deleteDBItem(oldGood);
                Double factor = oldGood.getGoods().getMainFactor();
                Boolean isInBasePackage = oldGood.getGoods().getUseBasePackage();
                if (isInBasePackage) {
                    oldGood.getGoods().setRemainder(oldGood.getGoods().getRemainder() - oldGood.getQuantity());
                    oldGood.getGoods().setFactRemainder(oldGood.getGoods().getFactRemainder() - oldGood.getQuantity());
                } else {
                    oldGood.getGoods().setRemainder(oldGood.getGoods().getRemainder() - oldGood.getQuantity() * factor);
                    oldGood.getGoods().setFactRemainder(oldGood.getGoods().getFactRemainder() - oldGood.getQuantity() * factor);
                }
                stockHelper.updateDBItem(oldGood.getGoods());
            }
        }
        oldGoods = stockHelper.getBillGoods(document.getId());
        for (DocumentGoods documentGoodsItem : documentGoodsItems) {
            Double change = 0.0;
            documentGoodsItem.setDocuments(document);
            stockHelper.saveOrUpdateDBItem(documentGoodsItem);

            Boolean isFound = false;
            DocumentGoods tmp = new DocumentGoods(-1);
            for (DocumentGoods oldGood : oldGoods) {
                if (oldGood.getId() == documentGoodsItem.getId()) {
                    tmp = oldGood;
                    isFound = true;
                }
            }

            if (isFound) {
                change = documentGoodsItem.getQuantity() - tmp.getQuantity();
                Double initialQuantity = documentGoodsItem.getGoods().getFactRemainder();
                Double initialRemainder = documentGoodsItem.getGoods().getRemainder();
                Double factor = documentGoodsItem.getGoods().getMainFactor();
                Boolean isInBasePackage = documentGoodsItem.getGoods().getUseBasePackage();
                if (isInBasePackage) {
                    documentGoodsItem.getGoods().setFactRemainder(initialQuantity + change);
                    documentGoodsItem.getGoods().setRemainder(initialRemainder + change);
                } else {
                    documentGoodsItem.getGoods().setFactRemainder(initialQuantity + change * factor);
                    documentGoodsItem.getGoods().setRemainder(initialRemainder + change * factor);
                }
            } else {
                Double quantity = documentGoodsItem.getQuantity();
                Double initialQuantity = documentGoodsItem.getGoods().getFactRemainder();
                Double initialRemainder = documentGoodsItem.getGoods().getRemainder();
                Double factor = documentGoodsItem.getGoods().getMainFactor();
                Boolean isInBasePackage = documentGoodsItem.getGoods().getUseBasePackage();
                if (isInBasePackage) {
                    documentGoodsItem.getGoods().setFactRemainder(initialQuantity + quantity);
                    documentGoodsItem.getGoods().setRemainder(initialRemainder + quantity);
                } else {
                    documentGoodsItem.getGoods().setFactRemainder(initialQuantity + quantity * factor);
                    documentGoodsItem.getGoods().setRemainder(initialRemainder + quantity * factor);
                }
            }
            

            stockHelper.updateDBItem(documentGoodsItem.getGoods());
        }
        Page.getCurrent().reload();
    }
}
