/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ru.funkdevelop.stockv.views.documents.windows;

import com.vaadin.data.HasValue;
import com.vaadin.data.ValueContext;
import com.vaadin.data.converter.LocalDateToDateConverter;
import com.vaadin.server.BrowserWindowOpener;
import com.vaadin.server.Page;
import com.vaadin.server.VaadinRequest;
import com.vaadin.server.VaadinService;
import com.vaadin.shared.ui.ContentMode;
import com.vaadin.ui.Alignment;
import com.vaadin.ui.Button;
import com.vaadin.ui.ComboBox;
import com.vaadin.ui.DateField;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Label;
import com.vaadin.ui.Panel;
import com.vaadin.ui.TextField;
import com.vaadin.ui.UI;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.Window;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.List;
import ru.funkdevelop.stockv.StockHelper;
import ru.funkdevelop.stockv.pojos.Contragents;
import ru.funkdevelop.stockv.pojos.Documents;
import ru.funkdevelop.stockv.pojos.Users;
import ru.funkdevelop.stockv.views.documents.CashReceiptView;
import ru.funkdevelop.stockv.views.service.RussianMoney;

/**
 *
 * @author artur
 */
public class CashReceiptWindow extends Window {
    private Documents document;
    private List<Contragents> allContragents;
    private List<Documents> allDocuments;
    private StockHelper stockHelper;
    private HashMap<String, Documents> documentsMap;
    private HashMap<String, Contragents> contragentsMap;

    public CashReceiptWindow(Documents document) {
        this.document = document;
        this.stockHelper = new StockHelper();
        this.allContragents = stockHelper.getContragents();
        this.allDocuments = stockHelper.getAllDocuments();
        this.documentsMap = new HashMap<>();
        this.contragentsMap = new HashMap<>();
        for (Contragents allContragent : this.allContragents) {
            contragentsMap.put(allContragent.getOfficialName(), allContragent);
        }
        
        for (Documents documentItem : allDocuments) {
                    if (documentItem.getContragents().getId() == document.getContragents().getId() & documentItem.getType() == 2) {
                        SimpleDateFormat format = new SimpleDateFormat("dd.MM.yyyy");
                        String key = "Реализация (купля-продажа) № " + String.valueOf(documentItem.getNumber()) + " от "
                                + format.format(documentItem.getDate()) + " на сумму "
                                + String.valueOf(documentItem.getSumm()) + " руб.";
                        documentsMap.put(key, documentItem);
                    }
                }
                
        
        this.setCaption("Приходный кассовый ордер № " + String.valueOf(this.document.getNumber()));
        VerticalLayout docBody = new VerticalLayout();
        Panel header = new Panel("Основное");
        HorizontalLayout headerLayout = new HorizontalLayout();
        TextField docType = new TextField("Тип документа", "ПКО");
        docType.setEnabled(false);
        docType.setWidth(15.0f, Unit.CM);
        TextField docNumber = new TextField("Номер", String.valueOf(this.document.getNumber()));
        docNumber.setEnabled(false);
        docNumber.setWidth(15.0f, Unit.CM);
        LocalDateToDateConverter dateConverter = new LocalDateToDateConverter();
        DateField docDate = new DateField("Дата", dateConverter.convertToPresentation(this.document.getDate(), new ValueContext()));
        docDate.setWidth(15.0f, Unit.CM);
        docDate.setDateFormat("dd.MM.yyyy");

        ComboBox selectBill = new ComboBox("Основание");
        selectBill.setEmptySelectionAllowed(false);
        selectBill.setWidth(15.0f, Unit.CM);
        selectBill.setValue((String)this.document.getWarehouse());
        selectBill.setItems(documentsMap.keySet());
        VerticalLayout column1 = new VerticalLayout();
        column1.addComponents(docType, selectBill);
        column1.setComponentAlignment(docType, Alignment.TOP_CENTER);
        column1.setComponentAlignment(selectBill, Alignment.TOP_CENTER);
        column1.setMargin(false);
        VerticalLayout column2 = new VerticalLayout();
        column2.addComponents(docNumber, docDate);
        column2.setComponentAlignment(docNumber, Alignment.TOP_CENTER);
        column2.setComponentAlignment(docDate, Alignment.TOP_CENTER);
        column2.setMargin(false);
        headerLayout.addComponents(column1, column2);
        headerLayout.setSpacing(false);
        headerLayout.setMargin(true);
        headerLayout.setComponentAlignment(column1, Alignment.TOP_CENTER);
        headerLayout.setComponentAlignment(column2, Alignment.TOP_CENTER);
        headerLayout.setSizeFull();

        header.setContent(headerLayout);

        Panel receiverData = new Panel("Получатель");
        HorizontalLayout receiverDataLayout = new HorizontalLayout();
        TextField docFirm = new TextField("Фирма", "И.П. Валиахметова Л.Н.");
        docFirm.setEnabled(false);
        docFirm.setWidth(15.0f, Unit.CM);
        TextField cashBox = new TextField("Касса", "Основная касса");
        cashBox.setEnabled(false);
        cashBox.setWidth(15.0f, Unit.CM);
        receiverDataLayout.addComponents(docFirm, cashBox);
        receiverDataLayout.setSpacing(true);
        receiverDataLayout.setMargin(true);
        receiverDataLayout.setSizeFull();
        receiverDataLayout.setComponentAlignment(docFirm, Alignment.TOP_CENTER);
        receiverDataLayout.setComponentAlignment(cashBox, Alignment.TOP_CENTER);
        receiverData.setContent(receiverDataLayout);

        Panel payerData = new Panel("Плательщик");
        HorizontalLayout payerDataLayout = new HorizontalLayout();
        ComboBox contragentSelect = new ComboBox("Контрагент");
        contragentSelect.setItems(contragentsMap.keySet());
        contragentSelect.setEmptySelectionAllowed(false);
        contragentSelect.setWidth(15.0f, Unit.CM);
        contragentSelect.setEnabled(false);
        contragentSelect.setValue((String) this.document.getContragents().getOfficialName());
        TextField contragentContract = new TextField("Договор");
        contragentContract.setEnabled(false);
        contragentContract.setWidth(15.0f, Unit.CM);
        TextField contragentCredit = new TextField("Долг контрагента");
        contragentCredit.setEnabled(false);
        contragentCredit.setWidth(15.0f, Unit.CM);
        contragentSelect.addValueChangeListener(new HasValue.ValueChangeListener() {
            @Override
            public void valueChange(HasValue.ValueChangeEvent event) {
                contragentContract.setValue(contragentsMap.get((String) event.getValue()).getMainContract());
                contragentCredit.setValue(String.valueOf(contragentsMap.get((String) event.getValue()).getCredit()));
                for (Documents document : allDocuments) {
                    if (document.getContragents().getId() == contragentsMap.get((String) event.getValue()).getId() & document.getType() == 2) {
                        SimpleDateFormat format = new SimpleDateFormat("dd.MM.yyyy");
                        String key = "Реализация (купля-продажа) № " + String.valueOf(document.getNumber()) + " от "
                                + format.format(document.getDate()) + " на сумму "
                                + String.valueOf(document.getSumm()) + " руб.";
                        documentsMap.put(key, document);
                    }
                }
                selectBill.setItems(documentsMap.keySet());
            }
        });
        
        contragentContract.setValue(contragentsMap.get((String) contragentSelect.getValue()).getMainContract());
                contragentCredit.setValue(String.valueOf(contragentsMap.get((String) contragentSelect.getValue()).getCredit()));
                for (Documents alldocument : allDocuments) {
                    if (alldocument.getContragents().getId() == contragentsMap.get((String) contragentSelect.getValue()).getId() & alldocument.getType() == 2) {
                        SimpleDateFormat format = new SimpleDateFormat("dd.MM.yyyy");
                        String key = "Реализация (купля-продажа) № " + String.valueOf(alldocument.getNumber()) + " от "
                                + format.format(alldocument.getDate()) + " на сумму "
                                + String.valueOf(alldocument.getSumm()) + " руб.";
                        documentsMap.put(key, alldocument);
                    }
                }
                selectBill.setItems(documentsMap.keySet());
                
        TextField paymentSize = new TextField("Размер платежа", String.valueOf(this.document.getSumm()));
        selectBill.addValueChangeListener(new HasValue.ValueChangeListener() {
            @Override
            public void valueChange(HasValue.ValueChangeEvent event) {
                Documents reason = (Documents) documentsMap.get(event.getValue());
                paymentSize.setValue(String.valueOf(reason.getSumm() - (reason.getPaidAmount() - reason.getChangeAmount())));
            }
        });
        paymentSize.addValueChangeListener(new HasValue.ValueChangeListener<String>() {
            @Override
            public void valueChange(HasValue.ValueChangeEvent<String> event) {
                if (!selectBill.isEmpty()) {
                    Documents reason = (Documents) documentsMap.get((String)selectBill.getValue());
                    if (Double.parseDouble(event.getValue()) >= reason.getSumm() - (reason.getPaidAmount() - reason.getChangeAmount()))
                        paymentSize.setValue(String.valueOf(reason.getSumm() - (reason.getPaidAmount() - reason.getChangeAmount())));
                    
                }
            }
        });
        paymentSize.setWidth(15.0f, Unit.CM);

        payerDataLayout.setSpacing(true);
        payerDataLayout.setMargin(true);
        payerDataLayout.setSizeFull();

        VerticalLayout payerColumn1 = new VerticalLayout();
        payerColumn1.addComponents(contragentSelect, contragentContract);
        payerColumn1.setMargin(false);
        payerColumn1.setComponentAlignment(contragentSelect, Alignment.TOP_CENTER);
        payerColumn1.setComponentAlignment(contragentContract, Alignment.TOP_CENTER);
        VerticalLayout payerColumn2 = new VerticalLayout();
        payerColumn2.addComponents(contragentCredit, paymentSize);
        payerColumn2.setMargin(false);
        payerColumn2.setComponentAlignment(contragentCredit, Alignment.TOP_CENTER);
        payerColumn2.setComponentAlignment(paymentSize, Alignment.TOP_CENTER);
        payerDataLayout.addComponents(payerColumn1, payerColumn2);
        payerDataLayout.setComponentAlignment(payerColumn1, Alignment.TOP_CENTER);
        payerDataLayout.setComponentAlignment(payerColumn2, Alignment.TOP_CENTER);
        payerDataLayout.setSpacing(false);
        payerDataLayout.setMargin(true);
        payerDataLayout.setSizeFull();
        payerData.setContent(payerDataLayout);

        Panel control = new Panel("Управление");
        HorizontalLayout controlLayout = new HorizontalLayout();
        Button saveButton = new Button("Сохранить документ", new Button.ClickListener() {
            @Override
            public void buttonClick(Button.ClickEvent event) {
                document.setChangeAmount(0.0);
                document.setContragents(contragentsMap.get(contragentSelect.getValue()));
                document.setCurrency("руб.");
                Date date = new Date();
                Date time = new Date();
                Calendar tmp = new GregorianCalendar();
                tmp.set(docDate.getValue().getYear(), docDate.getValue().getMonthValue() - 1, docDate.getValue().getDayOfMonth());
                date.setTime(tmp.getTimeInMillis());
                document.setDate(date);
                document.setFirm(docFirm.getValue());
                document.setIsCarried(false);
                document.setIsFullyPaid(true);
                document.setNumber(Integer.parseInt(docNumber.getValue()));
                document.setPaidAmount(Double.parseDouble(paymentSize.getValue()));
                document.setPayUntil(date);
                document.setTime(time);
                document.setType(5);
                document.setUsers((Users) VaadinService.getCurrentRequest().getWrappedSession().getAttribute("user"));
                document.setWarehouse((String) selectBill.getValue());
                document.setSumm(Double.parseDouble(paymentSize.getValue()));
                stockHelper.updateDBItem(document);
            }
        });
        saveButton.setWidth(7.5f, Unit.CM);
        Button printButton = new Button("Печать");
        Button carryButton = new Button("Провести документ", new Button.ClickListener() {
            @Override
            public void buttonClick(Button.ClickEvent event) {
                Double oldPayed;
                Boolean isCarried = false;
                if (document.getIsCarried() != null) {
                    isCarried = document.getIsCarried();
                }
                if (isCarried) {
                    oldPayed = document.getSumm();
                    if (oldPayed == null) {
                        oldPayed = 0.0;
                    }
                } else {
                    oldPayed = 0.0;
                }
                document.setChangeAmount(0.0);
                document.setContragents(contragentsMap.get((String) contragentSelect.getValue()));
                document.setCurrency("руб.");
                Date date = new Date();
                Date time = new Date();
                Calendar tmp = new GregorianCalendar();
                tmp.set(docDate.getValue().getYear(), docDate.getValue().getMonthValue() - 1, docDate.getValue().getDayOfMonth());
                date.setTime(tmp.getTimeInMillis());
                document.setDate(date);
                document.setFirm(docFirm.getValue());
                document.setIsCarried(true);
                document.setIsFullyPaid(true);
                document.setNumber(Integer.parseInt(docNumber.getValue()));
                document.setPaidAmount(Double.parseDouble(paymentSize.getValue()));
                document.setPayUntil(date);
                document.setTime(time);
                document.setType(5);
                document.setUsers((Users) VaadinService.getCurrentRequest().getWrappedSession().getAttribute("user"));
                document.setWarehouse((String) selectBill.getValue());
                document.setSumm(Double.parseDouble(paymentSize.getValue()));
                stockHelper.updateDBItem(document);

                int reasonId = documentsMap.get((String) selectBill.getValue()).getId();
                Documents reason = stockHelper.getDocumentById(reasonId);
                reason.setPaidAmount(reason.getPaidAmount() + document.getSumm());
                stockHelper.updateDBItem(reason);
                
                Double paySize = document.getSumm();
                Contragents contragent = document.getContragents();
                contragent.setCredit(contragent.getCredit() - (paySize - oldPayed));
                if (contragent.getCredit() < 0.0) {
                    contragent.setCredit(0.0);
                }
                stockHelper.updateDBItem(contragent);
                printButton.setEnabled(true);
            }
        });
        if (document.getIsCarried()) {
            carryButton.setEnabled(false);
            saveButton.setEnabled(false);
        }
        carryButton.setWidth(7.5f, Unit.CM);

        BrowserWindowOpener opener
                = new BrowserWindowOpener(CashReceiptView.MyPopupUI.class);
        opener.setFeatures("resizable");

        opener.extend(printButton);
        printButton.setWidth(7.5f, Unit.CM);
        printButton.setEnabled(this.document.getIsCarried());

        Button deleteButton = new Button("Удалить", new Button.ClickListener() {
            @Override
            public void buttonClick(Button.ClickEvent event) {
                String reasonTxt = document.getWarehouse();
                String reasonId = reasonTxt.substring(reasonTxt.indexOf("№ ") + 2);
                reasonId = reasonId.substring(0, reasonId.indexOf(" "));
                String[] reasonArr = reasonTxt.split(" ");
                int id = Integer.parseInt(reasonId);
                Documents reasonDoc;
                reasonDoc = stockHelper.getDocumentById(id);
                reasonDoc.setPaidAmount(reasonDoc.getPaidAmount() - document.getSumm());
                document.getContragents().setCredit(document.getContragents().getCredit() + document.getSumm());
                stockHelper.updateDBItem(reasonDoc);
                stockHelper.updateDBItem(document.getContragents());
                stockHelper.deleteDBItem(document);
                UI.getCurrent().getPage().reload();
            }
        });
        deleteButton.setWidth(7.5f, Unit.CM);
        controlLayout.addComponents(saveButton, carryButton, printButton, deleteButton);
        controlLayout.setSizeFull();
        controlLayout.setMargin(true);
        controlLayout.setSpacing(true);
        controlLayout.setComponentAlignment(saveButton, Alignment.TOP_CENTER);
        controlLayout.setComponentAlignment(carryButton, Alignment.TOP_CENTER);
        controlLayout.setComponentAlignment(printButton, Alignment.TOP_CENTER);
        control.setContent(controlLayout);
        docBody.addComponents(header, receiverData, payerData, control);
        docBody.setWidth(32.0f, Unit.CM);
        docBody.setMargin(false);
        docBody.setSpacing(false);
        
        this.setContent(docBody);
        this.setModal(true);
        this.center();
        UI.getCurrent().addWindow(this);
    }
    
    public static class MyPopupUI extends UI {

        private StockHelper stockHelper = new StockHelper();
        private int docId = stockHelper.getMaxBillId();

        private Documents bill = stockHelper.getDocumentById(docId);

        @Override
        public void init(VaadinRequest request) {
            getPage().setTitle("Печать приходного кассового ордера");
            SimpleDateFormat format = new SimpleDateFormat("dd.MM.yyyy");
            // Have some content for it
            VerticalLayout content = new VerticalLayout();

            String number = String.valueOf(bill.getNumber());
            String date = format.format(bill.getDate());
            String summ = String.valueOf(bill.getSumm());
            String contragentName = bill.getContragents().getOfficialName();
            String reason = bill.getWarehouse();
            String tmpReason1 = reason.substring(0, reason.lastIndexOf(")") + 1) + "<br/>";
            String tmpReason2 = reason.substring(reason.lastIndexOf(")") + 1);
            reason = tmpReason1 + tmpReason2;
            String summLiteral = RussianMoney.digits2text2Lines(bill.getSumm());
            String day = date.substring(0, 2);
            String month = getMonthNumber(bill.getDate());
            String year = date.substring(6);
            String summRubles = summ.substring(0, summ.lastIndexOf("."));
            String summKop = summ.substring(summ.lastIndexOf(".") + 1);
            String data = "<!DOCTYPE HTML PUBLIC \"-//W3C//DTD HTML 4.0 Transitional//EN\">\n"
                    + "\n"
                    + "<html>\n"
                    + "<head>\n"
                    + "	\n"
                    + "	<meta http-equiv=\"content-type\" content=\"text/html; charset=utf-8\"/>\n"
                    + "	<title></title>\n"
                    + "	<meta name=\"generator\" content=\"LibreOffice 5.1.6.2 (Linux)\"/>\n"
                    + "	<meta name=\"author\" content=\" Бланкер.ру\"/>\n"
                    + "	<meta name=\"created\" content=\"2003-05-08T11:01:28\"/>\n"
                    + "	<meta name=\"changed\" content=\"2017-07-26T14:27:22.108470816\"/>\n"
                    + "	\n"
                    + "	<style type=\"text/css\">\n"
                    + "		body,div,table,thead,tbody,tfoot,tr,th,td,p { font-family:\"Arial Cyr\"; font-size:x-small }\n"
                    + "		a.comment-indicator:hover + comment { background:#ffd; position:absolute; display:block; border:1px solid black; padding:0.5em;  } \n"
                    + "		a.comment-indicator { background:red; display:inline-block; border:1px solid black; width:0.5em; height:0.5em;  } \n"
                    + "		comment { display:none;  }\n"
                    + "		@page {\n"
                    + "			margin: 0cm;\n"
                    + "			overflow: visible;\n"
                    + "		} \n"
                    + "	</style>\n"
                    + "	\n"
                    + "</head>\n"
                    + "\n"
                    + "<body>\n"
                    + "<table cellspacing=\"0\" border=\"0\">\n"
                    + "	<colgroup span=\"73\" width=\"6\"></colgroup>\n"
                    + "	<colgroup width=\"4\"></colgroup>\n"
                    + "	<colgroup span=\"8\" width=\"6\"></colgroup>\n"
                    + "	<colgroup width=\"11\"></colgroup>\n"
                    + "	<colgroup span=\"17\" width=\"6\"></colgroup>\n"
                    + "	<colgroup width=\"11\"></colgroup>\n"
                    + "	<colgroup span=\"13\" width=\"6\"></colgroup>\n"
                    + "	<tr>\n"
                    + "		<td colspan=65 height=\"14\" align=\"right\" valign=middle><font face=\"Times New Roman\" size=1>Унифицированная форма № КО-1</font></td>\n"
                    + "		<td rowspan=35 align=\"center\" valign=middle><font face=\"Times New Roman\" size=1><br></font></td>\n"
                    + "		<td style=\"border-left: 1px solid #3c3c3c; border-right: 1px hidden #3c3c3c\" colspan=5 rowspan=35 align=\"center\" valign=middle><font face=\"Times New Roman\">Л<br/>и<br/>н<br/>и<br/>я<br/><br/>о<br/>т<br/>р<br/>е<br/>з<br/>а</font></td>\n"
                    + "		<td colspan=2 rowspan=35 align=\"center\" valign=middle><font face=\"Times New Roman\" size=1><br></font></td>\n"
                    + "		<td rowspan=35 align=\"center\" valign=middle><font face=\"Times New Roman\" size=1><br></font></td>\n"
                    + "		<td colspan=40 rowspan=2 align=\"center\" valign=middle><font face=\"Times New Roman\" size=1><br></font></td>\n"
                    + "		</tr>\n"
                    + "	<tr>\n"
                    + "		<td colspan=65 height=\"14\" align=\"right\" valign=middle><font face=\"Times New Roman\" size=1>Утверждена постановлением Госкомстата</font></td>\n"
                    + "		</tr>\n"
                    + "	<tr>\n"
                    + "		<td colspan=65 height=\"14\" align=\"right\" valign=middle><font face=\"Times New Roman\" size=1>России от 18.08.98 № 88</font></td>\n"
                    + "		<td style=\"border-bottom: 1px solid #3c3c3c\" colspan=40 align=\"center\"><font face=\"Times New Roman\" size=2>И.П. Валиахметова Л.Н.</font></td>\n"
                    + "		</tr>\n"
                    + "	<tr>\n"
                    + "		<td colspan=65 height=\"17\" align=\"center\" valign=middle><font face=\"Times New Roman\" size=1></font></td>\n"
                    + "		<td style=\"border-top: 1px solid #3c3c3c\" colspan=40 align=\"center\"><font face=\"Times New Roman\" size=1>(организация)</font></td>\n"
                    + "		</tr>\n"
                    + "	<tr>\n"
                    + "		<td colspan=50 height=\"20\" align=\"center\" valign=middle><font face=\"Times New Roman\" size=1><br></font></td>\n"
                    + "		<td style=\"border-top: 1px solid #3c3c3c; border-left: 1px solid #3c3c3c; border-right: 1px solid #3c3c3c\" colspan=15 align=\"center\"><font face=\"Times New Roman\" size=1>Код</font></td>\n"
                    + "		<td colspan=40 align=\"center\" valign=middle><font face=\"Times New Roman\" size=1><br></font></td>\n"
                    + "		</tr>\n"
                    + "	<tr>\n"
                    + "		<td colspan=50 height=\"20\" align=\"right\" valign=middle><font face=\"Times New Roman\" size=1>Форма по ОКУД</font></td>\n"
                    + "		<td style=\"border-top: 2px solid #3c3c3c; border-bottom: 1px solid #3c3c3c; border-left: 2px solid #3c3c3c; border-right: 2px solid #3c3c3c\" colspan=15 align=\"center\" sdnum=\"1049;0;@\"><font face=\"Times New Roman\" size=1>0310001</font></td>\n"
                    + "		<td colspan=40 align=\"center\"><b><font face=\"Times New Roman\">КВИТАНЦИЯ</font></b></td>\n"
                    + "		</tr>\n"
                    + "	<tr>\n"
                    + "		<td style=\"border-bottom: 1px solid #3c3c3c\" colspan=39 height=\"20\" align=\"center\"><font face=\"Times New Roman\" size=2>И.П. Валиахметова Л.Н.</font></td>\n"
                    + "		<td colspan=11 align=\"right\" valign=middle><font face=\"Times New Roman\" size=1>по ОКПО</font></td>\n"
                    + "		<td style=\"border-top: 1px solid #3c3c3c; border-bottom: 1px solid #3c3c3c; border-left: 2px solid #3c3c3c; border-right: 2px solid #3c3c3c\" colspan=15 align=\"center\" sdnum=\"1049;0;@\"><font face=\"Times New Roman\" size=1><br></font></td>\n"
                    + "		<td colspan=40 rowspan=2 align=\"center\" valign=middle><font face=\"Times New Roman\" size=1><br></font></td>\n"
                    + "		</tr>\n"
                    + "	<tr>\n"
                    + "		<td style=\"border-top: 1px solid #3c3c3c\" colspan=39 height=\"17\" align=\"center\"><font face=\"Times New Roman\" size=1>(организация)</font></td>\n"
                    + "		<td colspan=11 align=\"center\" valign=middle><font face=\"Times New Roman\" size=1><br></font></td>\n"
                    + "		<td style=\"border-top: 1px solid #3c3c3c; border-bottom: 2px solid #3c3c3c; border-left: 2px solid #3c3c3c; border-right: 2px solid #3c3c3c\" colspan=15 rowspan=2 align=\"center\" sdnum=\"1049;0;@\"><font face=\"Times New Roman\" size=1><br></font></td>\n"
                    + "		</tr>\n"
                    + "	<tr>\n"
                    + "		<td style=\"border-bottom: 1px solid #3c3c3c\" colspan=50 height=\"20\" align=\"center\"><font face=\"Times New Roman\" size=1><br></font></td>\n"
                    + "		<td colspan=27 align=\"right\" valign=middle><font face=\"Times New Roman\" size=1>к приходному кассовому ордеру №</font></td>\n"
                    + "		<td style=\"border-bottom: 1px solid #3c3c3c\" colspan=13 align=\"center\" sdnum=\"1049;0;@\"><font face=\"Times New Roman\" size=1>" + number + "</font></td>\n"
                    + "		</tr>\n"
                    + "	<tr>\n"
                    + "		<td style=\"border-top: 1px solid #3c3c3c\" colspan=50 height=\"20\" align=\"center\" valign=top><font face=\"Times New Roman\" size=1>(структурное подразделение)</font></td>\n"
                    + "		<td colspan=15 align=\"center\" valign=middle><font face=\"Times New Roman\" size=1><br></font></td>\n"
                    + "		<td colspan=4 align=\"right\" valign=middle><font face=\"Times New Roman\" size=1>от  &quot;</font></td>\n"
                    + "		<td style=\"border-bottom: 1px solid #3c3c3c\" colspan=4 align=\"center\" sdnum=\"1049;0;@\"><font face=\"Times New Roman\" size=1>" + day + "</font></td>\n"
                    + "		<td colspan=2 align=\"center\" valign=middle><font face=\"Times New Roman\" size=1>&quot;</font></td>\n"
                    + "		<td style=\"border-bottom: 1px solid #3c3c3c\" colspan=15 align=\"center\"><font face=\"Times New Roman\" size=1>" + month + "</font></td>\n"
                    + "		<td align=\"left\"><font face=\"Times New Roman\" size=1><br></font></td>\n"
                    + "		<td style=\"border-bottom: 1px solid #3c3c3c\" colspan=6 align=\"center\"><font face=\"Times New Roman\" size=1>" + year + "</font></td>\n"
                    + "		<td colspan=8 align=\"left\" valign=middle><font face=\"Times New Roman\" size=1> г.</font></td>\n"
                    + "		</tr>\n"
                    + "	<tr>\n"
                    + "		<td colspan=42 rowspan=2 height=\"34\" align=\"center\" valign=middle><font face=\"Times New Roman\" size=1><br></font></td>\n"
                    + "		<td style=\"border-top: 1px solid #3c3c3c; border-left: 1px solid #3c3c3c; border-right: 1px solid #3c3c3c\" colspan=11 rowspan=2 align=\"center\" valign=top><font face=\"Times New Roman\" size=1>Номер<br/>документа</font></td>\n"
                    + "		<td style=\"border-top: 1px solid #3c3c3c; border-left: 1px solid #3c3c3c; border-right: 1px solid #3c3c3c\" colspan=12 rowspan=2 align=\"center\" valign=top><font face=\"Times New Roman\" size=1>Дата<br/>составления</font></td>\n"
                    + "		<td colspan=40 align=\"center\" valign=middle><font face=\"Times New Roman\" size=1><br></font></td>\n"
                    + "		</tr>\n"
                    + "	<tr>\n"
                    + "		<td colspan=9 align=\"left\" valign=middle><font face=\"Times New Roman\" size=1>Принято от</font></td>\n"
                    + "		<td style=\"border-bottom: 1px solid #3c3c3c\" colspan=31 align=\"center\"><font face=\"Times New Roman\" size=1>" + contragentName + "</font></td>\n"
                    + "		</tr>\n"
                    + "	<tr>\n"
                    + "		<td colspan=42 height=\"20\" align=\"center\" valign=middle><font face=\"Times New Roman\" size=1>ПРИХОДНЫЙ КАССОВЫЙ ОРДЕР</font></td>\n"
                    + "		<td style=\"border-top: 2px solid #3c3c3c; border-bottom: 2px solid #3c3c3c; border-left: 2px solid #3c3c3c; border-right: 1px solid #3c3c3c\" colspan=11 align=\"center\" sdnum=\"1049;0;@\"><b><font face=\"Times New Roman\">" + number + "</font></b></td>\n"
                    + "		<td style=\"border-top: 2px solid #3c3c3c; border-bottom: 2px solid #3c3c3c; border-left: 1px solid #3c3c3c; border-right: 2px solid #3c3c3c\" colspan=12 align=\"center\" sdnum=\"1049;0;@\"><b><font face=\"Times New Roman\">" + date + "</font></b></td>\n"
                    //                    + "		<td style=\"border-bottom: 1px solid #3c3c3c\" colspan=40 align=\"center\"><font face=\"Times New Roman\" size=1><br></font></td>\n"
                    + "		</tr>\n"
                    + "	<tr>\n"
                    + "		<td colspan=65 height=\"20\" align=\"center\" valign=middle><font face=\"Times New Roman\" size=1><br></font></td>\n"
                    + "		<td colspan=9 align=\"left\" valign=middle><font face=\"Times New Roman\" size=1>Основание:</font></td>\n"
                    + "		<td style=\"border-top: 1px solid #3c3c3c; border-bottom: 1px solid #3c3c3c\" colspan=31 align=\"center\"><font face=\"Times New Roman\" size=1>" + reason + "</font></td>\n"
                    + "		</tr>\n"
                    + "	<tr>\n"
                    + "		<td style=\"border-top: 1px solid #3c3c3c; border-left: 1px solid #3c3c3c; border-right: 1px solid #3c3c3c\" colspan=6 rowspan=4 height=\"76\" align=\"center\" valign=top><font face=\"Times New Roman\" size=1>Дебет</font></td>\n"
                    + "		<td style=\"border-top: 1px solid #3c3c3c; border-bottom: 1px solid #3c3c3c; border-left: 1px solid #3c3c3c; border-right: 1px solid #3c3c3c\" colspan=34 align=\"center\" valign=top><font face=\"Times New Roman\" size=1>Кредит</font></td>\n"
                    + "		<td style=\"border-top: 1px solid #3c3c3c; border-left: 1px solid #3c3c3c; border-right: 1px solid #3c3c3c\" colspan=9 rowspan=4 align=\"center\" valign=top><font face=\"Times New Roman\" size=1>Сумма,<br>руб. коп.</font></td>\n"
                    + "		<td style=\"border-top: 1px solid #3c3c3c; border-left: 1px solid #3c3c3c; border-right: 1px solid #3c3c3c\" colspan=10 rowspan=4 align=\"center\" valign=top><font face=\"Times New Roman\" size=1>Код<br/>целевого<br/>назначения</font></td>\n"
                    + "		<td style=\"border-top: 1px solid #3c3c3c; border-left: 1px solid #3c3c3c; border-right: 1px solid #3c3c3c\" colspan=6 rowspan=4 align=\"center\" valign=top><font face=\"Times New Roman\" size=1><br></font></td>\n"
                    //                    + "		<td style=\"border-bottom: 1px solid #3c3c3c\" colspan=40 align=\"center\"><font face=\"Times New Roman\" size=1><br></font></td>\n"
                    + "		</tr>\n"
                    + "	<tr>\n"
                    + "		<td style=\"border-top: 1px solid #3c3c3c; border-left: 1px solid #3c3c3c; border-right: 1px solid #3c3c3c\" colspan=4 rowspan=3 align=\"center\" valign=top><font face=\"Times New Roman\" size=1><br></font></td>\n"
                    + "		<td style=\"border-top: 1px solid #3c3c3c; border-left: 1px solid #3c3c3c; border-right: 1px solid #3c3c3c\" colspan=9 rowspan=3 align=\"center\" valign=top><font face=\"Times New Roman\" size=1>код<br/>структурного<br/>подразделения</font></td>\n"
                    + "		<td style=\"border-top: 1px solid #3c3c3c; border-left: 1px solid #3c3c3c; border-right: 1px solid #3c3c3c\" colspan=10 rowspan=3 align=\"center\" valign=top><font face=\"Times New Roman\" size=1>корреспондирующий<br/>счет,<br/>субсчет</font></td>\n"
                    + "		<td style=\"border-top: 1px solid #3c3c3c; border-left: 1px solid #3c3c3c; border-right: 1px solid #3c3c3c\" colspan=11 rowspan=3 align=\"center\" valign=top><font face=\"Times New Roman\" size=1>код<br/>аналитического<br/>учета</font></td>\n"
                    //                    + "		<td style=\"border-top: 1px solid #3c3c3c; border-bottom: 1px solid #3c3c3c\" colspan=40 align=\"center\"><font face=\"Times New Roman\" size=1><br></font></td>\n"
                    + "		</tr>\n"
                    + "	<tr>\n"
                    //                    + "		<td style=\"border-top: 1px solid #3c3c3c; border-bottom: 1px solid #3c3c3c\" colspan=40 align=\"center\"><font face=\"Times New Roman\" size=1>test</font></td>\n"
                    + "		</tr>\n"
                    + "	<tr>\n"
                    //                    + "		<td colspan=40 align=\"center\" valign=middle><font face=\"Times New Roman\" size=1><br></font></td>\n"
                    + "		</tr>\n"
                    + "	<tr>\n"
                    + "		<td style=\"border-top: 2px solid #3c3c3c; border-bottom: 2px solid #3c3c3c; border-left: 2px solid #3c3c3c; border-right: 1px solid #3c3c3c\" colspan=6 height=\"27\" align=\"center\"><font face=\"Times New Roman\" size=1><br></font></td>\n"
                    + "		<td style=\"border-top: 2px solid #3c3c3c; border-bottom: 2px solid #3c3c3c; border-left: 1px solid #3c3c3c; border-right: 1px solid #3c3c3c\" colspan=4 align=\"center\"><font face=\"Times New Roman\" size=1><br></font></td>\n"
                    + "		<td style=\"border-top: 2px solid #3c3c3c; border-bottom: 2px solid #3c3c3c; border-left: 1px solid #3c3c3c; border-right: 1px solid #3c3c3c\" colspan=9 align=\"center\" sdnum=\"1049;0;@\"><font face=\"Times New Roman\" size=1><br></font></td>\n"
                    + "		<td style=\"border-top: 2px solid #3c3c3c; border-bottom: 2px solid #3c3c3c; border-left: 1px solid #3c3c3c; border-right: 1px solid #3c3c3c\" colspan=10 align=\"center\" sdnum=\"1049;0;@\"><font face=\"Times New Roman\" size=1><br></font></td>\n"
                    + "		<td style=\"border-top: 2px solid #3c3c3c; border-bottom: 2px solid #3c3c3c; border-left: 1px solid #3c3c3c; border-right: 1px solid #3c3c3c\" colspan=11 align=\"center\" sdnum=\"1049;0;@\"><font face=\"Times New Roman\" size=1><br></font></td>\n"
                    + "		<td style=\"border-top: 2px solid #3c3c3c; border-bottom: 2px solid #3c3c3c; border-left: 1px solid #3c3c3c; border-right: 1px solid #3c3c3c\" colspan=9 align=\"center\"><font face=\"Times New Roman\" size=1>" + summ + "</font></td>\n"
                    + "		<td style=\"border-top: 2px solid #3c3c3c; border-bottom: 2px solid #3c3c3c; border-left: 1px solid #3c3c3c; border-right: 1px solid #3c3c3c\" colspan=10 align=\"center\" sdnum=\"1049;0;@\"><font face=\"Times New Roman\" size=1><br></font></td>\n"
                    + "		<td style=\"border-top: 2px solid #3c3c3c; border-bottom: 2px solid #3c3c3c; border-left: 1px solid #3c3c3c; border-right: 2px solid #3c3c3c\" colspan=6 align=\"center\"><font face=\"Times New Roman\" size=1><br></font></td>\n"
                    + "		<td colspan=6 align=\"center\" valign=middle><font face=\"Times New Roman\" size=1>Сумма</font></td>\n"
                    + "		<td style=\"border-bottom: 1px solid #3c3c3c\" colspan=17 align=\"center\"><font face=\"Times New Roman\" size=1>" + summRubles + "</font></td>\n"
                    + "		<td colspan=5 align=\"center\" valign=middle><font face=\"Times New Roman\" size=1>руб.</font></td>\n"
                    + "		<td style=\"border-bottom: 1px solid #3c3c3c\" colspan=5 align=\"center\"><font face=\"Times New Roman\" size=1>" + summKop + "</font></td>\n"
                    + "		<td colspan=7 align=\"center\" valign=middle><font face=\"Times New Roman\" size=1> коп.</font></td>\n"
                    + "		</tr>\n"
                    + "	<tr>\n"
                    + "		<td colspan=65 height=\"17\" align=\"center\" valign=middle><font face=\"Times New Roman\" size=1><br></font></td>\n"
                    + "		<td colspan=6 align=\"center\" valign=middle><font face=\"Times New Roman\" size=1><br></font></td>\n"
                    + "		<td style=\"border-top: 1px solid #3c3c3c\" colspan=17 align=\"center\"><font face=\"Times New Roman\" size=1>(цифрами)</font></td>\n"
                    + "		<td colspan=17 align=\"center\" valign=middle><font face=\"Times New Roman\" size=1><br></font></td>\n"
                    + "		</tr>\n"
                    + "	<tr>\n"
                    + "		<td colspan=10 height=\"20\" align=\"center\" valign=middle><font face=\"Times New Roman\" size=1>Принято от</font></td>\n"
                    + "		<td style=\"border-bottom: 1px solid #3c3c3c\" colspan=55 align=\"center\"><font face=\"Times New Roman\" size=1>" + contragentName + "</font></td>\n"
                    + "		<td style=\"border-bottom: 1px solid #3c3c3c\" colspan=40 align=\"center\"><font face=\"Times New Roman\" size=1>" + summLiteral + "</font></td>\n"
                    + "		</tr>\n"
                    + "	<tr>\n"
                    + "		<td colspan=65 height=\"17\" align=\"center\" valign=middle><font face=\"Times New Roman\" size=1><br></font></td>\n"
                    + "		<td style=\"border-top: 1px solid #3c3c3c\" colspan=40 align=\"center\"><font face=\"Times New Roman\" size=1>(прописью)</font></td>\n"
                    + "		</tr>\n"
                    + "	<tr>\n"
                    + "		<td colspan=10 height=\"20\" align=\"center\" valign=middle><font face=\"Times New Roman\" size=1>Основание:</font></td>\n"
                    + "		<td style=\"border-bottom: 1px solid #3c3c3c\" colspan=55 align=\"center\"><font face=\"Times New Roman\" size=1>" + reason + "</font></td>\n"
                    //                    + "		<td style=\"border-bottom: 1px solid #3c3c3c\" colspan=40 align=\"center\"><font face=\"Times New Roman\" size=1><br></font></td>\n"
                    + "		</tr>\n"
                    + "	<tr>\n"
                    //                    + "		<td style=\"border-bottom: 1px solid #3c3c3c\" colspan=65 height=\"20\" align=\"center\"><font face=\"Times New Roman\" size=1><br></font></td>\n"
                    //                    + "		<td style=\"border-top: 1px solid #3c3c3c; border-bottom: 1px solid #3c3c3c\" colspan=23 align=\"center\"><font face=\"Times New Roman\" size=1><br></font></td>\n"
                    //                    + "		<td colspan=5 align=\"center\"><font face=\"Times New Roman\" size=1>руб.</font></td>\n"
                    //                    + "		<td style=\"border-bottom: 1px solid #3c3c3c\" colspan=5 align=\"center\"><font face=\"Times New Roman\" size=1><br></font></td>\n"
                    //                    + "		<td colspan=7 align=\"center\" valign=middle><font face=\"Times New Roman\" size=1> коп.</font></td>\n"
                    + "		</tr>\n"
                    + "	<tr>\n"
                    + "		<td colspan=7 height=\"20\" align=\"center\" valign=middle><font face=\"Times New Roman\" size=1>Сумма</font></td>\n"
                    + "		<td style=\"border-top: 1px solid #3c3c3c; border-bottom: 1px solid #3c3c3c\" colspan=58 align=\"center\"><font face=\"Times New Roman\" size=1>" + summLiteral + "</font></td>\n"
                    + "		<td colspan=10 align=\"center\" valign=middle><font face=\"Times New Roman\" size=1>Без налога(НДС)</font></td>\n"
                    //                    + "		<td style=\"border-bottom: 1px solid #3c3c3c\" colspan=30 align=\"center\"><font face=\"Times New Roman\" size=1><br></font></td>\n"
                    + "		</tr>\n"
                    + "	<tr>\n"
                    + "		<td colspan=7 height=\"17\" align=\"center\" valign=middle><font face=\"Times New Roman\" size=1><br></font></td>\n"
                    + "		<td style=\"border-top: 1px solid #3c3c3c\" colspan=58 align=\"center\"><font face=\"Times New Roman\" size=1>(прописью)</font></td>\n"
                    + "		<td colspan=40 align=\"center\" valign=middle><font face=\"Times New Roman\" size=1><br></font></td>\n"
                    + "		</tr>\n"
                    + "	<tr>\n"
                    + "		<td colspan=49 height=\"20\" align=\"center\"><font face=\"Times New Roman\" size=1><br></font></td>\n"
                    + "		<td colspan=5 align=\"center\"><font face=\"Times New Roman\" size=1><br></font></td>\n"
                    + "		<td colspan=5 align=\"center\"><font face=\"Times New Roman\" size=1><br></font></td>\n"
                    + "		<td colspan=6 align=\"center\" valign=middle><font face=\"Times New Roman\" size=1><br></font></td>\n"
                    + "		<td colspan=2 align=\"center\" valign=middle><font face=\"Times New Roman\" size=1>&quot;</font></td>\n"
                    + "		<td style=\"border-bottom: 1px solid #3c3c3c\" colspan=4 align=\"center\" sdnum=\"1049;0;@\"><font face=\"Times New Roman\" size=1>" + day + "</font></td>\n"
                    + "		<td colspan=2 align=\"center\" valign=middle sdnum=\"1049;0;@\"><font face=\"Times New Roman\" size=1>&quot;</font></td>\n"
                    + "		<td style=\"border-bottom: 1px solid #3c3c3c\" colspan=15 align=\"center\" sdnum=\"1049;0;@\"><font face=\"Times New Roman\" size=1>" + month + "</font></td>\n"
                    + "		<td align=\"left\" sdnum=\"1049;0;@\"><font face=\"Times New Roman\" size=1><br></font></td>\n"
                    + "		<td style=\"border-bottom: 1px solid #3c3c3c\" colspan=6 align=\"center\" sdnum=\"1049;0;@\"><font face=\"Times New Roman\" size=1>" + year + "</font></td>\n"
                    + "		<td colspan=10 align=\"center\" valign=middle><font face=\"Times New Roman\" size=1> г.</font></td>\n"
                    + "		</tr>\n"
                    + "	<tr>\n"
                    + "		<td colspan=10 height=\"20\" align=\"center\" valign=middle><font face=\"Times New Roman\" size=1>Без налога(НДС)</font></td>\n"
                    //                    + "		<td style=\"border-bottom: 1px solid #3c3c3c\" colspan=55 align=\"center\"><font face=\"Times New Roman\" size=1><br></font></td>\n"
                    + "		<td colspan=40 align=\"center\" valign=middle><font face=\"Times New Roman\" size=1><br></font></td>\n"
                    + "		</tr>\n"
                    + "	<tr>\n"
                    + "		<td colspan=65 height=\"7\" align=\"center\" valign=middle><font face=\"Times New Roman\" size=1><br></font></td>\n"
                    //                    + "		<td colspan=3 rowspan=3 align=\"center\" valign=middle><font face=\"Times New Roman\" size=1><br></font></td>\n"
                    + "		<td colspan=13 rowspan=2 align=\"left\" valign=middle><font face=\"Times New Roman\" size=1>М.П. (штампа)</font></td>\n"
                    + "		<td colspan=24 rowspan=3 align=\"center\" valign=middle><font face=\"Times New Roman\" size=1><br></font></td>\n"
                    + "		</tr>\n"
                    //                    + "	<tr>\n"
                    //                    + "		<td colspan=11 height=\"20\" align=\"center\" valign=middle><font face=\"Times New Roman\" size=1>Приложение</font></td>\n"
                    //                    + "		<td style=\"border-bottom: 1px solid #3c3c3c\" colspan=54 align=\"center\"><font face=\"Times New Roman\" size=1><br></font></td>\n"
                    //                    + "		</tr>\n"
                    + "	<tr>\n"
                    + "		<td colspan=65 height=\"20\" align=\"center\" valign=middle><font face=\"Times New Roman\" size=1><br></font></td>\n"
                    + "		<td colspan=13 align=\"center\" valign=middle><font face=\"Times New Roman\" size=1><br></font></td>\n"
                    + "		</tr>\n"
                    + "<tr>\n"
                    + "			<td align=\"left\" colspan=\"18\" height=\"20\" valign=\"middle\"><font face=\"Times New Roman\" size=\"1\">Главный бухгалтер</font></td>\n"
                    + "			<td align=\"center\" colspan=\"12\" style=\"border-bottom: 1px solid #3c3c3c\">&nbsp;</td>\n"
                    + "			<td align=\"center\" colspan=\"3\" valign=\"middle\">&nbsp;</td>\n"
                    + "			<td align=\"center\" colspan=\"25\" style=\"border-bottom: 1px solid #3c3c3c\">&nbsp;</td>\n"
                    + "			<td align=\"center\" colspan=\"7\" rowspan=\"4\" valign=\"middle\">&nbsp;</td>\n"
                    + "			<td align=\"center\" colspan=\"16\" valign=\"middle\"><font face=\"Times New Roman\" size=\"1\">Главный бухгалтер</font></td>\n"
                    + "			<td align=\"center\" colspan=\"9\" style=\"border-bottom: 1px solid #3c3c3c\">&nbsp;</td>\n"
                    + "			<td align=\"left\">&nbsp;</td>\n"
                    + "			<td align=\"center\" colspan=\"14\" style=\"border-bottom: 1px solid #3c3c3c\">&nbsp;</td>\n"
                    + "		</tr>\n"
                    + "		<tr>\n"
                    + "			<td align=\"center\" colspan=\"18\" height=\"20\" valign=\"middle\">&nbsp;</td>\n"
                    + "			<td align=\"center\" colspan=\"12\" style=\"border-top: 1px solid #3c3c3c\"><font face=\"Times New Roman\" size=\"1\">(подпись)</font></td>\n"
                    + "			<td align=\"center\" colspan=\"3\" rowspan=\"3\" valign=\"middle\">&nbsp;</td>\n"
                    + "			<td align=\"center\" colspan=\"25\" style=\"border-top: 1px solid #3c3c3c\"><font face=\"Times New Roman\" size=\"1\">(расшифровка подписи)</font></td>\n"
                    + "			<td align=\"center\" colspan=\"16\" valign=\"middle\">&nbsp;</td>\n"
                    + "			<td align=\"center\" colspan=\"9\" style=\"border-top: 1px solid #3c3c3c\"><font face=\"Times New Roman\" size=\"1\">(подпись)</font></td>\n"
                    + "			<td align=\"left\">&nbsp;</td>\n"
                    + "			<td align=\"center\" colspan=\"14\" style=\"border-top: 1px solid #3c3c3c\"><font face=\"Times New Roman\" size=\"1\">(расшифровка подписи)</font></td>\n"
                    + "		</tr>\n"
                    + "		<tr>\n"
                    + "			<td align=\"left\" colspan=\"18\" height=\"20\" valign=\"middle\"><font face=\"Times New Roman\" size=\"1\">Получил кассир</font></td>\n"
                    + "			<td align=\"center\" colspan=\"12\" style=\"border-bottom: 1px solid #3c3c3c\">&nbsp;</td>\n"
                    + "			<td align=\"center\" colspan=\"25\" style=\"border-bottom: 1px solid #3c3c3c\">&nbsp;</td>\n"
                    + "			<td colspan=\"16\" style=\"text-align: center; vertical-align: middle;\"><font face=\"Times New Roman\" size=\"1\">Кассир</font></td>\n"
                    + "			<td align=\"center\" colspan=\"9\" style=\"border-bottom: 1px solid #3c3c3c\">&nbsp;</td>\n"
                    + "			<td align=\"left\">&nbsp;</td>\n"
                    + "			<td align=\"center\" colspan=\"14\" style=\"border-bottom: 1px solid #3c3c3c\">&nbsp;</td>\n"
                    + "		</tr>\n"
                    + "		<tr>\n"
                    + "			<td align=\"center\" colspan=\"18\" height=\"20\" valign=\"middle\">&nbsp;</td>\n"
                    + "			<td align=\"center\" colspan=\"12\" style=\"border-top: 1px solid #3c3c3c\"><font face=\"Times New Roman\" size=\"1\">(подпись)</font></td>\n"
                    + "			<td align=\"center\" colspan=\"25\" style=\"border-top: 1px solid #3c3c3c\"><font face=\"Times New Roman\" size=\"1\">(расшифровка подписи)</font></td>\n"
                    + "			<td colspan=\"16\" style=\"text-align: center; vertical-align: middle;\">&nbsp;</td>\n"
                    + "			<td align=\"center\" colspan=\"9\" style=\"border-top: 1px solid #3c3c3c\"><font face=\"Times New Roman\" size=\"1\">(подпись)</font></td>\n"
                    + "			<td align=\"left\">&nbsp;</td>\n"
                    + "			<td align=\"center\" colspan=\"14\" style=\"border-top: 1px solid #3c3c3c\"><font face=\"Times New Roman\" size=\"1\">(расшифровка подписи)</font></td>\n"
                    + "		</tr>"
                    + "</table>\n"
                    + "<!-- ************************************************************************** -->\n"
                    + "</body>\n"
                    + "\n"
                    + "</html>";

            Label label = new Label(data);
            label.setContentMode(ContentMode.HTML);
            Button print = new Button("Печать");
            print.addClickListener(new Button.ClickListener() {
                @Override
                public void buttonClick(Button.ClickEvent event) {
                    print.setVisible(false);
                    Page.getCurrent().getJavaScript().execute("document.body.style.overflow = \"auto\";"
                            + "document.body.style.height  = \"auto\"");
                    UI.getCurrent().setSizeUndefined();

                    Page.getCurrent().getJavaScript().execute("window.print();");
                }
            });
            content.addComponent(print);
            content.addComponent(label);
            content.setMargin(true);
            content.setSpacing(false);
            content.setComponentAlignment(print, Alignment.TOP_RIGHT);
            content.setExpandRatio(print, 0.0f);
            content.setExpandRatio(label, 0.0f);
            this.setSizeUndefined();
            setContent(content);
        }

        private String getMonthNumber(Date date) {
            String result = "";
            Calendar c = new GregorianCalendar();
            c.setTimeInMillis(date.getTime());
            int month = c.get(Calendar.MONTH);
            switch (month) {
                case 0:
                    result = "января";
                    break;
                case 1:
                    result = "февраля";
                    break;
                case 2:
                    result = "марта";
                    break;
                case 3:
                    result = "апреля";
                    break;
                case 4:
                    result = "мая";
                    break;
                case 5:
                    result = "июня";
                    break;
                case 6:
                    result = "июля";
                    break;
                case 7:
                    result = "августа";
                    break;
                case 8:
                    result = "сентября";
                    break;
                case 9:
                    result = "октября";
                    break;
                case 10:
                    result = "ноября";
                    break;
                case 11:
                    result = "января";
                    break;
                default:
                    break;
            }
            return result;
        }
    }
    
    
}
