/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ru.funkdevelop.stockv.views;

import com.vaadin.event.selection.SelectionEvent;
import com.vaadin.event.selection.SelectionListener;
import com.vaadin.navigator.Navigator;
import com.vaadin.navigator.View;
import com.vaadin.navigator.ViewChangeListener;
import com.vaadin.server.Page;
import com.vaadin.ui.Alignment;
import com.vaadin.ui.Button;
import com.vaadin.ui.Grid;
import com.vaadin.ui.GridLayout;
import com.vaadin.ui.Panel;
import com.vaadin.ui.TextField;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.Window;
import com.vaadin.ui.renderers.NumberRenderer;
import com.vaadin.ui.renderers.TextRenderer;
import java.util.HashSet;
import java.util.List;
import ru.funkdevelop.stockv.MyUI;
import ru.funkdevelop.stockv.StockHelper;
import ru.funkdevelop.stockv.pojos.PriceType;
import ru.funkdevelop.stockv.views.service.MainMenu;
import ru.funkdevelop.stockv.views.service.UserData;

/**
 *
 * @author artur
 */
public class PriceTypeControlView extends GridLayout implements View {

    Navigator navigator;
    StockHelper stockHelper;
    List<PriceType> types;
    PriceType newType, editType, deleteType;

    @Override
    public void enter(ViewChangeListener.ViewChangeEvent event) {
        VerticalLayout fullLayout = new VerticalLayout();
        navigator = getUI().getNavigator();
        stockHelper = new StockHelper();
        types = stockHelper.getPriceTypes();
        newType = new PriceType();
        editType = types.get(0);
        GridLayout layout = new GridLayout(3, 3);
        UserData userData = new UserData();
        MainMenu mainMenu = new MainMenu(navigator);

        Panel allTypes = new Panel("Список всех типов цен");
        VerticalLayout allTypesLayout = new VerticalLayout();
        Grid<PriceType> allTypesGrid = new Grid<>(PriceType.class);
        allTypesGrid.setItems(types);
        allTypesGrid.removeAllColumns();
        allTypesGrid.addColumn(PriceType::getId, new NumberRenderer("%02d")).setCaption("ID");
        allTypesGrid.addColumn(PriceType::getName, new TextRenderer()).setCaption("Наименование типа цены");
        allTypesGrid.setSelectionMode(Grid.SelectionMode.SINGLE);
        allTypesGrid.addSelectionListener(new SelectionListener<PriceType>() {
            @Override
            public void selectionChange(SelectionEvent<PriceType> event) {
                HashSet<PriceType> types = (HashSet<PriceType>) allTypesGrid.getSelectedItems();
                for (PriceType type : types) {
                    editType = type;
                    deleteType = type;
                }
            }
        });

        allTypesLayout.addComponent(allTypesGrid);
        allTypesGrid.setSizeFull();
        allTypesLayout.setSizeFull();
        allTypes.setContent(allTypesLayout);
        allTypes.setWidth(25.0f, Unit.CM);
        allTypesLayout.setMargin(false);
        allTypesLayout.setSpacing(false);

        Panel typesControl = new Panel("Управление");
        VerticalLayout typesControLayout = new VerticalLayout();
        typesControLayout.addComponent(new Button("Добавить", new Button.ClickListener() {
            @Override
            public void buttonClick(Button.ClickEvent event) {
                goToAddType();
            }
        }));
        typesControLayout.addComponent(new Button("Редактировать", new Button.ClickListener() {
            @Override
            public void buttonClick(Button.ClickEvent event) {
                goToEditType();
            }
        }));
        typesControLayout.addComponent(new Button("Удалить", new Button.ClickListener() {
            @Override
            public void buttonClick(Button.ClickEvent event) {
                doDeleteType();
            }
        }));

        typesControLayout.getComponent(0).setWidth("200px");
        typesControLayout.setComponentAlignment(typesControLayout.getComponent(0), Alignment.TOP_CENTER);
        typesControLayout.getComponent(1).setWidth("200px");
        typesControLayout.setComponentAlignment(typesControLayout.getComponent(1), Alignment.TOP_CENTER);
        typesControLayout.getComponent(2).setWidth("200px");
        typesControLayout.setComponentAlignment(typesControLayout.getComponent(2), Alignment.TOP_CENTER);

        typesControl.setContent(typesControLayout);
        typesControl.setWidth("250px");

        userData.setHeight(19.0f, Unit.CM);
        allTypes.setHeight(19.0f, Unit.CM);
        typesControl.setHeight(19.0f, Unit.CM);

        layout.addComponent(mainMenu, 0, 0, 2, 0);
        layout.addComponent(userData, 0, 1, 0, 1);
        layout.addComponent(allTypes, 1, 1, 1, 1);
        layout.addComponent(typesControl, 2, 1, 2, 1);

        layout.setColumnExpandRatio(0, 0.0f);
        layout.setColumnExpandRatio(1, 0.0f);
        layout.setColumnExpandRatio(2, 0.0f);
        layout.setRowExpandRatio(0, 0.0f);
        layout.setRowExpandRatio(1, 0.0f);
        layout.setRowExpandRatio(2, 0.0f);

        layout.setComponentAlignment(allTypes, Alignment.TOP_CENTER);

        fullLayout.addComponent(layout);
        fullLayout.setComponentAlignment(layout, Alignment.MIDDLE_CENTER);
        fullLayout.setMargin(false);
        fullLayout.setSpacing(false);

        getUI().setContent(fullLayout);

    }

    private void goToAddType() {
        Window addTypeWindow = new Window("Добавить тип цены");
        VerticalLayout verticalLayout = new VerticalLayout();
        TextField typeName = new TextField("Наименование типа цены");
        typeName.setWidth(10.0f, Unit.CM);
        Button doAddTypeButton = new Button("Добавить", new Button.ClickListener() {
            @Override
            public void buttonClick(Button.ClickEvent event) {
                newType.setName(typeName.getValue());
                doAddType();
            }
        });
        verticalLayout.addComponents(typeName, doAddTypeButton);
        verticalLayout.setComponentAlignment(typeName, Alignment.TOP_CENTER);
        verticalLayout.setComponentAlignment(doAddTypeButton, Alignment.TOP_CENTER);
        addTypeWindow.setContent(verticalLayout);
        addTypeWindow.setWidth(350.0f, Unit.PIXELS);
        addTypeWindow.center();
        addTypeWindow.setModal(true);
        MyUI.getCurrent().addWindow(addTypeWindow);
    }

    private void doAddType() {
        stockHelper.addDBItem(newType);
        this.newType = new PriceType();
        types = stockHelper.getPriceTypes();
        Page.getCurrent().reload();
    }

    private void goToEditType() {
        Window editTypeWindow = new Window("Редактировать тип цены");
        VerticalLayout verticalLayout = new VerticalLayout();
        TextField typeName = new TextField("Наименование типа цены", this.editType.getName());
        typeName.setWidth(10.0f, Unit.CM);
        Button doEditTypeButton = new Button("Сохранить", new Button.ClickListener() {
            @Override
            public void buttonClick(Button.ClickEvent event) {
                editType.setName(typeName.getValue());
                doEditType();
            }
        });
        verticalLayout.addComponents(typeName, doEditTypeButton);
        verticalLayout.setComponentAlignment(typeName, Alignment.TOP_CENTER);
        verticalLayout.setComponentAlignment(doEditTypeButton, Alignment.TOP_CENTER);
        editTypeWindow.setContent(verticalLayout);
        editTypeWindow.setWidth(350.0f, Unit.PIXELS);
        editTypeWindow.center();
        editTypeWindow.setModal(true);
        MyUI.getCurrent().addWindow(editTypeWindow);
    }

    private void doEditType() {
        stockHelper.updateDBItem(this.editType);
        this.editType = new PriceType();
        this.deleteType = null;
        Page.getCurrent().reload();
    }

    private void doDeleteType() {
        stockHelper.deleteDBItem(deleteType);
        this.deleteType = null;
        this.editType = new PriceType();
        Page.getCurrent().reload();
    }

}
