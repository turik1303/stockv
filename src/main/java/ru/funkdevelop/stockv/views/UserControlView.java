/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ru.funkdevelop.stockv.views;

import com.vaadin.data.HasValue;
import com.vaadin.event.selection.SelectionEvent;
import com.vaadin.event.selection.SelectionListener;
import com.vaadin.navigator.Navigator;
import com.vaadin.navigator.View;
import com.vaadin.navigator.ViewChangeListener;
import com.vaadin.server.Page;
import com.vaadin.ui.Alignment;
import com.vaadin.ui.Button;
import com.vaadin.ui.ComboBox;
import com.vaadin.ui.Grid;
import com.vaadin.ui.GridLayout;
import com.vaadin.ui.Panel;
import com.vaadin.ui.PasswordField;
import com.vaadin.ui.TextField;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.Window;
import com.vaadin.ui.renderers.NumberRenderer;
import com.vaadin.ui.renderers.TextRenderer;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import ru.funkdevelop.stockv.MyUI;
import ru.funkdevelop.stockv.StockHelper;
import ru.funkdevelop.stockv.pojos.Roles;
import ru.funkdevelop.stockv.pojos.Users;
import ru.funkdevelop.stockv.views.service.MainMenu;
import ru.funkdevelop.stockv.views.service.UserData;

/**
 *
 * @author artur
 */
public class UserControlView extends GridLayout implements View {

    Navigator navigator;
    StockHelper stockHelper;
    List<Users> users;
    List<Roles> userRoles;
    Users newUser, editUser, deleteUser;
    VerticalLayout fullLayout;
    GridLayout layout;
    Grid<Users> allUsersGrid;

    @Override
    public void enter(ViewChangeListener.ViewChangeEvent event) {
        fullLayout = new VerticalLayout();
        navigator = getUI().getNavigator();
        stockHelper = new StockHelper();
        users = stockHelper.getUsers();
        newUser = new Users();

        layout = new GridLayout(3, 3);
        UserData userData = new UserData();
        MainMenu mainMenu = new MainMenu(navigator);

        Panel allUsers = new Panel("Список всех пользователей");
        VerticalLayout allUsersLayout = new VerticalLayout();
        allUsersGrid = new Grid<>(Users.class);
        allUsersGrid.setItems(users);
        allUsersGrid.removeAllColumns();
        allUsersGrid.addColumn(Users::getId, new NumberRenderer("%02d")).setCaption("ID");
        allUsersGrid.addColumn(Users::getLogin, new TextRenderer()).setCaption("Логин");
        allUsersGrid.addColumn((users1) -> users1.getRoles().getName(), new TextRenderer()).setCaption("Роль");
        allUsersGrid.addColumn(Users::getLastName, new TextRenderer()).setCaption("Фамилия");
        allUsersGrid.addColumn(Users::getFirstName, new TextRenderer()).setCaption("Имя");
        allUsersGrid.addColumn(Users::getMiddleName, new TextRenderer()).setCaption("Отчество");

        allUsersGrid.setSelectionMode(Grid.SelectionMode.SINGLE);
        allUsersGrid.addSelectionListener(new SelectionListener<Users>() {
            @Override
            public void selectionChange(SelectionEvent<Users> event) {
                HashSet<Users> u = (HashSet<Users>) allUsersGrid.getSelectedItems();
                for (Users user : u) {
                    editUser = user;
                    deleteUser = user;
                }
            }
        });

        allUsersLayout.addComponent(allUsersGrid);
        allUsersGrid.setSizeFull();
        allUsersLayout.setSizeFull();
        allUsers.setContent(allUsersLayout);
        allUsers.setWidth(25.0f, Unit.CM);
        userData.setHeight(19.0f, Unit.CM);
        allUsers.setHeight(19.0f, Unit.CM);
        allUsersLayout.setMargin(false);
        allUsersLayout.setSpacing(false);

        layout.addComponent(mainMenu, 0, 0, 2, 0);
        layout.addComponent(userData, 0, 1, 0, 1);
        layout.addComponent(allUsers, 1, 1, 1, 1);
        layout.setColumnExpandRatio(0, 0.0f);
        layout.setColumnExpandRatio(1, 0.0f);
        layout.setColumnExpandRatio(2, 0.0f);
        layout.setRowExpandRatio(0, 0.0f);
        layout.setRowExpandRatio(1, 0.0f);
        layout.setRowExpandRatio(2, 0.0f);
        layout.setComponentAlignment(allUsers, Alignment.TOP_CENTER);

        Panel userControl = new Panel("Управление");

        VerticalLayout userControlLayout = new VerticalLayout();
        userControlLayout.addComponent(new Button("Добавить", new Button.ClickListener() {
            @Override
            public void buttonClick(Button.ClickEvent event) {
                goToAddUser();
            }
        }));
        userControlLayout.addComponent(new Button("Редактировать", new Button.ClickListener() {
            @Override
            public void buttonClick(Button.ClickEvent event) {
                goToEditUser();
            }
        }));
        userControlLayout.addComponent(new Button("Удалить", new Button.ClickListener() {
            @Override
            public void buttonClick(Button.ClickEvent event) {
                doDeleteUser();
            }
        }));
        
        userControlLayout.getComponent(0).setWidth("200px");
        userControlLayout.setComponentAlignment(userControlLayout.getComponent(0), Alignment.TOP_CENTER);
        userControlLayout.getComponent(1).setWidth("200px");
        userControlLayout.setComponentAlignment(userControlLayout.getComponent(1), Alignment.TOP_CENTER);
        userControlLayout.getComponent(2).setWidth("200px");
        userControlLayout.setComponentAlignment(userControlLayout.getComponent(2), Alignment.TOP_CENTER);

        userControlLayout.setDefaultComponentAlignment(Alignment.TOP_CENTER);
        userControl.setContent(userControlLayout);
        userControl.setWidth("250px");
        userControl.setHeight(19.0f, Unit.CM);

        layout.addComponent(userControl, 2, 1, 2, 1);

        fullLayout.addComponent(layout);
        fullLayout.setComponentAlignment(layout, Alignment.MIDDLE_CENTER);
        fullLayout.setMargin(false);
        fullLayout.setSpacing(false);
        getUI().setContent(fullLayout);

    }

    private void initUserControl() {
        this.userRoles = stockHelper.getRoles();
        this.users = stockHelper.getUsers();
        this.newUser = new Users();
        this.editUser = this.users.get(0);
    }

    private void goToEditUser() {
        Window editUserWindow = new Window("Редактировать пользователя");
        VerticalLayout layoutW = new VerticalLayout();
        HashMap<String, Roles> selectRole = new HashMap<>();
        this.userRoles = stockHelper.getRoles();
        for (Roles userRole : userRoles) {
            selectRole.put(userRole.getName(), userRole);
        }
        TextField login = new TextField("Логин", this.editUser.getLogin());
        PasswordField password = new PasswordField("Пароль", this.editUser.getPassword());
        ComboBox setRole = new ComboBox("Роль");
        setRole.setItems(selectRole.keySet());
        setRole.setEmptySelectionAllowed(false);
        setRole.addValueChangeListener((HasValue.ValueChangeEvent event) -> {
            editUser.setRoles(selectRole.get(event.getValue()));
        });
        TextField firstName = new TextField("Имя", this.editUser.getFirstName());
        TextField middleName = new TextField("Отчество", this.editUser.getMiddleName());
        TextField lastName = new TextField("Фамилия", this.editUser.getLastName());
        Button doEditUserButton = new Button("Сохранить", new Button.ClickListener() {
            @Override
            public void buttonClick(Button.ClickEvent event) {
                editUser.setLogin(login.getValue());
                editUser.setPassword(password.getValue());
                editUser.setFirstName(firstName.getValue());
                editUser.setMiddleName(middleName.getValue());
                editUser.setLastName(lastName.getValue());
                doEditUser();
            }
        });
        layoutW.addComponents(login, password, setRole, lastName, firstName, middleName, doEditUserButton);
        layoutW.setComponentAlignment(login, Alignment.TOP_CENTER);
        layoutW.setComponentAlignment(password, Alignment.TOP_CENTER);
        layoutW.setComponentAlignment(setRole, Alignment.TOP_CENTER);
        layoutW.setComponentAlignment(firstName, Alignment.TOP_CENTER);
        layoutW.setComponentAlignment(middleName, Alignment.TOP_CENTER);
        layoutW.setComponentAlignment(lastName, Alignment.TOP_CENTER);
        layoutW.setComponentAlignment(doEditUserButton, Alignment.TOP_CENTER);
        editUserWindow.setContent(layoutW);
        editUserWindow.setWidth(350.0f, Unit.PIXELS);
        editUserWindow.center();
        editUserWindow.setModal(true);
        MyUI.getCurrent().addWindow(editUserWindow);
    }

    private void doEditUser() {
        stockHelper.updateDBItem(editUser);
        this.editUser = new Users();
        this.deleteUser = null;
        Page.getCurrent().reload();
    }

    private void goToAddUser() {
        Window addUserWindow = new Window("Добавить пользователя");
        VerticalLayout layoutW = new VerticalLayout();
        HashMap<String, Roles> selectRole = new HashMap<>();
        this.userRoles = stockHelper.getRoles();
        this.newUser = new Users();
        for (Roles userRole : userRoles) {
            selectRole.put(userRole.getName(), userRole);
        }
        TextField login = new TextField("Логин");
        PasswordField password = new PasswordField("Пароль");
        ComboBox setRole = new ComboBox("Роль");
        setRole.setItems(selectRole.keySet());
        setRole.setEmptySelectionAllowed(false);
        setRole.addValueChangeListener((HasValue.ValueChangeEvent event) -> {
            newUser.setRoles(selectRole.get(event.getValue()));
        });
        TextField firstName = new TextField("Имя");
        TextField middleName = new TextField("Отчество");
        TextField lastName = new TextField("Фамилия");
        Button doEditUserButton = new Button("Добавить", new Button.ClickListener() {
            @Override
            public void buttonClick(Button.ClickEvent event) {
                newUser.setLogin(login.getValue());
                newUser.setPassword(password.getValue());
                newUser.setFirstName(firstName.getValue());
                newUser.setMiddleName(middleName.getValue());
                newUser.setLastName(lastName.getValue());
                doAddUser();
            }
        });
        layoutW.addComponents(login, password, setRole, lastName, firstName, middleName, doEditUserButton);
        layoutW.setComponentAlignment(login, Alignment.TOP_CENTER);
        layoutW.setComponentAlignment(password, Alignment.TOP_CENTER);
        layoutW.setComponentAlignment(setRole, Alignment.TOP_CENTER);
        layoutW.setComponentAlignment(firstName, Alignment.TOP_CENTER);
        layoutW.setComponentAlignment(middleName, Alignment.TOP_CENTER);
        layoutW.setComponentAlignment(lastName, Alignment.TOP_CENTER);
        layoutW.setComponentAlignment(doEditUserButton, Alignment.TOP_CENTER);
        addUserWindow.setContent(layoutW);
        addUserWindow.setWidth(350.0f, Unit.PIXELS);
        addUserWindow.center();
        addUserWindow.setModal(true);
        MyUI.getCurrent().addWindow(addUserWindow);
    }

    private void doAddUser() {
        stockHelper.addDBItem(newUser);
        this.newUser = new Users();
        users = stockHelper.getUsers();
        Page.getCurrent().reload();
    }

    private void doDeleteUser() {
        stockHelper.deleteDBItem(deleteUser);
        this.deleteUser = null;
        this.editUser = new Users();
        Page.getCurrent().reload();
    }
}
