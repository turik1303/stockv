/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ru.funkdevelop.stockv.views;

import com.vaadin.event.selection.SelectionEvent;
import com.vaadin.event.selection.SelectionListener;
import com.vaadin.navigator.Navigator;
import com.vaadin.navigator.View;
import com.vaadin.navigator.ViewChangeListener;
import com.vaadin.server.Page;
import com.vaadin.ui.Alignment;
import com.vaadin.ui.Button;
import com.vaadin.ui.Grid;
import com.vaadin.ui.GridLayout;
import com.vaadin.ui.Panel;
import com.vaadin.ui.TextField;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.Window;
import com.vaadin.ui.renderers.NumberRenderer;
import com.vaadin.ui.renderers.TextRenderer;
import java.util.HashSet;
import java.util.List;
import ru.funkdevelop.stockv.MyUI;
import ru.funkdevelop.stockv.StockHelper;
import ru.funkdevelop.stockv.pojos.GoodsType;
import ru.funkdevelop.stockv.views.service.MainMenu;
import ru.funkdevelop.stockv.views.service.UserData;

/**
 *
 * @author artur
 */
public class GoodTypeControlView extends GridLayout implements View {
    Navigator navigator;
    StockHelper stockHelper;
    List<GoodsType> goodTypes;
    GoodsType newGoodsType, editGoodsType, deleteGoodsType;
    @Override
    public void enter(ViewChangeListener.ViewChangeEvent event) {
        VerticalLayout fullLayout = new VerticalLayout();
        navigator = getUI().getNavigator();
        stockHelper = new StockHelper();
        goodTypes = stockHelper.getGoodsTypes();
        newGoodsType = new GoodsType();
        editGoodsType = goodTypes.get(0);
        GridLayout layout = new GridLayout(3, 3);
        UserData userData = new UserData();
        MainMenu mainMenu = new MainMenu(navigator);
        
        Panel allGoodsTypes = new Panel("Список всех типов товаров");
        VerticalLayout allGoodsTypesLayout = new VerticalLayout();
        Grid<GoodsType> allGoodsTypeGrid = new Grid<>(GoodsType.class);
        allGoodsTypeGrid.setItems(goodTypes);
        allGoodsTypeGrid.removeAllColumns();
        allGoodsTypeGrid.addColumn(GoodsType::getId, new NumberRenderer("%02d")).setCaption("ID");
        allGoodsTypeGrid.addColumn(GoodsType::getTypeName, new TextRenderer()).setCaption("Наименование типа товара");
        allGoodsTypeGrid.setSelectionMode(Grid.SelectionMode.SINGLE);
        allGoodsTypeGrid.addSelectionListener(new SelectionListener<GoodsType>() {
            @Override
            public void selectionChange(SelectionEvent<GoodsType> event) {
                HashSet<GoodsType> goodsTypes = (HashSet<GoodsType>) allGoodsTypeGrid.getSelectedItems();
                for (GoodsType goodsType : goodsTypes) {
                    editGoodsType = goodsType;
                    deleteGoodsType = goodsType;
                }
            }
        });
        allGoodsTypesLayout.addComponent(allGoodsTypeGrid);
        allGoodsTypeGrid.setSizeFull();
        allGoodsTypesLayout.setSizeFull();
        allGoodsTypes.setContent(allGoodsTypesLayout);
        allGoodsTypes.setWidth(25.0f, Unit.CM);
        allGoodsTypesLayout.setMargin(false);
        allGoodsTypesLayout.setSpacing(false);
        
        Panel goodsTypesControl = new Panel("Управление");
        VerticalLayout goodsTypesControlLayout = new VerticalLayout();
        goodsTypesControlLayout.addComponent(new Button("Добавить", new Button.ClickListener() {
            @Override
            public void buttonClick(Button.ClickEvent event) {
                goToAddGoodsType();
            }
        }));
        goodsTypesControlLayout.addComponent(new Button("Редактировать", new Button.ClickListener() {
            @Override
            public void buttonClick(Button.ClickEvent event) {
                goToEditGoodsType();
            }
        }));
        goodsTypesControlLayout.addComponent(new Button("Удалить", new Button.ClickListener() {
            @Override
            public void buttonClick(Button.ClickEvent event) {
                doDeleteGoodsType();
            }
        }));
        
        goodsTypesControlLayout.getComponent(0).setWidth("200px");
        goodsTypesControlLayout.setComponentAlignment(goodsTypesControlLayout.getComponent(0), Alignment.TOP_CENTER);
        goodsTypesControlLayout.getComponent(1).setWidth("200px");
        goodsTypesControlLayout.setComponentAlignment(goodsTypesControlLayout.getComponent(1), Alignment.TOP_CENTER);
        goodsTypesControlLayout.getComponent(2).setWidth("200px");
        goodsTypesControlLayout.setComponentAlignment(goodsTypesControlLayout.getComponent(2), Alignment.TOP_CENTER);
        
        goodsTypesControl.setContent(goodsTypesControlLayout);
        goodsTypesControl.setWidth("250px");
        
        userData.setHeight(19.0f, Unit.CM);
        allGoodsTypes.setHeight(19.0f, Unit.CM);
        goodsTypesControl.setHeight(19.0f, Unit.CM);
        
        layout.addComponent(mainMenu, 0, 0, 2, 0);
        layout.addComponent(userData, 0, 1, 0, 1);
        layout.addComponent(allGoodsTypes, 1, 1, 1, 1);
        layout.addComponent(goodsTypesControl, 2, 1, 2, 1);
        
        layout.setColumnExpandRatio(0, 0.0f);
        layout.setColumnExpandRatio(1, 0.0f);
        layout.setColumnExpandRatio(2, 0.0f);
        layout.setRowExpandRatio(0, 0.0f);
        layout.setRowExpandRatio(1, 0.0f);
        layout.setRowExpandRatio(2, 0.0f);
        
        layout.setComponentAlignment(allGoodsTypes, Alignment.TOP_CENTER);
        
        fullLayout.addComponent(layout);
        fullLayout.setComponentAlignment(layout, Alignment.MIDDLE_CENTER);
        fullLayout.setMargin(false);
        fullLayout.setSpacing(false);
        
        getUI().setContent(fullLayout);
    }
    
    private void goToAddGoodsType() {
        Window addGoodsTypeWindow = new Window("Добавление типа товара");
        VerticalLayout layout = new VerticalLayout();
        TextField typeName = new TextField("Наименование типа товара");
        typeName.setWidth(10.0f, Unit.CM);
        Button doAddTypeButton = new Button("Добавить", new Button.ClickListener() {
            @Override
            public void buttonClick(Button.ClickEvent event) {
                newGoodsType.setTypeName(typeName.getValue());
                doAddGoodsType();
            }
        });
        layout.addComponents(typeName, doAddTypeButton);
        layout.setComponentAlignment(typeName, Alignment.TOP_CENTER);
        layout.setComponentAlignment(doAddTypeButton, Alignment.TOP_CENTER);
        addGoodsTypeWindow.setContent(layout);
        addGoodsTypeWindow.setWidth(350.0f, Unit.PIXELS);
        addGoodsTypeWindow.center();
        addGoodsTypeWindow.setModal(true);
        MyUI.getCurrent().addWindow(addGoodsTypeWindow);
    }
    
    private void doAddGoodsType() {
        stockHelper.addDBItem(newGoodsType);
        this.newGoodsType = new GoodsType();
        this.goodTypes = stockHelper.getGoodsTypes();
        Page.getCurrent().reload();
    }
    
    private void goToEditGoodsType() {
        Window editGoodsTypeWindow = new Window("Редактирование типа товара");
        VerticalLayout layout = new VerticalLayout();
        TextField typeName = new TextField("Наименование типа товара", this.editGoodsType.getTypeName());
        typeName.setWidth(10.0f, Unit.CM);
        Button doAddTypeButton = new Button("Сохранить", new Button.ClickListener() {
            @Override
            public void buttonClick(Button.ClickEvent event) {
                editGoodsType.setTypeName(typeName.getValue());
                doEditGoodsType();
            }
        });
        layout.addComponents(typeName, doAddTypeButton);
        layout.setComponentAlignment(typeName, Alignment.TOP_CENTER);
        layout.setComponentAlignment(doAddTypeButton, Alignment.TOP_CENTER);
        editGoodsTypeWindow.setContent(layout);
        editGoodsTypeWindow.setWidth(350.0f, Unit.PIXELS);
        editGoodsTypeWindow.center();
        editGoodsTypeWindow.setModal(true);
        MyUI.getCurrent().addWindow(editGoodsTypeWindow);
    }
    
    private void doEditGoodsType() {
        stockHelper.updateDBItem(this.editGoodsType);
        this.editGoodsType = new GoodsType();
        this.deleteGoodsType = null;
        Page.getCurrent().reload();
    }
    
    private void doDeleteGoodsType() {
        stockHelper.deleteDBItem(this.deleteGoodsType);
        this.deleteGoodsType = null;
        this.editGoodsType = new GoodsType();
        Page.getCurrent().reload();
    }
}
