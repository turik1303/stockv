/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ru.funkdevelop.stockv.views;

import com.vaadin.data.HasValue;
import com.vaadin.data.provider.ListDataProvider;
import com.vaadin.event.selection.SelectionEvent;
import com.vaadin.event.selection.SelectionListener;
import com.vaadin.navigator.Navigator;
import com.vaadin.navigator.View;
import com.vaadin.navigator.ViewChangeListener;
import com.vaadin.server.Page;
import com.vaadin.ui.Alignment;
import com.vaadin.ui.Button;
import com.vaadin.ui.ComboBox;
import com.vaadin.ui.Grid;
import com.vaadin.ui.GridLayout;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Notification;
import com.vaadin.ui.Panel;
import com.vaadin.ui.TextField;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.Window;
import com.vaadin.ui.components.grid.HeaderRow;
import com.vaadin.ui.renderers.NumberRenderer;
import com.vaadin.ui.renderers.TextRenderer;
import java.util.HashMap;
import java.util.List;
import java.util.Set;
import ru.funkdevelop.stockv.MyUI;
import ru.funkdevelop.stockv.StockHelper;
import ru.funkdevelop.stockv.pojos.Banks;
import ru.funkdevelop.stockv.pojos.ContragentType;
import ru.funkdevelop.stockv.pojos.Contragents;
import ru.funkdevelop.stockv.views.service.MainMenu;
import ru.funkdevelop.stockv.views.service.UserData;

/**
 *
 * @author artur
 */
public class ContragentControlView extends GridLayout implements View {

    Navigator navigator;
    StockHelper stockHelper;
    List<Contragents> allContragents;
    Contragents newContragent, viewContragent, editContragent, deleteContragent;
    List<Banks> allBanks;
    List<ContragentType> allTypes;

    @Override
    public void enter(ViewChangeListener.ViewChangeEvent event) {
        navigator = getUI().getNavigator();
        stockHelper = new StockHelper();
        allContragents = stockHelper.getContragents();
        allBanks = stockHelper.getBanks();
        allTypes = stockHelper.getContragentTypes();
        newContragent = new Contragents();
        viewContragent = allContragents.get(0);
        editContragent = allContragents.get(0);
        deleteContragent = allContragents.get(0);

        MainMenu mainMenu = new MainMenu(navigator);
        UserData userData = new UserData();

        GridLayout layout = new GridLayout(3, 3);

        Panel allContragentsPanel = new Panel("Список всех контрагентов");
        VerticalLayout allContragentsLayout = new VerticalLayout();
        ContragentsGrid allContragentsGrid = new ContragentsGrid(allContragents, allTypes);
        allContragentsGrid.setSizeFull();
        allContragentsLayout.addComponent(allContragentsGrid);
        allContragentsLayout.setSizeFull();
        allContragentsLayout.setSpacing(false);
        allContragentsLayout.setMargin(false);
        allContragentsPanel.setContent(allContragentsLayout);

        Panel contragentsControl = new Panel("Управление");
        VerticalLayout contragentsControlLayout = new VerticalLayout();
        contragentsControlLayout.addComponent(new Button("Добавить", new Button.ClickListener() {
            @Override
            public void buttonClick(Button.ClickEvent event) {
                goToAddContragent();
            }
        }));
        contragentsControlLayout.addComponent(new Button("Подробнее", new Button.ClickListener() {
            @Override
            public void buttonClick(Button.ClickEvent event) {
                if (viewContragent != null) {
                    goToViewContragent();
                }
            }
        }));
        contragentsControlLayout.addComponent(new Button("Редактировать", new Button.ClickListener() {
            @Override
            public void buttonClick(Button.ClickEvent event) {
                if (editContragent != null) {
                    goToEditContragent();
                }
            }
        }));
        contragentsControlLayout.addComponent(new Button("Удалить", new Button.ClickListener() {
            @Override
            public void buttonClick(Button.ClickEvent event) {
                if (deleteContragent != null) {
                    doDeleteContragent();
                }
            }
        }));

        contragentsControlLayout.getComponent(0).setWidth("200px");
        contragentsControlLayout.setComponentAlignment(contragentsControlLayout.getComponent(0), Alignment.TOP_CENTER);
        contragentsControlLayout.getComponent(1).setWidth("200px");
        contragentsControlLayout.setComponentAlignment(contragentsControlLayout.getComponent(1), Alignment.TOP_CENTER);
        contragentsControlLayout.getComponent(2).setWidth("200px");
        contragentsControlLayout.setComponentAlignment(contragentsControlLayout.getComponent(2), Alignment.TOP_CENTER);
        contragentsControlLayout.getComponent(3).setWidth("200px");
        contragentsControlLayout.setComponentAlignment(contragentsControlLayout.getComponent(3), Alignment.TOP_CENTER);
        contragentsControl.setContent(contragentsControlLayout);
        contragentsControl.setWidth("250px");

        allContragentsPanel.setWidth(25.0f, Unit.CM);
        userData.setHeight(19.0f, Unit.CM);
        allContragentsPanel.setHeight(19.0f, Unit.CM);
        contragentsControl.setHeight(19.0f, Unit.CM);
        layout.addComponent(mainMenu, 0, 0, 2, 0);
        layout.addComponent(userData, 0, 1, 0, 1);
        layout.addComponent(allContragentsPanel, 1, 1, 1, 1);
        layout.addComponent(contragentsControl, 2, 1, 2, 1);
        layout.setColumnExpandRatio(0, 0.0f);
        layout.setColumnExpandRatio(1, 0.0f);
        layout.setColumnExpandRatio(2, 0.0f);
        layout.setRowExpandRatio(0, 0.0f);
        layout.setRowExpandRatio(1, 0.0f);
        layout.setRowExpandRatio(2, 0.0f);

        VerticalLayout fullLayout = new VerticalLayout();
        fullLayout.addComponent(layout);
        fullLayout.setComponentAlignment(layout, Alignment.MIDDLE_CENTER);
        fullLayout.setSpacing(false);
        fullLayout.setMargin(false);
        getUI().setContent(fullLayout);

    }

    private void goToAddContragent() {
        allTypes = stockHelper.getContragentTypes();
        allBanks = stockHelper.getBanks();
        HashMap<String, ContragentType> typesMap = new HashMap<>();
        for (ContragentType cType : allTypes) {
            typesMap.put(cType.getTypeName(), cType);
        }
        HashMap<String, Banks> banksMap = new HashMap<>();
        for (Banks bank : allBanks) {
            banksMap.put(bank.getBankName(), bank);
        }

        Window addContragentWindow = new Window("Добавить контрагента");
        VerticalLayout addContragentLayout = new VerticalLayout();

        HorizontalLayout line1 = new HorizontalLayout();
        TextField displayName = new TextField("Наименование юр. лица");
        displayName.setRequiredIndicatorVisible(true);
        displayName.setWidth(8.0f, Unit.CM);
        TextField officialName = new TextField("Оффициальное наименование юр.лица");
        officialName.setRequiredIndicatorVisible(true);
        officialName.setWidth(8.0f, Unit.CM);
        ComboBox setType = new ComboBox("Тип контрагента");
        setType.setRequiredIndicatorVisible(true);
        setType.setItems(typesMap.keySet());
        setType.setEmptySelectionAllowed(false);
        setType.addValueChangeListener((HasValue.ValueChangeEvent event) -> {
            newContragent.setContragentType(typesMap.get(event.getValue()));
        });
        setType.setWidth(8.0f, Unit.CM);
        line1.addComponents(displayName, officialName, setType);

        HorizontalLayout line2 = new HorizontalLayout();
        TextField phones = new TextField("Телефонные номера");
        phones.setWidth(8.0f, Unit.CM);
        TextField innKpp = new TextField("ИНН/КПП");
        innKpp.setWidth(8.0f, Unit.CM);
        TextField okpo = new TextField("Код по ОКПО");
        okpo.setWidth(8.0f, Unit.CM);
        line2.addComponents(phones, innKpp, okpo);

        HorizontalLayout line3 = new HorizontalLayout();
        TextField legalAddress = new TextField("Юридический адрес");
        legalAddress.setWidth(8.0f, Unit.CM);
        TextField actualAddress = new TextField("Фактический адрес");
        actualAddress.setWidth(8.0f, Unit.CM);
        line3.addComponents(legalAddress, actualAddress);

        HorizontalLayout line4 = new HorizontalLayout();
        TextField contractName = new TextField("Наименование договора");
        contractName.setWidth(8.0f, Unit.CM);
        TextField discount = new TextField("Размер скидки");
        discount.setValue("0.0");
        discount.setWidth(8.0f, Unit.CM);
        line4.addComponents(contractName, discount);

        HorizontalLayout line5 = new HorizontalLayout();
        ComboBox setBank = new ComboBox("Наименование банка");
        setBank.setItems(banksMap.keySet());
        setBank.setRequiredIndicatorVisible(true);
        setBank.setEmptySelectionAllowed(false);
        setBank.addValueChangeListener((HasValue.ValueChangeEvent event) -> {
            newContragent.setBanks(banksMap.get(event.getValue()));
        });
        setBank.setWidth(8.0f, Unit.CM);
        TextField bankAccountName = new TextField("Наименование банковского счета");
        bankAccountName.setWidth(8.0f, Unit.CM);
        TextField bankAccountNumber = new TextField("Номер банкосвкого счета");
        bankAccountNumber.setWidth(8.0f, Unit.CM);
        line5.addComponents(setBank, bankAccountName, bankAccountNumber);

        addContragentLayout.addComponents(line1, line2, line3, line4, line5);
        
        Button doAddContragentButton = new Button("Добавить", new Button.ClickListener() {
            @Override
            public void buttonClick(Button.ClickEvent event) {
                boolean dataIsOk = true;
                if (!displayName.isEmpty())
                    newContragent.setDisplayName(displayName.getValue());
                else
                    dataIsOk = false;
                if (!officialName.isEmpty())                    
                    newContragent.setOfficialName(officialName.getValue());
                else
                    dataIsOk = false;
                if (!phones.isEmpty())
                    newContragent.setPhones(phones.getValue());
                else
                    newContragent.setPhones("");
                if (!innKpp.isEmpty())
                    newContragent.setInnKpp(innKpp.getValue());
                else
                    newContragent.setInnKpp("");
                if (!okpo.isEmpty())
                    newContragent.setOkpo(okpo.getValue());
                else
                    newContragent.setOkpo("");
                if (!legalAddress.isEmpty())
                    newContragent.setLegalAddress(legalAddress.getValue());
                else
                    newContragent.setLegalAddress("");
                if (!actualAddress.isEmpty())
                    newContragent.setActualAddress(actualAddress.getValue());
                else
                    newContragent.setActualAddress("");
                if (!contractName.isEmpty())
                    newContragent.setMainContract(contractName.getValue());
                else
                    newContragent.setMainContract("");
                try {
                    newContragent.setDiscount(Double.parseDouble(discount.getValue()));
                } catch (NumberFormatException e) {
                    newContragent.setDiscount(0.0);
                }
                if (!bankAccountName.isEmpty())
                    newContragent.setBankAccountName(bankAccountName.getValue());
                else
                    newContragent.setBankAccountName("");
                if (!bankAccountNumber.isEmpty())
                    newContragent.setBankAccountNumber(bankAccountNumber.getValue());
                else
                    newContragent.setBankAccountNumber("");
                newContragent.setCredit(0.0);
                if (setBank.isEmpty())
                    dataIsOk = false;
                if (setType.isEmpty())
                    dataIsOk = false;
                if (dataIsOk)
                    doAddContragent();
                else
                    Notification.show("Заполнены не все необходимые поля");
            }
        });
        addContragentLayout.addComponent(doAddContragentButton);
        addContragentLayout.setComponentAlignment(doAddContragentButton, Alignment.TOP_CENTER);
        addContragentWindow.setContent(addContragentLayout);
        addContragentWindow.center();
        addContragentWindow.setModal(true);
        MyUI.getCurrent().addWindow(addContragentWindow);

    }

    private void goToViewContragent() {
        Window viewContragentWindow = new Window("Просмотр данных контрагента");
        VerticalLayout viewContragentLayout = new VerticalLayout();

        HorizontalLayout line1 = new HorizontalLayout();

        TextField displayName = new TextField("Наименование юр.лица", this.viewContragent.getDisplayName());
        displayName.setEnabled(false);
        displayName.setWidth(8.0f, Unit.CM);

        TextField officialName = new TextField("Оффициальное наименование юр.лица", this.viewContragent.getOfficialName());
        officialName.setEnabled(false);
        officialName.setWidth(8.0f, Unit.CM);

        TextField cType = new TextField("Тип контрагента", this.viewContragent.getContragentType().getTypeName());
        cType.setEnabled(false);
        cType.setWidth(8.0f, Unit.CM);

        line1.addComponents(displayName, officialName, cType);

        HorizontalLayout line2 = new HorizontalLayout();

        TextField phones = new TextField("Телефонные номера", this.viewContragent.getPhones());
        phones.setEnabled(false);
        phones.setWidth(8.0f, Unit.CM);

        TextField innKpp = new TextField("ИНН/КПП", this.viewContragent.getInnKpp());
        innKpp.setEnabled(false);
        innKpp.setWidth(8.0f, Unit.CM);

        TextField okpo = new TextField("Код по ОКПО", this.viewContragent.getOkpo());
        okpo.setEnabled(false);
        okpo.setWidth(8.0f, Unit.CM);

        line2.addComponents(phones, innKpp, okpo);

        HorizontalLayout line3 = new HorizontalLayout();

        TextField legalAddress = new TextField("Юридический адрес", this.viewContragent.getLegalAddress());
        legalAddress.setEnabled(false);
        legalAddress.setWidth(8.0f, Unit.CM);

        TextField actualAddres = new TextField("Фактический адрес", this.viewContragent.getActualAddress());
        actualAddres.setEnabled(false);
        actualAddres.setWidth(8.0f, Unit.CM);

        line3.addComponents(legalAddress, actualAddres);

        HorizontalLayout line4 = new HorizontalLayout();

        TextField contract = new TextField("Наименование договора", this.viewContragent.getMainContract());
        contract.setEnabled(false);
        contract.setWidth(8.0f, Unit.CM);

        TextField discount = new TextField("Размер скидки", String.valueOf(this.viewContragent.getDiscount()));
        discount.setEnabled(false);
        discount.setWidth(8.0f, Unit.CM);
        
        TextField credit = new TextField("Размер долга", String.valueOf(this.viewContragent.getCredit()));
        credit.setEnabled(false);
        credit.setWidth(8.0f, Unit.CM);
        line4.addComponents(contract, discount, credit);

        HorizontalLayout line5 = new HorizontalLayout();

        TextField bankName = new TextField("Наименование банка", this.viewContragent.getBanks().getBankName());
        
        bankName.setEnabled(false);
        bankName.setWidth(8.0f, Unit.CM);

        TextField bankAccountName = new TextField("Наименование банковского счета", this.viewContragent.getBankAccountName());
        bankAccountName.setEnabled(false);
        bankAccountName.setWidth(8.0f, Unit.CM);

        TextField bankAccountNumber = new TextField("Номер банковского счета", this.viewContragent.getBankAccountNumber());
        bankAccountNumber.setEnabled(false);
        bankAccountNumber.setWidth(8.0f, Unit.CM);

        line5.addComponents(bankName, bankAccountName, bankAccountNumber);

        viewContragentLayout.addComponents(line1, line2, line3, line4, line5);
        viewContragentWindow.setContent(viewContragentLayout);
        viewContragentWindow.center();
        viewContragentWindow.setModal(true);
        MyUI.getCurrent().addWindow(viewContragentWindow);
    }

    private void goToEditContragent() {
        allTypes = stockHelper.getContragentTypes();
        allBanks = stockHelper.getBanks();
        HashMap<String, ContragentType> typesMap = new HashMap<>();
        for (ContragentType cType : allTypes) {
            typesMap.put(cType.getTypeName(), cType);
        }
        HashMap<String, Banks> banksMap = new HashMap<>();
        for (Banks bank : allBanks) {
            banksMap.put(bank.getBankName(), bank);
        }

        Window editContragentWindow = new Window("Редактировать контрагента");
        VerticalLayout editContragentLayout = new VerticalLayout();

        HorizontalLayout line1 = new HorizontalLayout();
        TextField displayName = new TextField("Наименование юр. лица", this.editContragent.getDisplayName());
        displayName.setRequiredIndicatorVisible(true);
        displayName.setWidth(8.0f, Unit.CM);
        TextField officialName = new TextField("Оффициальное наименование юр.лица", this.editContragent.getOfficialName());
        officialName.setRequiredIndicatorVisible(true);
        officialName.setWidth(8.0f, Unit.CM);
        ComboBox setType = new ComboBox("Тип контрагента");
        setType.setRequiredIndicatorVisible(true);
        setType.setItems(typesMap.keySet());
        setType.setEmptySelectionAllowed(false);
        setType.setValue(this.editContragent.getContragentType().getTypeName());
        setType.addValueChangeListener((HasValue.ValueChangeEvent event) -> {
            editContragent.setContragentType(typesMap.get(event.getValue()));
        });
        setType.setWidth(8.0f, Unit.CM);
        line1.addComponents(displayName, officialName, setType);

        HorizontalLayout line2 = new HorizontalLayout();
        TextField phones = new TextField("Телефонные номера", this.editContragent.getPhones());
        phones.setWidth(8.0f, Unit.CM);
        TextField innKpp = new TextField("ИНН/КПП", this.editContragent.getInnKpp());
        innKpp.setWidth(8.0f, Unit.CM);
        TextField okpo = new TextField("Код по ОКПО", this.editContragent.getOkpo());
        okpo.setWidth(8.0f, Unit.CM);
        line2.addComponents(phones, innKpp, okpo);

        HorizontalLayout line3 = new HorizontalLayout();
        TextField legalAddress = new TextField("Юридический адрес", this.editContragent.getLegalAddress());
        legalAddress.setWidth(8.0f, Unit.CM);
        TextField actualAddress = new TextField("Фактический адрес", this.editContragent.getActualAddress());
        actualAddress.setWidth(8.0f, Unit.CM);
        line3.addComponents(legalAddress, actualAddress);

        HorizontalLayout line4 = new HorizontalLayout();
        TextField contractName = new TextField("Наименование договора", this.editContragent.getMainContract());
        contractName.setWidth(8.0f, Unit.CM);
        TextField discount = new TextField("Размер скидки", String.valueOf(this.editContragent.getDiscount()));
        discount.setWidth(8.0f, Unit.CM);
        line4.addComponents(contractName, discount);

        HorizontalLayout line5 = new HorizontalLayout();
        ComboBox setBank = new ComboBox("Наименование банка");
        setBank.setRequiredIndicatorVisible(true);
        setBank.setItems(banksMap.keySet());
        setBank.setEmptySelectionAllowed(false);
        setBank.setValue(this.editContragent.getBanks().getBankName());
        setBank.addValueChangeListener((HasValue.ValueChangeEvent event) -> {
            editContragent.setBanks(banksMap.get(event.getValue()));
        });
        setBank.setWidth(8.0f, Unit.CM);
        TextField bankAccountName = new TextField("Наименование банковского счета", this.editContragent.getBankAccountName());
        bankAccountName.setWidth(8.0f, Unit.CM);
        TextField bankAccountNumber = new TextField("Номер банкосвкого счета", this.editContragent.getBankAccountNumber());
        bankAccountNumber.setWidth(8.0f, Unit.CM);
        line5.addComponents(setBank, bankAccountName, bankAccountNumber);

        editContragentLayout.addComponents(line1, line2, line3, line4, line5);
        Button doEditContragentButton = new Button("Сохранить", new Button.ClickListener() {
            @Override
            public void buttonClick(Button.ClickEvent event) {
                boolean dataIsOk = true;
                if (!displayName.isEmpty())
                    editContragent.setDisplayName(displayName.getValue());
                else
                    dataIsOk = false;
                if (!officialName.isEmpty())
                    editContragent.setOfficialName(officialName.getValue());
                else
                    dataIsOk = false;
                if (!phones.isEmpty())
                    editContragent.setPhones(phones.getValue());
                else
                    editContragent.setPhones("");
                if (!innKpp.isEmpty())
                    editContragent.setInnKpp(innKpp.getValue());
                else
                    editContragent.setInnKpp("");
                if (!okpo.isEmpty())
                    editContragent.setOkpo(okpo.getValue());
                else
                    editContragent.setOkpo("");
                if (!legalAddress.isEmpty())
                    editContragent.setLegalAddress(legalAddress.getValue());
                else
                    editContragent.setLegalAddress("");
                if (!actualAddress.isEmpty())
                    editContragent.setActualAddress(actualAddress.getValue());
                else
                    editContragent.setActualAddress("");
                if (!contractName.isEmpty())
                    editContragent.setMainContract(contractName.getValue());
                else
                    editContragent.setMainContract("");
                try {
                    editContragent.setDiscount(Double.parseDouble(discount.getValue()));
                } catch (NumberFormatException e) {
                    dataIsOk = false;
                }
                if (!bankAccountName.isEmpty())
                    editContragent.setBankAccountName(bankAccountName.getValue());
                else
                    editContragent.setBankAccountName("");
                if (!bankAccountNumber.isEmpty())
                    editContragent.setBankAccountNumber(bankAccountNumber.getValue());
                else
                    editContragent.setBankAccountNumber("");
                if (setBank.isEmpty())
                    dataIsOk = false;
                if (setType.isEmpty())
                    dataIsOk = false;
                if (dataIsOk)
                    doEditContragent();
                else
                    Notification.show("Не заполнены необходимые поля");
            }
        });
        editContragentLayout.addComponent(doEditContragentButton);
        editContragentLayout.setComponentAlignment(doEditContragentButton, Alignment.TOP_CENTER);
        editContragentWindow.setContent(editContragentLayout);
        editContragentWindow.center();
        editContragentWindow.setModal(true);
        MyUI.getCurrent().addWindow(editContragentWindow);
    }

    private void doAddContragent() {
        stockHelper.addDBItem(newContragent);
        this.newContragent = new Contragents();
        this.allContragents = stockHelper.getContragents();
        Page.getCurrent().reload();
    }

    private void doEditContragent() {
        stockHelper.updateDBItem(editContragent);
        this.editContragent = null;
        this.viewContragent = null;
        this.deleteContragent = null;
        this.allContragents = stockHelper.getContragents();
        Page.getCurrent().reload();
    }

    private void doDeleteContragent() {
        stockHelper.deleteDBItem(deleteContragent);
        this.editContragent = null;
        this.viewContragent = null;
        this.deleteContragent = null;
        Page.getCurrent().reload();
    }

    final class ContragentsGrid extends Grid<Contragents> {

        TextField nameFilter;
        ComboBox typeFilter;

        public ContragentsGrid(List<Contragents> contragents, List<ContragentType> contragentTypes) {

            this.addColumn(Contragents::getId, new NumberRenderer("%02d")).setCaption("ID");
            this.getColumns().get(0).setWidth(50.0d);
            this.addColumn(Contragents::getDisplayName, new TextRenderer()).setCaption("Наименование контрагента");
            this.addColumn((contragents1) -> contragents1.getContragentType().getTypeName(), new TextRenderer()).setCaption("Тип контрагента");
            this.setSelectionMode(Grid.SelectionMode.SINGLE);
            this.addSelectionListener(new SelectionListener<Contragents>() {
                @Override
                public void selectionChange(SelectionEvent<Contragents> event) {
                    Set<Contragents> contragents = event.getAllSelectedItems();
                    for (Contragents contragent : contragents) {
                        viewContragent = contragent;
                        editContragent = contragent;
                        deleteContragent = contragent;
                    }
                }
            });
            this.setItems(contragents);
            this.setSizeFull();
            HeaderRow filterRow = this.appendHeaderRow();
            nameFilter = new TextField();
            nameFilter.setPlaceholder("Наименование контрагента...");
            nameFilter.setSizeFull();
            nameFilter.setHeight(0.8f, Unit.CM);
            //nameFilter.addValueChangeListener(this::onNameFilterTextChange);

            HashMap<String, ContragentType> types = new HashMap<>();
            for (ContragentType contragentType : contragentTypes) {
                types.put(contragentType.getTypeName(), contragentType);
            }

            typeFilter = new ComboBox();
            typeFilter.setPlaceholder("Тип контрагента...");
            typeFilter.setItems(types.keySet());
            typeFilter.setEmptySelectionAllowed(false);
            typeFilter.setSizeFull();
            typeFilter.setHeight(0.8f, Unit.CM);
            //typeFilter.addValueChangeListener(this::onTypeFilterValueChange);

            filterRow.getCell(this.getColumns().get(1)).setComponent(nameFilter);
            filterRow.getCell(this.getColumns().get(2)).setComponent(typeFilter);
            Button makeFilter = new Button("Применить фильтры", new Button.ClickListener() {
                @Override
                public void buttonClick(Button.ClickEvent event) {
                    doFilter();
                }
            });

            Button resetFilter = new Button("Сбросить фильтры", new Button.ClickListener() {
                @Override
                public void buttonClick(Button.ClickEvent event) {
                    doResetFilter();
                }
            });
            makeFilter.setHeight(0.8f, Unit.CM);
            resetFilter.setHeight(0.8f, Unit.CM);
            HorizontalLayout buttonsLayout = new HorizontalLayout(makeFilter, resetFilter);
            buttonsLayout.setComponentAlignment(makeFilter, Alignment.TOP_LEFT);
            buttonsLayout.setComponentAlignment(resetFilter, Alignment.TOP_RIGHT);
            buttonsLayout.setSizeFull();
            HeaderRow buttonsRow = this.appendHeaderRow();
            buttonsRow.getCell(this.getColumns().get(2)).setComponent(buttonsLayout);
        }

        private void doFilter() {
            String name = nameFilter.getValue();
            String type = (String) typeFilter.getValue();
            ListDataProvider<Contragents> dataProvider = (ListDataProvider<Contragents>) this.getDataProvider();
            if (!name.isEmpty()) {
                dataProvider.addFilter(Contragents::getDisplayName, s -> caseInsensitiveContains(s, name));
            }
            if (!type.isEmpty()) {
                dataProvider.addFilter((contragents) -> contragents.getContragentType().getTypeName(),
                        s -> caseInsensitiveContains(s, type));
            }

        }

        private void doResetFilter() {
            ListDataProvider<Contragents> dataProvider = (ListDataProvider<Contragents>) this.getDataProvider();
            dataProvider.clearFilters();
        }

        private Boolean caseInsensitiveContains(String where, String what) {
            return where.toLowerCase().contains(what.toLowerCase());
        }
    }

}
