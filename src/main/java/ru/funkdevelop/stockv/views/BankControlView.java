/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ru.funkdevelop.stockv.views;

import com.vaadin.event.selection.SelectionEvent;
import com.vaadin.event.selection.SelectionListener;
import com.vaadin.navigator.Navigator;
import com.vaadin.navigator.View;
import com.vaadin.navigator.ViewChangeListener;
import com.vaadin.server.Page;
import com.vaadin.ui.Alignment;
import com.vaadin.ui.Button;
import com.vaadin.ui.Grid;
import com.vaadin.ui.GridLayout;
import com.vaadin.ui.Panel;
import com.vaadin.ui.TextField;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.Window;
import com.vaadin.ui.renderers.NumberRenderer;
import com.vaadin.ui.renderers.TextRenderer;
import java.util.HashSet;
import java.util.List;
import ru.funkdevelop.stockv.MyUI;
import ru.funkdevelop.stockv.StockHelper;
import ru.funkdevelop.stockv.pojos.Banks;
import ru.funkdevelop.stockv.views.service.MainMenu;
import ru.funkdevelop.stockv.views.service.UserData;

/**
 *
 * @author artur
 */
public class BankControlView extends GridLayout implements View{
    Navigator navigator;
    StockHelper stockHelper;
    List<Banks> banks;
    Banks newBank, editBank, deleteBank;
    @Override
    public void enter(ViewChangeListener.ViewChangeEvent event) {
        VerticalLayout fullLayout = new VerticalLayout();
        navigator = getUI().getNavigator();
        stockHelper = new StockHelper();
        banks = stockHelper.getBanks();
        newBank = new Banks();
        editBank = banks.get(0);
        GridLayout layout = new GridLayout(3, 3);
        UserData userData = new UserData();
        MainMenu mainMenu = new MainMenu(navigator);
        
        Panel allBanks = new Panel("Список всех банков");
        VerticalLayout allBanksLayout = new VerticalLayout();
        Grid<Banks> allBanksGrid = new Grid<>(Banks.class);
        allBanksGrid.setItems(banks);
        allBanksGrid.removeAllColumns();
        allBanksGrid.addColumn(Banks::getId, new NumberRenderer("%02d")).setCaption("ID");
        allBanksGrid.addColumn(Banks::getBankName, new TextRenderer()).setCaption("Наименование банка");
        allBanksGrid.setSelectionMode(Grid.SelectionMode.SINGLE);
        allBanksGrid.addSelectionListener(new SelectionListener<Banks>() {
            @Override
            public void selectionChange(SelectionEvent<Banks> event) {
                HashSet<Banks> b = (HashSet<Banks>) allBanksGrid.getSelectedItems();
                for (Banks bank : b) {
                    editBank = bank;
                    deleteBank = bank;
                }
            }
        });
        
        allBanksLayout.addComponent(allBanksGrid);
        allBanksGrid.setSizeFull();
        allBanksLayout.setSizeFull();
        allBanks.setContent(allBanksLayout);
        allBanks.setWidth(25.0f, Unit.CM);
        allBanksLayout.setMargin(false);
        allBanksLayout.setSpacing(false);
        
        Panel banksControl = new Panel("Управление");
        VerticalLayout banksControLayout = new VerticalLayout();
        banksControLayout.addComponent(new Button("Добавить", new Button.ClickListener() {
            @Override
            public void buttonClick(Button.ClickEvent event) {
                goToAddBank();
            }
        }));
        banksControLayout.addComponent(new Button("Редактировать", new Button.ClickListener() {
            @Override
            public void buttonClick(Button.ClickEvent event) {
                goToEditBank();
            }
        }));
        banksControLayout.addComponent(new Button("Удалить", new Button.ClickListener() {
            @Override
            public void buttonClick(Button.ClickEvent event) {
                doDeletebank();
            }
        }));
        
        banksControLayout.getComponent(0).setWidth("200px");
        banksControLayout.setComponentAlignment(banksControLayout.getComponent(0), Alignment.TOP_CENTER);
        banksControLayout.getComponent(1).setWidth("200px");
        banksControLayout.setComponentAlignment(banksControLayout.getComponent(1), Alignment.TOP_CENTER);
        banksControLayout.getComponent(2).setWidth("200px");
        banksControLayout.setComponentAlignment(banksControLayout.getComponent(2), Alignment.TOP_CENTER);
        
        banksControl.setContent(banksControLayout);
        banksControl.setWidth("250px");
        
        userData.setHeight(19.0f, Unit.CM);
        allBanks.setHeight(19.0f, Unit.CM);
        banksControl.setHeight(19.0f, Unit.CM);
        
        layout.addComponent(mainMenu, 0, 0, 2, 0);
        layout.addComponent(userData, 0, 1, 0, 1);
        layout.addComponent(allBanks, 1, 1, 1, 1);
        layout.addComponent(banksControl, 2, 1, 2, 1);
        
        layout.setColumnExpandRatio(0, 0.0f);
        layout.setColumnExpandRatio(1, 0.0f);
        layout.setColumnExpandRatio(2, 0.0f);
        layout.setRowExpandRatio(0, 0.0f);
        layout.setRowExpandRatio(1, 0.0f);
        layout.setRowExpandRatio(2, 0.0f);
        
        layout.setComponentAlignment(allBanks, Alignment.TOP_CENTER);
        
        fullLayout.addComponent(layout);
        fullLayout.setComponentAlignment(layout, Alignment.MIDDLE_CENTER);
        fullLayout.setMargin(false);
        fullLayout.setSpacing(false);
        
        getUI().setContent(fullLayout);
        
        
    }
    
    private void goToAddBank() {
        Window addBankWindow = new Window("Добавить банк");
        VerticalLayout verticalLayout = new VerticalLayout();
        TextField bankName = new TextField("Наименование банка");
        bankName.setWidth(10.0f, Unit.CM);
        Button doAddBankButton = new Button("Добавить", new Button.ClickListener() {
            @Override
            public void buttonClick(Button.ClickEvent event) {
                newBank.setBankName(bankName.getValue());
                doAddBank();
            }
        });
        verticalLayout.addComponents(bankName, doAddBankButton);
        verticalLayout.setComponentAlignment(bankName, Alignment.TOP_CENTER);
        verticalLayout.setComponentAlignment(doAddBankButton, Alignment.TOP_CENTER);
        addBankWindow.setContent(verticalLayout);
        addBankWindow.setWidth(350.0f, Unit.PIXELS);
        addBankWindow.center();
        addBankWindow.setModal(true);
        MyUI.getCurrent().addWindow(addBankWindow);
    }
    
    private void doAddBank() {
        stockHelper.addDBItem(newBank);
        this.newBank = new Banks();
        banks = stockHelper.getBanks();
        Page.getCurrent().reload();
    }
    
    private void goToEditBank() {
        Window editBankWindow = new Window("Редактировать банк");
        VerticalLayout verticalLayout = new VerticalLayout();
        TextField bankName = new TextField("Наименование банка", this.editBank.getBankName());
        bankName.setWidth(10.0f, Unit.CM);
        Button doAddBankButton = new Button("Сохранить", new Button.ClickListener() {
            @Override
            public void buttonClick(Button.ClickEvent event) {
                editBank.setBankName(bankName.getValue());
                doEditBank();
            }
        });
        verticalLayout.addComponents(bankName, doAddBankButton);
        verticalLayout.setComponentAlignment(bankName, Alignment.TOP_CENTER);
        verticalLayout.setComponentAlignment(doAddBankButton, Alignment.TOP_CENTER);
        editBankWindow.setContent(verticalLayout);
        editBankWindow.setWidth(350.0f, Unit.PIXELS);
        editBankWindow.center();
        editBankWindow.setModal(true);
        MyUI.getCurrent().addWindow(editBankWindow);
    }
    
    private void doEditBank() {
        stockHelper.updateDBItem(this.editBank);
        this.editBank = new Banks();
        this.deleteBank = null;
        Page.getCurrent().reload();
    }
    
    private void doDeletebank() {
        stockHelper.deleteDBItem(deleteBank);
        this.deleteBank = null;
        this.editBank = new Banks();
        Page.getCurrent().reload();
    }
}
