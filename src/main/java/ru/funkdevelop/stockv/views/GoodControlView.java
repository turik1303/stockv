/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ru.funkdevelop.stockv.views;

import com.vaadin.data.HasValue;
import com.vaadin.data.provider.ListDataProvider;
import com.vaadin.event.selection.SelectionEvent;
import com.vaadin.event.selection.SelectionListener;
import com.vaadin.navigator.Navigator;
import com.vaadin.navigator.View;
import com.vaadin.navigator.ViewChangeListener;
import com.vaadin.server.Page;
import com.vaadin.server.VaadinService;
import com.vaadin.ui.Alignment;
import com.vaadin.ui.Button;
import com.vaadin.ui.CheckBox;
import com.vaadin.ui.ComboBox;
import com.vaadin.ui.Grid;
import com.vaadin.ui.GridLayout;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Label;
import com.vaadin.ui.Notification;
import com.vaadin.ui.Panel;
import com.vaadin.ui.TextField;
import com.vaadin.ui.UI;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.Window;
import com.vaadin.ui.components.grid.HeaderRow;
import com.vaadin.ui.renderers.DateRenderer;
import com.vaadin.ui.renderers.NumberRenderer;
import com.vaadin.ui.renderers.TextRenderer;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Objects;
import java.util.Set;
import ru.funkdevelop.stockv.MyUI;
import ru.funkdevelop.stockv.StockHelper;
import ru.funkdevelop.stockv.pojos.Contragents;
import ru.funkdevelop.stockv.pojos.Goods;
import ru.funkdevelop.stockv.pojos.GoodsType;
import ru.funkdevelop.stockv.pojos.Packages;
import ru.funkdevelop.stockv.pojos.PriceLog;
import ru.funkdevelop.stockv.pojos.PriceType;
import ru.funkdevelop.stockv.pojos.Prices;
import ru.funkdevelop.stockv.pojos.Sertificates;
import ru.funkdevelop.stockv.pojos.Users;
import ru.funkdevelop.stockv.views.service.MainMenu;
import ru.funkdevelop.stockv.views.service.UserData;

/**
 *
 * @author artur
 */
public class GoodControlView extends GridLayout implements View {

    Navigator navigator;
    StockHelper stockHelper;
    List<Goods> allGoods, filteredGoods;
    Goods newGood, editGood, viewGood, deleteGood;
    List<GoodsType> allTypes;
    List<Contragents> allContragents;
    List<PriceType> allPriceTypes;
    List<Prices> newGoodPrices, viewGoodPrices, editGoodPrices;
    List<Sertificates> allSertificates;
    List<Packages> allPackages;
    //HashMap<Prices, HorizontalLayout> priceLayouts;
    Panel priceData;

    @Override
    public void enter(ViewChangeListener.ViewChangeEvent event) {

        VerticalLayout fullLayout = new VerticalLayout();
        GridLayout layout = new GridLayout(3, 3);

        navigator = getUI().getNavigator();
        stockHelper = new StockHelper();
        allGoods = stockHelper.getAllGoods();
        //filteredGoods = new ArrayList<>();
        allTypes = stockHelper.getGoodsTypes();
        allContragents = stockHelper.getContragents();
        MainMenu mainMenu = new MainMenu(navigator);
        UserData userData = new UserData();

        Panel allGoodsPanel = new Panel("Список товаров");
        VerticalLayout allGoodsLayout = new VerticalLayout();
        GoodsGrid allGoodsGrid = new GoodsGrid(allGoods, allContragents, allTypes);
        allGoodsLayout.addComponent(allGoodsGrid);
        allGoodsLayout.setSizeFull();
        allGoodsLayout.setMargin(false);
        allGoodsLayout.setSpacing(false);
        allGoodsPanel.setContent(allGoodsLayout);
        allGoodsPanel.setWidth(32.0f, Unit.CM);
        allGoodsPanel.setHeight(19.0f, Unit.CM);

        Panel goodsControlPanel = new Panel("Управление");
        VerticalLayout goodsControlLayout = new VerticalLayout();
        goodsControlLayout.addComponent(new Button("Добавить", new Button.ClickListener() {
            @Override
            public void buttonClick(Button.ClickEvent event) {
                goToAddGood();
            }
        }));
        goodsControlLayout.addComponent(new Button("Подробнее", new Button.ClickListener() {
            @Override
            public void buttonClick(Button.ClickEvent event) {
                goToViewGood();
            }
        }));
        goodsControlLayout.addComponent(new Button("Редактировать", new Button.ClickListener() {
            @Override
            public void buttonClick(Button.ClickEvent event) {
                goToEditGood();
            }
        }));
        goodsControlLayout.addComponent(new Button("Удалить", new Button.ClickListener() {
            @Override
            public void buttonClick(Button.ClickEvent event) {
                doDeleteGood();
            }
        }));
        for (int i = 0; i < 4; i++) {
            goodsControlLayout.getComponent(i).setWidth("200px");
            goodsControlLayout.setComponentAlignment(goodsControlLayout.getComponent(i), Alignment.TOP_CENTER);
        }
        goodsControlPanel.setContent(goodsControlLayout);
        goodsControlPanel.setWidth("250px");
        goodsControlPanel.setHeight(12.75f, Unit.CM);
        //userData.setHeight(19.0f, Unit.CM);
        layout.addComponent(mainMenu, 0, 0, 2, 0);
        layout.addComponent(userData, 0, 1, 0, 1);
        layout.addComponent(allGoodsPanel, 1, 1, 1, 2);
        layout.addComponent(goodsControlPanel, 0, 2, 0, 2);

        layout.setColumnExpandRatio(0, 0.0f);
        layout.setColumnExpandRatio(1, 0.0f);
        layout.setColumnExpandRatio(2, 0.0f);
        layout.setRowExpandRatio(0, 0.0f);
        layout.setRowExpandRatio(1, 0.0f);
        layout.setRowExpandRatio(2, 0.0f);
        layout.setSpacing(false);
        layout.setMargin(false);

        fullLayout.addComponent(layout);
        fullLayout.setComponentAlignment(layout, Alignment.MIDDLE_CENTER);
        fullLayout.setMargin(false);
        fullLayout.setSpacing(false);

        getUI().setContent(fullLayout);

    }

    private void goToAddGood() {
        Window addGoodWindow = new Window("Новый товар");
        VerticalLayout addGoodLayout = new VerticalLayout();
        allContragents = stockHelper.getContragents();
        allTypes = stockHelper.getGoodsTypes();
        allPriceTypes = stockHelper.getPriceTypes();
        allSertificates = stockHelper.getAllSertificates();
        allPackages = stockHelper.getPackages();
        newGoodPrices = new ArrayList<>();
        newGood = new Goods();
        Panel mainData = new Panel("Основные данные");
        HorizontalLayout mainDataLayout = new HorizontalLayout();
        TextField newGoodArticul = new TextField("Артикул товара");
        newGoodArticul.setEnabled(false);
        TextField newGoodName = new TextField("Наименование товара");
        newGoodName.setRequiredIndicatorVisible(true);
        ArrayList<String> types = new ArrayList<>();
        for (GoodsType type : allTypes) {
            types.add(type.getTypeName());
        }

        ArrayList<String> providers = new ArrayList<>();
        for (Contragents contragent : allContragents) {
            if (contragent.getContragentType().getId() == 1) {
                providers.add(contragent.getDisplayName());
            }
        }

        ArrayList<String> sertificates = new ArrayList<>();
        for (Sertificates sertificate : allSertificates) {
            sertificates.add(sertificate.getSertHeader());
        }

        ComboBox newGoodType = new ComboBox("Тип товара");
        newGoodType.setItems(types);
        newGoodType.setEmptySelectionAllowed(false);
        newGoodType.setRequiredIndicatorVisible(true);

        ComboBox newGoodProvider = new ComboBox("Поставщик");
        newGoodProvider.setItems(providers);
        newGoodProvider.setEmptySelectionAllowed(false);
        newGoodProvider.setRequiredIndicatorVisible(true);

        ComboBox newGoodSertificate = new ComboBox("Сертификат");
        newGoodSertificate.setItems(sertificates);
        newGoodSertificate.setEmptySelectionAllowed(false);
        newGoodSertificate.setRequiredIndicatorVisible(true);

        mainDataLayout.addComponents(newGoodArticul, newGoodName, newGoodType, newGoodProvider, newGoodSertificate);
        mainData.setContent(mainDataLayout);
        mainDataLayout.setSizeFull();

        Panel packageData = new Panel("Настройки фасовки");
        VerticalLayout packageLayout = new VerticalLayout();
        HorizontalLayout basePackageLayout = new HorizontalLayout();

        ArrayList<String> newGoodPackages = new ArrayList<>();
        for (Packages newPackage : allPackages) {
            newGoodPackages.add(newPackage.getPackageName());
        }
        ComboBox newGoodBasePackage = new ComboBox("Базовый тип фасовки");
        newGoodBasePackage.setItems(newGoodPackages);
        newGoodBasePackage.setEmptySelectionAllowed(false);
        newGoodBasePackage.setRequiredIndicatorVisible(true);

        TextField newGoodBaseMass = new TextField("Вес");
        newGoodBaseMass.addValueChangeListener(new HasValue.ValueChangeListener<String>() {
            @Override
            public void valueChange(HasValue.ValueChangeEvent<String> event) {
                newGoodBaseMass.setValue(newGoodBaseMass.getValue().replace(",", "."));
            }
        });
        newGoodBaseMass.setRequiredIndicatorVisible(true);

        basePackageLayout.addComponents(newGoodBasePackage, newGoodBaseMass);
        basePackageLayout.setSizeFull();
        HorizontalLayout mainPackageLayout = new HorizontalLayout();

        ComboBox newGoodMainPackage = new ComboBox("Основной тип фасовки");
        newGoodMainPackage.setItems(newGoodPackages);
        newGoodMainPackage.setEmptySelectionAllowed(false);
        newGoodMainPackage.setRequiredIndicatorVisible(true);

        TextField newGoodMainFactor = new TextField("Коэффициент");
        newGoodMainFactor.addValueChangeListener(new HasValue.ValueChangeListener<String>() {
            @Override
            public void valueChange(HasValue.ValueChangeEvent<String> event) {
                newGoodMainFactor.setValue(newGoodMainFactor.getValue().replace(",", "."));
            }
        });
        newGoodMainFactor.setRequiredIndicatorVisible(true);

        mainPackageLayout.addComponents(newGoodMainPackage, newGoodMainFactor);
        mainPackageLayout.setSizeFull();
        CheckBox useBasePackageOnly = new CheckBox("Использовать только базовый тип фасовки");

        packageLayout.addComponents(basePackageLayout, mainPackageLayout, useBasePackageOnly);
        packageLayout.setSizeFull();
        packageData.setContent(packageLayout);

        Panel pricesData = new Panel("Настройки цен");
        VerticalLayout pricesLayout = new VerticalLayout();

        Panel addPrice = new Panel("Добавление новой цены");
        HorizontalLayout addPriceLayout = new HorizontalLayout();

        ArrayList<String> newGoodPriceTypes = new ArrayList<>();
        for (PriceType priceType : allPriceTypes) {
            newGoodPriceTypes.add(priceType.getName());
        }

        ComboBox newGoodAddPriceType = new ComboBox("Тип цены");
        newGoodAddPriceType.setItems(newGoodPriceTypes);
        newGoodAddPriceType.setEmptySelectionAllowed(false);

        TextField addPriceStart = new TextField("Начальная цена", "0.0");
        addPriceStart.setWidth(3.0f, Unit.CM);
        TextField addPriceCharge = new TextField("Наценка", "0.0");
        addPriceCharge.setWidth(3.0f, Unit.CM);
        TextField addPriceResult = new TextField("Результат", "0.0");
        addPriceResult.setWidth(3.0f, Unit.CM);
        addPriceStart.addValueChangeListener(new HasValue.ValueChangeListener<String>() {
            @Override
            public void valueChange(HasValue.ValueChangeEvent<String> event) {
                try {
                    Double result, start, charge;
                    start = Double.parseDouble(addPriceStart.getValue());
                    charge = Double.parseDouble(addPriceCharge.getValue());
                    result = start + start * charge / 100.0;
                    result = round(result, 2);
                    addPriceResult.setValue(String.valueOf(result));
                } catch (NumberFormatException e) {
                    addPriceStart.setValue(addPriceStart.getValue().replace(",", "."));
                    addPriceCharge.setValue(addPriceCharge.getValue().replace(",", "."));
                }
            }
        });
        addPriceCharge.addValueChangeListener(new HasValue.ValueChangeListener<String>() {
            @Override
            public void valueChange(HasValue.ValueChangeEvent<String> event) {
                try {
                    Double result, start, charge;
                    start = Double.parseDouble(addPriceStart.getValue());
                    charge = Double.parseDouble(addPriceCharge.getValue());
                    result = start + start * charge / 100.0;
                    result = round(result, 2);
                    addPriceResult.setValue(String.valueOf(result));
                } catch (NumberFormatException e) {
                    addPriceStart.setValue(addPriceStart.getValue().replace(",", "."));
                    addPriceCharge.setValue(addPriceCharge.getValue().replace(",", "."));
                }
            }
        });
        Panel allPrices = getNewPricesPanel(newGoodPrices);

        Button doAddPriceButton = new Button("Добавить", new Button.ClickListener() {
            @Override
            public void buttonClick(Button.ClickEvent event) {
                Prices tmpPrice = new Prices();
                PriceType tmpPriceType = null;
                for (PriceType priceType : allPriceTypes) {
                    if (priceType.getName().equals(newGoodAddPriceType.getValue())) {
                        tmpPriceType = priceType;
                    }
                }
                tmpPrice.setGoods(newGood);
                tmpPrice.setPriceType(tmpPriceType);
                Double purchasePrice = 0.0;
                for (Prices newGoodPrice : newGoodPrices) {
                    if (newGoodPrice.getPriceType().getId() == 1) {
                        purchasePrice = newGoodPrice.getStartPrice();

                    }
                }
                if (purchasePrice == 0.0) {
                    tmpPrice.setStartPrice(Double.parseDouble(addPriceStart.getValue()));
                } else {
                    tmpPrice.setStartPrice(purchasePrice);
                }
                tmpPrice.setCharge(Double.parseDouble(addPriceCharge.getValue()));
                tmpPrice.setResult(round(Double.parseDouble(addPriceResult.getValue()), 2));
                if (tmpPrice.getPriceType().getId() == 1) {
                    addPriceStart.setEnabled(false);
                }
                newGoodPrices.add(tmpPrice);
                allPrices.setContent(getNewPricesPanel(newGoodPrices).getContent());

                addGoodWindow.center();
            }
        });

        addPriceLayout.addComponents(newGoodAddPriceType, addPriceStart, addPriceCharge, addPriceResult, doAddPriceButton);
        addPriceLayout.setComponentAlignment(doAddPriceButton, Alignment.MIDDLE_CENTER);

        addPriceLayout.setSizeFull();
        addPriceLayout.setMargin(true);
        addPriceLayout.setSpacing(true);
        addPrice.setContent(addPriceLayout);

        //allPrices.setContent(getPricesPanel(newGoodPrices));
        pricesLayout.addComponents(addPrice, allPrices);
        pricesData.setContent(pricesLayout);

        Button doAddGoodButton = new Button("Добавить", new Button.ClickListener() {
            @Override
            public void buttonClick(Button.ClickEvent event) {
                boolean dataIsOk = true;
                newGood.setCode(String.valueOf((stockHelper.getMaxGoodId() + 1)));
                if (!newGoodName.isEmpty()) {
                    newGood.setName(newGoodName.getValue());
                } else {
                    dataIsOk = false;
                }

                if (!newGoodType.isEmpty()) {
                    GoodsType type = new GoodsType();
                    for (GoodsType allType : allTypes) {
                        if (allType.getTypeName().equals(newGoodType.getValue())) {
                            type = allType;
                        }
                    }
                    newGood.setGoodsType(type);
                } else {
                    dataIsOk = false;
                }

                if (!newGoodProvider.isEmpty()) {
                    Contragents provider = new Contragents();
                    for (Contragents c : allContragents) {
                        if (c.getDisplayName().equals(newGoodProvider.getValue())) {
                            provider = c;
                        }
                    }
                    newGood.setContragents(provider);
                } else {
                    dataIsOk = false;
                }

                if (!newGoodSertificate.isEmpty()) {
                    Sertificates sertificate = null;
                    for (Sertificates allSertificate : allSertificates) {
                        if (allSertificate.getSertHeader().equals(newGoodSertificate.getValue())) {
                            sertificate = allSertificate;
                        }
                    }
                    newGood.setSertificates(sertificate);
                } else {
                    dataIsOk = false;
                }

                if (!newGoodBasePackage.isEmpty()) {
                    Packages base = new Packages();
                    for (Packages allPackage : allPackages) {
                        if (allPackage.getPackageName().equals(newGoodBasePackage.getValue())) {
                            base = allPackage;
                        }
                    }
                    newGood.setPackagesByBasePackageId(base);
                } else {
                    dataIsOk = false;
                }

                if (!newGoodMainPackage.isEmpty()) {
                    Packages main = new Packages();
                    for (Packages allPackage : allPackages) {
                        if (allPackage.getPackageName().equals(newGoodMainPackage.getValue())) {
                            main = allPackage;
                        }
                    }
                    newGood.setPackagesByMainPackageId(main);
                } else {
                    dataIsOk = false;
                }
                try {
                    newGood.setBasePackageMass(Double.parseDouble(newGoodBaseMass.getValue()));
                    newGood.setMainFactor(Double.parseDouble(newGoodMainFactor.getValue()));
                } catch (NumberFormatException e) {
                    dataIsOk = false;
                }
                newGood.setUseBasePackage(useBasePackageOnly.getValue());
                HashSet<Prices> prices = new HashSet<>();
                for (Prices newGoodPrice : newGoodPrices) {
                    prices.add(newGoodPrice);
                }
                if (!prices.isEmpty()) {
                    newGood.setPriceses(prices);
                } else {
                    dataIsOk = false;
                }
                try {
                newGood.setMainPackageMass(newGood.getBasePackageMass() * newGood.getMainFactor());
                } catch (Exception e) {
                    dataIsOk = false;
                }
                newGood.setRemainder(0.0);
                newGood.setFactRemainder(0.0);
                newGood.setReserve(0.0);
                if (dataIsOk) {
                    doAddGood();
                } else {
                    Notification.show("Не заполнены необходимые поля");
                }
            }
        });

        addGoodLayout.addComponent(mainData);
        addGoodLayout.addComponent(packageData);
        addGoodLayout.addComponent(pricesData);
        addGoodLayout.addComponent(doAddGoodButton);
        addGoodLayout.setComponentAlignment(doAddGoodButton, Alignment.TOP_CENTER);

        mainDataLayout.setMargin(true);
        mainDataLayout.setSpacing(true);

        packageLayout.setMargin(true);
        packageLayout.setSpacing(true);

        pricesLayout.setMargin(true);
        pricesLayout.setSpacing(true);

        addGoodWindow.setContent(addGoodLayout);
        addGoodWindow.center();
        addGoodWindow.setModal(true);
        MyUI.getCurrent().addWindow(addGoodWindow);

    }

    private void goToViewGood() {
        viewGoodPrices = stockHelper.getPricesByGoodId(viewGood.getId());

        Window viewGoodWindow = new Window("Просмотр данных товара");
        VerticalLayout viewGoodLayout = new VerticalLayout();

        Panel mainData = new Panel("Основные данные");
        VerticalLayout mainDataLayout = new VerticalLayout();

        HorizontalLayout line1 = new HorizontalLayout();
        TextField articul = new TextField("Артикул", viewGood.getCode());
        articul.setEnabled(false);
        TextField name = new TextField("Наименование товара", viewGood.getName());
        name.setEnabled(false);
        TextField type = new TextField("Тип товара", viewGood.getGoodsType().getTypeName());
        type.setEnabled(false);
        TextField provider = new TextField("Поставщик", viewGood.getContragents().getDisplayName());
        provider.setEnabled(false);
        TextField sertificate = new TextField("Сертификат", viewGood.getSertificates().getSertHeader());
        sertificate.setEnabled(false);
        line1.addComponents(articul, name, type, provider, sertificate);
//        line1.setSizeFull();
//        line1.setMargin(true);
//        line1.setSpacing(true);

        HorizontalLayout line2 = new HorizontalLayout();
        TextField remainder = new TextField("Остаток", String.valueOf(viewGood.getRemainder()));
        remainder.setEnabled(false);
        TextField reserve = new TextField("Резерв", String.valueOf(viewGood.getReserve()));
        reserve.setEnabled(false);
        TextField factRemainder = new TextField("Фактический остаток", String.valueOf(viewGood.getFactRemainder()));
        factRemainder.setEnabled(false);
        line2.addComponents(remainder, reserve, factRemainder);
//        line2.setSizeFull();
//        line2.setMargin(true);
//        line2.setSpacing(true);

        mainDataLayout.addComponents(line1, line2);
        mainData.setContent(mainDataLayout);
        mainDataLayout.setMargin(true);
        mainDataLayout.setSpacing(true);

        Panel packageData = new Panel("Данные о фасовке");
        VerticalLayout packageDataLayout = new VerticalLayout();

        HorizontalLayout basePackageLayout = new HorizontalLayout();
        TextField basePackageType = new TextField("Базовый тип фасовки", viewGood.getPackagesByBasePackageId().getPackageName());
        basePackageType.setEnabled(false);

        TextField basePackageMass = new TextField("Вес", String.valueOf(viewGood.getBasePackageMass()));
        basePackageMass.setEnabled(false);

        basePackageLayout.addComponents(basePackageType, basePackageMass);
        basePackageLayout.setSizeFull();
        basePackageLayout.setMargin(false);
        basePackageLayout.setSpacing(false);

        HorizontalLayout mainPackageLayout = new HorizontalLayout();
        TextField mainPackageType = new TextField("Основной тип фасовки", viewGood.getPackagesByMainPackageId().getPackageName());
        mainPackageType.setEnabled(false);

        TextField mainPackageFactor = new TextField("Коэффициент", String.valueOf(viewGood.getMainFactor()));
        mainPackageFactor.setEnabled(false);

        mainPackageLayout.addComponents(mainPackageType, mainPackageFactor);
        mainPackageLayout.setSizeFull();
        mainPackageLayout.setMargin(false);
        mainPackageLayout.setSpacing(false);

        CheckBox useBasePackageOnly = new CheckBox("Использовать только базовый тип фасовки", viewGood.getUseBasePackage());
        useBasePackageOnly.setEnabled(false);
        packageDataLayout.addComponents(basePackageLayout, mainPackageLayout, useBasePackageOnly);
        packageData.setContent(packageDataLayout);
        packageDataLayout.setMargin(true);
        packageDataLayout.setSpacing(true);

        Panel priceData = new Panel("Данные о ценах");
        VerticalLayout priceDataLayout = new VerticalLayout();

        for (Prices viewGoodPrice : viewGoodPrices) {
            HorizontalLayout priceLayout = new HorizontalLayout();
            priceLayout.addComponent(new Label(viewGoodPrice.getPriceType().getName()));
            priceLayout.addComponent(new Label(String.valueOf(viewGoodPrice.getResult()) + " руб."));
            priceLayout.addComponent(new Button("История цены", new Button.ClickListener() {
                @Override
                public void buttonClick(Button.ClickEvent event) {
                    goToPriceLog(viewGoodPrice);
                }
            }));
            priceLayout.setComponentAlignment(priceLayout.getComponent(2), Alignment.TOP_RIGHT);
            priceLayout.setSizeFull();
            priceLayout.setMargin(false);
            priceLayout.setSpacing(false);
            priceDataLayout.addComponent(priceLayout);
        }

        priceData.setContent(priceDataLayout);
        priceDataLayout.setMargin(true);
        priceDataLayout.setSpacing(true);

        viewGoodLayout.addComponent(mainData);
        viewGoodLayout.addComponent(packageData);
        viewGoodLayout.addComponent(priceData);
        viewGoodWindow.setContent(viewGoodLayout);
        viewGoodWindow.center();
        viewGoodWindow.setModal(true);
        MyUI.getCurrent().addWindow(viewGoodWindow);
    }

    private void goToPriceLog(Prices price) {
        Window priceLogWindow = new Window("История цены");
        VerticalLayout priceLogLayout = new VerticalLayout();

        List<PriceLog> tmpLog = stockHelper.getGoodPriceLog(price.getGoods().getId());
        List<PriceLog> log = new ArrayList<>();
        for (PriceLog priceLog : tmpLog) {
            if (priceLog.getPriceType().getId() == price.getPriceType().getId()) {
                log.add(priceLog);
            }
        }

        Label header = new Label("История цены: \"" + price.getPriceType().getName() + "\" на товар: " + price.getGoods().getName());

        Grid<PriceLog> priceLogGrid = new Grid<>(PriceLog.class);
        priceLogGrid.removeAllColumns();
        priceLogGrid.addColumn(PriceLog::getDate, new DateRenderer()).setCaption("Дата");
        priceLogGrid.addColumn((priceLog) -> priceLog.getUsers().getLogin(), new TextRenderer()).setCaption("Пользователь");
        priceLogGrid.addColumn(PriceLog::getOldPrice, new NumberRenderer("%.2f")).setCaption("Старая цена");
        priceLogGrid.addColumn(PriceLog::getNewPrice, new NumberRenderer("%.2f")).setCaption("Новая цена");
        priceLogGrid.setItems(log);
        priceLogGrid.setSizeFull();
        priceLogLayout.addComponent(header);
        priceLogLayout.addComponent(priceLogGrid);

        priceLogWindow.setContent(priceLogLayout);
        priceLogWindow.setWidth(15.0f, Unit.CM);
        priceLogWindow.center();
        priceLogWindow.setModal(true);
        MyUI.getCurrent().addWindow(priceLogWindow);
    }

    private void goToEditGood() {
        Window editGoodWindow = new Window("Редактирование товара");
        VerticalLayout editGoodLayout = new VerticalLayout();

        allContragents = stockHelper.getContragents();
        allTypes = stockHelper.getGoodsTypes();
        allPriceTypes = stockHelper.getPriceTypes();
        allSertificates = stockHelper.getAllSertificates();
        allPackages = stockHelper.getPackages();
        editGoodPrices = stockHelper.getPricesByGoodId(editGood.getId());

        HashMap<String, Contragents> providers = new HashMap<>();
        for (Contragents allContragent : allContragents) {
            if (allContragent.getContragentType().getId() == 1) {
                providers.put(allContragent.getDisplayName(), allContragent);
            }
        }

        HashMap<String, GoodsType> goodsTypes = new HashMap<>();
        for (GoodsType allType : allTypes) {
            goodsTypes.put(allType.getTypeName(), allType);
        }

        HashMap<String, PriceType> priceTypes = new HashMap<>();
        for (PriceType allPriceType : allPriceTypes) {
            priceTypes.put(allPriceType.getName(), allPriceType);
        }

        HashMap<String, Sertificates> sertificates = new HashMap<>();
        for (Sertificates allSertificate : allSertificates) {
            sertificates.put(allSertificate.getSertHeader(), allSertificate);
        }

        HashMap<String, Packages> packageTypes = new HashMap<>();
        for (Packages allPackage : allPackages) {
            packageTypes.put(allPackage.getPackageName(), allPackage);
        }

        Panel mainData = new Panel("Основные данные");
        HorizontalLayout mainDataLayout = new HorizontalLayout();

        TextField editGoodArticul = new TextField("Артикул", editGood.getCode());
        editGoodArticul.setEnabled(false);

        TextField editGoodName = new TextField("Наименование товара", editGood.getName());
        editGoodName.setRequiredIndicatorVisible(true);

        ComboBox editGoodType = new ComboBox("Тип товара", goodsTypes.keySet());
        editGoodType.setRequiredIndicatorVisible(true);
        editGoodType.setEmptySelectionAllowed(false);
        editGoodType.setValue(editGood.getGoodsType().getTypeName());

        ComboBox editGoodProvider = new ComboBox("Поставщик", providers.keySet());
        editGoodProvider.setRequiredIndicatorVisible(true);
        editGoodProvider.setEmptySelectionAllowed(false);
        editGoodProvider.setValue(editGood.getContragents().getDisplayName());

        ComboBox editGoodSertificate = new ComboBox("Сертификат", sertificates.keySet());
        editGoodSertificate.setRequiredIndicatorVisible(true);
        editGoodSertificate.setEmptySelectionAllowed(false);
        editGoodSertificate.setValue(editGood.getSertificates().getSertHeader());

        mainDataLayout.addComponents(editGoodArticul, editGoodName, editGoodType, editGoodProvider, editGoodSertificate);
        mainDataLayout.setSizeFull();
        mainData.setContent(mainDataLayout);

        Panel packageData = new Panel("Настройки фасовки");
        VerticalLayout packageDataLayout = new VerticalLayout();
        HorizontalLayout basePackageLayout = new HorizontalLayout();

        ComboBox editGoodBasePackage = new ComboBox("Базовый тип фасовки", packageTypes.keySet());
        editGoodBasePackage.setRequiredIndicatorVisible(true);
        editGoodBasePackage.setEmptySelectionAllowed(false);
        editGoodBasePackage.setValue(editGood.getPackagesByBasePackageId().getPackageName());

        TextField editGoodBasePackageMass = new TextField("Вес", String.valueOf(editGood.getBasePackageMass()));
        editGoodBasePackageMass.addValueChangeListener(new HasValue.ValueChangeListener<String>() {
            @Override
            public void valueChange(HasValue.ValueChangeEvent<String> event) {
                editGoodBasePackageMass.setValue(editGoodBasePackageMass.getValue().replace(",", "."));
            }
        });
        editGoodBasePackageMass.setRequiredIndicatorVisible(true);

        basePackageLayout.addComponents(editGoodBasePackage, editGoodBasePackageMass);
        basePackageLayout.setSizeFull();

        HorizontalLayout mainPackageLayout = new HorizontalLayout();

        ComboBox editGoodMainPackage = new ComboBox("Основной тип фасовки", packageTypes.keySet());
        editGoodMainPackage.setRequiredIndicatorVisible(true);
        editGoodMainPackage.setEmptySelectionAllowed(false);
        editGoodMainPackage.setValue(editGood.getPackagesByMainPackageId().getPackageName());

        TextField editGoodMainFactor = new TextField("Коэффициент", String.valueOf(editGood.getMainFactor()));
        editGoodMainFactor.addValueChangeListener(new HasValue.ValueChangeListener<String>() {
            @Override
            public void valueChange(HasValue.ValueChangeEvent<String> event) {
                editGoodMainFactor.setValue(editGoodMainFactor.getValue().replace(",", "."));
            }
        });
        editGoodMainFactor.setRequiredIndicatorVisible(true);

        mainPackageLayout.addComponents(editGoodMainPackage, editGoodMainFactor);
        mainPackageLayout.setSizeFull();

        CheckBox editGoodUseMainPackage = new CheckBox("Использовать только базовый тип фасовки", editGood.getUseBasePackage());

        packageDataLayout.addComponents(basePackageLayout, mainPackageLayout, editGoodUseMainPackage);
        packageDataLayout.setSizeFull();
        packageData.setContent(packageDataLayout);

        Panel pricesConfig = new Panel("Настройки цен");
        VerticalLayout pricesConfigLayout = new VerticalLayout();

        Panel addPrice = new Panel("Добавление новой цены");
        HorizontalLayout addPriceLayout = new HorizontalLayout();

        ComboBox selectPriceType = new ComboBox("Тип цены", priceTypes.keySet());
        selectPriceType.setEmptySelectionAllowed(false);

        TextField startPrice = new TextField("Начальная цена", "0.0");

        TextField chargePrice = new TextField("Наценка", "0.0");

        TextField resultPrice = new TextField("Итоговая цена", "0.0");

        startPrice.addValueChangeListener(new HasValue.ValueChangeListener<String>() {
            @Override
            public void valueChange(HasValue.ValueChangeEvent<String> event) {
                try {
                    Double start = Double.parseDouble(startPrice.getValue());
                    Double charge = Double.parseDouble(chargePrice.getValue());
                    Double result = start + start * charge / 100.0;
                    result = round(result, 2);
                    resultPrice.setValue(String.valueOf(result));
                } catch (NumberFormatException e) {
                    startPrice.setValue(startPrice.getValue().replace(",", "."));
                    chargePrice.setValue(chargePrice.getValue().replace(",", "."));
                }
            }
        });

        chargePrice.addValueChangeListener(new HasValue.ValueChangeListener<String>() {
            @Override
            public void valueChange(HasValue.ValueChangeEvent<String> event) {
                try {
                    Double start = Double.parseDouble(startPrice.getValue());
                    Double charge = Double.parseDouble(chargePrice.getValue());
                    Double result = start + start * charge / 100.0;
                    result = round(result, 2);
                    resultPrice.setValue(String.valueOf(result));
                } catch (NumberFormatException e) {
                    startPrice.setValue(startPrice.getValue().replace(",", "."));
                    chargePrice.setValue(chargePrice.getValue().replace(",", "."));
                }
            }
        });

        Button doAddPriceButton = new Button("Добавить", new Button.ClickListener() {
            @Override
            public void buttonClick(Button.ClickEvent event) {
                Prices tmp = new Prices();
                tmp.setGoods(editGood);
                try {
                    tmp.setPriceType(priceTypes.get(selectPriceType.getValue()));
                    tmp.setStartPrice(Double.parseDouble(startPrice.getValue()));
                    tmp.setCharge(Double.parseDouble(chargePrice.getValue()));
                    tmp.setResult(Double.parseDouble(resultPrice.getValue()));
                    stockHelper.addDBItem(tmp);
                    editGoodPrices = stockHelper.getPricesByGoodId(editGood.getId());
                    priceData.setContent(getPricesPanel(editGoodPrices).getContent());
                    editGoodWindow.center();
                } catch (NumberFormatException e) {
                    startPrice.setValue(startPrice.getValue().replace(",", "."));
                    chargePrice.setValue(chargePrice.getValue().replace(",", "."));
                    resultPrice.setValue(resultPrice.getValue().replace(",", "."));
                }

            }
        });
        addPriceLayout.addComponents(selectPriceType, startPrice, chargePrice, resultPrice, doAddPriceButton);
        addPriceLayout.setComponentAlignment(doAddPriceButton, Alignment.MIDDLE_CENTER);
        addPriceLayout.setSpacing(true);
        addPriceLayout.setMargin(true);
        addPriceLayout.setSizeFull();
        addPrice.setContent(addPriceLayout);
        editGoodPrices = stockHelper.getPricesByGoodId(editGood.getId());
        priceData = getPricesPanel(editGoodPrices);

        pricesConfigLayout.addComponents(addPrice, priceData);
        pricesConfig.setContent(pricesConfigLayout);

        Button doEditGoodButton = new Button("Сохранить", new Button.ClickListener() {
            @Override
            public void buttonClick(Button.ClickEvent event) {
                boolean dataIsOk = true;
                editGoodPrices = stockHelper.getPricesByGoodId(editGood.getId());
                if (!editGoodName.isEmpty()) {
                    editGood.setName(editGoodName.getValue());
                } else {
                    dataIsOk = false;
                }
                if (!editGoodType.isEmpty()) {
                    editGood.setGoodsType(goodsTypes.get(editGoodType.getValue()));
                } else {
                    dataIsOk = false;
                }
                if (!editGoodProvider.isEmpty()) {
                    editGood.setContragents(providers.get(editGoodProvider.getValue()));
                } else {
                    dataIsOk = false;
                }
                if (!editGoodSertificate.isEmpty()) {
                    editGood.setSertificates(sertificates.get(editGoodSertificate.getValue()));
                } else {
                    dataIsOk = false;
                }
                if (!editGoodBasePackage.isEmpty()) {
                    editGood.setPackagesByBasePackageId(packageTypes.get(editGoodBasePackage.getValue()));
                } else {
                    dataIsOk = false;
                }
                try {
                    editGood.setBasePackageMass(Double.parseDouble(editGoodBasePackageMass.getValue()));
                } catch (NumberFormatException e) {
                    dataIsOk = false;
                }
                if (!editGoodMainPackage.isEmpty()) {
                    editGood.setPackagesByMainPackageId(packageTypes.get(editGoodMainPackage.getValue()));
                } else {
                    dataIsOk = false;
                }
                try {
                    editGood.setMainFactor(Double.parseDouble(editGoodMainFactor.getValue()));
                } catch (NumberFormatException e) {
                    dataIsOk = false;
                }
                editGood.setMainPackageMass(editGood.getBasePackageMass() * editGood.getMainFactor());
                editGood.setUseBasePackage(editGoodUseMainPackage.getValue());
                HashSet<Prices> prices = new HashSet<>();
                for (Prices newGoodPrice : editGoodPrices) {
                    prices.add(newGoodPrice);
                }
                editGood.setPriceses(prices);
                if (dataIsOk) {
                    doEditGood();
                } else {
                    Notification.show("Заполнены не все необходимые поля");
                }
            }
        });
        mainDataLayout.setMargin(true);
        mainDataLayout.setSpacing(true);
        packageDataLayout.setMargin(true);
        packageDataLayout.setSpacing(true);

        editGoodLayout.addComponent(mainData);
        editGoodLayout.addComponent(packageData);
        editGoodLayout.addComponent(pricesConfig);
        editGoodLayout.addComponent(doEditGoodButton);
        editGoodLayout.setComponentAlignment(doEditGoodButton, Alignment.TOP_CENTER);

        editGoodWindow.setContent(editGoodLayout);
        editGoodWindow.center();
        editGoodWindow.setModal(true);
        MyUI.getCurrent().addWindow(editGoodWindow);
    }

    private Panel getPricesPanel(List<Prices> prices) {

        Panel pricePanel = new Panel("Список цен");
        VerticalLayout pricesLayout = new VerticalLayout();
        for (Prices price : prices) {
            HorizontalLayout priceLayout = new HorizontalLayout();
            priceLayout.addComponent(new Label(price.getPriceType().getName()));
            priceLayout.addComponent(new Label("Наценка: " + String.valueOf(round(price.getCharge(), 2)) + " %"));
            priceLayout.addComponent(new Label("Итог: " + String.valueOf(round(price.getResult(), 2)) + " руб."));
            priceLayout.addComponent(new Button("Редактировать", (Button.ClickEvent event) -> {
                goToEditPrice(price, pricePanel);
            }));
            priceLayout.addComponent(new Button("Удалить", (Button.ClickEvent event) -> {
                doDeletePrice(price, pricePanel);
            }));
            priceLayout.setSizeFull();
            pricesLayout.addComponent(priceLayout);
        }
        pricePanel.setContent(pricesLayout);
        pricesLayout.setMargin(true);
        pricesLayout.setSpacing(true);
        return pricePanel;
    }

    private Panel getNewPricesPanel(List<Prices> prices) {

        Panel pricePanel = new Panel("Список цен");
        VerticalLayout pricesLayout = new VerticalLayout();
        for (Prices price : prices) {
            HorizontalLayout priceLayout = new HorizontalLayout();
            priceLayout.addComponent(new Label(price.getPriceType().getName()));
            priceLayout.addComponent(new Label("Наценка: " + String.valueOf(round(price.getCharge(), 2)) + " %"));
            priceLayout.addComponent(new Label("Итог: " + String.valueOf(round(price.getResult(), 2)) + " руб."));

            priceLayout.addComponent(new Button("Удалить", (Button.ClickEvent event) -> {
                for (int i = 0; i < prices.size(); i++) {
                    if (prices.get(i).equals(price)) {
                        prices.remove(i);
                        pricesLayout.removeComponent(pricesLayout.getComponent(i));
                    }
                }
            }));
            priceLayout.setSizeFull();
            pricesLayout.addComponent(priceLayout);
        }
        pricePanel.setContent(pricesLayout);
        pricesLayout.setMargin(true);
        pricesLayout.setSpacing(true);
        return pricePanel;
    }

    private void goToEditPrice(Prices price, Panel pricePanel) {
        Window editPriceWindow = new Window("Редактирование цены");
        VerticalLayout editPriceLayout = new VerticalLayout();
        Label priceType = new Label(price.getPriceType().getName());
        TextField startPrice = new TextField("Начальная цена", String.valueOf(price.getStartPrice()));
        TextField charge = new TextField("Наценка", String.valueOf(price.getCharge()));
        Double tmpRes = price.getStartPrice() + price.getStartPrice() * price.getCharge() / 100.0;
        tmpRes = round(tmpRes, 2);
        TextField result = new TextField("Итог", String.valueOf(tmpRes));
        startPrice.addValueChangeListener(new HasValue.ValueChangeListener<String>() {
            @Override
            public void valueChange(HasValue.ValueChangeEvent<String> event) {
                try {
                Double start = Double.parseDouble(startPrice.getValue());
                Double chargeD = Double.parseDouble(charge.getValue());
                Double resultD = start + start * chargeD / 100.0;
                resultD = round(resultD, 2);
                result.setValue(String.valueOf(resultD));
                } catch (NumberFormatException e) {
                    startPrice.setValue(startPrice.getValue().replace(",", "."));
                    charge.setValue(charge.getValue().replace(",", "."));
                }
            }
        });
        charge.addValueChangeListener(new HasValue.ValueChangeListener<String>() {
            @Override
            public void valueChange(HasValue.ValueChangeEvent<String> event) {
                try {
                Double start = Double.parseDouble(startPrice.getValue());
                Double chargeD = Double.parseDouble(charge.getValue());
                Double resultD = start + start * chargeD / 100.0;
                resultD = round(resultD, 2);
                result.setValue(String.valueOf(resultD));
                } catch (NumberFormatException e) {
                    startPrice.setValue(startPrice.getValue().replace(",", "."));
                    charge.setValue(charge.getValue().replace(",", "."));
                }
            }
        });
        Button doEditPriceButton = new Button("Сохранить", new Button.ClickListener() {
            @Override
            public void buttonClick(Button.ClickEvent event) {
                Double oldPrice, newPrice;
                price.setStartPrice(Double.parseDouble(startPrice.getValue()));
                price.setCharge(Double.parseDouble(charge.getValue()));
                oldPrice = price.getResult();
                price.setResult(round(Double.parseDouble(result.getValue()), 2));
                newPrice = price.getResult();
                stockHelper.updateDBItem(price);
                Users currentUser = (Users) VaadinService.getCurrentRequest().getWrappedSession().getAttribute("user");
                if (!Objects.equals(newPrice, oldPrice)) {
                    PriceLog log = new PriceLog(editGood, price.getPriceType(), currentUser, new Date(), oldPrice, newPrice);
                    stockHelper.addDBItem(log);
                    if (price.getPriceType().getId() == 1) {
                        for (Prices editGoodPrice : editGoodPrices) {
                            editGoodPrice.setStartPrice(price.getResult());
                            stockHelper.updateDBItem(editGoodPrice);
                        }
                    }
                }
                UI.getCurrent().removeWindow(editPriceWindow);
//                pricePanel.setContent(getPricesPanel().getContent());
                VerticalLayout tmp = (VerticalLayout) priceData.getContent();
                tmp.removeAllComponents();
                editGoodPrices = stockHelper.getPricesByGoodId(editGood.getId());
                priceData.setContent(getPricesPanel(editGoodPrices).getContent());
            }
        });
        editPriceLayout.addComponents(priceType, startPrice, charge, result, doEditPriceButton);
        editPriceLayout.setComponentAlignment(doEditPriceButton, Alignment.TOP_CENTER);

        editPriceWindow.setContent(editPriceLayout);
        editPriceWindow.center();
        editPriceWindow.setModal(true);
        MyUI.getCurrent().addWindow(editPriceWindow);
    }

    private void doDeletePrice(Prices price, Panel pricePanel) {

        for (int i = 0; i < editGoodPrices.size(); i++) {
            if (editGoodPrices.get(i).equals(price)) {
                editGoodPrices.remove(price);
                break;
            }
        }
        stockHelper.deleteDBItem(price);
        editGoodPrices = stockHelper.getPricesByGoodId(editGood.getId());
        pricePanel.setContent(getPricesPanel(editGoodPrices).getContent());

    }

    private void doAddGood() {

        stockHelper.addDBItem(newGood);
        for (Prices newGoodPrice : newGoodPrices) {
            stockHelper.addDBItem(newGoodPrice);
        }
        newGoodPrices.clear();
        newGood = new Goods();
        Page.getCurrent().reload();
    }

    private void doEditGood() {
        stockHelper.updateDBItem(editGood);
        for (Prices editGoodPrice : editGoodPrices) {
            stockHelper.saveOrUpdateDBItem(editGoodPrice);
        }
        editGood = null;
        viewGood = null;
        deleteGood = null;
        Page.getCurrent().reload();
    }

    private void doDeleteGood() {
        List<Prices> deletePrices = stockHelper.getPricesByGoodId(deleteGood.getId());
        List<PriceLog> deletePriceLogs = stockHelper.getGoodPriceLog(deleteGood.getId());
        for (PriceLog deletePriceLog : deletePriceLogs) {
            stockHelper.deleteDBItem(deletePriceLog);
        }

        for (Prices deletePrice : deletePrices) {
            stockHelper.deleteDBItem(deletePrice);
        }

        stockHelper.deleteDBItem(deleteGood);
        editGood = null;
        viewGood = null;
        deleteGood = null;
        Page.getCurrent().reload();
    }

    private double round(double number, int scale) {
        int pow = 10;
        for (int i = 1; i < scale; i++) {
            pow *= 10;
        }
        double tmp = number * pow;
        return (double) (int) ((tmp - (int) tmp) >= 0.5 ? tmp + 1 : tmp) / pow;
    }
    
    private double round(Double number, int scale) {
        int pow = 10;
        for (int i = 1; i < scale; i++) {
            pow *= 10;
        }
        double tmp = number * pow;
        return (double) (int) ((tmp - (int) tmp) >= 0.5 ? tmp + 1 : tmp) / pow;
    }

    final class GoodsGrid extends Grid<Goods> {

        TextField goodArticul;
        TextField goodName;
        TextField goodMinPrice;
        TextField goodMaxPrice;
        TextField goodRemainder;
        ComboBox goodType;
        ComboBox goodProvider;

        public GoodsGrid(List<Goods> allGoods, List<Contragents> contragents, List<GoodsType> types) {
            this.addColumn(Goods::getCode, new TextRenderer()).setCaption("Артикул");
            this.addColumn(Goods::getName, new TextRenderer()).setCaption("Наименование");
            this.addColumn((goods) -> goods.getGoodsType().getTypeName(), new TextRenderer()).setCaption("Тип");
            this.addColumn((goods) -> goods.getContragents().getDisplayName(), new TextRenderer()).setCaption("Поставщик");
            this.addColumn((goods) -> {
                return stockHelper.getRetailPriceByGoodId(goods.getId()) + " руб.";
            }, new TextRenderer()).setCaption("Розничная цена");
            this.addColumn((goods) -> {
                String result;
                Packages base = goods.getPackagesByBasePackageId();
                result = String.valueOf(goods.getFactRemainder()) + " " + base.getPackageName();
                return result;
            }, new TextRenderer()).setCaption("Факт. остаток");
            this.setSelectionMode(SelectionMode.SINGLE);
            this.addSelectionListener(new SelectionListener<Goods>() {
                @Override
                public void selectionChange(SelectionEvent<Goods> event) {
                    Set<Goods> selectedGoods = event.getAllSelectedItems();
                    for (Goods selectedGood : selectedGoods) {
                        viewGood = selectedGood;
                        editGood = selectedGood;
                        deleteGood = selectedGood;
                    }
                }
            });
            this.setItems(allGoods);
            this.setSizeFull();

            goodArticul = new TextField();
            goodArticul.setPlaceholder("Артикул...");
            goodArticul.setWidth(2.0f, Unit.CM);
            goodArticul.setHeight(0.8f, Unit.CM);

            goodName = new TextField();
            goodName.setPlaceholder("Наименование...");
            goodName.setSizeFull();
            goodName.setHeight(0.8f, Unit.CM);

            goodMinPrice = new TextField();
            goodMinPrice.setPlaceholder("От...");
            goodMinPrice.setWidth(2.0f, Unit.CM);
            goodMinPrice.setHeight(0.8f, Unit.CM);

            goodMaxPrice = new TextField();
            goodMaxPrice.setPlaceholder("До...");
            goodMaxPrice.setWidth(2.0f, Unit.CM);
            goodMaxPrice.setHeight(0.8f, Unit.CM);

            goodRemainder = new TextField();
            goodRemainder.setPlaceholder("От...");
            goodRemainder.setWidth(2.5f, Unit.CM);
            goodRemainder.setHeight(0.8f, Unit.CM);

            HashMap<String, Contragents> providers = new HashMap<>();
            for (Contragents contragent : contragents) {
                if (contragent.getContragentType().getId() == 1) {
                    providers.put(contragent.getDisplayName(), contragent);
                }
            }
            HashMap<String, GoodsType> goodsTypes = new HashMap<>();
            for (GoodsType type : types) {
                goodsTypes.put(type.getTypeName(), type);
            }

            goodType = new ComboBox();
            goodType.setPlaceholder("Тип товара...");
            goodType.setItems(goodsTypes.keySet());
            goodType.setEmptySelectionAllowed(false);
            goodType.setSizeFull();
            goodType.setHeight(0.8f, Unit.CM);

            goodProvider = new ComboBox();
            goodProvider.setPlaceholder("Поставщик...");
            goodProvider.setItems(providers.keySet());
            goodProvider.setEmptySelectionAllowed(false);
            goodProvider.setSizeFull();
            goodProvider.setHeight(0.8f, Unit.CM);

            Button doFilterButton = new Button("Применить", new Button.ClickListener() {
                @Override
                public void buttonClick(Button.ClickEvent event) {
                    doFilter();
                }
            });
            doFilterButton.setSizeFull();
            doFilterButton.setHeight(0.8f, Unit.CM);
            Button doResetFilterButton = new Button("Сбросить", new Button.ClickListener() {
                @Override
                public void buttonClick(Button.ClickEvent event) {
                    doResetFilter();
                }
            });
            doResetFilterButton.setSizeFull();
            doResetFilterButton.setHeight(0.8f, Unit.CM);
            HeaderRow filterRow = this.appendHeaderRow();
            filterRow.getCell(this.getColumns().get(0)).setComponent(goodArticul);
            filterRow.getCell(this.getColumns().get(1)).setComponent(goodName);
            filterRow.getCell(this.getColumns().get(2)).setComponent(goodType);
            filterRow.getCell(this.getColumns().get(3)).setComponent(goodProvider);
            HorizontalLayout prices = new HorizontalLayout();
            prices.addComponent(goodMinPrice);
            prices.setComponentAlignment(goodMinPrice, Alignment.TOP_CENTER);
            prices.addComponent(goodMaxPrice);
            prices.setComponentAlignment(goodMaxPrice, Alignment.TOP_CENTER);
            filterRow.getCell(this.getColumns().get(4)).setComponent(prices);
            filterRow.getCell(this.getColumns().get(5)).setComponent(goodRemainder);
            HeaderRow buttonsRow = this.appendHeaderRow();
            buttonsRow.getCell(this.getColumns().get(4)).setComponent(doFilterButton);
            buttonsRow.getCell(this.getColumns().get(5)).setComponent(doResetFilterButton);
        }

        private void doFilter() {

            ListDataProvider<Goods> dataProvider = (ListDataProvider<Goods>) this.getDataProvider();
            if (!goodArticul.isEmpty()) {
                dataProvider.addFilter(Goods::getCode, s -> caseInsensitiveContains(s, goodArticul.getValue()));
            }
            if (!goodName.isEmpty()) {
                dataProvider.addFilter(Goods::getName, s -> caseInsensitiveContains(s, goodName.getValue()));
            }
            if (!goodType.isEmpty()) {
                dataProvider.addFilter((goods) -> goods.getGoodsType().getTypeName(),
                        s -> caseInsensitiveContains(s, (String) goodType.getValue()));
            }
            if (!goodProvider.isEmpty()) {
                dataProvider.addFilter((goods) -> goods.getContragents().getDisplayName(),
                        s -> caseInsensitiveContains(s, (String) goodProvider.getValue()));
            }
            if (!goodMinPrice.isEmpty() & !goodMaxPrice.isEmpty()) {
                Double minValue = Double.parseDouble(goodMinPrice.getValue());
                Double maxValue = Double.parseDouble(goodMaxPrice.getValue());
                dataProvider.addFilter((goods) -> {
                    return stockHelper.getRetailPriceByGoodId(goods.getId());
                },
                        d -> inDoublerange(d, minValue, maxValue));
            }
            if (!goodRemainder.isEmpty()) {
                Double minRemainder = Double.parseDouble(goodRemainder.getValue());
                dataProvider.addFilter(Goods::getFactRemainder, d -> moreThan(d, minRemainder));
            }
        }

        private void doResetFilter() {
            ListDataProvider<Goods> dataProvider = (ListDataProvider<Goods>) this.getDataProvider();
            dataProvider.clearFilters();
            goodArticul.clear();
            goodMaxPrice.clear();
            goodMinPrice.clear();
            goodName.clear();
            goodProvider.clear();
            goodRemainder.clear();
            goodType.clear();
        }

        private Boolean caseInsensitiveContains(String where, String what) {
            return where.toLowerCase().contains(what.toLowerCase());
        }

        private Boolean inDoublerange(Double what, Double min, Double max) {
            return what >= min & what <= max;
        }

        private Boolean moreThan(Double what, Double where) {
            return what >= where;
        }
    }

    
}
