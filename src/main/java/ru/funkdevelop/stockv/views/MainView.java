/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ru.funkdevelop.stockv.views;

import com.vaadin.navigator.Navigator;
import com.vaadin.navigator.View;
import com.vaadin.navigator.ViewChangeListener;
import com.vaadin.server.VaadinServletService;
import com.vaadin.ui.Alignment;
import com.vaadin.ui.Button;
import com.vaadin.ui.GridLayout;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Panel;
import com.vaadin.ui.VerticalLayout;
import java.io.Serializable;
import ru.funkdevelop.stockv.views.service.MainMenu;
import ru.funkdevelop.stockv.views.service.UserData;

/**
 *
 * @author artur
 */
public class MainView extends VerticalLayout implements View, Serializable {

    Navigator navigator;

    @Override
    public void enter(ViewChangeListener.ViewChangeEvent event) {
        navigator = getUI().getNavigator();
        GridLayout layout = new GridLayout(3, 3);
//        layout.setSizeFull();
        layout.setMargin(false);
        layout.setSpacing(false);

        MainMenu mainMenu = new MainMenu(navigator);
        mainMenu.setHeight("40px");
//        layout.setMargin(false);
//        layout.setColumns(3);
//        layout.setRowExpandRatio(0, 0.0f);
//        layout.setRowExpandRatio(1, 0.5f);
//        layout.setRows(3);
//        layout.setColumnExpandRatio(0, 0.0f);
        layout.addComponent(mainMenu, 0, 0, 2, 0);
//        layout.setComponentAlignment(mainMenu, Alignment.TOP_LEFT);
        UserData userData = new UserData();
        layout.addComponent(userData, 0, 1, 0, 1);
//        layout.setComponentAlignment(userData, Alignment.TOP_LEFT);

        Panel control = new Panel("Панель управления");
        HorizontalLayout controlLayout = new HorizontalLayout();
        Panel service = new Panel("Сервис");
        VerticalLayout serviceLayout = new VerticalLayout();
        Button userControl = new Button("Управление пользователями", new Button.ClickListener() {
            @Override
            public void buttonClick(Button.ClickEvent event) {
                if (navigator != null) {
                    navigator.navigateTo("UserControl");
                }
            }
        });

        Button contragentControl = new Button("Управление контрагентами", new Button.ClickListener() {
            @Override
            public void buttonClick(Button.ClickEvent event) {
                if (navigator != null) {
                    navigator.navigateTo("ContragentControl");
                }
            }
        });

        Button sertificateControl = new Button("Управление сертификатами", new Button.ClickListener() {
            @Override
            public void buttonClick(Button.ClickEvent event) {
                if (navigator != null) {
                    navigator.navigateTo("SertificateControl");
                }
            }
        });

        Panel fieldsControl = new Panel("Управление полями");
        VerticalLayout fieldsControlLayout = new VerticalLayout();

        Button banksControl = new Button("Управление банками", new Button.ClickListener() {
            @Override
            public void buttonClick(Button.ClickEvent event) {
                if (navigator != null) {
                    navigator.navigateTo("BankControl");
                }
            }
        });

        Button goodsTypesControl = new Button("Управление типами товаров", new Button.ClickListener() {
            @Override
            public void buttonClick(Button.ClickEvent event) {
                if (navigator != null) {
                    navigator.navigateTo("GoodTypeControl");
                }
            }
        });

        Button packageTypesControl = new Button("Управление типами фасовки", new Button.ClickListener() {
            @Override
            public void buttonClick(Button.ClickEvent event) {
                if (navigator != null) {
                    navigator.navigateTo("PackageControl");
                }
            }
        });

        Button contragentsTypesControl = new Button("Управление типами контрагентов", new Button.ClickListener() {
            @Override
            public void buttonClick(Button.ClickEvent event) {
                if (navigator != null) {
                    navigator.navigateTo("ContragentTypeControl");
                }
            }
        });

        Button priceTypesControl = new Button("Управление типами цен", new Button.ClickListener() {
            @Override
            public void buttonClick(Button.ClickEvent event) {
                if (navigator != null) {
                    navigator.navigateTo("PriceTypeControl");
                }
            }
        });

        fieldsControlLayout.addComponents(banksControl, goodsTypesControl, packageTypesControl, contragentsTypesControl, priceTypesControl);

        banksControl.setWidth(9.0f, Unit.CM);
        goodsTypesControl.setWidth(9.0f, Unit.CM);
        packageTypesControl.setWidth(9.0f, Unit.CM);
        contragentsTypesControl.setWidth(9.0f, Unit.CM);
        priceTypesControl.setWidth(9.0f, Unit.CM);

        fieldsControlLayout.setComponentAlignment(banksControl, Alignment.MIDDLE_CENTER);
        fieldsControlLayout.setComponentAlignment(goodsTypesControl, Alignment.MIDDLE_CENTER);
        fieldsControlLayout.setComponentAlignment(packageTypesControl, Alignment.MIDDLE_CENTER);
        fieldsControlLayout.setComponentAlignment(contragentsTypesControl, Alignment.MIDDLE_CENTER);
        fieldsControlLayout.setComponentAlignment(priceTypesControl, Alignment.MIDDLE_CENTER);

        fieldsControl.setContent(fieldsControlLayout);

        serviceLayout.addComponents(userControl, contragentControl, sertificateControl, fieldsControl);

        userControl.setWidth(9.5f, Unit.CM);
        contragentControl.setWidth(9.5f, Unit.CM);
        sertificateControl.setWidth(9.5f, Unit.CM);
        serviceLayout.setComponentAlignment(userControl, Alignment.MIDDLE_CENTER);
        serviceLayout.setComponentAlignment(contragentControl, Alignment.MIDDLE_CENTER);
        serviceLayout.setComponentAlignment(sertificateControl, Alignment.MIDDLE_CENTER);

        service.setContent(serviceLayout);

        Panel goods = new Panel("Товары");
        VerticalLayout goodsLayout = new VerticalLayout();

        Button goodsControl = new Button("Управление товарами", new Button.ClickListener() {
            @Override
            public void buttonClick(Button.ClickEvent event) {
                if (navigator != null)
            navigator.navigateTo("GoodControl");
            }
        });
        goodsControl.setWidth(9.5f, Unit.CM);
        goodsLayout.addComponent(goodsControl);
        goodsLayout.setComponentAlignment(goodsControl, Alignment.MIDDLE_CENTER);
        goods.setContent(goodsLayout);

        VerticalLayout column1 = new VerticalLayout();
        column1.setSpacing(false);
        column1.setMargin(false);

        Panel documents = new Panel("Документы");
        VerticalLayout documentsLayout = new VerticalLayout();

        Button allDocuments = new Button("Список документов", new Button.ClickListener() {
            @Override
            public void buttonClick(Button.ClickEvent event) {
                if (navigator != null)
            navigator.navigateTo("DocumentsView");
            }
        });

        Button incomeDocument = new Button("Поступление ТМЦ", new Button.ClickListener() {
            @Override
            public void buttonClick(Button.ClickEvent event) {
                if (navigator != null)
            navigator.navigateTo("Income");
            }
        });

        Button outcomeDocument = new Button("Реализация ТМЦ", new Button.ClickListener() {
            @Override
            public void buttonClick(Button.ClickEvent event) {
                if (navigator != null)
            navigator.navigateTo("Outcome");
            }
        });

        Button remainderDocument = new Button("Ввод остатков ТМЦ", new Button.ClickListener() {
            @Override
            public void buttonClick(Button.ClickEvent event) {
                if (navigator != null)
            navigator.navigateTo("RemainderEnter");
            }
        });

        Button writtingOffDocument = new Button("Списание ТМЦ", new Button.ClickListener() {
            @Override
            public void buttonClick(Button.ClickEvent event) {
                if (navigator != null)
            navigator.navigateTo("WritingOff");
            }
        });

        Button cashReceiptDocument = new Button("Приходный кассовый ордер", new Button.ClickListener() {
            @Override
            public void buttonClick(Button.ClickEvent event) {
                if (navigator != null)
            navigator.navigateTo("CashReceipt");
            }
        });

        allDocuments.setWidth(9.5f, Unit.CM);
        incomeDocument.setWidth(9.5f, Unit.CM);
        outcomeDocument.setWidth(9.5f, Unit.CM);
        remainderDocument.setWidth(9.5f, Unit.CM);
        writtingOffDocument.setWidth(9.5f, Unit.CM);
        cashReceiptDocument.setWidth(9.5f, Unit.CM);

        documentsLayout.addComponents(allDocuments, incomeDocument, outcomeDocument, remainderDocument, writtingOffDocument, cashReceiptDocument);

        documentsLayout.setComponentAlignment(allDocuments, Alignment.MIDDLE_CENTER);
        documentsLayout.setComponentAlignment(incomeDocument, Alignment.MIDDLE_CENTER);
        documentsLayout.setComponentAlignment(outcomeDocument, Alignment.MIDDLE_CENTER);
        documentsLayout.setComponentAlignment(remainderDocument, Alignment.MIDDLE_CENTER);
        documentsLayout.setComponentAlignment(writtingOffDocument, Alignment.MIDDLE_CENTER);
        documentsLayout.setComponentAlignment(cashReceiptDocument, Alignment.MIDDLE_CENTER);

        documents.setContent(documentsLayout);

        column1.addComponents(goods, documents);

        Panel reports = new Panel("Отчеты");
        VerticalLayout reportsLayout = new VerticalLayout();

        Button remainderReport = new Button("Ведомость по остаткам ТМЦ", new Button.ClickListener() {
            @Override
            public void buttonClick(Button.ClickEvent event) {
                if (navigator != null)
            navigator.navigateTo("RemainderReport");
            }
        });

        Button contragentReport = new Button("Взаиморасчеты по контрагентам", new Button.ClickListener() {
            @Override
            public void buttonClick(Button.ClickEvent event) {
                if (navigator != null)
            navigator.navigateTo("ContragentClaculationsReport");
            }
        });

        Button documentsReport = new Button("Реестр документов", new Button.ClickListener() {
            @Override
            public void buttonClick(Button.ClickEvent event) {
                if (navigator != null)
            navigator.navigateTo("DocumentRegistryReport");
            }
        });
        reportsLayout.addComponents(remainderReport, contragentReport, documentsReport);

        remainderReport.setWidth(9.5f, Unit.CM);
        contragentReport.setWidth(9.5f, Unit.CM);
        documentsReport.setWidth(9.5f, Unit.CM);

        reportsLayout.setComponentAlignment(remainderReport, Alignment.MIDDLE_CENTER);
        reportsLayout.setComponentAlignment(contragentReport, Alignment.MIDDLE_CENTER);
        reportsLayout.setComponentAlignment(documentsReport, Alignment.MIDDLE_CENTER);

        reports.setContent(reportsLayout);

        controlLayout.addComponents(column1, service, reports);
        controlLayout.setSpacing(false);
        controlLayout.setMargin(true);
        goods.setWidth(10.44f, Unit.CM);
        documents.setWidth(10.44f, Unit.CM);
        service.setWidth(10.44f, Unit.CM);
        reports.setWidth(10.44f, Unit.CM);

        control.setContent(controlLayout);
        control.setWidth(32.0f, Unit.CM);
        control.setHeight(19.0f, Unit.CM);
        layout.addComponent(control, 1, 1, 1, 1);

        layout.setColumnExpandRatio(0, 0.0f);
        layout.setColumnExpandRatio(1, 0.0f);
        layout.setColumnExpandRatio(2, 0.0f);
        layout.setRowExpandRatio(0, 0.0f);
        layout.setRowExpandRatio(1, 0.0f);
        layout.setRowExpandRatio(2, 0.0f);
        layout.setSpacing(false);
        layout.setMargin(false);

        VerticalLayout fullLayout = new VerticalLayout();
        fullLayout.addComponent(layout);
        fullLayout.setComponentAlignment(layout, Alignment.MIDDLE_CENTER);
        fullLayout.setMargin(false);
        fullLayout.setSpacing(false);

        getUI().setContent(fullLayout);
    }

}
