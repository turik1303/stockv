/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ru.funkdevelop.stockv.views.reports;

import com.vaadin.data.HasValue;
import com.vaadin.navigator.Navigator;
import com.vaadin.navigator.View;
import com.vaadin.navigator.ViewChangeListener;
import com.vaadin.ui.Alignment;
import com.vaadin.ui.Button;
import com.vaadin.ui.CheckBox;
import com.vaadin.ui.ComboBox;
import com.vaadin.ui.DateField;
import com.vaadin.ui.Grid;
import com.vaadin.ui.GridLayout;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Label;
import com.vaadin.ui.Panel;
import com.vaadin.ui.TabSheet;
import com.vaadin.ui.UI;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.renderers.TextRenderer;
import com.vaadin.ui.themes.ValoTheme;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.List;
import ru.funkdevelop.stockv.StockHelper;
import ru.funkdevelop.stockv.pojos.DocumentGoods;
import ru.funkdevelop.stockv.pojos.Documents;
import ru.funkdevelop.stockv.pojos.Goods;
import ru.funkdevelop.stockv.views.service.MainMenu;
import ru.funkdevelop.stockv.views.service.UserData;

/**
 *
 * @author artur
 */
public class RemainderReportView extends GridLayout implements View {
    MainMenu mainMenu;
    UserData userData;
    StockHelper stockHelper;
    Navigator navigator;
    
    @Override
    public void enter(ViewChangeListener.ViewChangeEvent event) {
        this.stockHelper = new StockHelper();
        this.navigator = UI.getCurrent().getNavigator();
        this.userData = new UserData();
        this.mainMenu = new MainMenu(navigator);
        
        GridLayout layout = new GridLayout(3, 3);
        
        TabSheet tabReport = new TabSheet();
        tabReport.addStyleName(ValoTheme.TABSHEET_FRAMED);
        tabReport.addStyleName(ValoTheme.TABSHEET_PADDED_TABBAR);
        
        
        
        VerticalLayout tabReportConfigLayout = new VerticalLayout();
        Panel period = new Panel("Период");
        VerticalLayout periodPanelLayout = new VerticalLayout();
        DateField beginDate = new DateField("Начало периода");
        beginDate.setDateFormat("dd.MM.yyyy");
        beginDate.setWidth(9.0f, Unit.CM);
        DateField endDate = new DateField("Конец периода");
        endDate.setDateFormat("dd.MM.yyyy");
        endDate.setWidth(9.0f, Unit.CM);
        periodPanelLayout.addComponents(beginDate, endDate);
        periodPanelLayout.setComponentAlignment(beginDate, Alignment.MIDDLE_CENTER);
        periodPanelLayout.setComponentAlignment(endDate, Alignment.MIDDLE_CENTER);
        period.setContent(periodPanelLayout);
        
        Panel packageConfig = new Panel("Единицы измерения");
        VerticalLayout packageConfigLayout = new VerticalLayout();
        CheckBox basePackage = new CheckBox("Базовые единицы измерения", true);
        basePackage.setWidth(9.0f, Unit.CM);
        CheckBox mainPackage = new CheckBox("Основные единицы измерения");
        mainPackage.setWidth(9.0f, Unit.CM);
        basePackage.addValueChangeListener(new HasValue.ValueChangeListener<Boolean>() {
            @Override
            public void valueChange(HasValue.ValueChangeEvent<Boolean> event) {
                if (event.getValue() == true)
                    mainPackage.setValue(Boolean.FALSE);
                else
                    mainPackage.setValue(Boolean.TRUE);
            }
        });
        mainPackage.addValueChangeListener(new HasValue.ValueChangeListener<Boolean>() {
            @Override
            public void valueChange(HasValue.ValueChangeEvent<Boolean> event) {
                if (event.getValue() == true)
                    basePackage.setValue(Boolean.FALSE);
                else
                    basePackage.setValue(Boolean.TRUE);
            }
        });
        
        packageConfigLayout.addComponents(basePackage, mainPackage);
        packageConfigLayout.setSizeFull();
        packageConfigLayout.setComponentAlignment(basePackage, Alignment.MIDDLE_CENTER);
        packageConfigLayout.setComponentAlignment(mainPackage, Alignment.MIDDLE_CENTER);
        packageConfig.setContent(packageConfigLayout);
        
        Panel goodSelect = new Panel("Выбор ТМЦ");
        VerticalLayout goodSelectLayout = new VerticalLayout();
        List<Goods> allGoodsList = stockHelper.getAllGoods();
        HashMap<String, Goods> allGoodsMap = new HashMap<>();
        for (Goods goods : allGoodsList) {
            allGoodsMap.put(goods.getName(), goods);
        }
        ComboBox goodSelectBox = new ComboBox("Наименование ТМЦ", allGoodsMap.keySet());
        goodSelectBox.setEmptySelectionAllowed(false);
        goodSelectBox.setWidth(9.0f, Unit.CM);
        goodSelectLayout.addComponent(goodSelectBox);
        goodSelectLayout.setSizeFull();
        goodSelectLayout.setComponentAlignment(goodSelectBox, Alignment.MIDDLE_CENTER);
        goodSelect.setContent(goodSelectLayout);
        
        HorizontalLayout line1 = new HorizontalLayout();
        period.setHeight(6.0f, Unit.CM);
        period.setWidth(10.0f, Unit.CM);
        packageConfig.setHeight(6.0f, Unit.CM);
        packageConfig.setWidth(10.0f, Unit.CM);
        goodSelect.setHeight(6.0f, Unit.CM);
        goodSelect.setWidth(10.0f, Unit.CM);
        line1.addComponents(period, packageConfig, goodSelect);
        line1.setWidth(31.3f, Unit.CM);
        line1.setComponentAlignment(period, Alignment.TOP_CENTER);
        line1.setComponentAlignment(packageConfig, Alignment.TOP_CENTER);
        line1.setComponentAlignment(goodSelect, Alignment.TOP_CENTER);
        
        tabReportConfigLayout.addComponent(line1);
        VerticalLayout tabReportDataLayout = new VerticalLayout();
        tabReportConfigLayout.addComponent(new Button("Сформировать", new Button.ClickListener() {
            @Override
            public void buttonClick(Button.ClickEvent event) {
                Boolean isInBasePackage = basePackage.getValue();
                tabReportDataLayout.removeAllComponents();
                tabReport.getTab(1).setEnabled(true);
                tabReport.setSelectedTab(1);
                LocalDate beginL = beginDate.getValue();
                LocalDate endL = endDate.getValue();
                Calendar c = new GregorianCalendar();
                c.set(beginL.getYear(), beginL.getMonthValue() - 1, beginL.getDayOfMonth());
                Date begin = new Date();
                begin.setTime(c.getTimeInMillis());
                Date end = new Date();
                c.set(endL.getYear(), endL.getMonthValue() - 1, endL.getDayOfMonth());
                end.setTime(c.getTimeInMillis());
                
                Label header = new Label("Ведомость по остаткам ТМЦ на складах");
                tabReportDataLayout.addComponent(header);
                tabReportDataLayout.setComponentAlignment(header, Alignment.TOP_CENTER);
                
                SimpleDateFormat format = new SimpleDateFormat("dd.MM.yyyy");
                Label dates = new Label("С " + format.format(begin) + " по " + format.format(end));

                
                Label goodName = new Label("Наименование товара: " + allGoodsMap.get((String)goodSelectBox.getValue()).getName());

                
                Label packageName = new Label();
                String packageNameString = "";
                if (isInBasePackage) {
                    packageNameString = allGoodsMap.get((String)goodSelectBox.getValue()).getPackagesByBasePackageId().getPackageName();
                } else {
                    packageNameString = allGoodsMap.get((String)goodSelectBox.getValue()).getPackagesByMainPackageId().getPackageName();
                }
                packageName.setCaption("Единицы измерения: " + packageNameString);

                
                List<RemainderReportData> dataList = 
                        generateRemainderReportData(begin, end, allGoodsMap.get((String)goodSelectBox.getValue()).getId());
                RemainderReportGrid remainderReportGrid = new RemainderReportGrid(dataList, isInBasePackage);
                
                Label initialRemainder = new Label("Начальный остаток: " + String.valueOf(remainderReportGrid.getInitialRemainder()));

                
                HorizontalLayout reportHeader = new HorizontalLayout();
                reportHeader.addComponents(dates, goodName, packageName, initialRemainder);
                reportHeader.setSizeFull();
                tabReportDataLayout.addComponent(reportHeader);
                tabReportDataLayout.addComponent(remainderReportGrid);
                
                
            }
        }));
        
       
        
        tabReport.addTab(tabReportConfigLayout, "Настройки");
        tabReport.addTab(tabReportDataLayout, "Отчет").setEnabled(false);
        
        tabReport.setWidth(32.0f, Unit.CM);
        tabReport.setHeight(19.0f, Unit.CM);
        
        layout.addComponent(mainMenu, 0, 0, 2, 0);
        layout.addComponent(userData, 0, 1, 0, 1);       
        layout.addComponent(tabReport, 1, 1, 1, 2);
        
        layout.setColumnExpandRatio(0, 0.0f);
        layout.setColumnExpandRatio(1, 0.0f);
        layout.setColumnExpandRatio(2, 0.0f);
        layout.setRowExpandRatio(0, 0.0f);
        layout.setRowExpandRatio(1, 0.0f);
        layout.setRowExpandRatio(2, 0.0f);
        layout.setSpacing(false);
        layout.setMargin(false);

        VerticalLayout fullLayout = new VerticalLayout();
        fullLayout.addComponent(layout);
        fullLayout.setComponentAlignment(layout, Alignment.MIDDLE_CENTER);
        fullLayout.setMargin(false);
        fullLayout.setSpacing(false);

        getUI().setContent(fullLayout);
    }
    
    
    private List<RemainderReportData> generateRemainderReportData (Date begin, Date end, int goodId) {
        List<Documents> documents = stockHelper.getDocumentsByDates(begin, end);
        List<RemainderReportData> documentGoods = new ArrayList<>();
        for (Documents document : documents) {
            List<DocumentGoods> tmp = stockHelper.getBillGoods(document.getId());
            for (DocumentGoods docGood : tmp) {
                if (docGood.getGoods().getId() == goodId)
                    documentGoods.add(new RemainderReportData(document.getType(), docGood, document));
            }
        }
        
        return documentGoods;
    }
    
    
    class RemainderReportData {
        private int type;
        private DocumentGoods docGood;
        private Documents document;
        
        public RemainderReportData(int type, DocumentGoods docGood, Documents document) {
            this.type = type;
            this.docGood = docGood;
            this.document = document;
        }

        
        
        public int getType() {
            return type;
        }

        public void setType(int type) {
            this.type = type;
        }

        public DocumentGoods getDocGood() {
            return docGood;
        }

        public void setDocGood(DocumentGoods docGood) {
            this.docGood = docGood;
        }        

        public Documents getDocument() {
            return document;
        }

        public void setDocument(Documents document) {
            this.document = document;
        }
        
        
    }
    
    class RemainderReportGrid extends Grid<RemainderReportData> {
        Double remainder;
        Double initialRemainder;
        public RemainderReportGrid(List<RemainderReportData> data, Boolean isInBasePackage) {
            remainder = data.get(0).getDocGood().getGoods().getFactRemainder();
            for (RemainderReportData remainderReportData : data) {
                int type = remainderReportData.getType();
                if (type == 1 | type == 3)
                    if (isInBasePackage) {
                        remainder -= remainderReportData.getDocGood().getQuantity() 
                                    * remainderReportData.getDocGood().getGoods().getMainFactor();
                    } else {
                        remainder -= remainderReportData.getDocGood().getQuantity();
                    }
                        
                if (type == 2 | type == 4)
                    if (isInBasePackage) {
                        remainder += remainderReportData.getDocGood().getQuantity() 
                                    * remainderReportData.getDocGood().getGoods().getMainFactor();
                    } else {
                        remainder += remainderReportData.getDocGood().getQuantity();
                    }
            }
            initialRemainder = remainder;
            this.addColumn((remainderReportData) -> {
                String result = "";
                SimpleDateFormat format = new SimpleDateFormat("dd.MM.yyyy");
                result = format.format(remainderReportData.getDocGood().getDocuments().getDate());
                return result;
            }, new TextRenderer()).setCaption("Дата");
            this.addColumn((remainderReportData) -> {
                String result = "";
                Integer type = remainderReportData.getType();
                SimpleDateFormat format = new SimpleDateFormat("dd.MM.yyyy");
                String docType = "";
                switch (type) {
                    case 1: 
                        docType = "Поступление ТМЦ (купля-продажа)";
                        break;
                    case 2:
                        docType = "Реализация (купля-продажа)";
                        break;
                    case 3:
                        docType = "Ввод остатков ТМЦ";
                        break;
                    case 4:
                        docType = "Списание ТМЦ";
                        break;
                        default:
                            break;
                }
                result += docType;
                result += " № ";
                result += remainderReportData.getDocument().getNumber().toString();
                result += " от ";
                result += format.format(remainderReportData.getDocument().getDate());
                result += " (";
                result += remainderReportData.getDocument().getContragents().getDisplayName();
                result += ")";                
                return result;
            }, new TextRenderer()).setCaption("Наименование документа");
            
            this.addColumn((remainderReportData) -> {
                String result = "";
                Integer type = remainderReportData.getType();
                if (type == 1 | type == 3)
                    if (isInBasePackage) {
                        result = String.valueOf(remainderReportData.getDocGood().getQuantity() * 
                                remainderReportData.getDocGood().getGoods().getMainFactor());
                    } else {
                        result = String.valueOf(remainderReportData.getDocGood().getQuantity());
                    }
                return result;
            }, new TextRenderer()).setCaption("Приход");
            this.addColumn((remainderReportData) -> {
                String result = "";
                Integer type = remainderReportData.getType();
                if (type == 2 | type == 4)
                    if (isInBasePackage) {
                        result = String.valueOf(remainderReportData.getDocGood().getQuantity() * 
                                remainderReportData.getDocGood().getGoods().getMainFactor());
                    } else {
                        result = String.valueOf(remainderReportData.getDocGood().getQuantity());
                    }
                
                return result;
            }, new TextRenderer()).setCaption("Расход");
            this.addColumn((remainderReportData) -> {
                String result = "";
                int type = remainderReportData.getType();
                if (type == 1 | type == 3)
                    if (isInBasePackage) {
                        remainder += remainderReportData.getDocGood().getQuantity() 
                                    * remainderReportData.getDocGood().getGoods().getMainFactor();
                    } else {
                        remainder += remainderReportData.getDocGood().getQuantity();
                    }
                        
                if (type == 2 | type == 4)
                    if (isInBasePackage) {
                        remainder -= remainderReportData.getDocGood().getQuantity() 
                                    * remainderReportData.getDocGood().getGoods().getMainFactor();
                    } else {
                        remainder -= remainderReportData.getDocGood().getQuantity();
                    }
                Double resultRemainder = remainder;
//                if (!isInBasePackage)
//                    resultRemainder = remainder / remainderReportData.getDocGood().getGoods().getMainFactor();
                result = String.valueOf(resultRemainder);
                return result;
            }, new TextRenderer()).setCaption("Конечный остаток");
            
            
            this.setSizeFull();
            this.setHeight(14.5f, Unit.CM);
            this.setItems(data);
            
            
        }

        public Double getRemainder() {
            return remainder;
        }

        public Double getInitialRemainder() {
            return initialRemainder;
        }
        
        
    }
}
