/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ru.funkdevelop.stockv.views.reports;

import com.vaadin.navigator.Navigator;
import com.vaadin.navigator.View;
import com.vaadin.navigator.ViewChangeListener;
import com.vaadin.ui.Alignment;
import com.vaadin.ui.Button;
import com.vaadin.ui.CheckBox;
import com.vaadin.ui.DateField;
import com.vaadin.ui.Grid;
import com.vaadin.ui.GridLayout;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Label;
import com.vaadin.ui.Panel;
import com.vaadin.ui.TabSheet;
import com.vaadin.ui.UI;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.renderers.TextRenderer;
import com.vaadin.ui.themes.ValoTheme;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;
import ru.funkdevelop.stockv.StockHelper;
import ru.funkdevelop.stockv.pojos.Documents;
import ru.funkdevelop.stockv.views.service.MainMenu;
import ru.funkdevelop.stockv.views.service.UserData;

/**
 *
 * @author artur
 */
public class DocumentRegistryReportView extends GridLayout implements View {

    MainMenu mainMenu;
    UserData userData;
    StockHelper stockHelper;
    Navigator navigator;
    
    @Override
    public void enter(ViewChangeListener.ViewChangeEvent event) {
        
        this.stockHelper = new StockHelper();
        this.navigator = UI.getCurrent().getNavigator();
        this.userData = new UserData();
        this.mainMenu = new MainMenu(navigator);
        
        GridLayout layout = new GridLayout(3, 3);

        TabSheet tabReport = new TabSheet();
        tabReport.addStyleName(ValoTheme.TABSHEET_FRAMED);
        tabReport.addStyleName(ValoTheme.TABSHEET_PADDED_TABBAR);

        HorizontalLayout tabReportConfigLayout = new HorizontalLayout();
        Panel period = new Panel("Настройки периода");
        VerticalLayout periodPanelLayout = new VerticalLayout();
        DateField beginDate = new DateField("Начало периода");
        beginDate.setDateFormat("dd.MM.yyyy");
        beginDate.setWidth(15.0f, Unit.CM);
        DateField endDate = new DateField("Конец периода");
        endDate.setDateFormat("dd.MM.yyyy");
        endDate.setWidth(15.0f, Unit.CM);
        periodPanelLayout.addComponents(beginDate, endDate);
        periodPanelLayout.setComponentAlignment(beginDate, Alignment.MIDDLE_CENTER);
        periodPanelLayout.setComponentAlignment(endDate, Alignment.MIDDLE_CENTER);
        period.setContent(periodPanelLayout);
        
        
        VerticalLayout tabReportDataLayout = new VerticalLayout();
        
        
        CheckBox isIncome = new CheckBox("Поступление ТМЦ (купля-продажа)", true);
        isIncome.setWidth(15.0f, Unit.CM);
        CheckBox isOutcome = new CheckBox("Реализация (купля-продажа)", true);
        isOutcome.setWidth(15.0f, Unit.CM);
        
        
        Button generateReport = new Button("Сформировать", new Button.ClickListener() {
            @Override
            public void buttonClick(Button.ClickEvent event) {
                SimpleDateFormat format = new SimpleDateFormat("dd.MM.yyyy");
                tabReportDataLayout.removeAllComponents();
                tabReport.getTab(1).setEnabled(true);
                tabReport.setSelectedTab(1);
                LocalDate beginL = beginDate.getValue();
                LocalDate endL = endDate.getValue();
                Calendar c = new GregorianCalendar();
                c.set(beginL.getYear(), beginL.getMonthValue() - 1, beginL.getDayOfMonth());
                Date begin = new Date();
                begin.setTime(c.getTimeInMillis());
                Date end = new Date();
                c.set(endL.getYear(), endL.getMonthValue() - 1, endL.getDayOfMonth());
                end.setTime(c.getTimeInMillis());
                
                Boolean isIncomeValue = isIncome.getValue();
                Boolean isOutcomeValue = isOutcome.getValue();
                
                List<Documents> gridData = generateReportData(begin, end, isIncomeValue, isOutcomeValue);
                
                ReportGrid reportGrid = new ReportGrid(gridData);
                
                Label header = new Label("Реестр документов");
                Label dates = new Label("С " + format.format(begin) + " по " + format.format(end));
                
                Double summ = 0.0;
                for (Documents documents : gridData) {
                    int docType = documents.getType();
                    if (docType == 1)
                        summ -= documents.getSumm();
                    else if (docType == 2)
                        summ += documents.getSumm();
                }
                
                Label totalSumm = new Label("Общая сумма: " + String.valueOf(summ) + "руб.");
                
                tabReportDataLayout.addComponents(header, dates, totalSumm, reportGrid);
                tabReportDataLayout.setComponentAlignment(header, Alignment.TOP_CENTER);
                
                
                
            }
        });
        
        generateReport.setWidth(15.65f, Unit.CM);

        Panel documentType = new Panel("Типы документов");
        VerticalLayout documentTypeLayout = new VerticalLayout();
//        CheckBox isIncome = new CheckBox("Поступление ТМЦ (купля-продажа)", true);
//        isIncome.setWidth(15.0f, Unit.CM);
//        CheckBox isOutcome = new CheckBox("Реализация (купля-продажа)", true);
//        isOutcome.setWidth(15.0f, Unit.CM);
        
        documentTypeLayout.addComponents(isIncome, isOutcome);
        documentType.setContent(documentTypeLayout);
        
        VerticalLayout column2 = new VerticalLayout();
        column2.addComponents(documentType, generateReport);
        column2.setComponentAlignment(generateReport, Alignment.BOTTOM_CENTER);
        column2.setMargin(false);

        tabReportConfigLayout.setMargin(false);
        tabReportConfigLayout.setSpacing(true);
        tabReportConfigLayout.addComponents(period, column2);

        tabReport.addTab(tabReportConfigLayout, "Настройки");
        tabReport.addTab(tabReportDataLayout, "Отчет").setEnabled(false);

        tabReport.setWidth(32.0f, Unit.CM);
        tabReport.setHeight(19.0f, Unit.CM);

        layout.addComponent(mainMenu, 0, 0, 2, 0);
        layout.addComponent(userData, 0, 1, 0, 1);
        layout.addComponent(tabReport, 1, 1, 1, 2);

        layout.setColumnExpandRatio(0, 0.0f);
        layout.setColumnExpandRatio(1, 0.0f);
        layout.setColumnExpandRatio(2, 0.0f);
        layout.setRowExpandRatio(0, 0.0f);
        layout.setRowExpandRatio(1, 0.0f);
        layout.setRowExpandRatio(2, 0.0f);
        layout.setSpacing(false);
        layout.setMargin(false);

        VerticalLayout fullLayout = new VerticalLayout();
        fullLayout.addComponent(layout);
        fullLayout.setComponentAlignment(layout, Alignment.MIDDLE_CENTER);
        fullLayout.setMargin(false);
        fullLayout.setSpacing(false);

        getUI().setContent(fullLayout);
        
    }
    
    private List<Documents> generateReportData(Date begin, Date end, Boolean isIncome, Boolean isOutcome) {
        List<Documents> result = new ArrayList<>();        
        List<Documents> rawDocumentsList = stockHelper.getDocumentsByDates(begin, end);
        for (Documents documents : rawDocumentsList) {
            int docType = documents.getType();
            if ((docType == 1 & isIncome) | (docType == 2 & isOutcome))
                result.add(documents);
        }
        return result;
    }
    
    class ReportGrid extends Grid<Documents> {
        int i = 1;
        public ReportGrid(List<Documents> gridData) {
            SimpleDateFormat format = new SimpleDateFormat("dd.MM.yyyy");
            this.addColumn((documents)-> {
                String result = "";
                result = String.valueOf(i);
                i++;
                return result;
            }, new TextRenderer()).setCaption("№ п/п");
            
            this.addColumn((documents) -> {
                String result = "";
                result = format.format(documents.getDate());
                return result;
            }, new TextRenderer()).setCaption("Дата");
            
            this.addColumn((documents) -> {
                String result = "";
                int docType = documents.getType();
                if (docType == 1)
                    result = "Поступление ТМЦ (купля-продажа)";
                else if (docType == 2)
                    result = "Реализация (купля-продажа)";
                return result;
            }, new TextRenderer()).setCaption("Документ");
            
            this.addColumn((documents) -> {
                String result = "";
                result = String.valueOf(documents.getNumber());
                return result;
            }, new TextRenderer()).setCaption("Номер");
            
            this.addColumn((documents) -> {
                String result = "";
                result = documents.getUsers().getLogin();
                return result;
            }, new TextRenderer()).setCaption("Автор");
            
            this.addColumn((documents) -> {
                String result = "";
                Boolean isCarryed = documents.getIsCarried();
                if (isCarryed)
                    result = "Проведен";
                else 
                    result = "Не проведен";
                return result;
            }, new TextRenderer()).setCaption("Статус");
            
            this.addColumn((documents) -> {
                String result = "";
                result = String.valueOf(documents.getSumm());
                return result;
            }, new TextRenderer()).setCaption("Сумма");
            
            this.addColumn((documents) -> {
                String result = "руб.";
                return result;
            }, new TextRenderer()).setCaption("Вал.");
            
            this.addColumn((documents) -> {
                String result = "";
                result = documents.getContragents().getOfficialName();
                return result;
            }, new TextRenderer()).setCaption("Информация");
            
            this.setSizeFull();
            this.setHeight(14.3f, Unit.CM);
            this.setItems(gridData);
        }
        
        
    }
    
}
