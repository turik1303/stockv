/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ru.funkdevelop.stockv.views.reports;

import com.vaadin.navigator.Navigator;
import com.vaadin.navigator.View;
import com.vaadin.navigator.ViewChangeListener;
import com.vaadin.shared.ui.ContentMode;
import com.vaadin.ui.Alignment;
import com.vaadin.ui.Button;
import com.vaadin.ui.ComboBox;
import com.vaadin.ui.DateField;
import com.vaadin.ui.Grid;
import com.vaadin.ui.GridLayout;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Label;
import com.vaadin.ui.Panel;
import com.vaadin.ui.TabSheet;
import com.vaadin.ui.UI;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.components.grid.FooterRow;
import com.vaadin.ui.renderers.TextRenderer;
import com.vaadin.ui.themes.ValoTheme;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.List;
import ru.funkdevelop.stockv.StockHelper;
import ru.funkdevelop.stockv.pojos.Contragents;
import ru.funkdevelop.stockv.pojos.Documents;
import ru.funkdevelop.stockv.views.service.MainMenu;
import ru.funkdevelop.stockv.views.service.UserData;

/**
 *
 * @author artur
 */
public class ContragentCalculationsReportView extends GridLayout implements View {

    MainMenu mainMenu;
    UserData userData;
    StockHelper stockHelper;
    Navigator navigator;
    List<Contragents> allContragents;
    HashMap<String, Contragents> contragentsMap;

    @Override
    public void enter(ViewChangeListener.ViewChangeEvent event) {
        this.stockHelper = new StockHelper();
        this.navigator = UI.getCurrent().getNavigator();
        this.userData = new UserData();
        this.mainMenu = new MainMenu(navigator);
        this.allContragents = stockHelper.getContragents();
        this.contragentsMap = new HashMap<>();

        for (Contragents allContragent : allContragents) {
            contragentsMap.put(allContragent.getDisplayName(), allContragent);
        }

        GridLayout layout = new GridLayout(3, 3);

        TabSheet tabReport = new TabSheet();
        tabReport.addStyleName(ValoTheme.TABSHEET_FRAMED);
        tabReport.addStyleName(ValoTheme.TABSHEET_PADDED_TABBAR);

        HorizontalLayout tabReportConfigLayout = new HorizontalLayout();
        Panel period = new Panel("Настройки периода");
        VerticalLayout periodPanelLayout = new VerticalLayout();
        DateField beginDate = new DateField("Начало периода");
        beginDate.setDateFormat("dd.MM.yyyy");
        beginDate.setWidth(15.0f, Unit.CM);
        DateField endDate = new DateField("Конец периода");
        endDate.setDateFormat("dd.MM.yyyy");
        endDate.setWidth(15.0f, Unit.CM);
        periodPanelLayout.addComponents(beginDate, endDate);
        periodPanelLayout.setComponentAlignment(beginDate, Alignment.MIDDLE_CENTER);
        periodPanelLayout.setComponentAlignment(endDate, Alignment.MIDDLE_CENTER);
        period.setContent(periodPanelLayout);

        Panel contragent = new Panel("Выбор контрагента");
        VerticalLayout contragentLayout = new VerticalLayout();
        ComboBox selectContragent = new ComboBox();
        selectContragent.setEmptySelectionAllowed(false);
        selectContragent.setItems(contragentsMap.keySet());
        selectContragent.setWidth(15.0f, Unit.CM);
        contragentLayout.addComponent(selectContragent);
        contragent.setContent(contragentLayout);

        VerticalLayout tabReportDataLayout = new VerticalLayout();

        Button generateReport = new Button("Сформировать", new Button.ClickListener() {
            @Override
            public void buttonClick(Button.ClickEvent event) {
                SimpleDateFormat format = new SimpleDateFormat("dd.MM.yyyy");
                tabReportDataLayout.removeAllComponents();
                tabReport.getTab(1).setEnabled(true);
                tabReport.setSelectedTab(1);
                LocalDate beginL = beginDate.getValue();
                LocalDate endL = endDate.getValue();
                Calendar c = new GregorianCalendar();
                c.set(beginL.getYear(), beginL.getMonthValue() - 1, beginL.getDayOfMonth());
                Date begin = new Date();
                begin.setTime(c.getTimeInMillis());
                Date end = new Date();
                c.set(endL.getYear(), endL.getMonthValue() - 1, endL.getDayOfMonth());
                end.setTime(c.getTimeInMillis());

                List<Documents> gridData = generateReportData(begin, end, contragentsMap.get((String) selectContragent.getValue()).getId());
                ReportGrid reportGrid = new ReportGrid(gridData);
                Label header = new Label("Ведомость по контрагентам: Общие взаиморасчеты, руб.");
//                header.setContentMode(ContentMode.HTML);
                Label dates = new Label("С " + format.format(begin) + " по " + format.format(end));
                Label contragent = new Label("По контрагенту " + selectContragent.getValue());
                Double startContragentCredit = gridData.get(0).getContragents().getCredit();
                Double increaseContragentCredit = 0.0;
                Double decreaseContragentCredit = 0.0;
                Double outFinalCredit = 0.0;
                Double contragentFinalCredit = 0.0;
                
                for (Documents documents : gridData) {
                    int type = documents.getType();
                    if (type == 2) {
//                        if ((documents.getSumm() - documents.getPaidAmount()) >= 0.0) {
//                            startContragentCredit -= (documents.getSumm() - documents.getPaidAmount());
                            increaseContragentCredit += (documents.getSumm() - documents.getPaidAmount());
                            outFinalCredit -= documents.getSumm() - (documents.getPaidAmount() - documents.getChangeAmount());
                            contragentFinalCredit += documents.getSumm() - (documents.getPaidAmount() - documents.getChangeAmount());
//                            decreaseContragentCredit += (documents.getSumm() - documents.getPaidAmount());
//                        }
                    } else if (type == 5) {
//                        startContragentCredit += documents.getSumm();
//                        increaseContragentCredit -= documents.getSumm();
                        decreaseContragentCredit += documents.getSumm();
                        outFinalCredit += documents.getSumm();
                        contragentFinalCredit -= documents.getSumm();
                    }
                }
                
                if (outFinalCredit < 0.0)
                    outFinalCredit = 0.0;
                
                if (contragentFinalCredit < 0.0)
                    contragentFinalCredit = 0.0;
                
                startContragentCredit = contragentsMap.get((String) selectContragent.getValue()).getCredit() 
                                      + decreaseContragentCredit - increaseContragentCredit;
                
                
                
                
                Label startCredit = new Label("Начальный долг контрагента: " + String.valueOf(startContragentCredit));
                Label increaseCredit = new Label("Увеличение долга: " + String.valueOf(increaseContragentCredit));
                Label decreaseCredit = new Label("Уменьшение долга: " + String.valueOf(decreaseContragentCredit));
                Label ourCredit = new Label("Наш долг: " + String.valueOf(outFinalCredit));
                Label contragentCredit = new Label("Долг контрагента: " + String.valueOf(contragentFinalCredit));
                
                HorizontalLayout line1 = new HorizontalLayout();
                line1.addComponents(dates, contragent, startCredit);
                HorizontalLayout line2 = new HorizontalLayout();
                line2.addComponents(increaseCredit, decreaseCredit, ourCredit, contragentCredit);
                
                tabReportDataLayout.addComponents(header, line1, line2, reportGrid);
                tabReportDataLayout.setComponentAlignment(header, Alignment.TOP_CENTER);
            }
        });
        generateReport.setWidth(15.65f, Unit.CM);

        VerticalLayout column2 = new VerticalLayout();
        column2.addComponents(contragent, generateReport);
        column2.setComponentAlignment(generateReport, Alignment.BOTTOM_CENTER);
        column2.setMargin(false);

        tabReportConfigLayout.setMargin(false);
        tabReportConfigLayout.setSpacing(true);
        tabReportConfigLayout.addComponents(period, column2);

        tabReport.addTab(tabReportConfigLayout, "Настройки");
        tabReport.addTab(tabReportDataLayout, "Отчет").setEnabled(false);

        tabReport.setWidth(32.0f, Unit.CM);
        tabReport.setHeight(19.0f, Unit.CM);

        layout.addComponent(mainMenu, 0, 0, 2, 0);
        layout.addComponent(userData, 0, 1, 0, 1);
        layout.addComponent(tabReport, 1, 1, 1, 2);

        layout.setColumnExpandRatio(0, 0.0f);
        layout.setColumnExpandRatio(1, 0.0f);
        layout.setColumnExpandRatio(2, 0.0f);
        layout.setRowExpandRatio(0, 0.0f);
        layout.setRowExpandRatio(1, 0.0f);
        layout.setRowExpandRatio(2, 0.0f);
        layout.setSpacing(false);
        layout.setMargin(false);

        VerticalLayout fullLayout = new VerticalLayout();
        fullLayout.addComponent(layout);
        fullLayout.setComponentAlignment(layout, Alignment.MIDDLE_CENTER);
        fullLayout.setMargin(false);
        fullLayout.setSpacing(false);

        getUI().setContent(fullLayout);

    }

    private List<Documents> generateReportData(Date begin, Date end, Integer contragentId) {
        List<Documents> result = new ArrayList<>();        
        List<Documents> rawDocumentsList = stockHelper.getDocumentsByDates(begin, end);
        for (Documents documents : rawDocumentsList) {
            if ((documents.getType() == 2 | documents.getType() == 5) & documents.getContragents().getId() == contragentId) {
                result.add(documents);
            }
        }
        return result;
    }

    class ReportGrid extends Grid<Documents> {

        private Double initContragentCredit;
        private Double increaseCredit;
        private Double decreaseCredit;
        private Double ourCredit;
        private Double contragentCredit;
        private Double startContragentCredit;
        int i = 0;

        public ReportGrid(List<Documents> gridData) {
            initContragentCredit = 0.0;
        increaseCredit = 0.0;
        decreaseCredit = 0.0;
        ourCredit = 0.0;
        contragentCredit = 0.0;
        startContragentCredit = 0.0;
            initContragentCredit = gridData.get(0).getContragents().getCredit();
            startContragentCredit = initContragentCredit;
            for (Documents documents : gridData) {
                int docType = documents.getType();
                if (docType == 2) {
                    startContragentCredit -= documents.getSumm() - (documents.getPaidAmount() - documents.getChangeAmount());
                } else if (docType == 5) {
                    startContragentCredit += documents.getSumm();
                }
            }
            SimpleDateFormat format = new SimpleDateFormat("dd.MM.yyyy");

            this.addColumn((documents) -> {
                String result = "";

                result = format.format(documents.getDate());
                return result;
            }, new TextRenderer()).setCaption("Дата");

            this.addColumn((documents) -> {
                String result = "";
                int docType = documents.getType();
                if (docType == 2) {
                    result = "Реализация (купля-продажа) № " + String.valueOf(documents.getNumber())
                            + " от " + format.format(documents.getDate()) + " ("
                            + documents.getContragents().getDisplayName() + ")";
                } else {
                    result = "Приходный кассовый ордер № " + String.valueOf(documents.getNumber())
                            + " от " + format.format(documents.getDate()) + " ("
                            + documents.getContragents().getDisplayName() + ")";
                }
                return result;
            }, new TextRenderer()).setCaption("Наименование документа");

            this.addColumn((documents) -> {
                String result = "";
                int docType = documents.getType();
                if (docType == 2) {
                    increaseCredit += documents.getSumm() - (documents.getPaidAmount() - documents.getChangeAmount());
                    decreaseCredit -= documents.getSumm() - (documents.getPaidAmount() - documents.getChangeAmount());
                    result = String.valueOf(documents.getSumm() - (documents.getPaidAmount() - documents.getChangeAmount()));
                }
                return result;
            }, new TextRenderer()).setCaption("+ долг");

            this.addColumn((documents) -> {
                String result = "";
                int docType = documents.getType();
                if (docType == 5) {
                    decreaseCredit += documents.getSumm();
                    increaseCredit -= documents.getSumm();
                    result = String.valueOf(documents.getSumm());
                }
                return result;
            }, new TextRenderer()).setCaption("- долг");

            this.addColumn((documents) -> {
                String result = "";
                int docType = documents.getType();
                if (docType == 2) {
                    ourCredit -= documents.getSumm() - (documents.getPaidAmount() - documents.getChangeAmount());
                } else if (docType == 5) {
                    ourCredit += documents.getSumm();
                }
                if (ourCredit <= 0.0) {
                    result = "";
                } else {
                    result = String.valueOf(ourCredit);
                }
                return result;
            }, new TextRenderer()).setCaption("Наш долг");

            this.addColumn((documents) -> {
                String result = "";
                int docType = documents.getType();
                if (docType == 2) {
                    contragentCredit += documents.getSumm() - (documents.getPaidAmount() - documents.getChangeAmount());
                } else if (docType == 5) {
                    contragentCredit -= documents.getSumm();
                }
                if (contragentCredit <= 0.0) {
                    result = "";
                } else {
                    result = String.valueOf(contragentCredit);
                }
                i++;
                return result;
            }, new TextRenderer()).setCaption("Долг контр.");

            
            
            this.setSizeFull();
            this.setHeight(14.3f, Unit.CM);
            this.setItems(gridData);
            
        }

        public Double getIncreaseCredit() {
            return increaseCredit;
        }

        public Double getDecreaseCredit() {
            return decreaseCredit;
        }

        public Double getOurCredit() {
            return ourCredit;
        }

        public Double getContragentCredit() {
            return contragentCredit;
        }

        public Double getStartContragentCredit() {
            return startContragentCredit;
        }

        
    }
}
