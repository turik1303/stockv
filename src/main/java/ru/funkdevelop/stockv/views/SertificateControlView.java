/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ru.funkdevelop.stockv.views;

import com.vaadin.data.HasValue;
import com.vaadin.data.provider.ListDataProvider;
import com.vaadin.event.selection.SelectionEvent;
import com.vaadin.event.selection.SelectionListener;
import com.vaadin.navigator.Navigator;
import com.vaadin.navigator.View;
import com.vaadin.navigator.ViewChangeListener;
import com.vaadin.server.FileDownloader;
import com.vaadin.server.FileResource;
import com.vaadin.server.Page;
import com.vaadin.server.Resource;
import com.vaadin.server.VaadinService;
import com.vaadin.ui.Alignment;
import com.vaadin.ui.Button;
import com.vaadin.ui.Grid;
import com.vaadin.ui.GridLayout;
import com.vaadin.ui.Notification;
import com.vaadin.ui.Panel;
import com.vaadin.ui.TextField;
import com.vaadin.ui.Upload;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.Window;
import com.vaadin.ui.renderers.NumberRenderer;
import com.vaadin.ui.renderers.TextRenderer;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.OutputStream;
import java.util.List;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;
import ru.funkdevelop.stockv.MyUI;
import ru.funkdevelop.stockv.StockHelper;
import ru.funkdevelop.stockv.pojos.Sertificates;
import ru.funkdevelop.stockv.views.service.MainMenu;
import ru.funkdevelop.stockv.views.service.UserData;

/**
 *
 * @author artur
 */
public class SertificateControlView extends GridLayout implements View {

    Navigator navigator;
    StockHelper stockHelper;
    List<Sertificates> allSertificates;
    Sertificates newSertificate, viewSertificate, deleteSertificate, editSertificate;
    File sertFile;

    @Override
    public void enter(ViewChangeListener.ViewChangeEvent event) {
        navigator = getUI().getNavigator();
        stockHelper = new StockHelper();
        allSertificates = stockHelper.getAllSertificates();
        newSertificate = new Sertificates();
        viewSertificate = allSertificates.get(0);
        editSertificate = allSertificates.get(0);
        deleteSertificate = allSertificates.get(0);
        MainMenu mainMenu = new MainMenu(navigator);
        UserData userData = new UserData();
        VerticalLayout fullLayout = new VerticalLayout();
        GridLayout layout = new GridLayout(3, 3);

        Panel allSertificatesPanel = new Panel("Список всех сертификатов");
        FilteredSertificatesGridLayout fc = new FilteredSertificatesGridLayout(allSertificates);
        fc.setSizeFull();
        fc.setMargin(false);
        fc.setSpacing(false);
        allSertificatesPanel.setContent(fc);

        Panel sertificatesControl = new Panel("Управление");
        VerticalLayout sertificatesControlLayout = new VerticalLayout();
        sertificatesControlLayout.addComponent(new Button("Добавить", new Button.ClickListener() {
            @Override
            public void buttonClick(Button.ClickEvent event) {
                goToAddSertificate();
            }
        }));
        sertificatesControlLayout.addComponent(new Button("Подробнее", new Button.ClickListener() {
            @Override
            public void buttonClick(Button.ClickEvent event) {
                goToViewSertificate();
            }
        }));
        sertificatesControlLayout.addComponent(new Button("Редактировать", new Button.ClickListener() {
            @Override
            public void buttonClick(Button.ClickEvent event) {
                goToEditSertificate();
            }
        }));
        sertificatesControlLayout.addComponent(new Button("Удалить", new Button.ClickListener() {
            @Override
            public void buttonClick(Button.ClickEvent event) {
                doDeleteSertificate();
            }
        }));

        sertificatesControlLayout.getComponent(0).setWidth("200px");
        sertificatesControlLayout.setComponentAlignment(sertificatesControlLayout.getComponent(0), Alignment.TOP_CENTER);
        sertificatesControlLayout.getComponent(1).setWidth("200px");
        sertificatesControlLayout.setComponentAlignment(sertificatesControlLayout.getComponent(1), Alignment.TOP_CENTER);
        sertificatesControlLayout.getComponent(2).setWidth("200px");
        sertificatesControlLayout.setComponentAlignment(sertificatesControlLayout.getComponent(2), Alignment.TOP_CENTER);
        sertificatesControlLayout.getComponent(3).setWidth("200px");
        sertificatesControlLayout.setComponentAlignment(sertificatesControlLayout.getComponent(3), Alignment.TOP_CENTER);

        sertificatesControl.setContent(sertificatesControlLayout);
        sertificatesControl.setWidth("250px");

        allSertificatesPanel.setWidth(25.0f, Unit.CM);
        allSertificatesPanel.setHeight(19.0f, Unit.CM);
        userData.setHeight(19.0f, Unit.CM);
        sertificatesControl.setHeight(19.0f, Unit.CM);

        layout.addComponent(mainMenu, 0, 0, 2, 0);
        layout.addComponent(userData, 0, 1, 0, 1);
        layout.addComponent(allSertificatesPanel, 1, 1, 1, 1);
        layout.addComponent(sertificatesControl, 2, 1, 2, 1);
        layout.setColumnExpandRatio(0, 0.0f);
        layout.setColumnExpandRatio(1, 0.0f);
        layout.setColumnExpandRatio(2, 0.0f);
        layout.setRowExpandRatio(0, 0.0f);
        layout.setRowExpandRatio(1, 0.0f);
        layout.setRowExpandRatio(2, 0.0f);

        fullLayout.addComponent(layout);
        fullLayout.setComponentAlignment(layout, Alignment.MIDDLE_CENTER);
        fullLayout.setMargin(false);
        fullLayout.setSpacing(false);

        getUI().setContent(fullLayout);
    }

    private void goToAddSertificate() {

        Window addSertificateWindow = new Window("Добавление сертификата");
        VerticalLayout addSertificateLayout = new VerticalLayout();
        TextField sertificateHeader = new TextField("Наименование сертификата");
        sertificateHeader.setWidth(11.0f, Unit.CM);
        Button doAddSertificateButton = new Button("Добавить", new Button.ClickListener() {
            @Override
            public void buttonClick(Button.ClickEvent event) {
                newSertificate.setSertHeader(sertificateHeader.getValue());
                doAddSertificate();
            }
        });
        doAddSertificateButton.setEnabled(false);
        Upload sertificateUpload = new Upload("Файл сертификата", new Upload.Receiver() {
            @Override
            public OutputStream receiveUpload(String filename, String mimeType) {
                String extension = filename.substring(filename.lastIndexOf("."));
                int sertNumber = stockHelper.getMaxSertificateId();
                sertNumber++;
                sertFile = new File(VaadinService.getCurrent().getBaseDirectory().getAbsolutePath() + "/Uploads/sertificates/sertificate" + Integer.toString(sertNumber) + extension);
                FileOutputStream fos = null;

                try {
                    fos = new FileOutputStream(sertFile);
                } catch (FileNotFoundException ex) {
                    Logger.getLogger(SertificateControlView.class.getName()).log(Level.SEVERE, null, ex);
                }
                return fos;
            }
        });
        sertificateUpload.setButtonCaption("Загрузить");
        sertificateUpload.setImmediateMode(false);
        sertificateUpload.addSucceededListener(new Upload.SucceededListener() {
            @Override
            public void uploadSucceeded(Upload.SucceededEvent event) {
                newSertificate.setSertFullPath(sertFile.getAbsolutePath());
                doAddSertificateButton.setEnabled(true);
            }
        });

        addSertificateLayout.addComponents(sertificateHeader, sertificateUpload, doAddSertificateButton);
        addSertificateLayout.setComponentAlignment(sertificateUpload, Alignment.TOP_CENTER);
        addSertificateLayout.setComponentAlignment(sertificateHeader, Alignment.TOP_CENTER);
        addSertificateLayout.setComponentAlignment(doAddSertificateButton, Alignment.TOP_CENTER);
        addSertificateWindow.setContent(addSertificateLayout);
        addSertificateWindow.center();
        addSertificateWindow.setModal(true);
        MyUI.getCurrent().addWindow(addSertificateWindow);
    }

    private void goToEditSertificate() {
        Window editSertificateWindow = new Window("Редактирование сертификата");
        VerticalLayout editSertificateLayout = new VerticalLayout();
        TextField sertificateHeader = new TextField("Наименование сертификата", editSertificate.getSertHeader());
        sertificateHeader.setWidth(11.0f, Unit.CM);
        Button doEditSertificateButton = new Button("Сохранить", new Button.ClickListener() {
            @Override
            public void buttonClick(Button.ClickEvent event) {
                editSertificate.setSertHeader(sertificateHeader.getValue());
                doEditSertificate();
            }
        });        
        Upload sertificateUpload = new Upload("Файл сертификата", new Upload.Receiver() {
            @Override
            public OutputStream receiveUpload(String filename, String mimeType) {
                String extension = filename.substring(filename.lastIndexOf("."));
                int sertNumber = editSertificate.getId();                
                sertFile = new File(VaadinService.getCurrent().getBaseDirectory().getAbsolutePath() + "/Uploads/sertificates/sertificate" + Integer.toString(sertNumber) + extension);
                FileOutputStream fos = null;

                try {
                    fos = new FileOutputStream(sertFile);
                } catch (FileNotFoundException ex) {
                    Logger.getLogger(SertificateControlView.class.getName()).log(Level.SEVERE, null, ex);
                }
                return fos;
            }
        });
        sertificateUpload.setButtonCaption("Загрузить");
        sertificateUpload.setImmediateMode(false);
        sertificateUpload.addSucceededListener(new Upload.SucceededListener() {
            @Override
            public void uploadSucceeded(Upload.SucceededEvent event) {                
                editSertificate.setSertFullPath(sertFile.getAbsolutePath());                
            }
        });
        
        editSertificateLayout.addComponents(sertificateHeader, sertificateUpload, doEditSertificateButton);
        editSertificateLayout.setComponentAlignment(sertificateHeader, Alignment.TOP_CENTER);
        editSertificateLayout.setComponentAlignment(sertificateUpload, Alignment.TOP_CENTER);
        editSertificateLayout.setComponentAlignment(doEditSertificateButton, Alignment.TOP_CENTER);
        editSertificateWindow.setContent(editSertificateLayout);
        editSertificateWindow.center();
        editSertificateWindow.setModal(true);
        MyUI.getCurrent().addWindow(editSertificateWindow);
    }

    private void goToViewSertificate() {
        Window viewSertificateWindow = new Window("Просмотр сертификата");
        VerticalLayout viewSertificateLayout = new VerticalLayout();
        TextField sertHeader = new TextField("Наименование сертификата", this.viewSertificate.getSertHeader());
        sertHeader.setEnabled(false);
        sertHeader.setWidth(11.0f, Unit.CM);
        File sertificateFile = new File(viewSertificate.getSertFullPath());
        Button downloadButton = new Button("Скачать");
        if (!sertificateFile.exists()) {
            downloadButton.setEnabled(false);
            Notification.show("Файл сертификата не загружен");
        }
        Resource sertRes = new FileResource(sertificateFile);
        FileDownloader sertDownloader = new FileDownloader(sertRes);
        sertDownloader.extend(downloadButton);
        viewSertificateLayout.addComponents(sertHeader, downloadButton);
        viewSertificateLayout.setComponentAlignment(sertHeader, Alignment.TOP_CENTER);
        viewSertificateLayout.setComponentAlignment(downloadButton, Alignment.TOP_CENTER);
        viewSertificateWindow.setContent(viewSertificateLayout);
        viewSertificateWindow.center();
        viewSertificateWindow.setModal(true);
        MyUI.getCurrent().addWindow(viewSertificateWindow);
    }

    private void doAddSertificate() {
        stockHelper.addDBItem(newSertificate);
        this.newSertificate = new Sertificates();
        this.allSertificates = stockHelper.getAllSertificates();
        Page.getCurrent().reload();
    }

    private void doEditSertificate() {
        stockHelper.updateDBItem(editSertificate);
        this.allSertificates = stockHelper.getAllSertificates();
        this.editSertificate = null;
        this.viewSertificate = null;
        this.deleteSertificate = null;
        Page.getCurrent().reload();
    }

    private void doDeleteSertificate() {
        File deleteSert = new File(deleteSertificate.getSertFullPath());
        if (deleteSert.exists()) {
            deleteSert.delete();
        }
        stockHelper.deleteDBItem(deleteSertificate);
        this.allSertificates = stockHelper.getAllSertificates();
        this.deleteSertificate = null;
        this.editSertificate = null;
        this.viewSertificate = null;
        Page.getCurrent().reload();

    }

    final class SertificatesGrid extends Grid<Sertificates> {

        public SertificatesGrid(List<Sertificates> sertificates) {

            this.addColumn(Sertificates::getId, new NumberRenderer("%02d")).setCaption("ID");
            this.getColumns().get(0).setWidth(50.0d);
            this.addColumn(Sertificates::getSertHeader, new TextRenderer()).setCaption("Наименование сертификата");
            this.setSelectionMode(Grid.SelectionMode.SINGLE);
            this.addSelectionListener(new SelectionListener<Sertificates>() {
                @Override
                public void selectionChange(SelectionEvent<Sertificates> event) {
                    Set<Sertificates> sertificates = event.getAllSelectedItems();
                    for (Sertificates sertificate : sertificates) {
                        viewSertificate = sertificate;
                        editSertificate = sertificate;
                        deleteSertificate = sertificate;
                    }
                }
            });
            this.setItems(sertificates);
        }

    }

    final class FilteredSertificatesGridLayout extends VerticalLayout {

        private final SertificatesGrid sertificatesGrid;
        private final TextField nameFilter;

        public FilteredSertificatesGridLayout(List<Sertificates> sertificates) {
            this.nameFilter = new TextField();
            this.nameFilter.setPlaceholder("Наименование сертификата");
            nameFilter.addValueChangeListener(this::onNameFilterTextChange);
            this.nameFilter.setWidth(7.0f, Unit.CM);
            addComponent(nameFilter);

            sertificatesGrid = new SertificatesGrid(sertificates);
            sertificatesGrid.setSizeFull();
            addComponentsAndExpand(sertificatesGrid);
        }

        private void onNameFilterTextChange(HasValue.ValueChangeEvent<String> event) {
            ListDataProvider<Sertificates> dataProvider = (ListDataProvider<Sertificates>) sertificatesGrid.getDataProvider();
            dataProvider.setFilter(Sertificates::getSertHeader, s -> caseInsensitiveContains(s, event.getValue()));
        }

        private Boolean caseInsensitiveContains(String where, String what) {
            return where.toLowerCase().contains(what.toLowerCase());
        }

    }
}
