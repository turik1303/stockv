/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ru.funkdevelop.stockv.views;

import com.vaadin.navigator.Navigator;
import com.vaadin.navigator.View;
import com.vaadin.navigator.ViewChangeListener;
import com.vaadin.server.VaadinServletService;
import com.vaadin.ui.Button;
import com.vaadin.ui.VerticalLayout;
import java.io.Serializable;

/**
 *
 * @author artur
 */
public class FirstView extends VerticalLayout implements View, Serializable{
    Navigator navigator;
    @Override
    public void enter(ViewChangeListener.ViewChangeEvent event) {
        navigator = getUI().getNavigator();
        String name;
        name = (String) VaadinServletService.getCurrentServletRequest().getAttribute("name");
        if (name == null) {
            if (navigator != null)
                navigator.navigateTo("MainView");
        } else {
        final VerticalLayout layout = new VerticalLayout();
        getUI().setContent(layout);
        Button button = new Button("Go to MainView", new Button.ClickListener() {
            @Override
            public void buttonClick(Button.ClickEvent event) {
                
                if (navigator != null)
                    navigator.navigateTo("MainView");
            }
        });
        
        layout.addComponent(button);
        }
    }
    
    
}
