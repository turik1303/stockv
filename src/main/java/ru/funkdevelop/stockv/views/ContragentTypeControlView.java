/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ru.funkdevelop.stockv.views;

import com.vaadin.event.selection.SelectionEvent;
import com.vaadin.event.selection.SelectionListener;
import com.vaadin.navigator.Navigator;
import com.vaadin.navigator.View;
import com.vaadin.navigator.ViewChangeListener;
import com.vaadin.server.Page;
import com.vaadin.ui.Alignment;
import com.vaadin.ui.Button;
import com.vaadin.ui.Grid;
import com.vaadin.ui.GridLayout;
import com.vaadin.ui.Panel;
import com.vaadin.ui.TextField;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.Window;
import com.vaadin.ui.renderers.NumberRenderer;
import com.vaadin.ui.renderers.TextRenderer;
import java.util.HashSet;
import java.util.List;
import ru.funkdevelop.stockv.MyUI;
import ru.funkdevelop.stockv.StockHelper;
import ru.funkdevelop.stockv.pojos.ContragentType;
import ru.funkdevelop.stockv.views.service.MainMenu;
import ru.funkdevelop.stockv.views.service.UserData;

/**
 *
 * @author artur
 */
public class ContragentTypeControlView extends GridLayout implements View {

    Navigator navigator;
    StockHelper stockHelper;
    List<ContragentType> types;
    ContragentType newType, editType, deleteType;

    @Override
    public void enter(ViewChangeListener.ViewChangeEvent event) {
        VerticalLayout fullLayout = new VerticalLayout();
        navigator = getUI().getNavigator();
        stockHelper = new StockHelper();
        types = stockHelper.getContragentTypes();
        newType = new ContragentType();
        editType = types.get(0);
        GridLayout layout = new GridLayout(3, 3);
        UserData userData = new UserData();
        MainMenu mainMenu = new MainMenu(navigator);

        Panel allTypes = new Panel("Список всех типов контрагентов");
        VerticalLayout allTypesLayout = new VerticalLayout();
        Grid<ContragentType> allTypesGrid = new Grid<>(ContragentType.class);
        allTypesGrid.setItems(types);
        allTypesGrid.removeAllColumns();
        allTypesGrid.addColumn(ContragentType::getId, new NumberRenderer("%02d")).setCaption("ID");
        allTypesGrid.addColumn(ContragentType::getTypeName, new TextRenderer()).setCaption("Наименование типа контрагента");
        allTypesGrid.setSelectionMode(Grid.SelectionMode.SINGLE);
        allTypesGrid.addSelectionListener(new SelectionListener<ContragentType>() {
            @Override
            public void selectionChange(SelectionEvent<ContragentType> event) {
                HashSet<ContragentType> types = (HashSet<ContragentType>) allTypesGrid.getSelectedItems();
                for (ContragentType type : types) {
                    editType = type;
                    deleteType = type;
                }
            }
        });

        allTypesLayout.addComponent(allTypesGrid);
        allTypesGrid.setSizeFull();
        allTypesLayout.setSizeFull();
        allTypes.setContent(allTypesLayout);
        allTypes.setWidth(25.0f, Unit.CM);
        allTypesLayout.setMargin(false);
        allTypesLayout.setSpacing(false);

        Panel typesControl = new Panel("Управление");
        VerticalLayout typesControLayout = new VerticalLayout();
        typesControLayout.addComponent(new Button("Добавить", new Button.ClickListener() {
            @Override
            public void buttonClick(Button.ClickEvent event) {
                goToAddType();
            }
        }));
        typesControLayout.addComponent(new Button("Редактировать", new Button.ClickListener() {
            @Override
            public void buttonClick(Button.ClickEvent event) {
                goToEditType();
            }
        }));
        typesControLayout.addComponent(new Button("Удалить", new Button.ClickListener() {
            @Override
            public void buttonClick(Button.ClickEvent event) {
                doDeleteType();
            }
        }));

        typesControLayout.getComponent(0).setWidth("200px");
        typesControLayout.setComponentAlignment(typesControLayout.getComponent(0), Alignment.TOP_CENTER);
        typesControLayout.getComponent(1).setWidth("200px");
        typesControLayout.setComponentAlignment(typesControLayout.getComponent(1), Alignment.TOP_CENTER);
        typesControLayout.getComponent(2).setWidth("200px");
        typesControLayout.setComponentAlignment(typesControLayout.getComponent(2), Alignment.TOP_CENTER);

        typesControl.setContent(typesControLayout);
        typesControl.setWidth("250px");

        userData.setHeight(19.0f, Unit.CM);
        allTypes.setHeight(19.0f, Unit.CM);
        typesControl.setHeight(19.0f, Unit.CM);

        layout.addComponent(mainMenu, 0, 0, 2, 0);
        layout.addComponent(userData, 0, 1, 0, 1);
        layout.addComponent(allTypes, 1, 1, 1, 1);
        layout.addComponent(typesControl, 2, 1, 2, 1);

        layout.setColumnExpandRatio(0, 0.0f);
        layout.setColumnExpandRatio(1, 0.0f);
        layout.setColumnExpandRatio(2, 0.0f);
        layout.setRowExpandRatio(0, 0.0f);
        layout.setRowExpandRatio(1, 0.0f);
        layout.setRowExpandRatio(2, 0.0f);

        layout.setComponentAlignment(allTypes, Alignment.TOP_CENTER);

        fullLayout.addComponent(layout);
        fullLayout.setComponentAlignment(layout, Alignment.MIDDLE_CENTER);
        fullLayout.setMargin(false);
        fullLayout.setSpacing(false);

        getUI().setContent(fullLayout);

    }

    private void goToAddType() {
        Window addTypeWindow = new Window("Добавить тип контрагента");
        VerticalLayout verticalLayout = new VerticalLayout();
        TextField typeName = new TextField("Наименование типа контрагента");
        typeName.setWidth(10.0f, Unit.CM);
        Button doAddTypeButton = new Button("Добавить", new Button.ClickListener() {
            @Override
            public void buttonClick(Button.ClickEvent event) {
                newType.setTypeName(typeName.getValue());
                doAddType();
            }
        });
        verticalLayout.addComponents(typeName, doAddTypeButton);
        verticalLayout.setComponentAlignment(typeName, Alignment.TOP_CENTER);
        verticalLayout.setComponentAlignment(doAddTypeButton, Alignment.TOP_CENTER);
        addTypeWindow.setContent(verticalLayout);
        addTypeWindow.setWidth(350.0f, Unit.PIXELS);
        addTypeWindow.center();
        addTypeWindow.setModal(true);
        MyUI.getCurrent().addWindow(addTypeWindow);
    }

    private void doAddType() {
        stockHelper.addDBItem(newType);
        this.newType = new ContragentType();
        types = stockHelper.getContragentTypes();
        Page.getCurrent().reload();
    }

    private void goToEditType() {
        Window editTypeWindow = new Window("Редактировать тип контрагента");
        VerticalLayout verticalLayout = new VerticalLayout();
        TextField typeName = new TextField("Наименование типа контрагента", this.editType.getTypeName());
        typeName.setWidth(10.0f, Unit.CM);
        Button doEditTypeButton = new Button("Сохранить", new Button.ClickListener() {
            @Override
            public void buttonClick(Button.ClickEvent event) {
                editType.setTypeName(typeName.getValue());
                doEditType();
            }
        });
        verticalLayout.addComponents(typeName, doEditTypeButton);
        verticalLayout.setComponentAlignment(typeName, Alignment.TOP_CENTER);
        verticalLayout.setComponentAlignment(doEditTypeButton, Alignment.TOP_CENTER);
        editTypeWindow.setContent(verticalLayout);
        editTypeWindow.setWidth(350.0f, Unit.PIXELS);
        editTypeWindow.center();
        editTypeWindow.setModal(true);
        MyUI.getCurrent().addWindow(editTypeWindow);
    }

    private void doEditType() {
        stockHelper.updateDBItem(this.editType);
        this.editType = new ContragentType();
        this.deleteType = null;
        Page.getCurrent().reload();
    }

    private void doDeleteType() {
        stockHelper.deleteDBItem(deleteType);
        this.deleteType = null;
        this.editType = new ContragentType();
        Page.getCurrent().reload();
    }

}
